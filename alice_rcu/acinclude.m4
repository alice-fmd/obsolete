dnl
dnl $Id: acinclude.m4,v 1.4 2006-07-14 23:14:24 cholm Exp $
dnl
dnl  Copyright (C) 2002 Christian Holm Christensen <cholm@nbi.dk>
dnl
dnl  This library is free software; you can redistribute it and/or
dnl  modify it under the terms of the GNU Lesser General Public License
dnl  as published by the Free Software Foundation; either version 2.1
dnl  of the License, or (at your option) any later version.
dnl
dnl  This library is distributed in the hope that it will be useful,
dnl  but WITHOUT ANY WARRANTY; without even the implied warranty of
dnl  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
dnl  Lesser General Public License for more details.
dnl
dnl  You should have received a copy of the GNU Lesser General Public
dnl  License along with this library; if not, write to the Free
dnl  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
dnl  02111-1307 USA
dnl
dnl ------------------------------------------------------------------
AC_DEFUN([AC_DEBUG],
[
  AC_REQUIRE([AC_PROG_CC])
  AC_REQUIRE([AC_PROG_CXX])
  AC_MSG_CHECKING(whether to make debug objects)
  AC_ARG_ENABLE(debug,
    [AC_HELP_STRING([--enable-debug],[Enable debugging symbols in objects])])
  if test "x$enable_debug" = "xno" ; then
    CFLAGS=`echo $CFLAGS | sed 's,-g,,'`
    CXXFLAGS=`echo $CXXFLAGS | sed 's,-g,,'`
  else
    case $CXXFLAGS in
    *-g*) ;;
    *)    CXXFLAGS="$CXXFLAGS -g" ;;
    esac
    case $CFLAGS in
    *-g*) ;;
    *)    CFLAGS="$CFLAGS -g" ;;
    esac
  fi
  AC_MSG_RESULT($enable_debug 'CFLAGS=$CFLAGS')
])

dnl ------------------------------------------------------------------
AC_DEFUN([AC_OPTIMIZATION],
[
  AC_REQUIRE([AC_PROG_CC])
  AC_REQUIRE([AC_PROG_CXX])

  AC_ARG_ENABLE(optimization,
    [AC_HELP_STRING([--enable-optimization],[Enable optimization of objects])])

  AC_MSG_CHECKING(for optimiztion level)

  changequote(<<, >>)dnl
  if test "x$enable_optimization" = "xno" ; then
    CFLAGS=`echo   $CFLAGS   | sed 's,-O\([0-9][0-9]*\|\),,'`
    CXXFLAGS=`echo $CXXFLAGS | sed 's,-O\([0-9][0-9]*\|\),,'`
  elif test "x$enable_optimization" = "xyes" ; then
    case $CXXFLAGS in
    *-O*) ;;
    *)    CXXFLAGS="$CXXFLAGS -O2" ;;
    esac
    case $CFLAGS in
    *-O*) ;;
    *)    CFLAGS="$CXXFLAGS -O2" ;;
    esac
  else
    CFLAGS=`echo   $CFLAGS   | sed "s,-O\([0-9][0-9]*\|\),-O$enable_optimization,"`
    CXXFLAGS=`echo $CXXFLAGS | sed "s,-O\([0-9][0-9]*\|\),-O$enable_optimization,"`
  fi
  changequote([, ])dnl
  AC_MSG_RESULT($enable_optimization 'CFLAGS=$CFLAGS')
])

dnl ------------------------------------------------------------------
AC_DEFUN([AC_CHECK_U2F],
[
   ac_tmp_have_u2f=no
   AH_TEMPLATE([HAVE_U2F], [Whether we have the U2F library])
   AC_MSG_CHECKING([for altro-config])
   AC_PATH_PROG([U2F_CONFIG], [altro-config])
   if test "x$U2F_CONFIG" = "x" ; then 
     :
   else
     if test $1 ; then 
       needed=$(echo $1|awk 'BEGIN {FS="."}{printf "%d",(($''1*1000)+$''2)*1000+$''3}')
       AC_MSG_CHECKING(if libaltro version is $1 or better)
       U2F_VERSION=$($U2F_CONFIG --version | sed -n 's/ALICE RCU\/U2F //p' )
       found=$(echo $U2F_VERSION | awk 'BEGIN{FS="."}{printf "%d",(($''1*1000)+$''2)*1000+$''3}')
       if test $found -ge $needed ; then 
         ac_tmp_have_u2f=yes
       fi
       AC_MSG_RESULT([$ac_tmp_have_u2f found $U2F_VERSION])
     fi
   fi
   if test "x$ac_tmp_have_u2f" = "xyes" ; then 
     save_LDFLAGS=$LDFLAGS
     save_CPPFLAGS="$CPPFLAGS"
     LDFLAGS="$save_LDFLAGS -L`$U2F_CONFIG --libdir`"
     CPPFLAGS="$save_CPPFLAGS `$U2F_CONFIG --cppflags`"
     AC_CHECK_LIB([u2f], [U2F_Open], 
                  [AC_CHECK_HEADERS([altro/u2f.h],
                                    [ac_tmp_have_u2f=yes],
                                    [ac_tmp_have_u2f=no])],
                  [ac_tmp_have_u2f=no],	[$($U2F_CONFIG --libs altro)])
     LDFLAGS="$save_LDFLAGS"
     CPPFLAGS="$save_CPPFLAGS"
   fi
   if test "x$ac_tmp_have_u2f" = "xyes" ; then
     U2F_LIBS=`$U2F_CONFIG --libs u2f`
     U2F_CPPFLAGS=`$U2F_CONFIG --cppflags` 
     U2F_LIBDIR=`$U2F_CONFIG --libdir` 
     U2F_LDFLAGS="-L$U2F_LIBDIR -R $U2F_LIBDIR"
     AC_DEFINE(HAVE_U2F)
   fi
   AC_SUBST(U2F_LIBS)
   AC_SUBST(U2F_CPPFLAGS)
   AC_SUBST(U2F_LDFLAGS)

  if test "x$ac_tmp_have_u2f" = "xyes" ; then 
    ifelse([$2], , :, [$2])     
  else 
    ifelse([$3], , :, [$3])     
  fi
])

dnl ------------------------------------------------------------------
dnl
dnl AC_ROOT_DIM([MINIMUM-VERSION 
dnl                   [,ACTION-IF_FOUND 
dnl                    [, ACTION-IF-NOT-FOUND]])
AC_DEFUN([AC_ROOT_DIM],
[
    AC_ARG_WITH([dim-prefix],
        [AC_HELP_STRING([--with-dim-prefix],
		[Prefix where DIM is installed])],
        dim_prefix=$withval, dim_prefix="")

    if test "x${DIM_CONFIG+set}" != xset ; then 
        if test "x$dim_prefix" != "x" ; then 
	    DIM_CONFIG=$dim_prefix/bin/dim-config
	fi
    fi   
    if test "x${DIM_CONFIG+set}" != xset ; then 
        if test "x$dim_prefix" != "x" ; then 
	    DIM_CONFIG=$dim_prefix/bin/dim-config
	fi
    fi   
    AC_PATH_PROG(DIM_CONFIG, dim-config, no)
    dim_min_version=ifelse([$1], ,0.1,$1)
    
    AC_MSG_CHECKING(for DIM version >= $dim_min_version)

    dim_found=no    
    if test "x$DIM_CONFIG" != "xno" ; then 
       DIM_CFLAGS=`$DIM_CONFIG --cflags`
       DIM_CPPFLAGS=`$DIM_CONFIG --cppflags`
       DIM_INCLUDEDIR=`$DIM_CONFIG --includedir`
       DIM_LIBS=`$DIM_CONFIG --libs`
       DIM_LIBDIR=`$DIM_CONFIG --libdir`
       DIM_LDFLAGS=`$DIM_CONFIG --ldflags`
       DIM_PREFIX=`$DIM_CONFIG --prefix`
       DIM_JNILIBS=`$DIM_CONFIG --libs`
       
       dim_version=`$DIM_CONFIG -V` 
       dim_vers=`echo $dim_version | \
         awk 'BEGIN { FS = " "; } \
	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
       dim_regu=`echo $dim_min_version | \
         awk 'BEGIN { FS = " "; } \
	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
       if test $dim_vers -ge $dim_regu ; then 
            dim_found=yes
       fi
    fi
    AC_MSG_RESULT($dim_found - is $dim_version) 
  
    if test "x$dim_found" = "xyes" ; then 
        ifelse([$2], , :, [$2])
    else 
        ifelse([$3], , :, [$3])
    fi
    AC_SUBST(DIM_PREFIX)
    AC_SUBST(DIM_CFLAGS)
    AC_SUBST(DIM_CPPFLAGS)
    AC_SUBST(DIM_INCLUDEDIR)
    AC_SUBST(DIM_LDFLAGS)
    AC_SUBST(DIM_LIBDIR)
    AC_SUBST(DIM_LIBS)
    AC_SUBST(DIM_JNILIBS)
])

dnl
dnl EOF
dnl 

#
# EOF
#
