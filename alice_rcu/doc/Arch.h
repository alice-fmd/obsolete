/*
 * $Id: Arch.h,v 1.1 2006-02-02 17:16:00 cholm Exp $
 *
 * RCU compiler
 * Copyright (c) 2004 Christian Holm <cholm@nbi.dk>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License 
 * as published by the Free Software Foundation; either version 2 of 
 * the License, or (at your option) any later version.  
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details. 
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
 * MA 02111-1307  USA  
 *
 */
/** @file   Arch.h
    @author Christian Holm
    @date   Sun Sep 15 16:29:24 2002
    @brief  Arhitecture documentation. */
/** @page arch Arhitecture 

    @image html Arch.png 

    This architecture is reflected more or less directly in the class
    hierarcy 

    @image html Classes.png 

    Sub-classes of Rcuxx::Rcu are not constructable.  Instead, the
    static member function Rcuxx::Rcu::Open instantises the proper
    backend based on the URL passes as a string.   A pointer to the
    concrete implementation is returned.  
*/

#error Not for compilation
//
// EOF
//
