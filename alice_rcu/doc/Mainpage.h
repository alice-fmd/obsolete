/*
 * $Id: Mainpage.h,v 1.5 2006-07-14 23:14:24 cholm Exp $
 *
 * RCU compiler
 * Copyright (c) 2004 Christian Holm <cholm@nbi.dk>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License 
 * as published by the Free Software Foundation; either version 2 of 
 * the License, or (at your option) any later version.  
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details. 
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
 * MA 02111-1307  USA  
 *
 */
/** @file   Mainpage.h
    @author Christian Holm
    @date   Sun Sep 15 16:29:24 2002
    @brief  Title page documentation. */
/** @mainpage ALICE RCU Abstraction layer, ROOT DAQ, and GUI 

    @section intro Introduction 
    
    This package provides a C++ class library for accessing the RCU
    internal structures via an abstract interface.   

    Currently, the U2F and FeeServer protocols are supported, but in
    the future other interfaces could be added (say to the InterCom
    layer, pRORC, or something completely different). 

    The package also provides abstract interfaces for defining
    data acquistion (DAQ) programs.   

    - @subpage arch
*/

#error Not for compilation
//
// EOF
//
