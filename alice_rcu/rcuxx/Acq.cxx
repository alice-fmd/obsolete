//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    Acq.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 01:04:55 2006
    @brief   Base class for data acquisition - implementation 
*/
#ifndef RCUXX_ACQ_H
# include <rcuxx/Acq.h>
#endif
#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <cstring>
#include <signal.h>
#include <stdexcept>
#ifndef __UNISTD_H
# include <unistd.h>
#endif
#include "DebugGuard.h"

namespace 
{
  volatile sig_atomic_t fgKeepGoing = 1;
  int fgSignal = 0;
  // Signal handler routine 
  void signal_handler(int sig)
  {
    fgSignal    = sig;
    fgKeepGoing = 0;
  }
}


    
    
    
    

//____________________________________________________________________
Rcuxx::Acq::Acq(Rcu& rcu, const char* out) 
  : fRcu(rcu), 
    fMeter(0,1), 
    fWait(50000), 
    fOutName(out), 
    fDebug(false)
{}


//____________________________________________________________________
unsigned int
Rcuxx::Acq::ClearStatus(unsigned int tries)
{
  Rcuxx::DebugGuard g(fDebug, "Acq::ClearStatus(%d)", tries);
  unsigned int ret = 0;
  try {
    std::cout << "ERRST " << std::flush;
    size_t i = 0;
    for (i = 0; i < tries; i++) {
      if ((ret = fRcu.ERRST()->Clear())) throw ret;
      usleep(fWait);
      if (fRcu.ERRST()->IsBusy()) {
	std::cout << "ABORT " << std::flush;
	if ((ret = fRcu.ABORT()->Commit())) throw ret;
	usleep(fWait);
      }
      if (fRcu.ERRST()->IsAltro()) {
	std::cout << "FECRST " << std::flush;
	if ((ret = fRcu.FECRST()->Commit())) throw ret;
	usleep(fWait);
      }
      if (!fRcu.ERRST()->IsAltro() && !fRcu.ERRST()->IsBusy())
	break;
    }
    if (i >= tries) 
      throw std::runtime_error("RCU still in bad state");
  }
  catch (std::exception& e) {
    std::cerr << "Error: " << e.what() << std::endl;
    ret   = 1;
    fStop = true;
  }
  catch (unsigned int r) {
    std::cerr << "Error: " << fRcu.ErrorString(ret) << std::endl;
    fStop = true;
  }
  return ret;
}


//____________________________________________________________________
unsigned int
Rcuxx::Acq::Setup(int run, int nevents, Trigger_t mode, 
		  unsigned int mask, unsigned int addr)
{
  Rcuxx::DebugGuard g(fDebug, "Acq::Setup(%d,%d,%d,%d,%d)", 
		      run, nevents, mode, mask, addr);
  std::cout << "Setting up " << std::flush;
  fRun             = run;
  fNEvents         = nevents;
  fMode            = mode;
  fStop            = false;
  fMeter.Reset(nevents);
  unsigned int ret = 0;
  bool         out = false;
  try {
    if ((ret = ClearStatus(5))) throw ret;
    std::cout << "EVWORD " << std::flush;
    if ((ret = fRcu.EVWORD()->Clear())) throw ret;
    usleep(fWait);

    if (mask & kACTFEC) { 
      std::cout << "ACTFEC " << std::flush;
      if ((ret = fRcu.ACTFEC()->Commit())) throw ret;
      usleep(fWait);
    }
    if (mask & kACL) { 
      std::cout << "ACL " << std::flush;
      if ((ret = fRcu.ACL()->Commit())) throw ret;
      usleep(fWait);
    }
    if (mask & kFECRST) { 
      std::cout << "FECRST " << std::flush;
      if ((ret = fRcu.FECRST()->Commit())) throw ret;
      usleep(fWait);
      if ((ret = fRcu.FECRST()->Commit())) throw ret;
      usleep(fWait);
    }
    if (mask & kPMEM) { 
      std::cout << "PMEM " << std::flush;
      if ((ret = fRcu.PMEM()->Commit())) throw ret;
      usleep(fWait);
    }
    if (mask & kIMEM) { 
      std::cout << "IMEM " << std::flush;
      if ((ret = fRcu.IMEM()->Commit())) throw ret;
      sleep(1);
      if (fDebug) {
	std::cout << std::endl;
	fRcu.IMEM()->Print();
      }
      usleep(fWait);
      fRcu.EXEC()->Commit(addr);
      sleep(1);
      usleep(fWait);
    }
    if (mask & kTRCFG1) { 
      std::cout << "TRCFG1 " << std::flush;
      if ((ret = fRcu.TRCFG1()->Commit())) throw ret;
      usleep(fWait);
    }
    std::cout << std::endl;
  }
  catch (unsigned int r) {
    std::cerr << "Error: " << fRcu.ErrorString(ret) << std::endl;
    fStop = true;
  }
  return ret;
}

    
    

//____________________________________________________________________
unsigned int
Rcuxx::Acq::Run() 
{
  DebugGuard g(fDebug, "Acq::Run()");
  std::cout << "Debug mode is " << (fDebug ? "on" : "off") << std::endl;
  if (fStop) return 1;
  int          ret    = 0;
  unsigned int evno   = 0;
  if (fRcu.TRCFG2()) {
    // std::cout << "TRCFG2 " << std::flush;
    fRcu.TRCFG2()->SetMode(Rcuxx::RcuTRCFG2::kPop);
    fRcu.TRCFG2()->SetEnableHT(fMode == kExternalTrigger);
    if ((ret = fRcu.TRCFG2()->Commit())) return ret;
    if ((ret = fRcu.TRCFG2()->Update())) return ret;
    if (fDebug) fRcu.TRCFG2()->Print();
  }
  std::cout << std::endl;
  do {
    if ((ret = Event(evno))) break;
  } while (fNEvents < 0 || evno < fNEvents);
  std::cout << std::endl;
  return CleanUp(ret);
}

//____________________________________________________________________
unsigned int
Rcuxx::Acq::CleanUp(int ret) 
{
  Rcuxx::DebugGuard g(fDebug, "CleanUp(%d)", ret);
  if (ret > 0) 
    std::cerr << "Error: " << fRcu.ErrorString(ret) << std::endl;
  if (ret == kStopped) {
    std::cout << "Run stopped by user" << std::endl;
    if (fgSignal) 
      std::cout  << "Got signal " << strsignal(fgSignal) << std::endl;
  }
  ret = ClearStatus(5);
  if (fRcu.TRCFG2()) {
    // fRcu.TRCFG2()->SetMode(Rcuxx::RcuTRCFG2::kPop);
    fRcu.TRCFG2()->SetEnableHT(false);
    fRcu.TRCFG2()->Commit();
  }
  fStop = true;
  unsigned int retval = 0;
  if (ret < 0) ret = 0;
  else         retval = ret;
  return ret;
}

//____________________________________________________________________
void
Rcuxx::Acq::InstallSignalHandler()
{
  signal(SIGTERM,signal_handler);
  signal(SIGINT,signal_handler);
  signal(SIGQUIT,signal_handler);
  // signal(SIGHUP,signal_handler);
}

//____________________________________________________________________
bool
Rcuxx::Acq::IsStopped()
{
  Rcuxx::DebugGuard g(fDebug, "IsStopped(%d,%d)", fgKeepGoing,fStop);
  if (!fgKeepGoing) {
    // Got a signal 
    fStop = true;
  }
  if (fStop) {
    // fRcu.RDABORT()->Commit();
    // fRcu.TRCFG2()->fEnableHT = false;
    // fRcu.TRCFG2()->Commit();
  }
  return fStop;
}
    
//____________________________________________________________________
int
Rcuxx::Acq::Event(unsigned int& evno) 
{
  DebugGuard g(fDebug, "Acq::Event(%d)", evno);
  unsigned int ret = 0;
  // Check if we got a stop
  if (IsStopped()) return kStopped;
  // See if we got a trigger, or what ever the user thinks we got.
  if ((ret = Trigger())) return ret;

  // We got a new event
  evno++;

  // Call user routine 
  StartEvent(evno);
  // usleep(10000);

  // Get the event word 
  if ((ret = fRcu.EVWORD()->Update())) return ret;
  // if (DebugGuard::fLevel) 
  DebugGuard::Message(fDebug, "EVWORD: 0x%08lx", fRcu.EVWORD()->View2());
  if (fDebug) fRcu.EVWORD()->Print();

  // Check that we don't have a bad trigger word
  if (!(fRcu.EVWORD()->HasTrigger() && fRcu.EVWORD()->IsStart()))
    if ((ret = NoData())) return ret;
  if (fRcu.EVWORD()->IsEnd()) {
    // Incomplete 
    std::cout << "Incomplete read-out " << std::endl;
    // Clear the trigger 
    if ((ret = fRcu.EVWORD()->Clear())) return ret;
    // Get ready for next
    return 1;
  }
  // Disable any further external triggers while we read out - only
  // needed for pop-mode - I think. 
  fRcu.TRCFG2()->SetEnableHT(false);
  if ((ret = fRcu.TRCFG2()->Commit())) return ret;
  if ((ret = fRcu.TRCFG2()->Update())) return ret;

  // Do the read-out 
  unsigned int cnt = 0;
  while (!fRcu.EVWORD()->IsEnd()) {
    if (IsStopped()) break;
    if ((ret = ReadDMEMs(cnt))) return ret;
    if ((ret = fRcu.EVWORD()->Update())) return ret;
    DebugGuard::Message(fDebug, "EVWORD: 0x%08x", fRcu.EVWORD()->View2()); 
  }

  // Clear the trigger 
  if ((ret = fRcu.EVWORD()->Clear())) return ret;
  
  // Call user routine 
  EndEvent(evno);
  
  // Get the event word 
  if ((ret = fRcu.EVWORD()->Update())) return ret;

  // Check if we got a stop
  if (IsStopped()) return kStopped;

  // Re-enable hardware triggers if that's what we're doing.  
  fRcu.TRCFG2()->SetEnableHT(fMode == kExternalTrigger);
  if ((ret = fRcu.TRCFG2()->Commit())) return ret;
  if ((ret = fRcu.TRCFG2()->Update())) return ret;

  // Return
  return ret;
}

//____________________________________________________________________
int
Rcuxx::Acq::Trigger()
{
  DebugGuard g(fDebug, "Acq::Trigger()");
  unsigned int ret = 0;
  switch (fMode) {
  case kSoftwareTrigger: ret = SoftwareTrigger(); break;
  case kExternalTrigger: ret = HardwareTrigger(); break;
  }
  return ret;
}

//____________________________________________________________________
int
Rcuxx::Acq::SoftwareTrigger()
{
  DebugGuard g(fDebug, "Acq::SoftwareTrigger");
  unsigned int ret = fRcu.SWTRG()->Commit();
  return ret;
}

//____________________________________________________________________
int
Rcuxx::Acq::HardwareTrigger()
{
  DebugGuard g(fDebug, "Acq::HardwareTrigger");
  unsigned int ret   = 0;
  unsigned int tick  = 0;
  unsigned int tries = 0;
  do {
    if ((ret = fRcu.EVWORD()->Update())) return ret;
    tick++;
    if (tick % 1000000) {
      // std::cout << "." << std::flush;
      if (IsStopped()) return kStopped;
      tries++;
    }
#if 0
    if (tries > 10) {
      std::cout << " (timed out)" << std::endl;
      return 1;
    }
#endif
    // Check if we got a stop
    if (IsStopped()) return kStopped;
  } while (!fRcu.EVWORD()->HasTrigger());
  return ret;
}
 

//____________________________________________________________________
int
Rcuxx::Acq::StartEvent(unsigned int evno) 
{
  DebugGuard g(fDebug, "Acq::StartEvent(%d)", evno);
  // if (evno % 10 != 0) return;
  // std::cout << "Event # " << std::setw(8) << evno << std::flush;
  if (!DebugGuard::fLevel) fMeter.Step();
  if (DebugGuard::fLevel) std::cout << std::endl;
  return 0;
}

//____________________________________________________________________
int
Rcuxx::Acq::EndEvent(unsigned int evno) 
{
  DebugGuard g(fDebug, "Acq::EndEvent(%d)", evno);
  // if (evno % 10 != 0) return;
  // std::cout << " done" << std::endl;
  return 0;
}

//____________________________________________________________________
int
Rcuxx::Acq::NoData() 
{
  DebugGuard g(fDebug, "Acq::NoData");
  unsigned int ret = 0;
  // fRcu.RDABORT()->Commit();
#if 1
  while (fRcu.EVWORD()->HasData(1) || fRcu.EVWORD()->HasData(2)) {
    // Read event word from HW
    if ((ret = fRcu.EVWORD()->Update())) return ret;

    // See if we have data in DMEM1, and if we do, clear it
    if (fRcu.EVWORD()->HasData(1)) if ((ret = fRcu.DM1()->Clear())) return ret;

    // See if we have data in DMEM2, and if we do, clear it
    if (fRcu.EVWORD()->HasData(2)) if ((ret = fRcu.DM2()->Clear())) return ret;
  } 
  // Read event word from HW
  if ((ret = fRcu.EVWORD()->Update())) return ret;

  // Clear the trigger 
  std::cout << "Clearing event word" << std::endl;
  if ((ret = fRcu.EVWORD()->Clear())) return ret;
  
  // Read event word from HW
  if ((ret = fRcu.EVWORD()->Update())) return ret;
  
  // If it's still bad, bail out 
  std::cout << "Cleared event word 0x" << std::hex 
	    << fRcu.EVWORD()->View2() << std::dec << std::endl;
  if (fRcu.EVWORD()->HasData(1) || fRcu.EVWORD()->HasData(2))  ret = 1;  
  if (fRcu.EVWORD()->IsEnd())   ret = 0;
  if (fRcu.EVWORD()->IsStart()) ret = 0;

#endif 
  // Return 
  return ret;
}


//____________________________________________________________________
int
Rcuxx::Acq::ReadDMEMs(unsigned int& cnt) 
{
  DebugGuard g(fDebug, "Acq::ReadDMEMs(%d)", cnt);
  unsigned int ret = 0;
  
  // Read event word from HW
  if ((ret = fRcu.EVWORD()->Update())) return ret;
  
  DebugGuard::Message(fDebug, "EVWORD: 0x%08x", fRcu.EVWORD()->View2()); 
  // Check for missing data
  if (!(fRcu.EVWORD()->HasData(1) || fRcu.EVWORD()->HasData(2))) 
    return 0; // kMissingData;
    
  // Read how much data we have 
  unsigned int lwadd1 = 0;
  unsigned int lwadd2 = 0;
  for (size_t tries = 0; tries < 10; tries++) {
    if ((ret = fRcu.LWADD()->Update())) return ret;
    lwadd1 = fRcu.LWADD()->Bank1();
    lwadd2 = fRcu.LWADD()->Bank2();
    // if (fDebug) fRcu.LWADD()->Print();
    if (lwadd1 != 0 || lwadd2 != 0) break;
    // Increment counter 
  }
  cnt++;
  
  if (fRcu.EVWORD()->HasData(1)) if ((ret = ReadDMEM(1, lwadd1))) return ret;
  if (fRcu.EVWORD()->HasData(2)) if ((ret = ReadDMEM(2, lwadd2))) return ret;

  return ret;
}

//____________________________________________________________________
int
Rcuxx::Acq::ReadDMEM(unsigned int which, unsigned int size) 
{
  DebugGuard g(fDebug, "Acq::ReadDMEM(%d,%d)", which,size);
  unsigned int ret;
  
  RcuDMEM* dm = (which == 1 ? fRcu.DM1() : fRcu.DM2());
  if (size > 0) {
    DebugGuard::Message(fDebug, "Reading from memory %s @ 0x%x (%p)", 
    			dm->Name().c_str(), dm->Base(), dm);
    if  ((ret = dm->Update(0, size))) return ret;
    
    // Call user function to deal with the data 
    ProcessChannel(dm->Data(), size);
    
    // Read the event word again 
    // if ((ret = fRcu.EVWORD()->Update())) return ret;
  }
  else 
    std::cerr << "No data to read from DMEM" <<  which << std::endl;
  // Reset the memory 
  // DebugGuard::Message(fDebug, "Reseting DMEM%d", which);
  if ((ret  = dm->Clear())) { 
    // DebugGuard::Message(fDebug, "Failed to reset DMEM%d: %d", which, ret);
    return ret;
  }

  // Read the event word again 
  // if ((ret = fRcu.EVWORD()->Update())) return ret;

  // Return 
  return ret;
}

    
//____________________________________________________________________
void
Rcuxx::Acq::PrintData(const DataVector_t& data, 
		      unsigned int size)
{
  for (int i = 0; i < size; i++) {
    std::cout << std::setw(4) << 4*i << std::flush; 
    for (int j = 3; j >= 0; j--) 
      std::cout << "  0x" << std::setw(3) << std::setfill('0')
		<< std::hex << data[4*i+j] << std::flush;
    std::cout << std::setfill(' ') << std::dec << std::endl;
  }
}

//____________________________________________________________________
void
Rcuxx::Acq::PrintTrailer(const unsigned int* data)
{
  unsigned int fill = ((data[3] & 0x3ff) << 4) + ((data[2] >> 6) & 0xf);
  unsigned int len  = ((data[2] & 0x3f)  << 6) + ((data[1] >> 6) & 0xf);
  unsigned int a    = (data[1] >> 2) & 0xf;
  unsigned int chip = ((data[1] & 0x3)   << 6) + ((data[0] >> 6) & 0x3f);
  unsigned int chan = (data[0] & 0xf);
  std::cout << "Trailer: " << std::hex << std::setfill('0') 
	    << "0x" << std::setw(4) << fill << " "
	    << "0x" << std::setw(3) << len  << " "
	    << "0x" << std::setw(1) << a    << " "
	    << "0x" << std::setw(2) << chip << " "
	    << "0x" << std::setw(1) << chan 
	    << std::dec << std::setfill(' ') << std::endl;
}



  

//____________________________________________________________________
int
Rcuxx::Acq::ProcessChannel(const DataVector_t& data, 
			   unsigned int size) 
{
  DebugGuard g(fDebug, "Acq::ProcessChannel(%p,%d)", &(data[0]),size);
  int ptr;
  unsigned int chan, chip, board, last;

  // PrintTrailer(&(data[4*(size-1)]));
  if ((ptr = DecodeTrailer(data, size, board, chip, chan, last)) < 0) 
    return 1;
  
  while (ptr >= 0) {
    unsigned int length = data[ptr]-2; ptr--;
    unsigned int time   = data[ptr];   ptr--;
    ProcessBunch(&(data[ptr-length]), length, time);
    ptr -= length;
  }
  return 0;
}

  
//____________________________________________________________________
int
Rcuxx::Acq::DecodeTrailer(const DataVector_t& data, 
			  unsigned int size, unsigned int& board,  
			  unsigned int& chip, unsigned int& chan, 
			  unsigned int& last)
{
  DebugGuard g(fDebug, "Acq::DecodeTrailer(%p,%d)", &(data[0]),size);
  typedef unsigned long long  w40_t;
  static w40_t trailerMask = ((w40_t(0x2aaa) << 26) + (w40_t(0xa) << 12));

  size_t n = 4 * size;
  // Get the trailer 
  w40_t trailer = ((w40_t(data[n - 1]) << 30)
                   + (w40_t(data[n - 2]) << 20)
                   + (w40_t(data[n - 3]) << 10)
                   +  data[n - 4]);
  if ((trailer & trailerMask) != trailerMask) {
    std::cerr << "Invalid trailer at " << n-1 << "->" << n-4 
	      << " 0x" << std::hex << trailer
              << " (" << data[n-1] << " " << data[n - 2]
              << " " << data[n-3] << " " << data[n - 4]
              << ")" << std::dec << std::endl;
    return -1;
  }
  chan  = trailer & 0xf;
  chip  = (trailer >> 4) & 0x7;
  board = (trailer >> 7) & 0x1f;
  last  = (trailer >> 16) & 0x3ff;
 
  unsigned int nfill = (last % 4 == 0 ? 0 : 4 - last %  4);
  int i = n - 5;
  for (size_t j = 0; j < nfill; j++, i--) {
    if (data[i] != 0x2aa) {
      std::cerr << "\nWarning: Invalid " << j << "th/" 
		<< nfill << " fill word 0x" << std::hex << data[i] << std::dec
		<< " at word " << i << "/" << n << " for channel " << chan 
		<< " chip " << chip  << " of size " << last << std::endl;
      return kInvalidTrailer;
    }
  }
  return i;
}

//____________________________________________________________________
int
Rcuxx::Acq::ProcessBunch(const Data_t* data, 
			 unsigned int length, 
			 unsigned int time) 
{
  DebugGuard g(fDebug, "Acq::ProcessBunch(%x,%d,%d)", data,length,time);
  // Here we should do something like 
  // 
  // int i = 0;
  // for (int j = 0; j < length; j++; i--) fData[time - j] = data[i];
#if 0
  if (DebugGuard::fDebug) {
    for (size_t i = 0; i < length; i++) {
      std::cout << std::hex << std::setfill('0') << "  0x" << std::setw(8)
		<< data[i] << std::flush;
      if (i % 6 == 0 && i != 0) std::cout << std::endl;
    }
    std::cout << std::endl;
  }
#endif
  return 0;
}

//____________________________________________________________________
//
// EOF
// 
