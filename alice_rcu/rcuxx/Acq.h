// -*- mode: c++ -*-  
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    Acq.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 01:04:26 2006
    @brief   Base class for data acquisition - declaration
*/
#ifndef RCUXX_ACQ_H
#define RCUXX_ACQ_H
#ifndef RCUXX_RCU_H
# include <rcuxx/Rcu.h>
#endif
#ifndef RCUXX_PROGRESSMETER_H
# include <rcuxx/ProgressMeter.h>
#endif

namespace Rcuxx 
{
  /** @defgroup rcuxx_util Rcu++ Utility classes 
   */
  /** @class Acq rcuxx/Acq.h <rcuxx/Acq.h> 
      @brief Abstract base class to do data aquisition. 
      @ingroup rcuxx_util 
   */
  class Acq 
  {
  public:
    /** Type of data vectors */ 
    typedef RcuMemory::Cache_t DataVector_t;
    /** Type of elements in data vector */
    typedef DataVector_t::value_type Data_t;
    /** Progress meter */
    ProgressMeter fMeter;
    /** Internal return codes */
    enum {
      kStopped        = -1, 
      kInvalidTrailer = -2, 
      kMissingData    = -3
    };
    
    /** What to commit */
    enum {
      kACTFEC = 1,
      kFECRST = 2,
      kTRCFG1 = 4, 
      kIMEM   = 8, 
      kPMEM   = 16, 
      kACL    = 32
    };
    /** Trigger types */
    enum Trigger_t { 
      /** External trigger */
      kExternalTrigger, 
      /** Internal trigger */
      kSoftwareTrigger
    };
    /** Constructor
	@param rcu  Reference to RCU object 
	@param out  Output file name  */
    Acq(Rcu& rcu, const char* out="");
    /** Install signal handler - should not be used in a threaded
	application. */
    static void InstallSignalHandler();
    /** Set up 
	@param nRun Run number
	@param nevents Number of events to process
	@param mode The trigger mode. 
	@param mask Mask of stuff to commit, 
	@param addr Address to execute */ 
    virtual unsigned int Setup(int          nRun=0, 
			       int          nevents=-1, 
			       Trigger_t    mode=kSoftwareTrigger,
			       unsigned int mask=(kACTFEC|kFECRST|kTRCFG1|
						  kIMEM|kPMEM|kACL), 
			       unsigned int addr=0);
    /** Run an acquisition
	@return 0 on success, error code otherwise */
    virtual unsigned int Run();
    /** Stop the acquisition */
    virtual void Stop() { fStop = true; }
    /** Set the wait between commits 
	@param w Wait time in microseconds between commits */
    virtual void SetWait(unsigned int w) { fWait = w; }
    /** @param out Set out put file name */ 
    virtual void SetOutName(const char* out) { fOutName = out; }
    /** @param debug Turn on/off debug output */
    void SetDebug(bool debug=true) { fDebug = debug; }
  protected:
    /** Reference to RCU interface */
    Rcu& fRcu;
    /** Whether to stop */ 
    bool fStop;
    /** Run number */
    int fRun;
    /** Number of events */
    int fNEvents;
    /** Trigger mode */
    Trigger_t fMode;
    /** Wait between commits in microseconds */
    unsigned int fWait;
    /** Output file name */
    std::string fOutName;
    /** Debug or not */ 
    bool fDebug;
    /** Try to clear the status register of the RCU
	@param tries Number of times to try to reset
	@return 0 on success, error code otherwise */
    unsigned int ClearStatus(unsigned int tries);
    /** Check if we need to stop */ 
    virtual bool IsStopped();
    /** Process one event 
	@param evno On entry, the old event number, on return the
	processed event number.  
	@return 0 on success, error code otherwise */
    virtual int Event(unsigned int& evno);
    
    /** Create or wait for a trigger.  
	@return error-code on failure, 0 on success */
    virtual int Trigger();
    
    /** Create a software trigger.  
	@return error-code on failure, 0 on success */
    virtual int SoftwareTrigger();
    
    /** Wait for an external (hardware) trigger.  
	@return error-code on failure, 0 on success */
    virtual int HardwareTrigger();
  
    /** Hook called at the beginning of an event, after accepting a
	trigger 
	@param evno The current event number */
    virtual int StartEvent(unsigned int evno);
    
    /** Hook called at end of an event
	@param evno The current event number */
    virtual int EndEvent(unsigned int evno);
    
    /** Handler called in case an event contained no data. 
	@return If this function returns non-zero, the run processing is
	stopped. */
    virtual int NoData();
    
    /** Read the data memories.  This is called several times during
	an event to read out the full event.  The argument @a cnt
	gives the iteration number.  It should be incremented on
	return.  
	@param cnt On entry, the old iteration count.  On exit the new
	iteration count.  
	@return non-zero on failure. */
    virtual int ReadDMEMs(unsigned int& cnt);
  
    /** Read out one of the DMEM's of the RCU.  Which one is
	determinded by the parameter @a which.  The function should
	read out 
	@a size 32 bit words. 
	@param which Which data memory to read out (1 or 2)
	@param size How many 32bit words to read out. 
	@return On success, 0, error code on failure */
    virtual int ReadDMEM(unsigned int which, unsigned int size);
  
    /** Process the data from one ALTRO channel.  The full data from
	one ALTRO channel is passed as the argument @a data.  The
	array has  @a size elements.   The elements are 10bit words
	formated as ALTRO data.  That is, it has the format 
	<table>
        <tr><td>S01</td><td>S02</td><td>S03</td><td>S04</td></tr>
        <tr><td>S05</td><td>T05</td><td>007</td><td>S05</td></tr>
        <tr><td>S06</td><td>S07</td><td>S08</td><td>T08</td></tr>
        <tr><td>006</td><td>S09</td><td>...</td><td>...</td></tr>
        <tr><td>...</td><td>...</td><td colspan=2>0x2aa</td></tr>
	<tr><td>0x2aaa</td><td>L</td><td>0xa</td><td>address</td></tr>
	</table>
	@param data The ALTRO formated data 
	@param size The size of the array @a data
	@return 0 on success, error-code otherwise */
    virtual int ProcessChannel(const DataVector_t& data, 
			       unsigned int size);
  
    /** Decode the ALTRO data trailer.   The trailer contains the
	length of the full data structure in 10bit words, as well as
	the channel address.  These are returned on exit.  The fill
	words are also extracted and checked. 
	@param data  The data encoded structure 
	@param size  The size of the data structure 
	@param board On return, the channel number. 
	@param chan  On return, the channel number.
	@param chip  On return, the ALTRO chip address. 
	@param last  On return, the size in 10bit words of the data
	structure.  
	@return Pointer into @a data where the header ends, including
	possible fill words (0x2aa).  On error -1 is returned. */
    virtual int DecodeTrailer(const DataVector_t& data, 
			      unsigned int size, unsigned int& board,
			      unsigned int& chip, unsigned int& chan,  
			      unsigned int& last);
  
    /** Process one bunch.  The bunch, consists of @a length 10bit 
	words, starting at time @a time.  If the user overrides this 
	member function, it should decode the data like 
	@code 
	for (size_t i = 0; i < length; i++)  fData[time+i] = data[i];
	@endcode 
	@param data The encoded data 
	@param length The number of elements in @a data. 
	@param time The start time of the bunch. 
	@return Should return 0 on success, non-zero on failure. */
    virtual int ProcessBunch(const Data_t* data, 
			     unsigned int length, 
			     unsigned int time);

    /** Clean up after run.  
	@param ret Return value from run 
	@return 0 on success, error code otherwise */
    virtual unsigned int CleanUp(int ret);
    /** Print the data 
	@param data Data 
	@param size Size of data */
    void PrintData(const DataVector_t& data, 
		   unsigned int size);
    /** Print trailer information for a channel 
	@param trailer Trailer as 4 10bit words */
    void PrintTrailer(const unsigned int* trailer);
  };
}

#endif

  
//
//  EOF
//
