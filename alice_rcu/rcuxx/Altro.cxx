//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    rcuxx/Altro.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 01:05:18 2006
    @brief   Implementation of ALTRO interface 
*/
#ifndef RCUXX_ALTRO_H
# include <rcuxx/Altro.h>
#endif
#include "DebugGuard.h"
#include <iostream>
#include <iomanip>

namespace 
{
  bool fDebug;
}

//====================================================================
Rcuxx::AltroRegister::AltroRegister(const char* name, unsigned int instr, 
				    RcuIMEM& imem, RcuRMEM& rmem,
				    bool clear, bool submit, bool global,
				    bool bcast) 
  : Register(name, submit), 
    fClear(clear), 
    fInstr(instr),
    fIMEM(imem),
    fRMEM(rmem),
    fAddress(0), 
    fGlobal(global),
    fAllowBroadcast(bcast)
{}

//____________________________________________________________________
void
Rcuxx::AltroRegister::WriteInstructions(unsigned int* out)
{
  out[0] = 0x600000 + fAddress + (fInstr & 0x1f);
  out[1] = 0x700000 + (fData & 0xfffff);
}

//____________________________________________________________________
void
Rcuxx::AltroRegister::ReadInstructions(unsigned int* out)
{
  out[0] = 0x400000 + fAddress + (fInstr & 0x1f);
  out[1] = 0x500000;
}

//____________________________________________________________________
void
Rcuxx::AltroRegister::SetBroadcast()
{ 
  if (!fAllowBroadcast) return;
  fAddress = (1 << 18); 
}

//____________________________________________________________________
void
Rcuxx::AltroRegister::SetAddress(unsigned int board, unsigned int chip, 
				 unsigned int channel) 
{
  unsigned int addr = (((board & 0xf) << 7) + ((chip & 0x7) << 4) + 
		       (fGlobal ? 0 : (channel & 0xf)));
  fAddress = addr << 5;
}

//____________________________________________________________________
unsigned int
Rcuxx::AltroRegister::Commit()
{
  Set();
  return Write(fData);
}

//____________________________________________________________________
unsigned int
Rcuxx::AltroRegister::Update() 
{
  unsigned int ret = Read(fData);
  Get();
  return ret;
}

//____________________________________________________________________
void
Rcuxx::AltroRegister::Print() const 
{
  std::cout << "Contents of ALTRO register " << fName << ": " 
	    << std::hex << "(0x" << fData << ")" << std::dec << std::endl;
}

//____________________________________________________________________
unsigned int
Rcuxx::AltroRegister::Read(unsigned int& data)
{
  DebugGuard g(fDebug, "AltroRegister::Read<%s>", fName.c_str());
  static unsigned int idata[] = { 0x0, 0x0, 0x390000 };
  static unsigned int store[3];
  unsigned int ret = 0;
  if (fAddress & (1 << 18)) return ret;
  if ((ret = fIMEM.Update(0, 3))) return ret;
  fIMEM.Get(0, 3, store);
  ReadInstructions(idata);
  try {
    fIMEM.Set(0, 3, idata);
    DebugGuard::Message(fDebug, "AltroRegister::Read: "
			"Write 3 words to instruction memory");
    DebugGuard::Message(fDebug, "\t%p %p %p", idata[0], idata[1], idata[2]);
    if ((ret = fIMEM.Commit(0, 3)))   throw ret;
    DebugGuard::Message(fDebug, "Executing instruction memory");
    if ((ret = fIMEM.Exec(0)))        throw ret;
    DebugGuard::Message(fDebug, "AltroRegister::Read: "
			"Read one word from result memory");
    if ((ret = fRMEM.Update(0, 1)))   throw ret;
    fRMEM.Get(0, 1, &data);
    DebugGuard::Message(fDebug, "AltroRegister::Read: Read 0x%08lx", data);
  }
  catch (unsigned int& r) {
    // We got an error 
  }
  fIMEM.Set(0, 3, store);
  fIMEM.Commit(0, 3);
  return ret;
}

//____________________________________________________________________
unsigned int
Rcuxx::AltroRegister::Write(unsigned int data)
{
  DebugGuard g(fDebug, "AltroRegister::Write<%s>", fName.c_str());
  static unsigned int idata[] = { 0x0, 0x0, 0x390000 };
  static unsigned int store[3];
  unsigned int ret = 0;
  fIMEM.Update(0, 3);
  fIMEM.Get(0, 3, store);
  if (!fAllowBroadcast && (fAddress & (1 << 18))) return 1;
  WriteInstructions(idata);
  try {
    fIMEM.Set(0, 3, idata);
    if ((ret = fIMEM.Commit(0, 3)))   throw ret;
    if ((ret = fIMEM.Exec(0))) throw ret;
  }
  catch (unsigned int& r) {
    // Got an error 
  }
  fIMEM.Set(0, 3, store);
  fIMEM.Commit(0, 3);
  return ret;
}

//====================================================================
Rcuxx::AltroCommand::AltroCommand(const char* name, unsigned int cmd, 
				  RcuIMEM& imem, bool bcast) 
  : Command(name, cmd), 
    fIMEM(imem),
    fAddress(0), 
    fAllowBroadcast(bcast),
    fData(0) 
{}

//____________________________________________________________________
void
Rcuxx::AltroCommand::SetBroadcast()
{ 
  if (!fAllowBroadcast) return;
  fAddress = (1 << 18); 
}

//____________________________________________________________________
void
Rcuxx::AltroCommand::SetAddress(unsigned int board, unsigned int chip, 
				unsigned int channel) 
{
  unsigned int addr = (((board & 0xf) << 7) + ((chip & 0x7) << 4) + 
		       (fAllowBroadcast ? 0 : (channel & 0xf)));
  fAddress = addr << 5;
}
//____________________________________________________________________
void
Rcuxx::AltroCommand::WriteInstructions(unsigned int* out)
{
  out[0] = 0x600000 + fAddress +  fCmd;
  out[1] = 0x700000 + (fData & 0xfffff);
}
//____________________________________________________________________
unsigned int
Rcuxx::AltroCommand::Commit()
{
  DebugGuard g(fDebug, "AltroCommand::Commit() <%s,0x%x>", 
	       fName.c_str(), fCmd); 
  static unsigned int idata[] = { 0x600000, 0x700000, 0x390000 };
  static unsigned int store[3];
  unsigned int ret;
  fIMEM.Update(0,3);
  fIMEM.Get(0, 3, store);
  if (!fAllowBroadcast && (fAddress & (1 << 18))) return 1;
  WriteInstructions(idata);
  try {
    fIMEM.Set(0, 3, idata);
    if ((ret = fIMEM.Commit(0,3)))   throw ret;
    if ((ret = fIMEM.Exec(0))) throw ret;
  }
  catch (unsigned int& r) {
    // Got an error
  }
  fIMEM.Set(0, 3, store);
  fIMEM.Commit(0, 3);
  return ret;
}
//====================================================================
Rcuxx::AltroKLCoeffs::AltroKLCoeffs(char which, size_t n, 
				    RcuIMEM& imem, RcuRMEM& rmem)
  : AltroRegister("  ", (which == 'k' || which == 'K' ? 0 : 3) + n - 1,
		  imem, rmem, false, true)
{
  char i;
  fName.insert(0, 1, which); 
  switch (n) {
  case 1: i = '1'; break;
  case 2: i = '2'; break;
  case 3: i = '3'; break;
  }
  fName.insert(1, 1, i);
}
    
//____________________________________________________________________
void
Rcuxx::AltroKLCoeffs::Print() const 
{
  AltroRegister::Print();
  std::cout << "\tValue:\t\t\t\t" << fValue << std::endl;
}

//====================================================================
void
Rcuxx::AltroVFPED::Print() const 
{
  AltroRegister::Print();
  std::cout << "\tVariable:\t\t\t" << fVP << "\n"
	    << "\tFixed:\t\t\t\t" << fFP << std::endl;
}

//====================================================================
void
Rcuxx::AltroPMDTA::Print() const 
{
  AltroRegister::Print();
  std::cout << "\tPedestal data:\t\t\t" << fPMDTA << std::endl;
}


//====================================================================
void
Rcuxx::AltroZSTHR::Get()  
{ 
  fOffset = (fData >> 10) & 0x3ff; 
  fZS_THR = fData & 0x3ff;
}

//____________________________________________________________________
void
Rcuxx::AltroZSTHR::Print() const 
{
  AltroRegister::Print();
  std::cout << "\tOffset:\t\t\t\t" << fOffset << "\n" 
	    << "\tThreshold:\t\t\t"<< fZS_THR << std::endl;
}


//====================================================================
void
Rcuxx::AltroBCTHR::Get() 
{
  fTHR_HI = (fData >> 10) & 0x3ff;
  fTHR_LO = fData & 0x3ff;
}

//____________________________________________________________________
void
Rcuxx::AltroBCTHR::Print() const 
{
  AltroRegister::Print();
  std::cout << "\tHigh:\t\t\t\t" << fTHR_HI << "\n" 
	    << "\tLow:\t\t\t\t"<< fTHR_LO << std::endl;
}

//====================================================================
void
Rcuxx::AltroTRCFG::Get() 
{
  fACQ_START = (fData >> 10) & 0x3ff;
  fACQ_END   = fData & 0x3ff;
}

//____________________________________________________________________
void
Rcuxx::AltroTRCFG::Set() 
{
  fData = ((fACQ_START & 0x3ff) << 10) + (fACQ_END & 0x3ff);
}

//____________________________________________________________________
void
Rcuxx::AltroTRCFG::Print() const 
{
  AltroRegister::Print();
  std::cout << "\tStart:\t\t\t\t" << fACQ_START << "\n" 
	    << "\tEnd:\t\t\t\t"<< fACQ_END << std::endl;
}

//====================================================================
void
Rcuxx::AltroDPCFG::Get()
{
  f1stBMode   = (fData & 0xf);
  f1stBPol    = fData & (1 << 4);
  f2ndBEnable = fData & (1 << 11);
  f2ndBPre    = (fData >> 5) & 0x3;
  f2ndBPost   = (fData >> 7) & 0xf;
  fZSEnable   = fData & (1 << 19);
  fZSPre      = (fData >> 17) & 0x3;
  fZSPost     = (fData >> 14) & 0x7;
  fZSGlitch   = (fData >> 12) & 0x3;
}
//____________________________________________________________________
void
Rcuxx::AltroDPCFG::Set()
{
 fData = (f1stBMode & 0xf) + (f1stBPol ? 0x10 : 0);
 if (f2ndBEnable) 
   fData += (((f2ndBPre  & 0x3) << 5) 
	     + ((f2ndBPost & 0x7) << 5) 
	     + (1 << 11)); 
 if (fZSEnable) 
   fData += (((fZSGlitch & 0x3) << 12) 
	     + ((fZSPre & 0x7) << 14) 
	     + ((fZSPost & 0x3) << 17) 
	     + (1 << 19));
} 
//____________________________________________________________________
void
Rcuxx::AltroDPCFG::Print() const
{
  AltroRegister::Print();
  std::cout << std::boolalpha 
	    << "\tPolarity:\t\t\t" << (f1stBPol ? "inverse" : "normal") << "\n"
	    << "\t1st baseline mode:\t\t" << std::hex << f1stBMode 
	    << " (" << std::dec << std::flush;
  switch (f1stBMode) {
  case 0x0:  std::cout << "din-fpd";			break;
  case 0x1:  std::cout << "din-f(t)";			break;
  case 0x2:  std::cout << "din-f(din)";			break;
  case 0x3:  std::cout << "din-f(din-vpd)";		break;
  case 0x4:  std::cout << "din-vpd-fpd";		break;
  case 0x5:  std::cout << "din-vpd-f(t)";		break;
  case 0x6:  std::cout << "din-vpd-f(din)";		break;
  case 0x7:  std::cout << "din-vpd-f(din-vpd)";		break;
  case 0x8:  std::cout << "f(din)-fpd";			break;
  case 0x9:  std::cout << "f(din-vpd)-fpd";		break;
  case 0xA:  std::cout << "f(t)-fpd";			break;
  case 0xB:  std::cout << "f(t)-f(t)";			break;
  case 0xC:  std::cout << "f(din)-f(din)";		break;
  case 0xD:  std::cout << "f(din-vpd)-f(din-vpd)";	break;
  case 0xE:  std::cout << "din-fpd";			break;
  case 0xF:  std::cout << "din-fpd";			break;
  }
  std::cout << ")\n" 
	    << "\t2nd baseline enabled:\t\t" << f2ndBEnable << "\n"
	    << "\t\tPre-excluded:\t\t" << f2ndBPre << "\n"
	    << "\t\tPost-excluded:\t\t" << f2ndBPost << "\n"
	    << "\tZero suppression enabled:\t" << fZSEnable << "\n" 
	    << "\t\tPre-excluded:\t\t" << fZSPre << "\n"
	    << "\t\tPost-excluded:\t\t" << fZSPost << "\n"
	    << "\t\tGlitch mode:\t\t" << fZSGlitch << " (" << std::flush;
  switch (fZSGlitch) {
  case 0x0:  std::cout << "din-fpd";		break;
  case 0x1:  std::cout << "din-f(t)";		break;
  case 0x2:  std::cout << "din-f(din)";		break;
  case 0x3:  std::cout << "din-f(din-vpd)";		break;
  }
  std::cout << ")" << std::endl;
}

//====================================================================
void
Rcuxx::AltroDPCF2::Get()
{
  fPTRG   = (fData & 0xf);
  fFLT_EN = fData & (1 << 5);
  fPWSV   = fData & (1 << 6);
  fBUF    = (fData & (1 << 4) ? k4Buffers : k8Buffers);
}
//____________________________________________________________________
void
Rcuxx::AltroDPCF2::Set()
{
  fData = ((fPTRG & 0xf) + 
	   (fBUF == k4Buffers ? 0 : 0x10) +
	   (fFLT_EN ? 0x20 : 0) + 
	   (fPWSV ? 0x40 : 0));
} 
//____________________________________________________________________
void
Rcuxx::AltroDPCF2::Print() const
{
  AltroRegister::Print();
  std::cout << std::boolalpha
	    << "\tPre-trigger:\t\t\t" << fPTRG << "\n" 
	    << "\tDigitial filter:\t\t" << fFLT_EN << "\n" 
	    << "\tPower save:\t\t\t" <<  fPWSV << "\n" 
	    << "\t# Buffers:\t\t\t"  << (fBUF == k4Buffers ? 4 : 8)
	    << std::endl;
}

//====================================================================
void
Rcuxx::AltroPMADD::Print() const 
{
  AltroRegister::Print();
  std::cout << "\tPedestal address:\t\t" << fPMADD << std::endl;
}

//====================================================================
void
Rcuxx::AltroERSTR::Get() 
{
  fRp	   	= fData & 0x7;
  fWp	   	= (fData >> 3) & 0x7;
  fBuffers	= (fData >> 6) & 0xf;
  fFull	   	= ! (fData & (1 << 10));
  fEmpty	= ! (fData & (1 << 11));
  fParity	= ! (fData & (1 << 12));
  fInstruction  = ! (fData & (1 << 13));
  fTrigger	= ! (fData & (1 << 14));
  fMMU1Seu	= ! (fData & (1 << 15));
  fMMU2Seu	= ! (fData & (1 << 16));
  fInt1Seu	= ! (fData & (1 << 17));
  fInt2Seu	= ! (fData & (1 << 18));
  fRdo	   	= ! (fData & (1 << 19));
}
//____________________________________________________________________
void
Rcuxx::AltroERSTR::Print() const
{
  AltroRegister::Print();
  std::cout << std::boolalpha 
	    << "\tEmpty:\t\t\t\t" << fEmpty << "\n" 
	    << "\tFull:\t\t\t\t" << fFull << "\n" 
	    << "\t# buffers:\t\t\t" << fBuffers << "\n" 
	    << "\tWrite pointer:\t\t\t0x" << std::hex << fWp << "\n" 
	    << "\tRead pointer:\t\t\t0x" << std::hex << fRp << "\n"
	    << "\tErrors set:\n";
  if (!fRdo)         std::cout << "\t\tNothing to readout\n";
  if (!fInt2Seu)     std::cout << "\t\t2 single event upsets in bus FSM\n";
  if (!fInt1Seu)     std::cout << "\t\t1 single event upset in bus FSM\n";
  if (!fMMU2Seu)     std::cout << "\t\t2 single event upsets in buffer FSM\n";
  if (!fMMU1Seu)     std::cout << "\t\t1 single event upset in buffer FSM\n";
  if (!fTrigger)     std::cout << "\t\tOverlapping triggers\n";
  if (!fInstruction) std::cout << "\t\tIllegal instruction\n";
  if (!fParity)      std::cout << "\t\tError in parity of instruction\n";
  std::cout << std::dec << std::flush;
}

//====================================================================
void
Rcuxx::AltroADEVL::Print() const 
{
  AltroRegister::Print();
  std::cout << "\tHarware address:\t\t0x" << std::hex << fHADD << "\n" 
	    << "\tEvent length:\t\t\t" << std::dec << fEVL << std::endl;
}

//====================================================================
unsigned int 
Rcuxx::AltroTRCNT::Clear()
{
  unsigned int save_addr = fAddress;
  fAddress = 0x1C;
  unsigned int ret = Write(0);
  fAddress = save_addr;
  return ret;
}

//____________________________________________________________________
void
Rcuxx::AltroTRCNT::Print() const 
{
  AltroRegister::Print();
  std::cout << "\t# Triggers:\t\t\t" << fTRCNT << std::endl;
}



//====================================================================
Rcuxx::Altro::Altro(Rcu& rcu) 
{
  // 
  fWPINC = new AltroCommand("Write pointer++", 0x18, *(rcu.IMEM()));
  fRPINC = new AltroCommand("Read pointer++", 0x19, *(rcu.IMEM()));
  fCHRDO = new AltroCommand("Channel readout", 0x1A, *(rcu.IMEM()), false);
  fSWTRG = new AltroCommand("Make trigger", 0x1B, *(rcu.IMEM()));
  fTRCLR = new AltroCommand("Clear triggers", 0x1C, *(rcu.IMEM()));
  fERCLR = new AltroCommand("Clear errors", 0x1D, *(rcu.IMEM()));


  fERSTR   = new AltroERSTR(*(rcu.IMEM()), *(rcu.RMEM()));
  fTRCFG   = new AltroTRCFG(*(rcu.IMEM()), *(rcu.RMEM()));
  fDPCFG   = new AltroDPCFG(*(rcu.IMEM()), *(rcu.RMEM()));
  fDPCF2   = new AltroDPCF2(*(rcu.IMEM()), *(rcu.RMEM()));
  fVFPED   = new AltroVFPED(*(rcu.IMEM()), *(rcu.RMEM()));
  fPMDTA   = new AltroPMDTA(*(rcu.IMEM()), *(rcu.RMEM()));
  fPMADD   = new AltroPMADD(*(rcu.IMEM()), *(rcu.RMEM()));
  fZSTHR   = new AltroZSTHR(*(rcu.IMEM()), *(rcu.RMEM()));
  fBCTHR   = new AltroBCTHR(*(rcu.IMEM()), *(rcu.RMEM()));
  fK1      = new AltroKLCoeffs('K', 1, *(rcu.IMEM()), *(rcu.RMEM()));
  fK2      = new AltroKLCoeffs('K', 2, *(rcu.IMEM()), *(rcu.RMEM()));
  fK3      = new AltroKLCoeffs('K', 3, *(rcu.IMEM()), *(rcu.RMEM()));
  fL1      = new AltroKLCoeffs('L', 1, *(rcu.IMEM()), *(rcu.RMEM()));
  fL2      = new AltroKLCoeffs('L', 2, *(rcu.IMEM()), *(rcu.RMEM()));
  fL3      = new AltroKLCoeffs('L', 3, *(rcu.IMEM()), *(rcu.RMEM()));
  fADEVL   = new AltroADEVL(*(rcu.IMEM()), *(rcu.RMEM()));
  fTRCNT   = new AltroTRCNT(*(rcu.IMEM()), *(rcu.RMEM()));

  SetAddress(0);
}

//____________________________________________________________________
void 
Rcuxx::Altro::SetBroadcast() 
{

  fK1->SetBroadcast();
  fK2->SetBroadcast();
  fK3->SetBroadcast();
  fL1->SetBroadcast();
  fL2->SetBroadcast();
  fL3->SetBroadcast();
  fVFPED->SetBroadcast();
  fPMDTA->SetBroadcast();
  fPMADD->SetBroadcast();
  fZSTHR->SetBroadcast();
  fBCTHR->SetBroadcast();
  fTRCFG->SetBroadcast();
  fDPCFG->SetBroadcast();
  fDPCF2->SetBroadcast();
  fERSTR->SetBroadcast();
  fADEVL->SetBroadcast();
  fTRCNT->SetBroadcast();
  fWPINC->SetBroadcast();
  fRPINC->SetBroadcast();
  fCHRDO->SetBroadcast();
  fSWTRG->SetBroadcast();
  fERCLR->SetBroadcast();
  fTRCLR->SetBroadcast();
}

//____________________________________________________________________
void
Rcuxx::Altro::SetAddress(unsigned int board, unsigned int chip, 
			 unsigned int channel) 
{
  fK1->SetAddress(board, chip, channel);
  fK2->SetAddress(board, chip, channel);
  fK3->SetAddress(board, chip, channel);
  fL1->SetAddress(board, chip, channel);
  fL2->SetAddress(board, chip, channel);
  fL3->SetAddress(board, chip, channel);
  fVFPED->SetAddress(board, chip, channel);
  fPMDTA->SetAddress(board, chip, channel);
  fPMADD->SetAddress(board, chip, channel);
  fZSTHR->SetAddress(board, chip, channel);
  fBCTHR->SetAddress(board, chip, channel);
  fTRCFG->SetAddress(board, chip, channel);
  fDPCFG->SetAddress(board, chip, channel);
  fDPCF2->SetAddress(board, chip, channel);
  fERSTR->SetAddress(board, chip, channel);
  fADEVL->SetAddress(board, chip, channel);
  fTRCNT->SetAddress(board, chip, channel);
  fWPINC->SetAddress(board, chip, channel);
  fRPINC->SetAddress(board, chip, channel);
  fCHRDO->SetAddress(board, chip, channel);
  fSWTRG->SetAddress(board, chip, channel);
  fERCLR->SetAddress(board, chip, channel);
  fTRCLR->SetAddress(board, chip, channel);
}


//____________________________________________________________________
unsigned int 
Rcuxx::Altro::Update()
{
  unsigned int ret;
  if ((ret = fK1->Update())) 	return ret;
  if ((ret = fK2->Update())) 	return ret;
  if ((ret = fK3->Update())) 	return ret;
  if ((ret = fL1->Update())) 	return ret;
  if ((ret = fL2->Update())) 	return ret;
  if ((ret = fL3->Update())) 	return ret;
  if ((ret = fVFPED->Update())) return ret;
  if ((ret = fPMDTA->Update())) return ret;
  if ((ret = fPMADD->Update())) return ret;
  if ((ret = fZSTHR->Update())) return ret;
  if ((ret = fBCTHR->Update())) return ret;
  if ((ret = fTRCFG->Update())) return ret;
  if ((ret = fDPCFG->Update())) return ret;
  if ((ret = fDPCF2->Update())) return ret;
  if ((ret = fERSTR->Update())) return ret;
  if ((ret = fADEVL->Update())) return ret;
  if ((ret = fTRCNT->Update())) return ret;
  return ret;
}

//____________________________________________________________________
void
Rcuxx::Altro::Print() const
{
  fK1->Print();
  fK2->Print();
  fK3->Print();
  fL1->Print();
  fL2->Print();
  fL3->Print();
  fVFPED->Print();
  fPMDTA->Print();
  fPMADD->Print();
  fZSTHR->Print();
  fBCTHR->Print();
  fTRCFG->Print();
  fDPCFG->Print();
  fDPCF2->Print();
  fERSTR->Print();
  fADEVL->Print();
  fTRCNT->Print();
}

//____________________________________________________________________
void
Rcuxx::Altro::SetDebug(bool debug)
{
  fDebug = debug;
}
  
  
  
//____________________________________________________________________
//
// EOF
//
