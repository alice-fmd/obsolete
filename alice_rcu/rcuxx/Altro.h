// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    rcuxx/Altro.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 01:05:40 2006
    @brief   Declaration of ALTRO interface 
*/
#ifndef RCUXX_ALTRO_H
#define RCUXX_ALTRO_H
#ifndef RCUXX_RCU_H
# include <rcuxx/Rcu.h>
#endif

namespace Rcuxx 
{
  /** @defgroup rcuxx_altro ALTRO interface classes
      @ingroup rcuxx
   */
  //====================================================================
  /** @struct AltroRegister rcuxx/Altro.h <rcuxx/Altro.h> 
   *  @brief Interface to an ALTRO (bus) register.
   *  Note, that the access is implemented via the RcuIMEM and RcuRMEM
   *  interfaces, so this interface, and it's derived classes are
   *  completly general and does not need to be specialised for a
   *  particular back-end. 
   *  @ingroup rcuxx_altro
   */
  struct AltroRegister : public Register
  {
    /** Constructor 
	@param name Name 
	@param instr Instruction code 
	@param imem Instruction memory
	@param rmem Result memory 
	@param clear Whether to enable Clear command 
	@param submit Whether to enable submit command 
	@param global Whether this is global 
	@param bcast Whether to allow broadcast */
    AltroRegister(const char* name, unsigned int instr, 
		  RcuIMEM& imem, RcuRMEM& rmem,
		  bool clear, bool submit, bool global=false,
		  bool bcast=true);
    /** Set to broadcast, if allowed. */
    virtual void   SetBroadcast();
    /** Set target address
	@param board   Board number   (0x1f)
	@param chip    Chip number    (0x7)
	@param channel Channel number (0xf) */
    virtual void   SetAddress(unsigned int board, unsigned int chip, 
			      unsigned int channel=0);
    /** Write @a data to register 
	@param data Data to put in register
	@return 0 on success, error code otherwise */
    virtual unsigned int Write(unsigned int data);
    /** Read value of register into @a data
	@param data On return, value of register 
	@return 0 on success, error code otherwise */
    virtual unsigned int Read(unsigned int& data);
    /** Make write instructions and store them in @a imem
	@param imem On return, instructions for writing */
    virtual void WriteInstructions(unsigned int* imem);
    /** Make read instructions and store them in @a imem
	@param imem On return, instructions for reading */
    virtual void ReadInstructions(unsigned int* imem);
    /** Set internal structures */
    virtual void Set() { fData = 0; }
    /** Get internal structures */
    virtual void Get() { }
    /** Commit stored values to hardware 
	@return 0 on success, error code otherwise */
    virtual unsigned int Commit();
    /** Read from hardware to stored values
	@return 0 on success, error code otherwise */
    virtual unsigned int Update();
    /** Print contents of this register */
    virtual void Print() const;
    /** Whether we got Clear */
    virtual bool IsClearable() const { return fClear; }
    /** Whether this can be broadcast */ 
    virtual bool IsBroadcastable() const { return fAllowBroadcast; }
  protected:
    /** Can clear */
    bool fClear;
    /** Instruction number */
    unsigned int fInstr;
    /** Instruction memory */
    RcuIMEM&        fIMEM;
    /** Result memory */
    RcuRMEM&        fRMEM;
    /** Address */
    unsigned int fAddress;
    /** Whether it's global */
    bool         fGlobal;
    /** Whether it allows broad cast */
    bool         fAllowBroadcast;
    /** Data cache */
    unsigned int fData;
  };

  //====================================================================
  /** @struct AltroCommand rcuxx/Altro.h <rcuxx/Altro.h> 
   *  @brief Interface to an ALTRO (bus) command.
   *  Note, that the access is implemented via the RcuIMEM
   *  interface, so this interface, and it's derived classes are
   *  completly general and does not need to be specialised for a
   *  particular back-end. 
   *  @ingroup rcuxx_altro
   */
  struct AltroCommand : public Command
  {
    /** Constructor 
	@param name Name of command 
	@param cmd Command code
	@param imem Instruction memory
	@param bcast Whether to allow broadcast 
	@return  */
    AltroCommand(const char* name, unsigned int cmd, 
		 RcuIMEM& imem, bool bcast=true);
    /** Set to broadcast, if allowed. */
    virtual void   SetBroadcast();
    /** Set target address
	@param board   Board number   (0x1f)
	@param chip    Chip number    (0x7)
	@param channel Channel number (0xf) */
    virtual void   SetAddress(unsigned int board, unsigned int chip, 
			      unsigned int channel=0);
    /** Make write instructions and store them in @a imem
	@param imem On return, instructions for writing */
    virtual void WriteInstructions(unsigned int* imem);
    /** Commit stored values to hardware 
	@return 0 on success, error code otherwise */
    virtual unsigned int Commit();
  protected:
    /** Instruction memory */
    RcuIMEM&   fIMEM;
    /** Address */
    unsigned int fAddress;
    /** Data */ 
    unsigned int fData;
    /** Whether to allow broad cast */
    bool fAllowBroadcast;
  };


  //====================================================================
  /** @struct AltroKLCoeffs rcuxx/Altro.h <rcuxx/Altro.h>
   *  @brief Interface to ALTRO K or L registers.  There is a set of 6
   *  16bit registers for each channel independently. The @f$ K_i@f$ 
   *  correspond to the poles of each stage of the tail Cancellation
   *  Filter and the @f$ L_i@f$ are similarly the zeros of the
   *  Filter. A broadcast of the Coefficients will give the same Tail  
   *  Cancellation Filter settings to all the channels and indeed all
   *  the ALTROs.    The parameters @f$ K_i, L_i@f$ should be in the
   *  range @f$ [0,1]@f$, so the floating point value from a given
   *  integer input is given by e.g., @f$ K_1=K_{1i}/65535@f$, or the
   *  integer input needed for a given floating point number is e.g., 
   *  @f$ K_{1i} = K_1 \times 65335@f$
   *  @note This is definend per channel 
   *  @ingroup rcuxx_altro
   */
  struct AltroKLCoeffs : public AltroRegister 
  {
    /** Constructor 
	@param which Which (K or L) 
	@param n Which (1-3)
	@param imem Instruction memory
	@param rmem Result memory */
    AltroKLCoeffs(char which, size_t n, RcuIMEM& imem, RcuRMEM& rmem);
    /** Get the values from internal cache after update */
    void Get() { fValue = fData; }
    /** Set the internal cache before commit */
    void Set() { fData = fValue; }
    /** K or L coefficent */
    unsigned int Value() const { return fValue; }
    /** K or L coefficent */
    void SetValue(unsigned int val) { fValue = val; }
    /** K or L coefficent */
    void SetValue(float val) { fValue = (unsigned int)(val*65535); }
    /** Print contents of this register */
    virtual void Print() const;
  protected:
    /** Current value */
    unsigned int fValue;
  };

  //====================================================================
  /** @struct AltroVFPED rcuxx/Altro.h <rcuxx/Altro.h>
   *  @brief Interface to ALTRO register VFPED - the variable and
   *  fixed pedestals used in the baseline unit (First baseline
   *  correction unit). If the correct mode is used, the input signal
   *  coming from the ADC will be subtracted to FPD or VPD. Both are
   *  coded in 10bit each. The variable pedestal can only be read, the 
   *  fixed one can be both read and written.
   *  @note This is definend per channel 
   *  @ingroup rcuxx_altro
   */
  struct AltroVFPED : public AltroRegister 
  {
    AltroVFPED(RcuIMEM& imem, RcuRMEM& rmem)
      : AltroRegister("Pedestals", 0x6, imem, rmem, false, true)
    {}
    /** Get the values from internal cache after update */
    void Get() { fVP = (fData >> 10) & 0x3ff; fFP = fData & 0x3ff; }
    /** Set the internal cache before commit */
    void Set() { fData = fFP & 0x3ff; }
    /** Variable pedestal */
    unsigned int VP() const { return fVP; }
    /** Fixed pedestal */
    unsigned int FP() const { return fFP; }
    /** Fixed pedestal */
    void SetFP(unsigned int val) { fFP = val; }
    /** Print contents of this register */
    virtual void Print() const;
  protected:  
    /** Variable pedestal */
    unsigned int fVP;
    /** Fixed pedestal */
    unsigned int fFP;
  
  };

  //====================================================================
  /** @struct AltroPMDTA rcuxx/Altro.h <rcuxx/Altro.h>
   *  @brief Interface to ALTRO register PMDTA - The pedestal memory
   *  data.   Data to be stored in the Pedestal Memory of each one of
   *  the 16 channels. The memories are cuts of 1K x 10bit and can be
   *  used in different modes, see register AltroDPCFG. 
   * 
   *  @note Data written to or read from this register is routed
   *  to/from the Pedestal Memory of the corresponding channel at the
   *  address specified in the global register PMADD. PMADD is common
   *  for all the channels. Therefore, the strategy to fill up the
   *  Pedestal Memories is first to write the address and then the
   *  data for that address across all the 16 channels. The procedure
   *  is repeated again for each address. 
   *
   *  Before writing or reading the Pedestal Memory, make sure that
   *  the First Baseline Correction is in a mode that does not access
   *  the memory, otherwise data will be corrupted. The recommended
   *  operation mode is @b din-fpd. 
   *  
   *  @note This is definend per channel.  
   *  @ingroup rcuxx_altro
   */
  struct AltroPMDTA : public AltroRegister 
  {
    AltroPMDTA(RcuIMEM& imem, RcuRMEM& rmem)
      : AltroRegister("Pedestal memory data", 0x7, imem, rmem, false, true)
    {}
    /** Get the values from internal cache after update */
    void Get() { fPMDTA = fData; }
    /** Set the internal cache before commit */
    void Set() { fData = fPMDTA & 0x3ff; }
    /** @return Pedestal memory data address */
    unsigned int PMDTA() const { return fPMDTA; }
    /** Pedestal memory data address */
    void SetPMDTA(unsigned int val) { fPMDTA = val; }
    /** Print contents of this register */
    virtual void Print() const;
  protected:
    unsigned int fPMDTA;  
  };
      
      
  //====================================================================
  /** @struct AltroZSTHR rcuxx/Altro.h <rcuxx/Altro.h>
   *  @brief Interface to ALTRO register ZSTHR - the zero
   *  suppression threshold.  The Offset (higher 10bit) enables the
   *  signal to sit in a higher baseline so that negative variations
   *  can be seen. The samples below the threshold (lower 10bit) are
   *  suppressed. This register has a direct influence on the amount
   *  of data transmitted and the number of samples / event stored in
   *  the memory. 
   *  @ingroup rcuxx_altro
   */
  struct AltroZSTHR : public AltroRegister 
  {
    AltroZSTHR(RcuIMEM& imem, RcuRMEM& rmem)
      : AltroRegister("Zero supression threshold", 0x8, imem, rmem, 
		      false, true, true)
    {}
    /** Get the values from internal cache after update */
    void Get();
    /** Set the internal cache before commit */
    void Set() { fData =((fOffset & 0x3ff) << 10)+(fZS_THR & 0x3ff); }
    /** Zero suppresson offset */
    unsigned int Offset() const { return fOffset; }
    /** Zero suppresson offset */
    void SetOffset(unsigned int val) { fOffset = val; }
    /** Zero suppresson threshold */
    unsigned int ZS_THR() const { return fZS_THR; }
    /** Zero suppresson threshold */
    void SetZS_THR(unsigned int val) { fZS_THR = val; }
    /** Print contents of this register */
    virtual void Print() const;
  protected:
    /** Offset */
    unsigned int fOffset;
    /** Threshold */
    unsigned int fZS_THR;
  };

  //====================================================================
  /** @struct AltroBCTHR rcuxx/Altro.h <rcuxx/Altro.h>
   *  @brief Interface to ALTRO register BCTHR - Base line correction
   *  threshold.  The higher 10bit correspond to the upper threshold
   *  and the lower 10bit to the bottom threshold. The range in
   *  between these two levels is indeed an estimation of where the
   *  baseline really is. The average baseline is calculated whenever
   * the input signal lies in that range. 
   *  @ingroup rcuxx_altro
   */
  struct AltroBCTHR : public AltroRegister 
  {  
    AltroBCTHR(RcuIMEM& imem, RcuRMEM& rmem)
      : AltroRegister("2nd baseline threshold", 0x9, imem, rmem, 
		      false, true, true)
    {}
    /** Get the values from internal cache after update */
    void Get();
    /** Set the internal cache before commit */
    void Set() { fData=((fTHR_HI & 0x3ff) << 10)+(fTHR_LO & 0x3ff); }
    /** Baseline correction high threshold */
    unsigned int THR_HI() const { return fTHR_HI; }
    /** Baseline correction high threshold */
    void SetTHR_HI(unsigned int val) { fTHR_HI = val; }
    /** Baseline correction low threshold */
    unsigned int THR_LO() const { return fTHR_LO; }
    /** Baseline correction low threshold */
    void SetTHR_LO(unsigned int val) { fTHR_LO = val; }
    /** Print contents of this register */
    virtual void Print() const;
  protected:
    /** High threshold */
    unsigned int fTHR_HI;
    /** Low threshold */
    unsigned int fTHR_LO;
  };

  //====================================================================
  /** @struct AltroTRCFG rcuxx/Altro.h <rcuxx/Altro.h>
   *  @brief Interface to ALTRO register TRCFG - trigger
   *  configuration.    The higher 10bit code for the trigger
   *  delay. The delay between the global trigger and the arrival of
   *  data to the pads depends on the position of the pads themselves
   *  in the chamber. For specific chips the delay can be adjusted in
   *  order to compensate for this. Number of samples/event codes the
   *  number of samples / event to be processed and it ranges from 0
   *  to 1000. 
   * 
   *  @note ACQ_START must be less or equal than ACQ_END. When
   *  Pretrigger is used, ACQ_START is ignored. Pretrigger and
   *  ACQ_START are mutually exclusive features. To avoid overflowing
   *  the data memory when it is divided in 8 buffers, ACQ_END should
   *  not exceed 506 (1FA). 
   *  @ingroup rcuxx_altro
   */
  struct AltroTRCFG : public AltroRegister 
  {  
    AltroTRCFG(RcuIMEM& imem, RcuRMEM& rmem)
      : AltroRegister("Acquisition window", 0xA, imem, rmem, 
		      false, true, true)
    {}
    /** Get the values from internal cache after update */
    void Get();
    /** Set the internal cache before commit */
    void Set();
    /** Acquisition start (in time-bins) */
    unsigned int ACQ_START() const { return fACQ_START; }
    /** Acquisition start (in time-bins) */
    void SetACQ_START(unsigned int val) { fACQ_START = val; }
    /** Acquisition end (in time-bins) */
    unsigned int ACQ_END() const { return fACQ_END; }
    /** Acquisition end (in time-bins) */
    void SetACQ_END(unsigned int val) { fACQ_END = val; }
    /** Print contents of this register */
    virtual void Print() const;
  protected:
    /** Acquisition start (in time-bins) */
    unsigned int fACQ_START;
    /** Acquisition end (in time-bins) */
    unsigned int fACQ_END;
  };

  //====================================================================
  /** @struct AltroDPCFG rcuxx/Altro.h <rcuxx/Altro.h>
   *  @brief Interface to ALTRO register DPCFG - data path
   *  configuration. Register containing configuration parameters for
   *  the BSU, MAU and ZSU. The table below shows in detail the
   *  function of each bit. 
   *  
   *  @ingroup rcuxx_altro
   */
  struct AltroDPCFG : public AltroRegister 
  {  
    AltroDPCFG(RcuIMEM& imem, RcuRMEM& rmem)
      : AltroRegister("Data path configuration 1", 0xB, imem, rmem, 
		      false, true, true)
    {}
    /** Get the values from internal cache after update */
    void Get();
    /** Set the internal cache before commit */
    void Set();
    /** 1st baseline mode */
    unsigned int FirstBMode() const { return f1stBMode; }
    /** 1st baseline mode 
	Valid modes are 
	<table> 
	  <tr><th>bits</th><th>hex</th><th>Meaning</th></tr>
	  <tr><td>0000</td><td>0x0</td><td>din-fpd</td></tr>
	  <tr><td>0001</td><td>0x1</td><td>din-f(t)</td></tr>
	  <tr><td>0010</td><td>0x2</td><td>din-f(din)</td></tr>
	  <tr><td>0011</td><td>0x3</td><td>din-f(din-vpd)</td></tr>
	  <tr><td>0100</td><td>0x4</td><td>din-vpd-fpd</td></tr>
	  <tr><td>0101</td><td>0x5</td><td>din-vpd-f(t)</td></tr>
	  <tr><td>0110</td><td>0x6</td><td>din-vpd-f(din)</td></tr>
	  <tr><td>0111</td><td>0x7</td><td>din-vpd-f(din-vpd)</td></tr>
	  <tr><td>1000</td><td>0x8</td><td>f(din)-fpd</td></tr>
	  <tr><td>1001</td><td>0x9</td><td>f(din-vpd)-fpd</td></tr>
	  <tr><td>1010</td><td>0xA</td><td>f(t)-fpd</td></tr>
	  <tr><td>1011</td><td>0xB</td><td>f(t)-f(t)</td></tr>
	  <tr><td>1100</td><td>0xC</td><td>f(din)-f(din)</td></tr>
	  <tr><td>1101</td><td>0xD</td><td>f(din-vpd)-f(din-vpd)</td></tr>
	  <tr><td>1110</td><td>0xE</td><td>din-fpd</td></tr>
	  <tr><td>1111</td><td>0xF</td><td>din-fpd</td></tr>
        </table>
        The abbrivations have the following meaning:
	- din stands for the data stream coming from the ADC.
	- fpd stands for the fixed pedestal, a constant value
	  stored in register VFPED that is to be subtracted from the
	  ADC data stream  (see AltroVFPED). 
	- vpd stands for the self-calibrated pedestal value, that is,
	  the average DC level that the ADC sees outside the
	  acquisition window (i.e. when there is no signal from the
	  gas chamber) (see AltroVFPED)
	- @f$ f(x)@f$ Look-up in pedestal memory with index @f$ x@f$. 
	  That is, for example
	  - f(t) stands for the data of the Pedestal Memory, played
	    back as a function of time for the duration of the
	    acquisition after a L1 trigger is received. (Pattern
	    Generator Mode). 
	  - f(din) stands for the data of the Pedestal Memory, played
	    back a function of the ADC data at any time. (Look-up
	    Table Mode).
     */
    void SetFirstBMode(unsigned int val) { f1stBMode = val; }
    /** 1's compliment */
    bool IsFirstBPol() const { return f1stBPol; }
    /** @param on Set 1's compliment to on or off */
    void SetFirstBPol(bool on=true) { f1stBPol = on; }

    /** 2nd baseline pre excl. */
    unsigned int SecondBPre() const { return f2ndBPre; }
    /** 2nd baseline pre excl. */
    void SetSecondBPre(unsigned int val) { f2ndBPre = val; }
    /** 2nd baseline post exclude */
    unsigned int SecondBPost() const { return f2ndBPost; }
    /** 2nd baseline post exclude */
    void SetSecondBPost(unsigned int val) { f2ndBPost = val; }
    /** Is 2nd baseline correction enabled */
    bool IsSecondBEnable() const { return f2ndBEnable; }
    /** @param on Set 2nd baseline correction to on or off */
    void SetSecondBEnable(bool on=true) { f2ndBEnable = on; }

    /** Zero suppression pre samples */
    unsigned int ZSPre() const { return fZSPre; }
    /** Zero suppression pre samples */
    void SetZSPre(unsigned int val) { fZSPre = val; }
    /** Zero suppression post samples */
    unsigned int ZSPost() const { return fZSPost; }
    /** Zero suppression post samples */
    void SetZSPost(unsigned int val) { fZSPost = val; }
    /** Zero suppression glitch mode */
    unsigned int ZSGlitch() const { return fZSGlitch; }
    /** Zero suppression glitch mode 
        Mode can be one of 
        <table> 
	  <tr><th>bits</th><th>hex</th><th>Meaning</th></tr>
	  <tr><td>00</td><td>0x0</td><td>din-fpd</td></tr>
	  <tr><td>01</td><td>0x1</td><td>din-f(t)</td></tr>
	  <tr><td>10</td><td>0x2</td><td>din-f(din)</td></tr>
	  <tr><td>11</td><td>0x3</td><td>din-f(din-vpd)</td></tr>
	</table>
	See the member function AltroDPCG::SetFirstBMode for the
	meaning of the abbriviations. 
    */
    void SetZSGlitch(unsigned int val) { fZSGlitch = val; }
    /** Is zero suppression enabled  */
    bool IsZSEnable() const { return fZSEnable; }
    /** @param on Set zero suppression to on or off */
    void SetZSEnable(bool on=true) { fZSEnable = on; }
    /** Print contents of this register */
    virtual void Print() const;
  protected:
    /** Mode */
    unsigned int f1stBMode;
    /** 1's compliment */
    bool         f1stBPol;

    /** Pre excl. */
    unsigned int f2ndBPre;
    /** Post excl. */
    unsigned int f2ndBPost;
    /**  */
    bool         f2ndBEnable;

    /** pre samples */
    unsigned int fZSPre;
    /** post samples */
    unsigned int fZSPost;
    /** Glitch moe */
    unsigned int fZSGlitch;
    /** enable  */
    bool         fZSEnable;
  };

  //====================================================================
  /** @struct AltroDPCF2 rcuxx/Altro.h <rcuxx/Altro.h>
   *  @brief Interface to ALTRO register AltroDPCF2
   *  @note The Power Save bit may reduce the power consumption
   *  dramatically under certain data path configurations.
   *  @ingroup rcuxx_altro
   */
  struct AltroDPCF2 : public AltroRegister 
  {
    enum BUF_t {
      k4Buffers, 
      k8Buffers
    }; 
    AltroDPCF2(RcuIMEM& imem, RcuRMEM& rmem)
      : AltroRegister("Data path configuration 2", 0xC, imem, rmem, 
		      false, true, true)
    {}
    /** Get the values from internal cache after update */
    void Get();    
    /** Set the internal cache before commit */
    void Set();
    /** Pre trigger */
    unsigned int PTRG() const { return fPTRG; }
    void SetPTRG(unsigned int val) { fPTRG = val; } 
    /** # buffers */
    BUF_t BUF() const { return fBUF; }
    /** # buffers */
    void SetBUF(BUF_t buf) { fBUF = buf; }
    /** Is Digital filter enabled */
    bool IsFLT_EN() const { return fFLT_EN; }
    /** @param on Set digital filter to on or off */
    void SetFLT_EN(bool on=true) { fFLT_EN = on; } 
    /** Is power save enabled */
    bool IsPWSV() const { return fPWSV; }
    /** @param on Set power save to on or off */
    void SetPWSV(bool on=true) { fPWSV = on; }   
    /** Print contents of this register */
    virtual void Print() const;
  protected:
    /** Pre trigger */
    unsigned int fPTRG; 
    /** # buffers */
    BUF_t fBUF; 
    /** Digital filter */
    bool fFLT_EN; 
    /** Power save */
    bool fPWSV;   
  };
	
  //====================================================================
  /** @struct AltroPMADD rcuxx/Altro.h <rcuxx/Altro.h>
   *  @brief Interface to ALTRO register PMADD.  It contains the value
   *  of the pedestal memory address.
   *  @note The value set in PMA is common for all the
   *  channels. Therefore, the recommended strategy to fill up the
   *  Pedestal Memories is to write the PMA first, and then the
   *  corresponding data across all the 16 channels. This sequence is
   *  repeated until all the memories all filled up.  
   *
   *  Before writing
   *  or reading the Pedestal Memory, make sure that the First
   *  Baseline Correction is in a mode that does not access the
   *  memory, otherwise data will be corrupted. The recommended
   *  operation mode is @b din-fpd. 
   *  @note This is per channel
   *  @ingroup rcuxx_altro
   */
  struct AltroPMADD : public AltroRegister 
  {  
    AltroPMADD(RcuIMEM& imem, RcuRMEM& rmem)
      : AltroRegister("Pedestal memory address", 0xD, imem, rmem, false, true)
    {}
    /** Get the values from internal cache after update */
    void Get() { fPMADD = fData & 0x3ff; }
    /** Set the internal cache before commit */
    void Set() { fData = fPMADD & 0x3ff; }
    unsigned int PMADD() const { return fPMADD; }
    void SetPMADD(unsigned int val) { fPMADD = val; }
    /** Print contents of this register */
    virtual void Print() const;
  protected:
    unsigned int fPMADD;
  };

  //====================================================================
  /** @struct AltroERSTR rcuxx/Altro.h <rcuxx/Altro.h>
   *  @brief Interface to ALTRO register ERSTR.  It contains 8 bit for
   *  coding errors in the circuit: Readout error, single and double
   *  event  upsets (SEU) in the MMU and Interface modules, trigger
   *  overlap and instruction error.  This last error embraces the
   *  cases of writing or reading in the wrong or non-existent
   *  address. The lower 12 bits give information on the state of the 
   *  multi-event buffer: empty, full, remaining buffers and the
   *  position of the Read and Write pointers. The table below
   *  summarizes the error and status register. 
   * 
   *  This table summarizes the meaning og register's bits 
   * 
   *  <table>
   *    <tr><th>Parameter</th><th>Description</th></tr>
   *    <tr><td>Read Pointer</td><td>Pointer to the buffer that is to
   *      be read out</td></tr>
   *    <tr><td>Write Pointer</td><td>Pointer to the buffer that is to
   *      be written on next trigger</td></tr>
   *    <tr><td>Remaining Buffers</td><td>Number of empty buffers
   *      remaining in the Data Memory</td></tr>
   *    <tr><td>FULL</td><td>Flag signalling that all the buffers of
   *      the memory are filled with valid event</td></tr>
   *    <tr><td>EMPTY</td><td>Flag signalling that all the buffers of
   *      the memory are available for writing</td></tr>
   *    <tr><td>Parity Error</td><td>A parity error has been detected
   *      while decoding an instruction (sticky bit)</td></tr>
   *    <tr><td>Instruction Error</td><td>An illegal instruction has
   *      been received (sticky bit)</td></tr>
   *    <tr><td>Trigger Overlap</td><td>A trigger pulse has been
   *      received during the processing window of a previous trigger  
   *      (sticky bit)</td></tr>  
   *    <tr><td>MMU 1 SEU</td><td>One Single Event Upset has been
   *      detected in the state machine that controls the buffers of
   *      the Data Memory (sticky bit)</td></tr>
   *    <tr><td>MMU 2 SEU</td><td>Two Single Event Upsets have been
   *      detected in the state machine that controls the buffers of
   *      the Data Memory</td></tr>
   *    <tr><td>INT 1 SEU</td><td>One Single Event Upset has been
   *      detected in the state machine that controls the interface to
   *      the external bus (sticky bit)</td></tr> 
   *    <tr><td>INT 2 SEU</td><td>Two Single Event Upsets have been
   *      detected in the state machine that controls the interface to
   *      the external bus (sticky bit)</td></tr> 
   *    <tr><td>RDO Error</td><td>A readout command has been received
   *      when there was nothing to read out. (sticky bit)</td></tr> 
   *  </table>
   *  @note 
   *    - Single Event Upsets (SEU) will only occur in the presence of
   *      radiation. If a SEU happens, the affected state machine will
   *      recover automatically. If a double SEU is detected, the
   *      corresponding state machine has interrupted its logical
   *      sequence and gone to idle state.  The chip must therefore be
   *      reset when possible. 
   *    - All of the error bits are sticky, that is, they remain in
   *      the @b 1 state after they are set. The error bits are reset
   *      when the chip is reset or powered off or the ERCLR command
   *      is issued. 
   *    - When running in 4-buffer mode, the Write Pointer and Read
   *      Pointer can only take the values 0, 2, 4 or 6. In the
   *      8-buffer mode, they take all values between 0 and 7. 
   *    - The number of remaining buffers ranges from 0 to 4 in the
   *      4-buffer mode and from 0 to 8 in the 8-buffer mode.
   *    - Valid instructions can produce an instruction error if they
   *      are issued in the wrong mode (e.g. broadcasting a register
   *      read, or writing a read-only register) 
   *  @note When the FULL flag is set, any further L1 or L2 triggers 
   *  will be ignored. The Readout Controller Unit must take care of
   *  filtering the triggers and avoiding this
   *  situation. Nevertheless, if a lost L1 trigger was to be
   *  identified, the user can check the value of the Trigger Counter
   *  Register (TRCNT). 
   *  @ingroup rcuxx_altro
   */
  struct AltroERSTR : public AltroRegister 
  {
    AltroERSTR(RcuIMEM& imem, RcuRMEM& rmem)
      : AltroRegister("Error and status", 0x10, imem, rmem, true, false, true)
    {}
    /** Get the values from internal cache after update */
    void Get();
    /** Clear the register */
    unsigned int Clear() { Write(0); }
    /** Is there Nothing to readout? */
    bool IsRdo() const { return fRdo; }		
    /** Is there 2 single event upsets in bus FSM? */
    bool IsInt2Seu() const { return fInt2Seu; }
    /** Is there 1 single event upset in bus FSM? */
    bool IsInt1Seu() const { return fInt1Seu; }
    /** Is there 2 single event upsets in buffer FSM? */
    bool IsMMU2Seu() const { return fMMU2Seu; }
    /** Is there 1 single event upset in buffer FSM? */
    bool IsMMU1Seu() const { return fMMU1Seu; }
    /** Is there Overlapping triggers? */
    bool IsTrigger() const { return fTrigger; }
    /** Is there Illegal instruction? */
    bool IsInstruction() const { return fInstruction; }
    /** Is there Error in parity of instruction? */
    bool IsParity() const { return fParity; }
    /** Is there The buffers are empty? */
    bool IsEmpty() const { return fEmpty; }
    /** Is there All buffers full? */
    bool IsFull() const { return fFull; }
    /** Number of buffers */
    unsigned int Buffers() const { return fBuffers; }
    /** Write pointer */
    unsigned int Wp() const { return fWp; }
    /** Read pointer */
    unsigned int Rp() const { return fRp; }
    /** Print contents of this register */
    virtual void Print() const;
  protected:
    /** Readout - Nothing to readout */
    bool fRdo;		
    /** 2 bus SEU - 2 single event upsets in bus FSM */
    bool fInt2Seu;
    /** 1 bus SEU - 1 single event upset in bus FSM */
    bool fInt1Seu;
    /** 2 buffer SEU - 2 single event upsets in buffer FSM */
    bool fMMU2Seu;
    /** 1 buffer SEU - 1 single event upset in buffer FSM */
    bool fMMU1Seu;
    /** Trigger - Overlapping triggers */
    bool fTrigger;
    /** Instr. - Illegal instruction */
    bool fInstruction;
    /** Parity - Error in parity of instruction */
    bool fParity;
    /** Empty - The buffers are empty */
    bool fEmpty;
    /** Full - All buffers full */
    bool fFull;
    /** Number of buffers */
    unsigned int fBuffers;
    /** Write pointer */
    unsigned int fWp;
    /** Read pointer */
    unsigned int fRp;
  };
      
  //====================================================================
  /** @struct AltroADEVL rcuxx/Altro.h <rcuxx/Altro.h>
   *  @brief Interface to ALTRO register ADEVL.  The 8bit channel
   *  address can be read from AD[15-8]. The Event Length for a given
   *  channel is coded in the lower 8 bit of the address space.
   *  @note Read only
   *  @ingroup rcuxx_altro
   */
  struct AltroADEVL : public AltroRegister 
  {  
    AltroADEVL(RcuIMEM& imem, RcuRMEM& rmem)
      : AltroRegister("Chip addres and event length", 0x11, imem, rmem, 
		      false, false)
    {}
  
    /** Get the values from internal cache after update */
    void Get() { fHADD = (fData >> 8) & 0xff; fEVL = fData & 0xff; }
    /** @return Hardware address */
    unsigned int HADD() const { return fHADD; }
    /** @return Event length */
    unsigned int EVL() const { return fEVL; }
    /** Print contents of this register */
    virtual void Print() const;
  protected:
    unsigned int fHADD;
    unsigned int fEVL;
  };

  //====================================================================
  /** @struct AltroTRCNT rcuxx/Altro.h <rcuxx/Altro.h>
   *  @brief Interface to ALTRO register TRCNT.  The 16 lower bits
   *  code the number of level 1 triggers received by the ALTRO chip. 
   *  @note This counter is set to 0 when the chip is reset or when
   *  the command TRCLR is issued. The count includes also the
   *  triggers that are ignored when the memory is full. 
   *  @note Read only
   *  @ingroup rcuxx_altro
   */
  struct AltroTRCNT : public AltroRegister 
  {
    AltroTRCNT(RcuIMEM& imem, RcuRMEM& rmem)
      : AltroRegister("Trigger counter", 0x12, imem, rmem, true, false, true)
    {}
    /** Get the values from internal cache after update */
    void Get() { fTRCNT = fData; }
    /** Clear the register */
    unsigned int Clear();
    /** @return Trigger count  */
    unsigned int  TRCNT() const { return fTRCNT; }
    /** Print contents of this register */
    virtual void Print() const;
  protected:
    unsigned int  fTRCNT;
  };

  //====================================================================
  /** @class Altro rcuxx/Altro.h <rcuxx/Altro.h>
      @brief Interface to ALTRO's 
      @ingroup rcuxx_altro
   */
  class Altro 
  {
  public:
    /** Consntructor 
	@param rcu Reference to controlling RCU interface */
    Altro(Rcu& rcu);
    /** Update values
	@return 0 on success, error code otherwise */
    unsigned int Update();
    /** Set the address to talk to
	@param board Board number
	@param chip  Chip number 
	@param channel Channel number */
    void SetAddress(unsigned int board, unsigned int chip=0, 
		    unsigned int channel=0);
    /** Set to broadcast to all ALTRO's */
    void SetBroadcast();
    /** Print contents */
    void Print() const;
    /** Set the debuging stuff */
    virtual void SetDebug(bool debug);

    /** Command WPINC - Wrte pointer increment 
	This command is equivalent to the Level 2 Trigger Accept. The
	effect of this command is to freeze in one of the buffers of
	the data memory the data taken after the last Level 1
	Trigger. This is done by increasing the Write Pointer that
	points to the memory position where data is to be written when
	a L1 is received. 

	@note WPINC must be issued only after the acquisition of the
	event is achieved. Data will be corrupted and not retrievable
	if the WPINC is issued while the chip is still  ecording
	data. Refer to Chapter 4 of the ALTRO manual for timing
	specifications.  

	@note If an event is to be kept in memory, the WPINC command
	must be issued before the next L1 trigger arrives.
     */
    AltroCommand* WPINC() const { return fWPINC; }
    /** Command RPINC - Read pointer increment. 
	This command releases a buffer of the Data Memory, making it
	available for writing new data. Buffers are used and released
	on a FIFO basis, therefore this command will free the first
	(read or unread) buffer. 
	
	@note RPINC is intended to be issued after the readout of all
	the channels has been done. Once the command is executed,
	there is no way to recover the data stored in the released
	buffer. 
    */
    AltroCommand* RPINC() const { return fRPINC; }
    /** Command CHRDO - Channel readout. 
	This command produces the readout of the specified
	channel. The readout starts immediately after the command is
	acknowledged. During the readout, the ALTRO becomes the owner
	of the bus. 

	@note After CHRDO is acknowledged, the RCU should not issue
	any further instructions and must wait for the TRSF line to go
	low. 

	@note The readout may be interrupted if a L1 trigger is
	received on its dedicated line.  Therefore, the RCU must wait
	for the completion of the acquisition and then continue to
	store the readout. 
    */
    AltroCommand* CHRDO() const { return fCHRDO; }
    /** Command SWTRG - software trigger.
	This command sends a Level 1 trigger to the processing chain
	of the chip. It is entirely equivalent to the dedicated L1
	line, except that the timing depends on both the readout and
	the sampling clocks. 
	@note This command is provided for testing purposes. In normal
	operation mode, the dedicated L1 line should be used.
    */
    AltroCommand* SWTRG() const { return fSWTRG; }
    /** Command TRCLR - TRigger CLeaR. 
	This command sets the trigger counter (TRCNT) to 0. */
    AltroCommand* TRCLR() const { return fTRCLR; }
    /** Command ERCLR - ERror CLeaR. 
	This command resets the sticky bits of the Status and Error
	Register (ERSTR)  */
    AltroCommand* ERCLR() const { return fERCLR; }
    /** Register K1 - */
    AltroKLCoeffs* K1() const { return fK1; }
    /** Register K2 - */
    AltroKLCoeffs* K2() const { return fK2; }
    /** Register K3 - */
    AltroKLCoeffs* K3() const { return fK3; }
    /** Register L1 - */
    AltroKLCoeffs* L1() const { return fL1; }
    /** Register L2 - */
    AltroKLCoeffs* L2() const { return fL2; }
    /** Register L3 - */
    AltroKLCoeffs* L3() const { return fL3; }
    /** Register VFPED - Variable and fixed pedestal */
    AltroVFPED* VFPED() const { return fVFPED; }
    /** Register PMDTA - Pedestal memory data */
    AltroPMDTA* PMDTA() const { return fPMDTA; }
    /** Register ZSTHR - Zero suppression mode and threshold */
    AltroZSTHR* ZSTHR() const { return fZSTHR; }
    /** Register BCTHR - Baseline correction mode and threshold */
    AltroBCTHR* BCTHR() const { return fBCTHR; }
    /** Register TRCFG - Trigger configuration */
    AltroTRCFG* TRCFG() const { return fTRCFG; }
    /** Register DPCFG - Data path configuration 1 */
    AltroDPCFG* DPCFG() const { return fDPCFG; }
    /** Register DPCF2 - Data path configuration 2 */
    AltroDPCF2* DPCF2() const { return fDPCF2; }
    /** Register PMADD - Pedestal memory address */
    AltroPMADD* PMADD() const { return fPMADD; }
    /** Register ERSTR - Error and status */
    AltroERSTR* ERSTR() const { return fERSTR; }
    /** Register ADEVL - Address */
    AltroADEVL* ADEVL() const { return fADEVL; }
    /** Register TRCNT - Trigger counter */
    AltroTRCNT* TRCNT() const { return fTRCNT; }
  protected:
    /** Command WPINC - Wrte pointer increment */
    AltroCommand* fWPINC;
    /** Command RPINC - Read pointer increment */
    AltroCommand* fRPINC;
    /** Command CHRDO - Channel readout */
    AltroCommand* fCHRDO;
    /** Command SWTRG - software trigger */
    AltroCommand* fSWTRG;
    /** Command TRCLR - TRigger CLeaR. */
    AltroCommand* fTRCLR;
    /** Command ERCLR - ERror CLeaR. */
    AltroCommand* fERCLR;
    /** Register K1 - */
    AltroKLCoeffs*	fK1;
    /** Register K2 - */
    AltroKLCoeffs*	fK2;
    /** Register K3 - */
    AltroKLCoeffs*	fK3;
    /** Register L1 - */
    AltroKLCoeffs*	fL1;
    /** Register L2 - */
    AltroKLCoeffs*	fL2;
    /** Register L3 - */
    AltroKLCoeffs*	fL3;
    /** Register VFPED - Variable and fixed pedestal */
    AltroVFPED*		fVFPED;
    /** Register PMDTA - Pedestal memory data */
    AltroPMDTA*		fPMDTA;
    /** Register ZSTHR - Zero suppression mode and threshold */
    AltroZSTHR*		fZSTHR;
    /** Register BCTHR - Baseline correction mode and threshold */
    AltroBCTHR*		fBCTHR;
    /** Register TRCFG - Trigger configuration */
    AltroTRCFG*		fTRCFG;
    /** Register DPCFG - Data path configuration 1 */
    AltroDPCFG*		fDPCFG;
    /** Register DPCF2 - Data path configuration 2 */
    AltroDPCF2*		fDPCF2;
    /** Register PMADD - Pedestal memory address */
    AltroPMADD*		fPMADD;
    /** Register ERSTR - Error and status */
    AltroERSTR*		fERSTR;
    /** Register ADEVL - Address */
    AltroADEVL*		fADEVL;
    /** Register TRCNT - Trigger counter */
    AltroTRCNT*		fTRCNT;
  };
}
#endif
//____________________________________________________________________
//
// EOF
//
