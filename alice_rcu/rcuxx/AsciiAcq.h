// -*- mode: c++ -*-  
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    AsciiAcq.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 02:19:43 2006
    @brief   Declaration of ASCII data acquisition 
*/
#ifndef RCUXX_ASCIIACQ_H
#define RCUXX_ASCIIACQ_H
#ifndef RCUXX_ACQ_H
# include <rcuxx/Acq.h>
#endif
#include <iosfwd>

namespace Rcuxx
{
  /** @class AsciiAcq rcuxx/AsciiAcq.h <rcuxx/AsciiAcq.h>
      @brief Abstract base class to do data aquisition with ASCII file
      data output 
      @ingroup rcuxx_util
   */
  class AsciiAcq : public Acq
  {
  public:
    /** Constructor
	@param rcu  Reference to RCU object 
	@param out  Output directory name.  If left blank, an automatic
	name, based on the run number will be chosen. */
    AsciiAcq(Rcu& rcu, const char* out="") 
      : Acq(rcu, out)
    {}
    /** Run an acquisition
	@return 0 on success, error code otherwise */
    virtual unsigned int Run();
  protected:
    /** Hook called at the beginning of an event, after accepting a
	trigger.  This opens a new ascii file 
	@param evno The current event number */
    virtual int StartEvent(unsigned int evno);
    
    /** Hook called at end of an event.  This closes the current ascii
	file. 
	@param evno The current event number */
    virtual int EndEvent(unsigned int evno); 
    
    /** Process the data from one ALTRO channel.  The full data from
	one ALTRO channel is passed as the argument @a data.  The
	array has  @a size elements.   The elements are 10bit words
	formated as ALTRO data.  That is, it has the format 
	<table>
        <tr><td>S01</td><td>S02</td><td>S03</td><td>S04</td></tr>
        <tr><td>S05</td><td>T05</td><td>007</td><td>S05</td></tr>
        <tr><td>S06</td><td>S07</td><td>S08</td><td>T08</td></tr>
        <tr><td>006</td><td>S09</td><td>...</td><td>...</td></tr>
        <tr><td>...</td><td>...</td><td colspan=2>0x2aa</td></tr>
	<tr><td>0x2aaa</td><td>L</td><td>0xa</td><td>address</td></tr>
	</table>
	@param data The ALTRO formated data 
	@param size The size of the array @a data
	@return 0 on success, error-code otherwise */
    virtual int ProcessChannel(const std::vector<unsigned int>& data, 
			       unsigned int size);
    /** Service member function to close current output file */
    void CloseCurrent();
    /** Current output file, if any */
    std::ofstream* fFile;    
  };
}

#endif

  
