//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    rcuxx/Bc.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 02:20:16 2006
    @brief   Implementation of Board Controller interface 
*/
#ifndef RCUXX_BC_H
# include <rcuxx/Bc.h>
#endif
#ifndef __UNISTD_H
# include <unistd.h>
#endif
#ifndef __IOSTREAM__
# include <iostream>
#endif
#include "DebugGuard.h"

namespace 
{
  bool fDebug = false;
}
//====================================================================
const float Rcuxx::BcTEMP::fgkCounts2C  = 1/4.;
const float Rcuxx::BcAV::fgkCounts2mV = 4.43;
const float Rcuxx::BcAC::fgkCounts2mA = 17;
const float Rcuxx::BcDV::fgkCounts2mV = 4.43;
const float Rcuxx::BcDC::fgkCounts2mA = 30;

//====================================================================
Rcuxx::BcRegister::BcRegister(const char* name, unsigned int instr, 
			      RcuIMEM& imem, RcuRMEM& rmem, bool clear, 
			      bool submit, bool global, bool bcast) 
  : AltroRegister(name, instr, imem, rmem, clear, submit, global, bcast)
{}


//____________________________________________________________________
void
Rcuxx::BcRegister::SetBroadcast() 
{
  AltroRegister::SetBroadcast();
  fAddress += (1 << 17);
}

//____________________________________________________________________
void
Rcuxx::BcRegister::SetAddress(unsigned int board, unsigned int, unsigned int)
{
  AltroRegister::SetAddress(board, 0, 0);
  fAddress += (1 << 17);
}

//====================================================================
Rcuxx::BcCommand::BcCommand(const char* name, unsigned int cmd, RcuIMEM& imem, 
			    bool bcast)
  : AltroCommand(name, cmd, imem, bcast)
{
  fCmd += (1 << 17);
}

//====================================================================
void
Rcuxx::BcL1CNT::Print()  const
{
  AltroRegister::Print();
  std::cout << "\t# L1 triggers:\t\t\t" << fData << std::endl;
}

//====================================================================
void
Rcuxx::BcL2CNT::Print() const 
{
  AltroRegister::Print();
  std::cout << "\t# L2 triggers:\t\t\t" << fData << std::endl;
}

//====================================================================
void
Rcuxx::BcSCLKCNT::Print() const 
{
  AltroRegister::Print();
  std::cout << "\t# slow clocks:\t\t\t" << fData << std::endl;
}

//====================================================================
void
Rcuxx::BcDSTBCNT::Print() const 
{
  AltroRegister::Print();
  std::cout << "\t# data strobes:\t\t\t" << fData << std::endl;
}

//====================================================================
void
Rcuxx::BcTSMWORD::Print() const 
{
  AltroRegister::Print();
  std::cout << "\t# words:\t\t\t" << fData << std::endl;
}

//====================================================================
void
Rcuxx::BcUSRATIO::Print() const
{
  AltroRegister::Print();
  std::cout << "\tUnder sampling ratio:\t\t" << fData << std::endl;
}

//====================================================================
void
Rcuxx::BcCSR0::Get() 
{
  DebugGuard g(fDebug, "BcCSR0::Get [0x%x]", fData);
  fTempOverTh	= fData & (1 << 0);
  fAvOverTh	= fData & (1 << 1);
  fAcOverTh	= fData & (1 << 2);
  fDvOverTh	= fData & (1 << 3);
  fDcOverTh	= fData & (1 << 4);
  fPaps		= fData & (1 << 5);
  fAlps		= fData & (1 << 6);
  fMissedSclk	= fData & (1 << 7);
  fParity	= fData & (1 << 8);
  fInstruction	= fData & (1 << 9);
  fCnv		= fData & (1 << 10);
  DebugGuard::Message(fDebug, "TempOverTh: %s", (fTempOverTh ?  "yes" : "no"));
  DebugGuard::Message(fDebug, "AvOverTh:   %s", (fAvOverTh ?    "yes" : "no"));
  DebugGuard::Message(fDebug, "AcOverTh:   %s", (fAcOverTh ?    "yes" : "no"));
  DebugGuard::Message(fDebug, "DvOverTh:   %s", (fDvOverTh ?    "yes" : "no"));
  DebugGuard::Message(fDebug, "DcOverTh:   %s", (fDcOverTh ?    "yes" : "no"));
  DebugGuard::Message(fDebug, "Paps:       %s", (fPaps ?        "yes" : "no"));
  DebugGuard::Message(fDebug, "Alps:       %s", (fAlps ?        "yes" : "no"));
  DebugGuard::Message(fDebug, "MissedSclk: %s", (fMissedSclk ?  "yes" : "no"));
  DebugGuard::Message(fDebug, "Parity:     %s", (fParity ?      "yes" : "no"));
  DebugGuard::Message(fDebug, "Instruction:%s", (fInstruction ? "yes" : "no"));
  DebugGuard::Message(fDebug, "Cnv:        %s", (fCnv ?         "yes" : "no"));
}

//____________________________________________________________________
void
Rcuxx::BcCSR0::Set()
{
  DebugGuard g(fDebug, "BcCSR0::Set [0x%x]", fData);
  DebugGuard::Message(fDebug, "TempOverTh: %s", (fTempOverTh ?  "yes" : "no"));
  DebugGuard::Message(fDebug, "AvOverTh:   %s", (fAvOverTh ?    "yes" : "no"));
  DebugGuard::Message(fDebug, "AcOverTh:   %s", (fAcOverTh ?    "yes" : "no"));
  DebugGuard::Message(fDebug, "DvOverTh:   %s", (fDvOverTh ?    "yes" : "no"));
  DebugGuard::Message(fDebug, "DcOverTh:   %s", (fDcOverTh ?    "yes" : "no"));
  DebugGuard::Message(fDebug, "Paps:       %s", (fPaps ?        "yes" : "no"));
  DebugGuard::Message(fDebug, "Alps:       %s", (fAlps ?        "yes" : "no"));
  DebugGuard::Message(fDebug, "MissedSclk: %s", (fMissedSclk ?  "yes" : "no"));
  DebugGuard::Message(fDebug, "Parity:     %s", (fParity ?      "yes" : "no"));
  DebugGuard::Message(fDebug, "Instruction:%s", (fInstruction ? "yes" : "no"));
  DebugGuard::Message(fDebug, "Cnv:        %s", (fCnv ?         "yes" : "no"));
  fData =  0;
  fData += (fTempOverTh		? (1 << 0)	: 0);
  fData += (fAvOverTh		? (1 << 1)	: 0);
  fData += (fAcOverTh		? (1 << 2)	: 0);
  fData += (fDvOverTh		? (1 << 3)	: 0);
  fData += (fDcOverTh		? (1 << 4)	: 0);
  fData += (fPaps		? (1 << 5)	: 0);
  fData += (fAlps		? (1 << 6)	: 0);
  fData += (fMissedSclk		? (1 << 7)	: 0);
  fData += (fParity		? (1 << 8)	: 0);
  fData += (fInstruction	? (1 << 9)	: 0);
  fData += (fCnv		? (1 << 10)	: 0);
  DebugGuard::Message(fDebug, "Data:       0x%x", fData);
}

//____________________________________________________________________
void
Rcuxx::BcCSR0::Print() const
{
  AltroRegister::Print();
  std::cout << std::boolalpha 
	    << "\tContinous conversion:\t\t" << fCnv << "\n"
	    << "\tInterrupts enabled:" << std::endl;
  if (fParity)		std::cout << "\t\tParity error\n";
  if (fInstruction)	std::cout << "\t\tInstruction error\n";
  if (fMissedSclk)	std::cout << "\t\tMissed sample clocks\n";
  if (fAlps)		std::cout << "\t\tALTRO power supply error\n";
  if (fPaps)		std::cout << "\t\tPASA power supply error\n";
  if (fDcOverTh)	std::cout << "\t\tDigital current over threshold\n";
  if (fDvOverTh)	std::cout << "\t\tDigital voltage over threshold\n";
  if (fAcOverTh)	std::cout << "\t\tAnalog current over threshold\n";
  if (fAvOverTh)	std::cout << "\t\tAnalog voltage over threshold\n";
  if (fTempOverTh)	std::cout << "\t\tTemperture over threshold\n";
  std::cout << std::flush;
}


  
//====================================================================
void
Rcuxx::BcCSR1::Get() 
{
  fTempOverTh	= fData & (1 << 0);
  fAvOverTh	= fData & (1 << 1);
  fAcOverTh	= fData & (1 << 2);
  fDvOverTh	= fData & (1 << 3);
  fDcOverTh	= fData & (1 << 4);
  fPaps		= fData & (1 << 5);
  fAlps		= fData & (1 << 6);
  fMissedSclk	= fData & (1 << 7);
  fParity	= fData & (1 << 8);
  fInstruction	= fData & (1 << 9);
  fAltroError	= fData & (1 << 10);
  fSlowControl	= fData & (1 << 11);
  fError	= fData & (1 << 12);
  fInterrupt	= fData & (1 << 13);
}

//____________________________________________________________________
void 
Rcuxx::BcCSR1::Print() const
{
  AltroRegister::Print();
  std::cout << std::boolalpha 
	    << "\tInstruction error:\t\t" << fAltroError << "\n" 
	    << "\tSlow control error:\t\t" << fSlowControl << "\n" 
	    << "\tError:\t\t\t\t" << fError << std::endl;
  if (fParity)		std::cout << "\t\tParity error\n";
  if (fInstruction)	std::cout << "\t\tInstruction error\n";
  if (fMissedSclk)	std::cout << "\t\tMissed sample clocks\n";
  if (fAlps)		std::cout << "\t\tALTRO power supply error\n";
  if (fPaps)		std::cout << "\t\tPASA power supply error\n";
  std::cout << "\tInterrupt:\t\t\t" << fInterrupt << std::endl;
  if (fDcOverTh)	std::cout << "\t\tDigital current over threshold\n";
  if (fDvOverTh)	std::cout << "\t\tDigital voltage over threshold\n";
  if (fAcOverTh)	std::cout << "\t\tAnalog current over threshold\n";
  if (fAvOverTh)	std::cout << "\t\tAnalog voltage over threshold\n";
  if (fTempOverTh)	std::cout << "\t\tTemperture over threshold\n";
  std::cout << std::flush;
}

//====================================================================
void 
Rcuxx::BcCSR2::Get() 
{
  fALTROSwitch	= (fData & (1 << 0));
  fPASASwitch	= (fData & (1 << 1));
  fAdcClock	= (fData & (1 << 2));
  fRdoClock	= (fData & (1 << 3));
  fADCAddress	= (fData >> 4) & 0x3;
  fALTROAddress	= (fData >> 6) & 0x7;
  fContinousTSM	= (fData & (1 << 9));
  fCardIsolated	= (fData & (1 << 10));
  fHADD		= (fData >> 11) & 0x1f;
}
//____________________________________________________________________
void 
Rcuxx::BcCSR2::Set() 
{
  fData = 0;
  fData += (fALTROSwitch  ? (1 << 0)	: 0);
  fData += (fPASASwitch	  ? (1 << 1)	: 0);
  fData += (fAdcClock	  ? (1 << 2)	: 0);
  fData += (fRdoClock	  ? (1 << 3)	: 0);
  fData += (fADCAddress   & 0x3) << 4;
  fData += (fALTROAddress & 0x7) << 6;
  fData += (fContinousTSM ? (1 << 9)	: 0);
  fData += (fCardIsolated ? (1 << 10)	: 0);
}  
//____________________________________________________________________
void 
Rcuxx::BcCSR2::Print() const
{
  AltroRegister::Print();
  std::cout << std::boolalpha 
	    << "\tHardware address:\t\t0x" << std::hex << fHADD << "\n" 
	    << "\tIsolated:\t\t\t" << fCardIsolated << "\n" 
	    << "\tTest mode:\t\t\t" << fContinousTSM << "\n" 
	    << "\tALTRO address:\t\t\t0x" << fADCAddress << std::dec << "\n" 
	    << "\tADC clock enabled:\t\t" << fAdcClock << "\n" 
	    << "\tRead-out clock enabled:\t\t" << fRdoClock << "\n" 
	    << "\tPASA enabled:\t\t\t" << fPASASwitch << "\n" 
	    << "\tALTRO enabled:\t\t\t" << fALTROSwitch << std::endl; 
}

//====================================================================
void  
Rcuxx::BcCSR3::Get() 
{
  fWatchDog	 = (fData >> 8) & 0x7f;
  fWarnRatio 	 = fData & 0xff;
  fCnvEnd	 = (fData & (1 << 15));
}
//____________________________________________________________________
void 
Rcuxx::BcCSR3::Set() 
{
  fData = (((fWatchDog & 0x7f) << 8) 
	   + (fWarnRatio & 0xff) 
	   +(fCnvEnd ? (1 << 15) : 0));
}
//____________________________________________________________________
void 
Rcuxx::BcCSR3::Print() const
{
  AltroRegister::Print();
  std::cout << std::boolalpha 
	    << "\tWatch dog:\t\t\t0x" << std::hex << fWatchDog << "\n"
	    << "\tWarning ratio:\t\t\t" << std::dec << fWarnRatio << "\n" 
	    << "\tConversion ended:\t\t" << fCnvEnd << std::endl;
}

//____________________________________________________________________
void 
Rcuxx::BcTEMP::Print() const
{
  AltroRegister::Print();
  std::cout << "\tTemperature:\t\t\t" << Centigrade() << "C" << std::endl;
}
//____________________________________________________________________
void 
Rcuxx::BcAV::Print() const
{
  AltroRegister::Print();
  std::cout << "\tVoltage:\t\t\t" << MiliVolts() << "mV" << std::endl;
}
//____________________________________________________________________
void 
Rcuxx::BcAC::Print() const
{
  AltroRegister::Print();
  std::cout << "\tCurrent:\t\t\t" << MiliAmps() << "mA" << std::endl;
}
//____________________________________________________________________
void 
Rcuxx::BcDV::Print() const
{
  AltroRegister::Print();
  std::cout << "\tVoltage:\t\t\t" << MiliVolts() << "mV" << std::endl;
}
//____________________________________________________________________
void 
Rcuxx::BcDC::Print() const
{
  AltroRegister::Print();
  std::cout << "\tCurrent:\t\t\t" << MiliAmps() << "mA" << std::endl;
}

//____________________________________________________________________
void 
Rcuxx::BcTEMP_TH::Print() const
{
  AltroRegister::Print();
  std::cout << "\tTemperature:\t\t\t" << Centigrade() << "C" << std::endl;
}
//____________________________________________________________________
void 
Rcuxx::BcAV_TH::Print() const
{
  AltroRegister::Print();
  std::cout << "\tVoltage:\t\t\t" << MiliVolts() << "mV" << std::endl;
}
//____________________________________________________________________
void 
Rcuxx::BcAC_TH::Print() const
{
  AltroRegister::Print();
  std::cout << "\tCurrent:\t\t\t" << MiliAmps() << "mA" << std::endl;
}
//____________________________________________________________________
void 
Rcuxx::BcDV_TH::Print() const
{
  AltroRegister::Print();
  std::cout << "\tVoltage:\t\t\t" << MiliVolts() << "mV" << std::endl;
}
//____________________________________________________________________
void 
Rcuxx::BcDC_TH::Print() const
{
  AltroRegister::Print();
  std::cout << "\tCurrent:\t\t\t" << MiliAmps() << "mA" << std::endl;
}

//====================================================================
Rcuxx::BcMonitored::BcMonitored(const char* name, int addr, 
				RcuIMEM& imem, RcuRMEM& rmem, 
				BcCSR3& csr3, AltroCommand& start) 
  : BcRegister(name, addr, imem, rmem, false, false), 
    fStart(start), 
    fCsr3(csr3)
{}

//____________________________________________________________________
bool
Rcuxx::BcMonitored::Monitor(int tries) 
{
  fStart.Commit();
  int i = 0;
  for (i = 0; i < tries; i++) {
    usleep(1);
    fCsr3.Update();
    if (fCsr3.IsCnvEnd()) break;
  }
  if (i == tries) return false;
  Update();
  return true;
}

//====================================================================
Rcuxx::Bc::Bc(Rcu& rcu) 
{
  // 
  fCNTLAT   = new BcCommand("Latch counters", 0x16, *(rcu.IMEM()));
  fCNTCLR   = new BcCommand("Clear counters", 0x17, *(rcu.IMEM()));
  fCSR1CLR  = new BcCommand("Clear errors/intr.", 0x18, *(rcu.IMEM()));
  fALRST    = new BcCommand("Reset ALTRO's", 0x19, *(rcu.IMEM()));
  fBCRST    = new BcCommand("Reset", 0x1A, *(rcu.IMEM()));
  fSTCNV    = new BcCommand("Start conversion", 0x1B, *(rcu.IMEM()));
  fSCEVL    = new BcCommand("Scan event length", 0x1C, *(rcu.IMEM()));
  fEVLRDO   = new BcCommand("Read event length", 0x1D, *(rcu.IMEM()), false);
  fSTTSM    = new BcCommand("Start test mode", 0x1E, *(rcu.IMEM()));
  fACQRDO   = new BcCommand("Read acquisition memory",0x1F, *(rcu.IMEM()), 
			    false);

  fCSR0     = new BcCSR0(*(rcu.IMEM()), *(rcu.RMEM()));
  fCSR1     = new BcCSR1(*(rcu.IMEM()), *(rcu.RMEM()), fCSR1CLR);
  fCSR2     = new BcCSR2(*(rcu.IMEM()), *(rcu.RMEM()));
  fCSR3     = new BcCSR3(*(rcu.IMEM()), *(rcu.RMEM()));

  
  fTEMP_TH  = new BcTEMP_TH(*(rcu.IMEM()), *(rcu.RMEM()));
  fAV_TH    = new BcAV_TH(*(rcu.IMEM()), *(rcu.RMEM()));
  fAC_TH    = new BcAC_TH(*(rcu.IMEM()), *(rcu.RMEM()));
  fDV_TH    = new BcDV_TH(*(rcu.IMEM()), *(rcu.RMEM()));
  fDC_TH    = new BcDC_TH(*(rcu.IMEM()), *(rcu.RMEM()));

  fTEMP     = new BcTEMP(*(rcu.IMEM()), *(rcu.RMEM()), *fCSR3, *fSTCNV);
  fAV       = new BcAV(*(rcu.IMEM()), *(rcu.RMEM()), *fCSR3, *fSTCNV);
  fAC       = new BcAC(*(rcu.IMEM()), *(rcu.RMEM()), *fCSR3, *fSTCNV);
  fDV       = new BcDV(*(rcu.IMEM()), *(rcu.RMEM()), *fCSR3, *fSTCNV);
  fDC       = new BcDC(*(rcu.IMEM()), *(rcu.RMEM()), *fCSR3, *fSTCNV);

  fL1CNT    = new BcL1CNT(*(rcu.IMEM()), *(rcu.RMEM()));
  fL2CNT    = new BcL2CNT(*(rcu.IMEM()), *(rcu.RMEM()));
  fSCLKCNT  = new BcSCLKCNT(*(rcu.IMEM()), *(rcu.RMEM()));
  fDSTBCNT  = new BcDSTBCNT(*(rcu.IMEM()), *(rcu.RMEM()));
  fTSMWORD  = new BcTSMWORD(*(rcu.IMEM()), *(rcu.RMEM()));
  fUSRATIO  = new BcUSRATIO(*(rcu.IMEM()), *(rcu.RMEM()));

  SetAddress(0);
}

//____________________________________________________________________
void 
Rcuxx::Bc::SetBroadcast() 
{
  fCNTLAT->SetBroadcast();
  fALRST->SetBroadcast();
  fBCRST->SetBroadcast();
  fSTCNV->SetBroadcast();
  fSCEVL->SetBroadcast();
  fEVLRDO->SetBroadcast();
  fSTTSM->SetBroadcast();
  fACQRDO->SetBroadcast();
  
  // 
  fTEMP_TH->SetBroadcast();
  fAV_TH->SetBroadcast();
  fAC_TH->SetBroadcast();
  fDV_TH->SetBroadcast();
  fDC_TH->SetBroadcast();
  fTEMP->SetBroadcast();
  fAV->SetBroadcast();
  fAC->SetBroadcast();
  fDV->SetBroadcast();
  fDC->SetBroadcast();
  fL1CNT->SetBroadcast();
  fL2CNT->SetBroadcast();
  fSCLKCNT->SetBroadcast();
  fDSTBCNT->SetBroadcast();
  fTSMWORD->SetBroadcast();
  fUSRATIO->SetBroadcast();
  fCSR0->SetBroadcast();
  fCSR1->SetBroadcast();
  fCSR2->SetBroadcast();
  fCSR3->SetBroadcast();
}

//____________________________________________________________________
void
Rcuxx::Bc::SetAddress(unsigned int board) 
{
  fCNTLAT->SetAddress(board, 0, 0);
  fALRST->SetAddress(board, 0, 0);
  fBCRST->SetAddress(board, 0, 0);
  fSTCNV->SetAddress(board, 0, 0);
  fSCEVL->SetAddress(board, 0, 0);
  fEVLRDO->SetAddress(board, 0, 0);
  fSTTSM->SetAddress(board, 0, 0);
  fACQRDO->SetAddress(board, 0, 0);
  
  // 
  fTEMP_TH->SetAddress(board, 0, 0);
  fAV_TH->SetAddress(board, 0, 0);
  fAC_TH->SetAddress(board, 0, 0);
  fDV_TH->SetAddress(board, 0, 0);
  fDC_TH->SetAddress(board, 0, 0);
  fTEMP->SetAddress(board, 0, 0);
  fAV->SetAddress(board, 0, 0);
  fAC->SetAddress(board, 0, 0);
  fDV->SetAddress(board, 0, 0);
  fDC->SetAddress(board, 0, 0);
  fL1CNT->SetAddress(board, 0, 0);
  fL2CNT->SetAddress(board, 0, 0);
  fSCLKCNT->SetAddress(board, 0, 0);
  fDSTBCNT->SetAddress(board, 0, 0);
  fTSMWORD->SetAddress(board, 0, 0);
  fUSRATIO->SetAddress(board, 0, 0);
  fCSR0->SetAddress(board, 0, 0);
  fCSR1->SetAddress(board, 0, 0);
  fCSR2->SetAddress(board, 0, 0);
  fCSR3->SetAddress(board, 0, 0);
}

//____________________________________________________________________
unsigned int
Rcuxx::Bc::Update() 
{
  unsigned int ret;
  if ((ret = fTEMP_TH->Update())) 	return ret;
  if ((ret = fAV_TH->Update())) 	return ret;
  if ((ret = fAC_TH->Update())) 	return ret;
  if ((ret = fDV_TH->Update())) 	return ret;
  if ((ret = fDC_TH->Update())) 	return ret;
  if ((ret = fTEMP->Update())) 		return ret;
  if ((ret = fAV->Update())) 		return ret;
  if ((ret = fAC->Update())) 		return ret;
  if ((ret = fDV->Update())) 		return ret;
  if ((ret = fDC->Update())) 		return ret;
  if ((ret = fL1CNT->Update())) 	return ret;
  if ((ret = fL2CNT->Update())) 	return ret;
  if ((ret = fSCLKCNT->Update())) 	return ret;
  if ((ret = fDSTBCNT->Update())) 	return ret;
  if ((ret = fTSMWORD->Update())) 	return ret;
  if ((ret = fUSRATIO->Update())) 	return ret;
  if ((ret = fCSR0->Update())) 		return ret;
  if ((ret = fCSR1->Update())) 		return ret;
  if ((ret = fCSR2->Update())) 		return ret;
  if ((ret = fCSR3->Update())) 		return ret;
  return ret;
}

//____________________________________________________________________
void
Rcuxx::Bc::Print() const
{
  fTEMP_TH->Print();
  fAV_TH->Print();
  fAC_TH->Print();
  fDV_TH->Print();
  fDC_TH->Print();
  fTEMP->Print();
  fAV->Print();
  fAC->Print();
  fDV->Print();
  fDC->Print();
  fL1CNT->Print();
  fL2CNT->Print();
  fSCLKCNT->Print();
  fDSTBCNT->Print();
  fTSMWORD->Print();
  fUSRATIO->Print();
  fCSR0->Print();
  fCSR1->Print();
  fCSR2->Print();
  fCSR3->Print();
}

//____________________________________________________________________
void
Rcuxx::Bc::SetDebug(bool debug)
{
  fDebug = debug;
}

//____________________________________________________________________
//
// EOF
//
