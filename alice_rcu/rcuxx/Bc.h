// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    rcuxx/Bc.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 02:20:35 2006
    @brief   Declaration of Board Controller interface 
*/
#ifndef RCUXX_BC_H
#define RCUXX_BC_H
#ifndef RCUXX_ALTRO_H
# include <rcuxx/Altro.h>
#endif

namespace Rcuxx 
{  
  /** @defgroup rcuxx_bc Board Controller classes 
      @ingroup rcuxx 
   */
  //====================================================================
  /** @struct BcRegister rcuxx/Bc.h <rcuxx/Bc.h>
   *  @brief Base class for interfaces to Board Controller registers
   *  @ingroup rcuxx_bc
   */
  struct BcRegister : public AltroRegister 
  {
    /** Constructor 
	@param name    Register name 
	@param instr   Instruction number
	@param imem    Reference to Instruction memory
	@param rmem    Reference to Result memory
	@param clear   Whether the register is clear-able
	@param submit  Whether the register is write-able
	@param global  Whether the register is global or per channel
	@param bcast   Whether this register can be broadcasted to. */
    BcRegister(const char* name, unsigned int instr, 
	       RcuIMEM& imem, RcuRMEM& rmem, bool clear, bool submit, 
	       bool global=false, bool bcast=true);
    /** Set to broadcast mode */
    void SetBroadcast();
    /** Set Address
	@param board  Board number 
	@param chip   Chip number (not used)
	@param channel Channel number (not uses). */
    void SetAddress(unsigned int board, unsigned int chip=0, 
		    unsigned int channel=0);
  };

  //====================================================================
  /** @struct BcCommand rcuxx/Bc.h <rcuxx/Bc.h>
   *  @brief Base class for interfaces to Board Controller commands
   *  @ingroup rcuxx_bc
   */
  struct BcCommand : public AltroCommand
  {
    /** Constructor 
	@param name Name of command 
	@param cmd Command number 
	@param imem Reference to instruction memory 
	@param bcast Whether this command can be broadcasted */
    BcCommand(const char* name, unsigned int cmd, RcuIMEM& imem, 
	      bool bcast=true);
  };


  //====================================================================
  /** @struct BcL1CNT rcuxx/Bc.h <rcuxx/Bc.h>
   *  @brief Interface to Board Controller register L1CNT - Number of
   *  L1 Triggers Received 
   *  @ingroup rcuxx_bc
   */
  struct BcL1CNT : public BcRegister
  {
    /** Constructor 
	@param imem Reference to instruction memory 
	@param rmem Reference to result memory */
    BcL1CNT(RcuIMEM& imem, RcuRMEM& rmem) 
      : BcRegister("Level 1 counter", 0xB, imem, rmem, false, false)
    {}
    /** Decode read data */
    void Get() { fData &= 0xffff; }
    /** @return counts */
    unsigned int Counts() const { return fData; }
    /** Print contents of this register */
    virtual void Print() const;
  };

  //====================================================================
  /** @struct BcL2CNT rcuxx/Bc.h <rcuxx/Bc.h>
   *  @brief Interface to Board Controller register L2CNT - Number of
   *  L2 Triggers Received
   *  @ingroup rcuxx_bc
   */
  struct BcL2CNT : public BcRegister
  {
    /** Constructor 
	@param imem Reference to instruction memory 
	@param rmem Reference to result memory */
    BcL2CNT(RcuIMEM& imem, RcuRMEM& rmem) 
      : BcRegister("Level 2 counter", 0xC, imem, rmem, false, false)
    {}
    /** Decode read data */
    void Get() { fData &= 0xffff; }
    /** @return counts */
    unsigned int Counts() const { return fData; }
    /** Print contents of this register */
    virtual void Print() const;
  };

  //====================================================================
  /** @struct BcSCLKCNT rcuxx/Bc.h <rcuxx/Bc.h>
   *  @brief Interface to Board Controller register SCLKCNT - Number
   *  of SCLK (sample clocks).
   *  @ingroup rcuxx_bc
   */
  struct BcSCLKCNT : public BcRegister
  {
    /** Constructor 
	@param imem Reference to instruction memory 
	@param rmem Reference to result memory */
    BcSCLKCNT(RcuIMEM& imem, RcuRMEM& rmem) 
      : BcRegister("Sample clock counter", 0xD, imem, rmem, false, false)
    {}
    /** Decode read data */
    void Get() { fData &= 0xffff; }
    /** @return counts */
    unsigned int Counts() const { return fData; }
    /** Print contents of this register */
    virtual void Print() const;
  };

  //====================================================================
  /** @struct BcDSTBCNT rcuxx/Bc.h <rcuxx/Bc.h>
   *  @brief Interface to Board Controller register DSTBCNT - Number
   *  of DSTB (Data STroBes) Asserted 
   *  @ingroup rcuxx_bc
   */
  struct BcDSTBCNT : public BcRegister
  {
    /** Constructor 
	@param imem Reference to instruction memory 
	@param rmem Reference to result memory */
    BcDSTBCNT(RcuIMEM& imem, RcuRMEM& rmem) 
      : BcRegister("Data strobe counter", 0xE, imem, rmem, false, false)
    {}
    /** Decode read data */
    void Get() { fData &= 0xff; }
    /** @return counts */
    unsigned int Counts() const { return fData; }
    /** Print contents of this register */
    virtual void Print() const;
  };

  //====================================================================
  /** @struct BcTSMWORD rcuxx/Bc.h <rcuxx/Bc.h>
   *  @brief Interface to Board Controller register TSMWORD - Number
   *  of words to store/read in test mode.  
   *  @ingroup rcuxx_bc
   */
  struct BcTSMWORD : public BcRegister
  {
    /** Constructor 
	@param imem Reference to instruction memory 
	@param rmem Reference to result memory */
    BcTSMWORD(RcuIMEM& imem, RcuRMEM& rmem) 
      : BcRegister("Test mode words", 0xF, imem, rmem, false, true)
    {}
    /** Decode read data */
    void Get() { fData &= 0x1ff; }
    /** Encode set data before committing */
    void Set() { fData &= 0x1ff; }
    /** @return number */
    unsigned int Number() const { return fData; }
    /** @return number */
    void SetNumber(unsigned int val) { fData = val; }
    /** Print contents of this register */
    virtual void Print() const;
  };

  //====================================================================
  /** @struct BcUSRATIO rcuxx/Bc.h <rcuxx/Bc.h>
   *  @brief Interface to Board Controller register USRATIO - Under
   *  sampling factor 
   *  @ingroup rcuxx_bc
   */
  struct BcUSRATIO : public BcRegister
  {
    /** Constructor 
	@param imem Reference to instruction memory 
	@param rmem Reference to result memory */
    BcUSRATIO(RcuIMEM& imem, RcuRMEM& rmem) 
      : BcRegister("Under sampling rato", 0x10, imem, rmem, false, true)
    {}
    /** Decode read data */
    void Get() { fData &= 0xffff; }
    /** Encode set data before committing */
    void Set() { fData &= 0xffff; }
    /** @return undersamplin ratio */
    unsigned int Ratio() const { return fData; }
    /** undersampling ratio */
    void SetRatio(unsigned int val) { fData = val; }
    /** Print contents of this register */
    virtual void Print() const;
  };

  //====================================================================
  /** @struct BcCSR0 rcuxx/Bc.h <rcuxx/Bc.h>
   *  @brief Interface to Board Controller register CSR0. 
   *  CSR0 controls the interrupt and error mask - that is, the
   *  interrupts and errors from the BC that signals an error to the
   *  RCU.   Also, the 10th bit or CSR0 controls whether to do
   *  continuous monitoring or voltages, currents, and temperature. 
   *  @ingroup rcuxx_bc
   */
  struct BcCSR0 : public BcRegister
  {
    /** Constructor 
	@param imem Reference to instruction memory 
	@param rmem Reference to result memory */
    BcCSR0(RcuIMEM& imem, RcuRMEM& rmem) 
      : BcRegister("Error/Interrupt mask", 0x11, imem, rmem, false, true)
    {}
    /** Decode read data */
    void Get();
    /** Encode set data before committing */
    void Set();
    /** @return If parity error interrupts are enabled */
    bool IsParity() const { return fParity; } 
    /** Enable/dispable parity error interrupts */
    void SetParity(bool on=true) { fParity = on; }
    /** @return If instruction error interrupts are enabled */
    bool IsInstruction() const { return fInstruction; } 
    /** Enable/dispable instruction error interrupts */
    void SetInstruction(bool on=true) { fInstruction = on; }
    /** @return If missed sample clocks interrupts are enabled */
    bool IsMissedSclk() const { return fMissedSclk; } 
    /** Enable/dispable missed sample clocks interrupts */
    void SetMissedSclk(bool on=true) { fMissedSclk = on; }
    /** @return If ALTRO power supply error interrupts are enabled */
    bool IsAlps() const { return fAlps; } 
    /** Enable/dispable ALTRO power supply error interrupts */
    void SetAlps(bool on=true) { fAlps = on; }
    /** @return If PASA power supply error interrupts are enabled */
    bool IsPaps() const { return fPaps; } 
    /** Enable/dispable PASA power supply error interrupts */
    void SetPaps(bool on=true) { fPaps = on; }
    /** @return If digital current over threshold interrupts are enabled */
    bool IsDcOverTh() const { return fDcOverTh; } 
    /** Enable/dispable digital current over threshold interrupts */
    void SetDcOverTh(bool on=true) { fDcOverTh = on; }
    /** @return If digital voltage over threshold interrupts are enabled */
    bool IsDvOverTh() const { return fDvOverTh; } 
    /** Enable/dispable digital voltage over threshold interrupts */
    void SetDvOverTh(bool on=true) { fDvOverTh = on; }
    /** @return If analog current over threshold interrupts are enabled */
    bool IsAcOverTh() const { return fAcOverTh; } 
    /** Enable/dispable analog current over threshold interrupts */
    void SetAcOverTh(bool on=true) { fAcOverTh = on; }
    /** @return If analog voltage over threshold interrupts are enabled */
    bool IsAvOverTh() const { return fAvOverTh; } 
    /** Enable/dispable analog voltage over threshold interrupts */
    void SetAvOverTh(bool on=true) { fAvOverTh = on; }
    /** @return If temperture over threshold interrupts are enabled */
    bool IsTempOverTh() const { return fTempOverTh; } 
    /** Enable/dispable temperture over threshold interrupts */
    void SetTempOverTh(bool on=true) { fTempOverTh = on; }
    /** @return If continues ADC conversion is enabled */
    bool IsCnv() const { return fCnv; } 
    /** Enable/dispable continues ADC conversion  */
    void SetCnv(bool on=true) { fCnv = on; }
    /** Print contents of this register */
    virtual void Print() const;
  protected:
    /** Parity - Parity error */
    bool fParity;
    /** Instr - Instruction error */
    bool fInstruction;
    /** SCLK - Missed sample clocks */
    bool fMissedSclk;
    /** Alps - ALTRO power supply error */
    bool fAlps;
    /** Paps - PASA power supply error */
    bool fPaps;
    /** DC - Digital current over threshold */
    bool fDcOverTh;
    /** DV - Digital voltage over threshold */
    bool fDvOverTh;
    /** AC - Analog current over threshold */
    bool fAcOverTh;
    /** AV - Analog voltage over threshold */
    bool fAvOverTh;
    /** T - Temperture over threshold */
    bool fTempOverTh;
    /** Continues ADC conversion */
    bool fCnv;
  };

  //====================================================================
  /** @struct BcCSR1 rcuxx/Bc.h <rcuxx/Bc.h>
   *  @brief Interface to Board Controller register CSR1
   *  CSR1 holds the actual interrupts and errors as well as two bits
   *  signaling if an interrupt and/or error was seen. 
   *  @ingroup rcuxx_bc
   */
  struct BcCSR1 : public BcRegister
  {
    /** Constructor 
	@param imem Reference to instruction memory 
	@param rmem Reference to result memory 
	@param clear Clear registers */
    BcCSR1(RcuIMEM& imem, RcuRMEM& rmem, AltroCommand* clear) 
      : BcRegister("Errors/Interrupts", 0x12, imem, rmem, true, false), 
	fClear(clear)
    {}
    /** Decode read data */
    void Get();
    unsigned int Clear() {  return fClear->Commit(); }
    /** @return If a parity error interrupt is set */
    bool IsParity() const { return fParity; } 
    /** @return If a instruction error interrupt is set */
    bool IsInstruction() const { return fInstruction; } 
    /** @return If a missed sample clocks interrupt is set */
    bool IsMissedSclk() const { return fMissedSclk; } 
    /** @return If a ALTRO power supply error interrupt is set */
    bool IsAlps() const { return fAlps; } 
    /** @return If a PASA power supply error interrupt is set */
    bool IsPaps() const { return fPaps; } 
    /** @return If a digital current over threshold interrupt is set */
    bool IsDcOverTh() const { return fDcOverTh; } 
    /** @return If a digital voltage over threshold interrupt is set */
    bool IsDvOverTh() const { return fDvOverTh; } 
    /** @return If a analog current over threshold interrupt is set */
    bool IsAcOverTh() const { return fAcOverTh; } 
    /** @return If a analog voltage over threshold interrupt is set */
    bool IsAvOverTh() const { return fAvOverTh; } 
    /** @return If a temperture over threshold interrupt is set */
    bool IsTempOverTh() const { return fTempOverTh; } 
    /** @return If a ALTRO instruction error  interrupt is set */
    bool IsAltroError() const { return fAltroError; } 
    /** @return If a slow control error  interrupt is set */
    bool IsSlowControl() const { return fSlowControl; } 
    /** @return If an error is set */
    bool IsError() const { return fError; } 
    /** @return If an interrupt is set */
    bool IsInterrupt() const { return fInterrupt; } 
    /** Print contents of this register */
    virtual void Print() const;
  protected:    
    /** Parity - Parity error */
    bool fParity;
    /** Instr - Instruction error */
    bool fInstruction;
    /** SCLK - Missed sample clocks */
    bool fMissedSclk;
    /** Alps - ALTRO power supply error */
    bool fAlps;
    /** Paps - PASA power supply error */
    bool fPaps;
    /** DC - Digital current over threshold */
    bool fDcOverTh;
    /** DV - Digital voltage over threshold */
    bool fDvOverTh;
    /** AC - Analog current over threshold */
    bool fAcOverTh;
    /** AV - Analog voltage over threshold */
    bool fAvOverTh;
    /** T - Temperture over threshold */
    bool fTempOverTh;
    /** ALTRO instruction error  */
    bool fAltroError;
    /** Slow control error  */
    bool fSlowControl;
    /** Have an error  */
    bool fError;
    /** Have an interrupt */
    bool fInterrupt;
    /** Clear command */ 
    AltroCommand* fClear;
  };

  //____________________________________________________________________
  /** @struct BcCSR2 rcuxx/Bc.h <rcuxx/Bc.h>
   *  @brief Interface to Board Controller register CSR2 - test mode,
   *  clock enables, and power supply enables. 
   *  @ingroup rcuxx_bc
   */
  struct BcCSR2 : public BcRegister 
  {  
    /** Constructor 
	@param imem Reference to instruction memory 
	@param rmem Reference to result memory */
    BcCSR2(RcuIMEM& imem, RcuRMEM& rmem) 
      : BcRegister("Test and enables", 0x13, imem, rmem, false, true)
    {}
    /** Decode read data */
    void Get();
    /** Encode set data before committing */
    void Set();
    /** @return HADD */
    unsigned HADD() const { return fHADD; }
    /** @return if CardIsolated */
    bool IsCardIsolated() const { return fCardIsolated; }
    /** Set CardIsolated on or off */
    void SetCardIsolated(bool on=true) { fCardIsolated = on; }
    /** @return if ContinousTSM */
    bool IsContinousTSM() const { return fContinousTSM; }
    /** Set ContinousTSM on or off */
    void SetContinousTSM(bool on=true) { fContinousTSM = on; }
    /** @return ALTROAddress */
    unsigned ALTROAddress() const { return fALTROAddress; }
    /** Set ALTROAddress to @a val */
    void SetALTROAddress(unsigned int val) { fALTROAddress = val; }
    /** @return ADCAddress */
    unsigned ADCAddress() const { return fADCAddress; }
    /** Set ADCAddress to @a val */
    void SetADCAddress(unsigned int val) { fADCAddress = val; }
    /** @return if AdcClock */
    bool IsAdcClock() const { return fAdcClock; }
    /** Set AdcClock on or off */
    void SetAdcClock(bool on=true) { fAdcClock = on; }
    /** @return if RdoClock */
    bool IsRdoClock() const { return fRdoClock; }
    /** Set RdoClock on or off */
    void SetRdoClock(bool on=true) { fRdoClock = on; }
    /** @return if PASASwitch */
    bool IsPASASwitch() const { return fPASASwitch; }
    /** Set PASASwitch on or off */
    void SetPASASwitch(bool on=true) { fPASASwitch = on; }
    /** @return if ALTROSwitch */
    bool IsALTROSwitch() const { return fALTROSwitch; }
    /** Set ALTROSwitch on or off */
    void SetALTROSwitch(bool on=true) { fALTROSwitch = on; }
    /** Print contents of this register */
    virtual void Print() const;
  protected:
    /** Hard-wired address */
    unsigned int  fHADD;
    /** Whether card is isolated in test mode */
    bool          fCardIsolated;
    /** Whether to do continues tests */
    bool          fContinousTSM;
    /** Altro to read */
    unsigned int  fALTROAddress;
    /** ADC channel to read  */
    unsigned int  fADCAddress;
    /** Enable ADC (monitor) clock */
    bool          fAdcClock;
    /** Enable Read-out clock */
    bool          fRdoClock;
    /** Enable PASA power supply */
    bool          fPASASwitch;
    /** Enable ALTRO power supply */
    bool          fALTROSwitch;
  };

  //====================================================================
  /** @struct BcCSR3 rcuxx/Bc.h <rcuxx/Bc.h>
   *  @brief Interface to Board Controller register CSR3 - watch dog
   *  and missed clock warning ratios. 
   *  @ingroup rcuxx_bc
   */
  struct BcCSR3 : public BcRegister 
  {  
    /** Constructor 
	@param imem Reference to instruction memory 
	@param rmem Reference to result memory */
    BcCSR3(RcuIMEM& imem, RcuRMEM& rmem) 
      : BcRegister("Watch dog", 0x14, imem, rmem, false, true)
    {}
    /** Decode read data */
    void Get();
    /** Encode set data before committing */
    void Set();
    /** @return WarnRatio */
    unsigned WarnRatio() const { return fWarnRatio; }
    /** Set WarnRatio to @a val */
    void SetWarnRatio(unsigned int val) { fWarnRatio = val; }
    /** @return WatchDog */
    unsigned WatchDog() const { return fWatchDog; }
    /** Set WatchDog to @a val */
    void SetWatchDog(unsigned int val) { fWatchDog = val; }
    /** @return if conversion is ended */
    bool IsCnvEnd() const { return fCnvEnd; }
    /** Print contents of this register */
    virtual void Print() const;
  protected:
    /** Warning ratio*/
    unsigned int fWarnRatio;
    /** Watch dog */
    unsigned int fWatchDog;
    /** Whether conversion has ended */
    bool         fCnvEnd;
  };

  //====================================================================
  /** @struct BcMonitored rcuxx/Bc.h <rcuxx/Bc.h>
   *  @brief Base class for interfaces to Board Controller monitor
   *  register  
   *  @ingroup rcuxx_bc
   */
  struct BcMonitored : public BcRegister
  {
    /** Reference to start command */
    AltroCommand& fStart;
    /** Referene to Control/Status Register 3 */
    BcCSR3&       fCsr3;
    /** Constructor 
	@param name  Name of register 
	@param addr  Address of register 
	@param imem  Reference to Instruction memory 
	@param rmem  Reference to result memory 
	@param csr3  Reference to Control/Status Register 3
	@param start Reference to conversion start command */
    BcMonitored(const char* name, int addr, 
		RcuIMEM& imem, RcuRMEM& rmem, 
		BcCSR3& csr3, AltroCommand& start);
    /** Start monitoring 
	@param tries Number of times to try (1 @f$ \mu s@f$ sleep
	between each try).
	@return @c true on success, @c false in case of time-out or
	other error  */
    bool Monitor(int tries=10);
    /** @return Get current value (ADC counts) */
    unsigned int Current() { return fData; }
    /** @return Get current value (natural units) */
    float CurrentNatural() { return fData / Factor(); }
    /** @return Get conversion factor */
    virtual float Factor() const { return 0; }
    /** @return Unit string */
    virtual const char* Unit() const { return ""; }
  };


  //====================================================================
  /** @struct BcTEMP rcuxx/Bc.h <rcuxx/Bc.h>
   *  @brief Interface to Board Controller register TEMP - current
   *  temperature. The conversion is @f$ T = 0.25^\circ C/ADC@f$ 
   *  @ingroup rcuxx_bc
   */
  struct BcTEMP : public BcMonitored
  {
    /** Centrigrades per count */
    static const float fgkCounts2C;  
    /** Constructor 
	@param imem  Reference to Instruction memory 
	@param rmem  Reference to result memory 
	@param csr3  Reference to Control/Status Register 3
	@param start Reference to conversion start command */
    BcTEMP(RcuIMEM& imem, RcuRMEM& rmem, BcCSR3& csr3, AltroCommand& start) 
      : BcMonitored("Current temperatur", 0x6, imem, rmem, csr3, start)
    {}
    /** @return Current value in centrigrades  */
    float Centigrade() const { return fData * fgkCounts2C; }
    /** @return Conversion factor */
    float Factor() const { return fgkCounts2C; }
    /** @return Unit */
    const char* Unit() const { return "C"; }
    /** Decode current value from buffer */
    void Get() { fData &= 0x3ff; }
    /** Print contents of this register */
    virtual void Print() const;
  };

  //====================================================================
  /** @struct BcAV rcuxx/Bc.h <rcuxx/Bc.h>
   *  @brief Interface to Board Controller register AV - current
   *  analog voltage. The conversion is @f$ U = 4.43mV/ADC@f$ 
   *  @ingroup rcuxx_bc
   */
  struct BcAV : public BcMonitored
  {
    /** mV per count */
    static const float fgkCounts2mV;
    /** Constructor 
	@param imem  Reference to Instruction memory 
	@param rmem  Reference to result memory 
	@param csr3  Reference to Control/Status Register 3
	@param start Reference to conversion start command */
    BcAV(RcuIMEM& imem, RcuRMEM& rmem, BcCSR3& csr3, AltroCommand& start) 
      : BcMonitored("Current analog voltage", 0x7, imem, rmem, csr3, start)
    {}
    /** @return Current value in milivolts */
    float MiliVolts() const { return fData * fgkCounts2mV; }
    /** @return Conversion factor */
    float Factor() const { return fgkCounts2mV; }
    /** @return Unit */
    const char* Unit() const { return "mV"; }
    /** Decode read data */
    void Get()  { fData &= 0x3ff; }
    /** Print contents of this register */
    virtual void Print() const;
  };

  //====================================================================
  /** @struct BcAC rcuxx/Bc.h <rcuxx/Bc.h>
   *  @brief Interface to Board Controller register AC - current
   *  analog current. The conversion is @f$ I = 17mA/ADC@f$ 
   *  @ingroup rcuxx_bc
   */
  struct BcAC : public BcMonitored
  {
    /** mA per count */
    static const float fgkCounts2mA;
    /** Constructor 
	@param imem  Reference to Instruction memory 
	@param rmem  Reference to result memory 
	@param csr3  Reference to Control/Status Register 3
	@param start Reference to conversion start command */
    BcAC(RcuIMEM& imem, RcuRMEM& rmem, BcCSR3& csr3, AltroCommand& start) 
      : BcMonitored("Current analog current", 0x8, imem, rmem, csr3, start)
    {}
    /** @return Get current value in mili amperes */
    float MiliAmps() const { return fData * fgkCounts2mA; }
    /** @return Conversion factor */
    float Factor() const { return fgkCounts2mA; }
    /** @return Unit */
    const char* Unit() const { return "mA"; }
    /** Decode read data */
    void Get() { fData &= 0x3ff; }
    /** Print contents of this register */
    virtual void Print() const;
  };


  //====================================================================
  /** @struct BcDV rcuxx/Bc.h <rcuxx/Bc.h>
   *  @brief Interface to Board Controller register DV - current
   *  digital voltage. The conversion is @f$ U = 4.43mV/ADC@f$ 
   *  @ingroup rcuxx_bc
   */
  struct BcDV : public BcMonitored
  {
    /** mV per count */
    static const float fgkCounts2mV;
    /** Constructor 
	@param imem  Reference to Instruction memory 
	@param rmem  Reference to result memory 
	@param csr3  Reference to Control/Status Register 3
	@param start Reference to conversion start command */
    BcDV(RcuIMEM& imem, RcuRMEM& rmem, BcCSR3& csr3, AltroCommand& start) 
      : BcMonitored("Current digital voltage", 0x9, imem, rmem, csr3, start)
    {}
    /** @return Current value in milivolts */
    float MiliVolts() const { return fData * fgkCounts2mV; }
    /** @return Conversion factor */
    float Factor() const { return fgkCounts2mV; }
    /** @return Unit */
    const char* Unit() const { return "mV"; }
    /** Decode read data */
    void Get()  { fData &= 0x3ff; }
    /** Print contents of this register */
    virtual void Print() const;
  };

  //====================================================================
  /** @struct BcDC rcuxx/Bc.h <rcuxx/Bc.h>
   *  @brief Interface to Board Controller register DC - current
   *  digital current. The conversion is @f$ I = 30mA/ADC@f$ 
   *  @ingroup rcuxx_bc
   */
  struct BcDC : public BcMonitored
  {
    /** mA per count */
    static const float fgkCounts2mA;
    /** Constructor 
	@param imem  Reference to Instruction memory 
	@param rmem  Reference to result memory 
	@param csr3  Reference to Control/Status Register 3
	@param start Reference to conversion start command */
    BcDC(RcuIMEM& imem, RcuRMEM& rmem, BcCSR3& csr3, AltroCommand& start) 
      : BcMonitored("Current digital current", 0xA, imem, rmem, csr3, start)
    {}
    /** @return Get current value in mili amperes */
    float MiliAmps() const { return fData * fgkCounts2mA; }
    /** @return Conversion factor */
    float Factor() const { return fgkCounts2mA; }
    /** @return Unit */
    const char* Unit() const { return "mA"; }
    /** Decode read data */
    void Get() { fData &= 0x3ff; }
    /** Print contents of this register */
    virtual void Print() const;
  };

  //====================================================================
  /** @struct BcThreshold rcuxx/Bc.h <rcuxx/Bc.h>
   *  @brief Interface to Board Controller threshold registers
   *  @ingroup rcuxx_bc
   */
  struct BcThreshold : public BcRegister
  {
    /** Constructor 
        @param name Name
        @param addr Address
	@param imem Referece to instruction memory 
	@param rmem Referece to result memory */
    BcThreshold(const char* name, size_t addr, RcuIMEM& imem, RcuRMEM& rmem) 
      : BcRegister(name, addr, imem, rmem, false, true)
    {}
    /** @return threshold value in ADC */
    unsigned int Threshold() const { return fData; }
    /** @return threshold value in natural units */
    float ThresholdNatural() const { return fData / Factor(); }
    /** Set threshold value in ADC 
	@param val Threshold */
    void SetThreshold(unsigned int val) { fData = val; }
    /** Set threshold value in natural units
	@param val Threshold in natural units */
    void SetThreshold(float val) { fData = unsigned(val * Factor()); }
    /** @return Get conversion factor */
    virtual float Factor() const { return 0; }
    /** @return Unit string */
    virtual const char* Unit() const { return ""; }
  };
  
  //====================================================================
  /** @struct BcTEMP_TH rcuxx/Bc.h <rcuxx/Bc.h>
   *  @brief Interface to Board Controller register TEMP_TH - the
   *  temperature threshold 
   *  @ingroup rcuxx_bc
   */
  struct BcTEMP_TH : public BcThreshold
  {
    /** Constructor 
	@param imem Referece to instruction memory 
	@param rmem Referece to result memory */
    BcTEMP_TH(RcuIMEM& imem, RcuRMEM& rmem) 
      : BcThreshold("Temperatur threshold", 0x1, imem, rmem)
    {}
    /** Set threshold in centigrades 
	@param c Centigrades */
    void SetCentigrade(float c) { fData = unsigned(c  / BcTEMP::fgkCounts2C); }
    /** @return Get current human-readable value */
    float Centigrade() const { return fData * BcTEMP::fgkCounts2C; }
    /** @return Conversion factor */
    float Factor() const { return BcTEMP::fgkCounts2C; }
    /** @return Unit */
    const char* Unit() const { return "C"; }
    /** Decode read data */
    void Get()  { fData &= 0x3ff; }
    /** Encode set data before committing */
    void Set() { fData &= 0x3ff; }
    /** Print contents of this register */
    virtual void Print() const;
  };

  //====================================================================
  /** @struct BcAV_TH rcuxx/Bc.h <rcuxx/Bc.h>
   *  @brief Interface to Board Controller register AV_TH
   *  @ingroup rcuxx_bc
   */
  struct BcAV_TH : public BcThreshold
  {
    BcAV_TH(RcuIMEM& imem, RcuRMEM& rmem) 
      : BcThreshold("Analog voltage threshold", 0x2, imem, rmem)
    {}
    /** Set threshold in mili volts 
	@param v Threshold */
    void SetMiliVolts(float v) { fData = unsigned(v / BcAV::fgkCounts2mV); }
    /** @return Get current human-readable value */
    float MiliVolts() const { return fData * BcAV::fgkCounts2mV; }
    /** @return Conversion factor */
    float Factor() const { return BcAV::fgkCounts2mV; }
    /** @return Unit */
    const char* Unit() const { return "mV"; }
    /** Decode read data */
    void Get()  { fData &= 0x3ff; }
    /** Encode set data before committing */
    void Set() { fData &= 0x3ff; }
    /** Print contents of this register */
    virtual void Print() const;
  };


  //====================================================================
  /** @struct BcAC_TH rcuxx/Bc.h <rcuxx/Bc.h>
   *  @brief Interface to Board Controller register AC_TH
   *  @ingroup rcuxx_bc
   */
  struct BcAC_TH : public BcThreshold
  {
    BcAC_TH(RcuIMEM& imem, RcuRMEM& rmem) 
      : BcThreshold("Analog current threshold", 0x3, imem, rmem)
    {}
    /** Set threshold in mili amps
	@param v Threshold */
    void SetMiliAmps(float v) { fData = unsigned(v / BcAC::fgkCounts2mA); }
    /** @return Get current human-readable value */
    float MiliAmps() const { return fData * BcAC::fgkCounts2mA; }
    /** @return Conversion factor */
    float Factor() const { return BcAC::fgkCounts2mA; }
    /** @return Unit */
    const char* Unit() const { return "mA"; }
    /** Decode read data */
    void Get()  { fData &= 0x3ff; }
    /** Encode set data before committing */
    void Set() { fData &= 0x3ff; }
    /** Print contents of this register */
    virtual void Print() const;
  };

  //====================================================================
  /** @struct BcDV_TH rcuxx/Bc.h <rcuxx/Bc.h>
   *  @brief Interface to Board Controller register DV_TH
   *  @ingroup rcuxx_bc
   */
  struct BcDV_TH : public BcThreshold
  {
    BcDV_TH(RcuIMEM& imem, RcuRMEM& rmem) 
      : BcThreshold("Digital voltage threshold", 0x4, imem, rmem)
    {}
    /** Set threshold in mili volts 
	@param v Threshold */
    void SetMiliVolts(float v) { fData = unsigned(v / BcDV::fgkCounts2mV); }
    /** @return Get current human-readable value */
    float MiliVolts() const { return fData * BcDV::fgkCounts2mV; }
    /** @return Conversion factor */
    float Factor() const { return BcDV::fgkCounts2mV; }
    /** @return Unit */
    const char* Unit() const { return "mV"; }
    /** Decode read data */
    void Get()  { fData &= 0x3ff; }
    /** Encode set data before committing */
    void Set() { fData &= 0x3ff; }
    /** Print contents of this register */
    virtual void Print() const;
  };

  //====================================================================
  /** @struct BcDC_TH rcuxx/Bc.h <rcuxx/Bc.h>
   *  @brief Interface to Board Controller register DC_TH
   *  @ingroup rcuxx_bc
   */
  struct BcDC_TH : public BcThreshold
  {
    BcDC_TH(RcuIMEM& imem, RcuRMEM& rmem) 
      : BcThreshold("Digital current threshold", 0x5, imem, rmem)
    {}
    /** Set threshold in mili amps
	@param v Threshold */
    void SetMiliAmps(float v) { fData = unsigned(v / BcDC::fgkCounts2mA); }
    /** @return Get current human-readable value */
    float MiliAmps() const { return fData * BcDC::fgkCounts2mA; }
    /** @return Conversion factor */
    float Factor() const { return BcDC::fgkCounts2mA; }
    /** @return Unit */
    const char* Unit() const { return "mA"; }
    /** Decode read data */
    void Get()  { fData &= 0x3ff; }
    /** Encode set data before committing */
    void Set() { fData &= 0x3ff; }
    /** Print contents of this register */
    virtual void Print() const;
  };


  //====================================================================
  /** @class Bc rcuxx/Bc.h <rcuxx/Bc.h>
      @brief Interface to Board Controller 
      @ingroup rcuxx_bc
   */
  class Bc 
  {
  public:
    /** Constructor 
	@param rcu Reference to controlling RCU */
    Bc(Rcu& rcu);
    /** Set broadcast */
    void SetBroadcast();
    /** Set recipient/query board 
	@param board Board number on ALTRO bus */
    void SetAddress(unsigned int board);
    /** Update all */
    unsigned int Update();
    /** Print all */
    void Print() const;
    /** Set the debuging stuff */
    virtual void SetDebug(bool debug);
  public:
    /** Board address */
    unsigned int  BoardAddress() const { return fBoardAddress; }

    /** Command CNTLAT - Latch counters */
    AltroCommand*  CNTLAT() const { return fCNTLAT; }
    /** Command ALRST - Altro reset */
    AltroCommand*  CNTCLR() const { return fCNTCLR; }
    /** Command ALRST - Altro reset */
    AltroCommand*  CSR1CLR() const { return fCSR1CLR; }
    /** Command ALRST - Altro reset */
    AltroCommand*  ALRST() const { return fALRST; }
    /** Command BCRST - Board controller reset */
    AltroCommand*  BCRST() const { return fBCRST; }
    /** Command STCNV - Start conversion */
    AltroCommand*  STCNV() const { return fSTCNV; }
    /** Command SCEVL - Slow control */
    AltroCommand*  SCEVL() const { return fSCEVL; }
    /** Command EVLRDO -  */
    AltroCommand*  EVLRDO() const { return fEVLRDO; }
    /** Command STTSM - Start test mode */
    AltroCommand*  STTSM() const { return fSTTSM; }
    /** Command ACQRDO - Acquisition read-out */
    AltroCommand*  ACQRDO() const { return fACQRDO; }
  
    // 
    /** Register TEMP_TH - temperature threshold */
    BcTEMP_TH*	 TEMP_TH() const { return fTEMP_TH; }
    /** Register AV_TH - Analog voltage threshold */
    BcAV_TH*	 AV_TH() const { return fAV_TH; }
    /** Register AC_TH - Analog current threshold */
    BcAC_TH*	 AC_TH() const { return fAC_TH; }
    /** Register DV_TH - Digital voltage threshold */
    BcDV_TH*	 DV_TH() const { return fDV_TH; }
    /** Register DC_TH - Digitial current threshold */
    BcDC_TH*	 DC_TH() const { return fDC_TH; }
    /** Register TEMP - Current temperatur */
    BcTEMP*	 TEMP() const { return fTEMP; }
    /** Register AV - Current analog voltage */
    BcAV*	 AV() const { return fAV; }
    /** Register AC - Current analog current */
    BcAC*	 AC() const { return fAC; }
    /** Register DV - Current digital voltage */
    BcDV*	 DV() const { return fDV; }
    /** Register DC - Current digital current */
    BcDC*	 DC() const { return fDC; }
    /** Register L1CNT - L1 counter */
    BcL1CNT*	 L1CNT() const { return fL1CNT; }
    /** Register L2CNT - L2 counter */
    BcL2CNT*	 L2CNT() const { return fL2CNT; }
    /** Register SCLKCNT - Sample clock counter */
    BcSCLKCNT*	 SCLKCNT() const { return fSCLKCNT; }
    /** Register DSTBCNT - Data strobe counter */
    BcDSTBCNT*	 DSTBCNT() const { return fDSTBCNT; }
    /** Register TSMWORD - Test mode word */
    BcTSMWORD*	 TSMWORD() const { return fTSMWORD; }
    /** Register USRATIO - Under sampling ratio */
    BcUSRATIO*	 USRATIO() const { return fUSRATIO; }
    /** Register CSR0 - Control and status 0 */
    BcCSR0*	 CSR0() const { return fCSR0; }
    /** Register CSR1 - Control and status 1 */
    BcCSR1*	 CSR1() const { return fCSR1; }
    /** Register CSR2 - Control and status 2 */
    BcCSR2*	 CSR2() const { return fCSR2; }
    /** Register CSR3 - Control and status 3 */
    BcCSR3*	 CSR3() const { return fCSR3; }
  protected:
    /** Board address */
    unsigned int fBoardAddress;
    /** Command CNTLAT - Latch counters */
    AltroCommand* fCNTLAT;
    /** Command ALRST - Altro reset */
    AltroCommand* fCNTCLR;
    /** Command ALRST - Altro reset */
    AltroCommand* fCSR1CLR;
    /** Command ALRST - Altro reset */
    AltroCommand* fALRST;
    /** Command BCRST - Board controller reset */
    AltroCommand* fBCRST;
    /** Command STCNV - Start conversion */
    AltroCommand* fSTCNV;
    /** Command SCEVL - Slow control */
    AltroCommand* fSCEVL;
    /** Command EVLRDO -  */
    AltroCommand* fEVLRDO;
    /** Command STTSM - Start test mode */
    AltroCommand* fSTTSM;
    /** Command ACQRDO - Acquisition read-out */
    AltroCommand* fACQRDO;
  
    // 
    /** Register TEMP_TH - temperature threshold */
    BcTEMP_TH*	fTEMP_TH;
    /** Register AV_TH - Analog voltage threshold */
    BcAV_TH*	fAV_TH;
    /** Register AC_TH - Analog current threshold */
    BcAC_TH*	fAC_TH;
    /** Register DV_TH - Digital voltage threshold */
    BcDV_TH*	fDV_TH;
    /** Register DC_TH - Digitial current threshold */
    BcDC_TH*	fDC_TH;
    /** Register TEMP - Current temperatur */
    BcTEMP*	fTEMP;
    /** Register AV - Current analog voltage */
    BcAV*	fAV;
    /** Register AC - Current analog current */
    BcAC*	fAC;
    /** Register DV - Current digital voltage */
    BcDV*	fDV;
    /** Register DC - Current digital current */
    BcDC*	fDC;
    /** Register L1CNT - L1 counter */
    BcL1CNT*	fL1CNT;
    /** Register L2CNT - L2 counter */
    BcL2CNT*	fL2CNT;
    /** Register SCLKCNT - Sample clock counter */
    BcSCLKCNT*	fSCLKCNT;
    /** Register DSTBCNT - Data strobe counter */
    BcDSTBCNT*	fDSTBCNT;
    /** Register TSMWORD - Test mode word */
    BcTSMWORD*	fTSMWORD;
    /** Register USRATIO - Under sampling ratio */
    BcUSRATIO*	fUSRATIO;
    /** Register CSR0 - Control and status 0 */
    BcCSR0*	fCSR0;
    /** Register CSR1 - Control and status 1 */
    BcCSR1*	fCSR1;
    /** Register CSR2 - Control and status 2 */
    BcCSR2*	fCSR2;
    /** Register CSR3 - Control and status 3 */
    BcCSR3*	fCSR3;
  };
}

#endif

//____________________________________________________________________
//
// EOF
//

