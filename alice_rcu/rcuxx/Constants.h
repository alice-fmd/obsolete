// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USAq
//
/** @file    Constants.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Sat May 27 23:06:33 2006
    @brief   macros that define various constants 
*/
#ifndef RCUXX_Constants
#define RCUXX_Constants


/**@{
   @name Addresses of memories 
   @ingroup rcuxx
*/
#define A_RMEM		0x6000		//< Result memory
#define A_ACL		0x6400		//< Active Channel List
#define A_PMEM		0x6800		//< Pattern memory
#define A_IMEM		0x7000		//< Instruction memory
#define A_DML1		0x7400		//< Data memory 1 - low bits
#define A_DMH1		0x7500		//< Data memory 1 - high bits
#define A_REGISTERS	0x7800		//< Base address for registers
#define A_DML2	    	0x7C00		//< Data memory 1 - low bits
#define A_DMH2	    	0x7D00		//< Data memory 2 - high bits
#define A_DM2HH	    	0xFD00		//< Data memory 2 - high bits
#ifndef RCUXX_U2F
# define A_MONITOR	0x8000		//< Base address for monitor registers
# define A_STATUS       0x8100		//< Status memory 
# define A_HEADER	0x4001		//< Event header words
#else 
# define A_MONITOR	0x7800		//< Base address for monitor registers
#endif
/** @} */

/** @{
    @name Sizes of memories 
    @ingroup rcuxx
*/
#define S_IMEM		256		//< Size of instruction memory
#define S_PMEM		1024		//< Size of pattern memory
#define B_PMEM		256		//< Size of pattern memory
#define S_RMEM		128		//< Size of result memory
#define S_ACL		256		//< Size of active channel memory
#define S_DM		256		//< Size of partial data memory
#ifndef RCUXX_U2F
# define S_REGISTERS	8		//< Size of register memory
# define S_MONITOR	101		//< Size of monitor memory
# define S_HEADER	8		//< Size of header memory 
# define S_STATUS	32		//< Size of status memory
#else
# define S_REGISTERS	15		//< Size of register memory
# define S_MONITOR	15		//< Size of monitor memory
#endif
/** @} */

/** @{
    @name Registers 
    @ingroup rcuxx
*/
#define O_ERRST		0		//< Error and status
#define O_TRCFG1	1		//< Trigger configuration
#define O_TRCNT		2		//< Trigger counter
#define O_LWADD		3		//< Last words in data buffers
#define O_IRADD		4		//< Last instruction address
#define O_IRDAT		5		//< Last instruction data 
#ifndef RCUXX_U2F
# define O_PMCFG  	6		//< Pedestal memory configuration
# define O_CHADD	7		//< Last channel address
# define O_ACTFEC	0		//< Active FEC bit mask
# define O_RDOFEC	1		//< Read-out FEC bit mask
# define O_INTREG	100		//< Interrupt information
# define O_RESREG	2		//< Return BC register to DCS
# define O_ERRREG	3		//< Error type register
# define O_INTMOD	4		//< Interrupt mask for branches
#else
# define O_EVWORD	6		//< Event word register
# define O_ACTFEC	7		//< Active front-end card register
# define O_FMIREG	8		//< Firmware input register
# define O_FMOREG	9		//< Firmware output register
# define O_TRCFG2	10		//< Trigger config 2 register 
# define O_RESREG       11		//< Return BC register to DCS
# define O_ERRREG	12		//< Error type register
# define O_INTMOD	13		//< Interrupt mask for branches
# define O_INTREG	14		//< Interrupt mask for branches
#endif
/** @} */

/** @{
    @name Commands 
    @ingroup rcuxx
*/
#define C_RS_STATUS 	0x6c01		//< ReSet STATUS command
#define C_RS_TRCFG  	0x6c02		//< ReSet TRigger ConFiGuration command
#define C_RS_TRCNT  	0x6c03		//< ReSet TRigger CouNTer command
#define C_EXEC		0x0000		//< EXECute instructions command 
#define C_ABORT		0x0800		//< ABORT instruction execution
#define C_FECRST	0x2001		//< Front-End Card ReSeT command 
#define C_SWTRG		0xD000		//< SoftWare TRiGger command
#ifndef RCUXX_U2F
# define C_RS_DMEM1	0x6C04		//< ReSet DMEM1 command
# define C_RS_DMEM2	0x6C05		//< ReSet DMEM2 command
# define C_DCS_ON	0xE000		//< DCS OwN bus command 
# define C_DDL_ON	0xF000		//< DDL OwN bus comman
# define C_L1_TTC	0xE800		//< L1-enable via TTC command
# define C_L1_I2C	0xF800		//< L1-enable via I2C command
# define C_L1_CMD	0xD800		//< L1-enable via CoMmanD command
# define C_GLB_RESET	0x2000		//< GLoBal RESET command
# define C_RCU_RESET	0x2002		//< RCU RESET command
# define C_RS_ERRREG    0x8010          //< ReSet ERRor REGister
#else
# define C_RS_DMEM1	0xD001		//< ReSet DMEM1 command
# define C_RS_DMEM2	0xD002		//< ReSet DMEM2 command
# define C_TRG_CLR	0xD003		//< TRiGger CLeaR command
# define C_WRFM		0xD004		//< WRite FirMware command
# define C_RDFM		0xD005		//< ReaD FirMware command
# define C_RDABORT	0xD006		//< ReaD-out ABORT command
# define C_CLR_EVTAG	0xD007		//< CLeaR EVent TAG command
# define C_SCCOMMAND	0xC000		//< Slow Control COMMAND
#endif
/** @} */

#endif
//
// EOF
//
