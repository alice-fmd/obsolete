//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    rcuxx/Fee.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 01:03:24 2006
    @brief   Implementation of FeeClient back-end 
*/
#ifdef HAVE_CONFIG_H
# include "config.h"
#endif
#ifndef RCUXX_Fee
# include <rcuxx/Fee.h>
#endif
#ifndef RCUXX_Constants
# include <rcuxx/Constants.h>
#endif
#ifndef __UNISTD_H
# include <unistd.h>
#endif
#include <stdexcept>
#include <cstdarg>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <string>
#include <numeric>
#include <feeclient/rcu_issue.h>
#include <feeclient/FeeClientLibInterface.hpp>
#include "DebugGuard.h"
#ifndef USE_OWN_FEE_CLIENT
# include <feeclient/FeeClientLibImp.hpp>
#endif
#include <feeclient/MessageStruct.hpp>
#include <feeclient/FeePacket.hpp>

//====================================================================
#define FEECLIENTFLAGS 0x0 // bit 0x2 to switch on check sum
namespace 
{
  bool fDebug = false;
}

//==================================================================== 
namespace Rcuxx
{
#ifndef USE_OWN_FEE_CLIENT
  typedef dcs::fee::FeeClientLibImp FeeClient;
#else
# define logLevelAll       0
# define logLevelBenchmark 1
# define logLevelDebug     2
# define logLevelInfo      3
# define logLevelWarning   4
# define logLevelError     5
# define logLevelFatal     6
# define logLevelNone      7
# define logLevel          logLevelAll

// Client flags 
/* Instruction codes */
# define I_WRITE  0
# define I_READ   1
# define I_GRESET 2
  
  //====================================================================
  struct FeeException : public std::exception
  {
    std::string fWhat;
    virtual ~FeeException() throw () {}
    FeeException(const char* func, const char* format, ...)
      : fWhat("")
    {
      static char buf[1024];
      va_list ap;
      va_start(ap, format);
      vsprintf(buf,format, ap);
      va_end(ap);
      std::stringstream s;
      s << std::setw(8) << "Error" << " from " << func << ": " << buf;
      fWhat = s.str();
      // fgErrorStr = fWhat;
    }
    const char* what() const throw () { return fWhat.c_str(); }
  };
#define FeeWarn(ref, id, format, ... ) \
   FeeLogging(logLevelWarning, ref, id, __func__, format, ##__VA_ARGS__)
#define FeeError(format, ... ) \
   throw FeeException(__func__, format, ##__VA_ARGS__);

  //====================================================================
  struct LockGuard 
  {
    pthread_mutex_t& fMutex;
    LockGuard(pthread_mutex_t& l) 
      : fMutex(l)
    {
      pthread_mutex_lock(&fMutex);
    }
    ~LockGuard() 
    {
      pthread_mutex_unlock(&fMutex);
    }
  };

  //____________________________________________________________________
  pthread_mutex_t mut = PTHREAD_MUTEX_INITIALIZER;

  //====================================================================
  /** @struct FeeClient 
      @brief Dummy client interface 
      @ingroup feexx
  */
  struct FeeClient : public dcs::fee::FeeClientLibInterface
  {
    /** Constructor */
    FeeClient() {}
    /** Destructor */
    virtual ~FeeClient() {}
    
    /** Type of flags */
    // typedef unsigned short FlagBits;
    /** Type of data array */
    typedef std::vector<unsigned int> DataArray;

    /** Send a command
	@param serverName Address of server name string.  On return,
	contains pointer to newly allocated memory with string of
	responding server name  
	@param size Input data size (in elements).  On return the size of
	the returned data (in elements). 
	@param data Message data.  On return the possible return data. 
	@param flags Message flags 
	@param errorCode On return,  error code if any
	@param status On return, the status
	@return @c true (1) on success, @c false (0) otherwise */
    bool writeReadData(std::string&        serverName, 
		       size_t&             size, 
		       DataArray&          data, 
		       dcs::fee::FlagBits& flags, 
		       short&              errorCode, 
		       short&              status)
    {
      Rcuxx::DebugGuard g(fDebug, 
			  "FeeClient::writeReadData(%s,%d,%p,%d,%d,%d)", 
			  serverName.c_str(),size,&(data[0]),flags,
			  errorCode,status);
      int id  = 0;
      int ret = 0;
      curId   = 0;  // reset id to filter out stucking acknowledge data
      try {
	Rcuxx::DebugGuard::Message(fDebug, 
				   "FeeClient::writeReadData: size is %d", 
				   size);
	if(serverName.empty())   FeeError("No serverName");
	if(size == 0)            FeeWarn(1,  -1, "Size is zero");
	// This doesn't work, because reads may not have allocated
	// enough memory when we enter here.  However, that memory
	// will be allocated later, so there's no need to worry
	// really.
	if(data.size()<size)     FeeError("Too little data %d < %d",
					  data.size(), size);

	size_t osize = size * sizeof(DataArray::value_type);
	Rcuxx::DebugGuard::Message(fDebug, "sending command %d", osize);
	id = sendCommand(const_cast<char*>(serverName.c_str()), osize, 
			 reinterpret_cast<char*>(&(data[0])), flags); 
	Rcuxx::DebugGuard::Message(fDebug, " id returned is %d", id);
	if(id <= 0) FeeError("id=%id, sendCommand failed", id);
    
	// size        = 0;
	flags       = 0;
	errorCode   = 0;
	status      = 0;
	ret = getAcknowledge(serverName, size, data, id, flags, 
			     errorCode, status);
	
	Rcuxx::DebugGuard::Message(fDebug, "size returned is %d", size);
	if(!ret) FeeError("ret=%i, getAcknowledge failed", ret);
      }
      catch (std::exception& e) {
	std::cerr << "FeeClient::writeReadData(" << serverName << "," 
		  << size << "," << &(data[0]) << "," << flags << "," 
		  << errorCode << "," << status << "):\n\t" 
		  << e.what() << std::endl;
	return false;
      }
      return true;
    }
    /** Get an aknowledgement 
	@param serverName Name of server responding
	@param size Size of data returned
	@param data data returned from server. 
	@param id Identifier obtained by sendCommand
	@param flags Flags used 
	@param errorCode Return error code
	@param status Return status code 
	@return time left before timeout, or 0 on timeout */
    int getAcknowledge(std::string&        serverName, 
		       size_t&             size, 
		       DataArray&          data, 
		       unsigned int        id, 
		       dcs::fee::FlagBits& flags, 
		       short&              errorCode, 
		       short&              status)
    {
      Rcuxx::DebugGuard g(fDebug, 
			  "FeeClient::getAcknowledge(%s,%d,%p,%d,%d,%d,%d)", 
			  serverName.c_str(), size, &(data[0]), 
			  id, flags, errorCode, status);
      int maxtime      = 50;
      int timeout      = maxtime;
      int usleepPeriod = 50000;
      int done         = false;
      try {
	if(id <= 0)       FeeError("id=%id, invalid id", id);
	
	while(!done && timeout > 0) {
	  Rcuxx::DebugGuard::Message(fDebug, "Timeout is now %i", timeout);
	  usleep(usleepPeriod);
	  timeout--;

	  LockGuard l(mut);
	  if(id == curId) {
	    Rcuxx::DebugGuard::Message(fDebug, "Size returned is %i", size);
	    
	    if(curServerName.empty())  FeeError("no curServerName");
	    // if(curSize <= 0)           FeeError("no curData");
	    
	    serverName = curServerName;
	    size       = curSize;
	    flags      = curFlags;
	    errorCode  = curErrorCode;
	    status     = curStatus;
	    done       = true;
	    if (curSize > 0) {
	      if (data.size() < curSize) data.resize(curSize);
	      std::copy(&(curData[0]), &(curData[curSize]), data.begin());
	    }
	  }
	}
	if(timeout <= 0) 
	  FeeError("timeout=%i, no response from FeeServer within timeout", 
		   timeout);
      }
      catch (std::exception& e) {
	std::cerr << e.what() << std::endl;
	return 0;
      }
      return timeout;
    }
  protected:
    /** Log messages */
    void FeeLogging(int         type, 
		    int         ref, 
		    int         id, 
		    const char* func, 
		    const char* format, 
		    ...)
    {
      if (type > logLevel) return;
      
      static char buf[1024];
      va_list ap;
      va_start(ap, format);
      vsprintf(buf,format, ap);
      va_end(ap);
      std::cerr << "id=" << id << ": "; 
      switch(type){
      case logLevelDebug:
	std::cerr << std::setw(8)  << "Debug"; break;
      case logLevelWarning:
	std::cerr << std::setw(8)  << "Warning"; break;
      case logLevelError: 
	std::cerr << std::setw(8)  << "Error"; break;
      }
      std::cerr << "(" << ref << ") from " << func << ": " << buf << std::endl;
    }

    /** Handle acknowledgments
	@param serverName Server 
	@param size data size 
	@param data data
	@param id Id 
	@param flags Flags
	@param errorCode Error code
	@param status status */
    void acknowledgeHandler(char*              serverName, 
			    unsigned int       size, 
			    char*              data, 
			    unsigned int       id, 
			    dcs::fee::FlagBits flags, 
			    short              errorCode, 
			    short              status)
    {
      try {
	Rcuxx::DebugGuard 
	  g(fDebug, "FeeClient, handling acknowledge from %s of size %d", 
	    serverName, size);
	if(!serverName)   FeeError("no serverName");
	if(!*serverName)  FeeError("no *serverName");
	if(!data)         FeeError("no data");
	if(id == 0 && status < 0) {
	  std::cerr << "Received status " << status << " from FeeServer\n"
		    << "serverName=" << serverName << "\n"
		    << "size      =" << size << "\n"
		    << "id        =" << id << "\n" 
		    << "flags     =" << flags << "\n"
		    << "errorCode =" << errorCode << std::endl;
	}
	else{
	  LockGuard l(mut);
	  Rcuxx::DebugGuard::Message(fDebug, "Storing data of size %d", size);
	  
	  curId        = id;
	  curFlags     = flags;
	  curErrorCode = errorCode;
	  curStatus    = status;
	  curServerName = serverName;
	  curSize       = size / sizeof(DataArray::value_type);
	  if (curSize > 0) {
	    if (curData.size() < curSize) curData.resize(curSize);
	    memcpy(&(curData[0]), data, size);
	  }
	}
      }
      catch (std::exception& e) {
	std::cerr << e.what() << std::endl;
	return;
      }
    }
    /** Handle messages
	@param msg Message */
    void messageHandler(const dcs::fee::MessageStruct msg)
    {
      // cerr << "Received message from " 
      //      << msg.detector << "-detector: " << endl;
      std::cerr << "Message Type: " << msg.eventType << ", " 
		<< "Source: "       << msg.source << ", "
		<< "Date: "         << msg.date << "\n"
		<< "- "             << msg.description << " -\n" 
		<< std::endl;
    }
  private:
    /** Current server name */
    std::string curServerName;
    /** Current data size (in bytes) */
    size_t curSize;
    /** Current data */
    DataArray curData;
    /** Current ID */
    unsigned int curId;
    /** Current flags */
    dcs::fee::FlagBits curFlags;
    /** Current error code */
    short        curErrorCode;
    /** Current status */
    short        curStatus;
  };
#endif    
}

//====================================================================
Rcuxx::Fee::Fee()
{
  throw std::runtime_error("Cannot use defualt Rcuxx::Fee constructor");
}

//____________________________________________________________________
Rcuxx::Fee::Fee(const Fee& other)
{
  throw std::runtime_error("Cannot use Rcuxx::Fee copy constructor");
}

//____________________________________________________________________
void
Rcuxx::Fee::ParseUrl(char* node) 
{
  DebugGuard g(fDebug, "Parsing node %s", node);
  std::string url(node);
  std::string::size_type slash = url.find('/');
  if (slash != std::string::npos) {
    if (slash != 0) {
      // std::string host("DIM_DNS_NODE=");
      std::string host(url.substr(0, slash));
      DebugGuard::Message(fDebug, 
			  "Setting environment variable DIM_DNS_HOST=%s", 
			  host.c_str());
      if (setenv("DIM_DNS_NODE", host.c_str(),1))
	throw std::runtime_error("Couldn't set DIM_DNS_NODE");
    }
    DebugGuard::Message(fDebug, "Removing %d to %d from \"%s\" of size %d", 
			0, slash+1, url.c_str(), url.size());
    url.erase(0, std::min(slash+1,url.size()));
  }
  else {
    if (!getenv("DIM_DNS_NODE")) 
      throw std::runtime_error("DIM_DNS_NODE not set");
  }
  DebugGuard::Message(fDebug,"DIM DNS node to use: %s",getenv("DIM_DNS_NODE"));
  if (url.empty() && getenv("FEE_SERVER_NAME")) {
    DebugGuard::Message(fDebug, "Getting server name from environment: %s",
			getenv("FEE_SERVER_NAME"));
    fServerName = getenv("FEE_SERVER_NAME");
  }
  else {
    DebugGuard::Message(fDebug, "Setting server name from node to \"%s\"", 
			url.c_str());
    fServerName = url;
  }
  DebugGuard::Message(fDebug, "Server name set to %s", fServerName.c_str());
}

//____________________________________________________________________
Rcuxx::Fee::Fee(char* node, bool emul) 
  : fServerName(""), fClient(0)
{
  DebugGuard g(fDebug, "Opening FEE server %s", node);

  if (emul) 
    throw std::runtime_error("emulation not supported by Fee interface");
  
  ParseUrl(node);

  if (fServerName.empty()) throw std::runtime_error("No server name");
  fClient = new FeeClient;
  if (!fClient->registerFeeServerName(fServerName.c_str())) 
    throw std::runtime_error("Couldn't register server");
  if (!fClient->startFeeClient()) 
    throw std::runtime_error("Couldn't start client");
  
  fABORT	 = new RcuCommand(*this, "ABORT",	 C_ABORT);
  //fCLR_EVTAG	 = new RcuCommand(*this, "CLR_EVTAG",	 C_CLR_EVTAG);
  fDCS_ON	 = new RcuCommand(*this, "DCS_ON",	 C_DCS_ON);
  fDDL_ON	 = new RcuCommand(*this, "DDL_ON",	 C_DDL_ON);
  fEXEC		 = new RcuCommand(*this, "EXEC",	 C_EXEC,	true);
  fFECRST	 = new RcuCommand(*this, "FECRST",	 C_FECRST);
  fGLB_RESET	 = new RcuCommand(*this, "GLB_RESET",	 C_GLB_RESET);
  fL1_CMD	 = new RcuCommand(*this, "L1_CMD",	 C_L1_CMD);
  fL1_I2C	 = new RcuCommand(*this, "L1_I2C",	 C_L1_I2C);
  fL1_TTC	 = new RcuCommand(*this, "L1_TTC",	 C_L1_TTC);
  fRCU_RESET	 = new RcuCommand(*this, "RCU_RESET",	 C_RCU_RESET);
  //fRDABORT	 = new RcuCommand(*this, "RDABORT",	 C_RDABORT);
  //fRDFM	 = new RcuCommand(*this, "RDFM",	 C_RDFM);
  fRS_DMEM1	 = new RcuCommand(*this, "RS_DMEM1",	 C_RS_DMEM1);
  fRS_DMEM2	 = new RcuCommand(*this, "RS_DMEM2",	 C_RS_DMEM2);
  fRS_STATUS	 = new RcuCommand(*this, "RS_STATUS",	 C_RS_STATUS);
  fRS_TRCFG	 = new RcuCommand(*this, "RS_TRCFG",	 C_RS_TRCFG);
  fRS_TRCNT	 = new RcuCommand(*this, "RS_TRCNT",	 C_RS_TRCNT);
  //fSCCOMMAND	 = new RcuCommand(*this, "SCCOMMAND",	 C_SCCOMMAND);
  fSWTRG	 = new RcuCommand(*this, "SWTRG",	 C_SWTRG);
  //fTRG_CLR	 = new RcuCommand(*this, "TRG_CLR",	 C_TRG_CLR);
  //fWRFM	  = new RcuCommand(*this, "WRFM",	 C_WRFM);
  fRS_ERRREG	 = new RcuCommand(*this, "RS_ERRREG",	 C_RS_ERRREG);
  
  fIMEM		 = new RcuIMEM(*this, A_IMEM, S_IMEM, fEXEC, fABORT);
  fRMEM		 = new RcuRMEM(*this, A_RMEM, S_RMEM);
  fPMEM		 = new RcuPMEM(*this, A_PMEM, B_PMEM);
  fDM1		 = new RcuDMEM(1, *this, A_DML1,  A_DMH1, S_DM, fRS_DMEM1);
  fDM2		 = new RcuDMEM(2, *this, A_DML2,  A_DMH2, S_DM, fRS_DMEM2);
  fACL		 = new RcuACL(*this, A_ACL,  S_ACL);
  fHEADER	 = new RcuHEADER(*this, A_HEADER, S_HEADER);
  fSTATUS	 = new RcuSTATUS(*this, A_STATUS, S_STATUS);
  fRegisters	 = new RcuMemory(*this, "Registers", A_REGISTERS, S_REGISTERS, 
				 0xffffffff, false);
  fMonitor	 = new RcuMemory(*this, "Monitor", A_MONITOR, S_MONITOR,
				 0xffffffff, false);
  
  fERRST	 = new RcuERRST(*fRegisters,	O_ERRST,	fRS_STATUS);
  fTRCNT	 = new RcuTRCNT(*fRegisters,	O_TRCNT,	fRS_TRCNT);
  fLWADD	 = new RcuLWADD(*fRegisters,	O_LWADD);
  fIRADD	 = new RcuIRADD(*fRegisters,	O_IRADD);
  fIRDAT	 = new RcuIRDAT(*fRegisters,	O_IRDAT);
  //fEVWORD	 = new RcuEVWORD(*fRegisters,	O_EVWORD);
  fACTFEC	 = new RcuACTFEC(*fMonitor,	O_ACTFEC);
  fRDOFEC	 = new RcuRDOFEC(*fMonitor,	O_RDOFEC);
  fTRCFG1	 = new RcuTRCFG1(*fRegisters,	O_TRCFG1,	fRS_TRCFG);
  //fTRCFG2	 = new RcuTRCFG2(*fRegisters,	O_TRCFG2);
  //fFMIREG	 = new RcuFMIREG(*fRegisters,	O_FMIREG);
  //fFMOREG	 = new RcuFMOREG(*fRegisters,	O_FMOREG);
  fPMCFG	 = new RcuPMCFG(*fRegisters,	O_PMCFG);
  fCHADD	 = new RcuCHADD(*fRegisters,	O_CHADD);
  fINTREG	 = new RcuINTREG(*fMonitor,	O_INTREG);
  fRESREG	 = new RcuRESREG(*fMonitor,	O_RESREG);
  fERRREG	 = new RcuERRREG(*fMonitor,	O_ERRREG,	fRS_ERRREG);
  fINTMOD	 = new RcuINTMOD(*fMonitor,	O_INTMOD);
}

//____________________________________________________________________
Rcuxx::Fee::~Fee()
{
  std::cout << "Closing connection to FEE via DTOR" << std::endl;
  delete fClient;
}

//____________________________________________________________________
unsigned int 
Rcuxx::Fee::ExecCommand(unsigned int cmd)
{
  size_t size = 1;
  unsigned int data = 0;
  unsigned int off  = 0;
  return WriteMemory(cmd, off, size, &data);
}

//____________________________________________________________________
unsigned int 
Rcuxx::Fee::ReadMemory(unsigned int base, unsigned int offset,  
		       unsigned int& isize, unsigned int* data)
{
  Rcuxx::DebugGuard g(fDebug, "Fee::ReadMemory(%p,%d, %d, %p) [\"%s\"]", 
		      base,offset,isize,data,fServerName.c_str());
  unsigned int   retval    = 0;
  unsigned short flags     = FEECLIENTFLAGS;
  short          errorCode = 0;
  short          status    = 0;
  unsigned int   size      = 3;
  std::string    server    = fServerName;
  
  try {
    size_t newSize = std::max(3U, isize);
    if (fBuffer.size() < newSize) fBuffer.resize(newSize);
    fBuffer[0] = RCU_READ_MEMBLOCK | isize;
    fBuffer[1] = base + offset;
    fBuffer[2] = CE_CMD_TAILER;
    if (!fClient->writeReadData(server,size,fBuffer,flags,errorCode,status))
      return kMessageError;
    // Check consistency of size 
    size_t osize = size;
    if (osize != isize) return kNotEnough;
    // Copy to user 
    if (size > isize) return kNotEnough;

    isize = size;
    std::copy(&(fBuffer[0]), &(fBuffer[size]), data);
  }
  catch (std::exception& e) {
    fClientError = e.what();
    return kClient;
  }
  return retval;
}

//____________________________________________________________________
unsigned int 
Rcuxx::Fee::WriteMemory(unsigned int base, unsigned int offset,  
			unsigned int& osize, unsigned int* data)
{
  Rcuxx::DebugGuard g(fDebug, "Fee::WriteMemory(%p, %d, %d, %p) [\"%s\"]", 
		      base, offset, osize, data,fServerName.c_str());
  unsigned int      retval    = 0;
  unsigned short    flags     = FEECLIENTFLAGS;
  short             errorCode = 0;
  short             status    = 0;
  unsigned int      size      = 3 + osize;
  std::string       server    = fServerName;
  try {
    if (fBuffer.size() < size) fBuffer.resize(size);
    fBuffer[0]          = RCU_WRITE_MEMBLOCK | osize;
    fBuffer[1]          = base + offset;
    fBuffer[osize + 2]  = CE_CMD_TAILER;
    std::copy(&(data[0]), &(data[osize]), &(fBuffer[2]));
    if (!fClient->writeReadData(server, size, fBuffer,flags,errorCode, status))
      return kMessageError;
  }
  catch (std::exception& e) {
    fClientError = e.what();
    return kClient;
  }
  return retval;
}

//____________________________________________________________________
void
Rcuxx::Fee::SetDebug(int id, int lvl)
{
  Rcu::SetDebug(id, lvl);
  switch (id) {
  case kBackend: fDebug = lvl;  break;
  default:       break;
  }
}

//____________________________________________________________________
std::string
Rcuxx::Fee::ErrorString(unsigned int code)
{
  switch (code) {
  case kNotEnough: 
    return std::string("Not enough data returned");
  case kInconsistentSizes:
    return std::string("Inconsistent size when reading data memory");
  case kUnknownMemory:
    return std::string("Unknown memory bank addressed");
  case kNoName:
    return std::string("No server name specified");
  case kNoStart:
    return std::string("Couldn't start client");
  case kMessageError:
    return std::string("Message error");
  case kClient:
    return std::string(fClientError);
  }
  return std::string();
}

//====================================================================
//
// EOF
//
