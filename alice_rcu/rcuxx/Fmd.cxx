//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    rcuxx/Fmd.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:04:29 2006
    @brief   Implementation of FMD interface 
*/
#ifndef RCUXX_FMD_H
# include <rcuxx/Fmd.h>
#endif
#ifndef __IOSTREAM__
# include <iostream>
#endif
#include "DebugGuard.h"

namespace
{
  bool fDebug = false;
}
//====================================================================
Rcuxx::FmdRegister::FmdRegister(const char* name, unsigned int instr, 
		RcuIMEM& imem, RcuRMEM& rmem, bool clear, bool submit, 
		bool bcast) 
  : BcRegister(name, 0x15, imem, rmem, clear, submit, true, bcast), 
    fSub((instr & 0xff) << 5)
{}

//____________________________________________________________________
void
Rcuxx::FmdRegister::ReadInstructions(unsigned int* data) 
{
  AltroRegister::ReadInstructions(data);
  data[0] += fSub;
}
//____________________________________________________________________
void
Rcuxx::FmdRegister::WriteInstructions(unsigned int* data) 
{
  AltroRegister::WriteInstructions(data);
  data[0] += fSub;
}

//====================================================================
Rcuxx::FmdCommand::FmdCommand(const char* name, unsigned int cmd, 
			      RcuIMEM& imem, bool bcast)
  : BcCommand(name, 0x15, imem, bcast)
{ 
  fData += (1 << cmd); 
  fCmd += (0xa << 5); 
}

//====================================================================
Rcuxx::FmdTopBottom::FmdTopBottom(const char* name, unsigned int addr, 
				  RcuIMEM& imem, RcuRMEM& rmem)
  : FmdRegister(name, addr, imem, rmem, false, true) 
{}

//____________________________________________________________________
void
Rcuxx::FmdTopBottom::Set(unsigned int top, int bot) 
{ 
  fTop    = top; 
  fBottom = (bot < 0 ? fTop : bot);
}

//____________________________________________________________________
void
Rcuxx::FmdTopBottom::Print() const
{
  AltroRegister::Print();
  std::cout << "\tTop:\t\t\t\t\t" << fTop << "\n" 
	    << "\tBottom:\t\t\t\t" << fBottom << std::endl;
}

//====================================================================
void
Rcuxx::FmdClock::Print() const 
{
  AltroRegister::Print();
  std::cout << "\tDivider:\t\t\t\t" << fDivision << "\n" 
	    << "\tPhase:\t\t\t\t" << fPhase << std::endl;
}

//====================================================================
void
Rcuxx::FmdHoldWait::Print() const 
{
  AltroRegister::Print();
  std::cout << "\t# of 25ns clock cycles:\t\t" << fData << std::endl;
}

//====================================================================
void
Rcuxx::FmdL0Timeout::Print() const 
{
  AltroRegister::Print();
  std::cout << "\t# of 25ns clock cycles:\t\t" << fData << std::endl;
}

//====================================================================
void
Rcuxx::FmdL1Timeout::Print() const 
{
  AltroRegister::Print();
  std::cout << "\t# of 25ns clock cycles:\t\t" << fData << std::endl;
}

//====================================================================
void
Rcuxx::FmdRange::Print() const 
{
  AltroRegister::Print();
  std::cout << "\tRange:\t\t\t\t" << fMin << " - " << fMax << std::endl;
}

//====================================================================
void
Rcuxx::FmdL0Triggers::Print() const 
{
  AltroRegister::Print();
  std::cout << "\t# of L0 triggers recieved:\t\t" << fData << std::endl;
}

//====================================================================
void
Rcuxx::FmdL1Triggers::Print() const 
{
  AltroRegister::Print();
  std::cout << "\t# of L1 triggers recieved:\t\t" << fData << std::endl;
}

//====================================================================
void
Rcuxx::FmdStatus::Get() 
{ 
  fCalOn        = fData & 0x01; 
  fTriggerBusy  = fData & 0x02;
  fDacBusy      = fData & 0x04;
  fReadoutBusy  = fData & 0x08;
  fIncompleteRo = fData & 0x10;
}

//____________________________________________________________________
void
Rcuxx::FmdStatus::Print() const 
{
  AltroRegister::Print();
  std::cout << std::boolalpha 
	    << "\tCalibration pulse on:\t\t" << fCalOn << "\n" 
	    << "\tTrigger FSM busy:\t\t\t" << fTriggerBusy << "\n" 
	    << "\tDAC FSM busy:\t\t\t" << fDacBusy << "\n" 
	    << "\tRead-out FSM busy:\t\t\t" << fReadoutBusy << "\n"
	    << "\tIncomplete Read-out:\t\t\t" << fIncompleteRo << std::endl;
}

//====================================================================
Rcuxx::Fmd::Fmd(Rcu& rcu) 
{
  fChangeDacs  = new FmdCommand("Change DAC's", 0, *(rcu.IMEM()));
  fFakeTrigger = new FmdCommand("Fake trigger", 1, *(rcu.IMEM()));
  fSoftReset   = new FmdCommand("Reset FSMs", 3, *(rcu.IMEM()));
  fPulserOn    = new FmdCommand("Pulser on", 4, *(rcu.IMEM()));
  fPulserOff   = new FmdCommand("Pulser off", 5, *(rcu.IMEM()));

  fStatus               = new FmdStatus(*(rcu.IMEM()), *(rcu.RMEM()));
  fShapeBias            = new FmdShapeBias(*(rcu.IMEM()), *(rcu.RMEM()));
  fVFP 		        = new FmdVFP(*(rcu.IMEM()), *(rcu.RMEM()));
  fVFS          	= new FmdVFS(*(rcu.IMEM()), *(rcu.RMEM()));
  fPulser               = new FmdPulser(*(rcu.IMEM()), *(rcu.RMEM()));
  fShiftClock           = new FmdShiftClock(*(rcu.IMEM()), *(rcu.RMEM()));
  fSampleClock          = new FmdSampleClock(*(rcu.IMEM()), *(rcu.RMEM()));
  fRange                = new FmdRange(*(rcu.IMEM()), *(rcu.RMEM()));
  fHoldWait             = new FmdHoldWait(*(rcu.IMEM()), *(rcu.RMEM()));
  fL0Timeout            = new FmdL0Timeout(*(rcu.IMEM()), *(rcu.RMEM()));
  fL1Timeout            = new FmdL1Timeout(*(rcu.IMEM()), *(rcu.RMEM()));
  fL0Triggers           = new FmdL0Triggers(*(rcu.IMEM()), *(rcu.RMEM()));
  fL1Triggers           = new FmdL1Triggers(*(rcu.IMEM()), *(rcu.RMEM()));
  SetAddress(0);
}

//____________________________________________________________________
void 
Rcuxx::Fmd::SetBroadcast() 
{
  fChangeDacs->SetBroadcast();
  fFakeTrigger->SetBroadcast();
  fPulserOn->SetBroadcast();
  fPulserOff->SetBroadcast();
  fStatus->SetBroadcast();
  fShapeBias->SetBroadcast();
  fVFP->SetBroadcast();
  fVFS->SetBroadcast();
  fPulser->SetBroadcast();
  fShiftClock->SetBroadcast();
  fSampleClock->SetBroadcast();
  fRange->SetBroadcast();
  fHoldWait->SetBroadcast();
  fL0Timeout->SetBroadcast();
  fL1Timeout->SetBroadcast();
  fL0Triggers->SetBroadcast();
  fL1Triggers->SetBroadcast();
}

//____________________________________________________________________
void
Rcuxx::Fmd::SetAddress(unsigned int board, unsigned int, unsigned int) 
{
  fChangeDacs->SetAddress(board, 0, 0);
  fFakeTrigger->SetAddress(board, 0, 0);
  fPulserOn->SetAddress(board, 0, 0);
  fPulserOff->SetAddress(board, 0, 0);
  fStatus->SetAddress(board, 0, 0);
  fShapeBias->SetAddress(board, 0, 0);
  fVFP->SetAddress(board, 0, 0);
  fVFS->SetAddress(board, 0, 0);
  fPulser->SetAddress(board, 0, 0);
  fShiftClock->SetAddress(board, 0, 0);
  fSampleClock->SetAddress(board, 0, 0);
  fRange->SetAddress(board, 0, 0);
  fHoldWait->SetAddress(board, 0, 0);
  fL0Timeout->SetAddress(board, 0, 0);
  fL1Timeout->SetAddress(board, 0, 0);
  fL0Triggers->SetAddress(board, 0, 0);
  fL1Triggers->SetAddress(board, 0, 0);
}

//____________________________________________________________________
unsigned int
Rcuxx::Fmd::Update() 
{
  unsigned int ret;
  if ((ret = fStatus->Update())) 	return ret;
  if ((ret = fShapeBias->Update())) 	return ret;
  if ((ret = fVFP->Update())) 		return ret;
  if ((ret = fVFS->Update())) 		return ret;
  if ((ret = fPulser->Update())) 	return ret;
  if ((ret = fShiftClock->Update())) 	return ret;
  if ((ret = fSampleClock->Update())) 	return ret;
  if ((ret = fRange->Update())) 	return ret;
  if ((ret = fHoldWait->Update())) 	return ret;
  if ((ret = fL0Timeout->Update())) 	return ret;
  if ((ret = fL1Timeout->Update())) 	return ret;
  if ((ret = fL0Triggers->Update())) 	return ret;
  if ((ret = fL1Triggers->Update())) 	return ret;
  return ret;
}

//____________________________________________________________________
void
Rcuxx::Fmd::Print() const
{
  fStatus->Print();
  fShapeBias->Print();
  fVFP->Print();
  fVFS->Print();
  fPulser->Print();
  fShiftClock->Print();
  fSampleClock->Print();
  fRange->Print();
  fHoldWait->Print();
  fL0Timeout->Print();
  fL1Timeout->Print();
  fL0Triggers->Print();
  fL1Triggers->Print();
}
//____________________________________________________________________
void
Rcuxx::Fmd::SetDebug(bool debug)
{
  fDebug = debug;
}

//____________________________________________________________________
//
// EOF
//
