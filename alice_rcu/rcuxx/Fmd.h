// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    rcuxx/Fmd.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:04:46 2006
    @brief   Declaration of FMD interface 
*/
#ifndef RCUXX_FMD_H
#define RCUXX_FMD_H
#ifndef RCUXX_BC_H
# include <rcuxx/Bc.h>
#endif

namespace Rcuxx 
{  
  /** @defgroup rcuxx_fmd FMD Interace classes 
      @ingroup rcuxx 
   */
  //====================================================================
  /** @struct FmdRegister rcuxx/Fmd.h <rcuxx/Fmd.h>
   *  @brief Interface to FMD register Register
   *  @ingroup rcuxx_fmd
   */
  struct FmdRegister : public BcRegister 
  {
    /** Register sub-address*/
    unsigned int fSub;
    /** Constructor
	@param name Name or register 
	@param instr Instruction code
	@param imem Reference to instruction memory
	@param rmem Reference to result memory
	@param clear Whether to enable a clear command 
	@param submit Whether to enable a submit command 
	@param bcast Whether to allow broadcast */
    FmdRegister(const char* name, unsigned int instr, 
		RcuIMEM& imem, RcuRMEM& rmem, bool clear, bool submit, 
		bool bcast=true);
    /** Make instructions in @a val for reading the register
	@param val On return, instructions to read the register */
    virtual void ReadInstructions(unsigned int* val);
    /** Make instructions in @a val for writing to the register
	@param val On return, instructions to write to the register */
    virtual void WriteInstructions(unsigned int* val);
    // virtual unsigned int Read(unsigned int& val);
    // virtual unsigned int Write(unsigned int val);
  };

  //====================================================================
  /** @struct FmdCommand rcuxx/Fmd.h <rcuxx/Fmd.h>
   *  @brief Interface to FMD register Command
   *  @ingroup rcuxx_fmd
   */
  struct FmdCommand : public BcCommand
  {
    /** Register sub-address*/
    unsigned int  fSub;
    /** Constructor 
	@param name Name of commad 
	@param cmd Command code
	@param imem Reference to instruction memory
	@param bcast Whether to allow broadcast  */
    FmdCommand(const char* name, unsigned int cmd, RcuIMEM& imem, 
	       bool bcast=true);
  };
  
  //====================================================================
  /** @struct FmdTopBottom rcuxx/Fmd.h <rcuxx/Fmd.h>
   *  @brief Base class for interfaces to FMD register with top/bottom
   *  partition 
   *  @ingroup rcuxx_fmd
   */
  struct FmdTopBottom : public FmdRegister 
  {
    /** Constructor 
	@param name Register name 
	@param addr Register address
	@param imem Reference to instruction memory 
	@param rmem Reference to result memory */
    FmdTopBottom(const char* name, unsigned int addr, 
		 RcuIMEM& imem, RcuRMEM& rmem);
    /** Decode read data */ 
    void Get() { fTop = fData & 0xff; fBottom = (fData >> 8) & 0xff; }
    /** Encode set data */ 
    void Set() { fData = ((fTop & 0xff) + ((fBottom & 0xff) << 8)); }
    /** @return value of top 4 chips */
    virtual unsigned int Top() const { return fTop; }
    /** @return value of bottom 4 chips */
    virtual unsigned int Bottom() const { return fBottom; }
    /** Set top value */ 
    virtual void SetTop(unsigned int val) { fTop = val; }
    /** Set bottom value */ 
    virtual void SetBottom(unsigned int val) { fBottom = val; }
    /** Set both values 
	@param top Top value, if @a bot is negative, also bottom value
	@param bot If not negative, bottom value, otherwise use @a top */
    void Set(unsigned int top, int bot=-1);
    /** Print contents of register */
    virtual void Print() const;
  protected:
    /** Top value */
    unsigned int fTop;
    /** Bottom value */
    unsigned int fBottom;
  };    
		 
  //====================================================================
  /** @struct FmdShapeBias rcuxx/Fmd.h <rcuxx/Fmd.h>
   *  @brief Interface to FMD register Shape Bias
   *  @ingroup rcuxx_fmd
   */
  struct FmdShapeBias : public FmdTopBottom
  {
    /** Constructor 
	@param imem Reference to instruction memory 
	@param rmem Reference to result memory */
    FmdShapeBias(RcuIMEM& imem, RcuRMEM& rmem)
      : FmdTopBottom("Shape bias", 0x0, imem, rmem)
    {}
  };

  //____________________________________________________________________
  /** @struct FmdVFP rcuxx/Fmd.h <rcuxx/Fmd.h>
   *  @brief Interface to FMD register VFP
   *  @ingroup rcuxx_fmd
   */
  struct FmdVFP : public FmdTopBottom
  {
    /** Constructor 
	@param imem Reference to instruction memory 
	@param rmem Reference to result memory */
    FmdVFP(RcuIMEM& imem, RcuRMEM& rmem)
      : FmdTopBottom("VFP", 0x1, imem, rmem)
    {}
  };

  //____________________________________________________________________
  /** @struct FmdVFS rcuxx/Fmd.h <rcuxx/Fmd.h>
   *  @brief Interface to FMD register VFS
   *  @ingroup rcuxx_fmd
   */
  struct FmdVFS : public FmdTopBottom
  {
    /** Constructor 
	@param imem Reference to instruction memory 
	@param rmem Reference to result memory */
    FmdVFS(RcuIMEM& imem, RcuRMEM& rmem)
      : FmdTopBottom("VFS", 0x2, imem, rmem)
    {}
  };

  //____________________________________________________________________
  /** @struct FmdPulser rcuxx/Fmd.h <rcuxx/Fmd.h>
   *  @brief Interface to FMD register Pulser
   *  @ingroup rcuxx_fmd
   */
  struct FmdPulser : public FmdTopBottom
  {
    /** Constructor 
	@param imem Reference to instruction memory 
	@param rmem Reference to result memory */
    FmdPulser(RcuIMEM& imem, RcuRMEM& rmem)
      : FmdTopBottom("Pulser", 0x3 , imem, rmem)
    {}
    /** @return value of top 4 chips */
    unsigned int Value() const { return fTop; }
    /** @return value of bottom 4 chips */
    unsigned int Test() const { return fBottom; }
    /** Set top value */ 
    void SetValue(unsigned int val) { fTop = val; }
    /** Set bottom value */ 
    void SetTest(unsigned int val) { fBottom = val; }
  protected:
    /** @return Top value */
    unsigned int Top() const { return fTop; }
    /** @return Bottom value */
    unsigned int Bottom() const { return fTop; }
    /** @param v Top value */
    void SetTop(unsigned int v)  { fTop = v; }
    /** @param v Bottom value */
    void SetBottom(unsigned int v) { fBottom = v; }
  };

  //____________________________________________________________________
  /** @struct FmdClock rcuxx/Fmd.h <rcuxx/Fmd.h>
   *  @brief Base class for interfaces to FMD clock register 
   *  @ingroup rcuxx_fmd
   */
  struct FmdClock : public FmdRegister 
  {
    /** Constructor 
	@param name Name of register 
	@param addr Address of register 
	@param imem Reference to instruction memory 
	@param rmem Reference to result memory */
    FmdClock(const char* name, unsigned int addr, RcuIMEM& imem, RcuRMEM& rmem)
      : FmdRegister(name, addr, imem, rmem, false, true)
    {}
    /** Decode read value */
    void Get() { fPhase = fData & 0xff; fDivision = (fData >> 8) & 0xff;  }
    /** Encode set value */
    void Set() { fData = ((fPhase & 0xff) + ((fDivision & 0xff) << 8)); }
    /** @return division factor */
    unsigned int Division() const { return fDivision; }
    /** Set division factor */
    void SetDivision(unsigned int val) { fDivision = val; }
    /** @return phase */
    unsigned int Phase() const { return fPhase; }
    /** Set the phase shift */
    void SetPhase(unsigned int val) { fPhase = val; }
    /** Print contents of register */
    virtual void Print() const;
  protected:
    /** division factor */
    unsigned int fDivision;
    /**  phase */
    unsigned int fPhase;
  };

  //____________________________________________________________________
  /** @struct FmdShiftClock rcuxx/Fmd.h <rcuxx/Fmd.h>
   *  @brief Interface to FMD register ShiftClock
   *  @ingroup rcuxx_fmd
   */
  struct FmdShiftClock : public FmdClock
  {
    /** Constructor 
	@param imem Reference to instruction memory 
	@param rmem Reference to result memory */
    FmdShiftClock(RcuIMEM& imem, RcuRMEM& rmem)
      : FmdClock("VA1 Shift clock", 0x4, imem, rmem)
    {}
  };

  //____________________________________________________________________
  /** @struct FmdSampleClock rcuxx/Fmd.h <rcuxx/Fmd.h>
   *  @brief Interface to FMD register SampleClock
   *  @ingroup rcuxx_fmd
   */
  struct FmdSampleClock : public FmdClock 
  {
    /** Constructor 
	@param imem Reference to instruction memory 
	@param rmem Reference to result memory */
    FmdSampleClock(RcuIMEM& imem, RcuRMEM& rmem)
      : FmdClock("ALTRO Sample clock", 0x5, imem, rmem)
    {}
  };

  //____________________________________________________________________
  /** @struct FmdHoldWait rcuxx/Fmd.h <rcuxx/Fmd.h>
   *  @brief Interface to FMD register HoldWait
   *  @ingroup rcuxx_fmd
   */
  struct FmdHoldWait : public FmdRegister 
  {
    /** Constructor 
	@param imem Reference to instruction memory 
	@param rmem Reference to result memory */
    FmdHoldWait(RcuIMEM& imem, RcuRMEM& rmem)
      : FmdRegister("Hold wait", 0x6, imem, rmem, false, true)
    {}
    /** Decode read data */
    void Get() { fData &= 0xffff; }
    /** Encode set data */
    void Set() { fData &= 0xffff; }
    /** @return number of clock cycles for time-out */
    unsigned int Clocks() const { return fData; }
    /** Set number of clock cycles for time-out */
    void SetClocks(unsigned int val) { fData = val; }
    /** Print contents of register */
    virtual void Print() const;
  };

  //____________________________________________________________________
  /** @struct FmdL0Timeout rcuxx/Fmd.h <rcuxx/Fmd.h>
   *  @brief Interface to FMD register L0Timeout
   *  @ingroup rcuxx_fmd
   */
  struct FmdL0Timeout : public FmdRegister 
  {
    /** Constructor 
	@param imem Reference to instruction memory 
	@param rmem Reference to result memory */
    FmdL0Timeout(RcuIMEM& imem, RcuRMEM& rmem)
      : FmdRegister("L1 timeout", 0x7, imem, rmem, false, true)
    {}
    /** Decode read data */
    void Get() { fData &= 0xffff; }
    /** Encode set data */
    void Set() { fData &= 0xffff; }
    /** @return number of clock cycles for time-out */
    unsigned int Clocks() const { return fData; }
    /** Set number of clock cycles for time-out */
    void SetClocks(unsigned int val) { fData = val; }
    /** Print contents of register */
    virtual void Print() const;
  };

  //____________________________________________________________________
  /** @struct FmdL1Timeout rcuxx/Fmd.h <rcuxx/Fmd.h>
   *  @brief Interface to FMD register L1Timeout
   *  @ingroup rcuxx_fmd
   */
  struct FmdL1Timeout : public FmdRegister 
  {
    /** Constructor 
	@param imem Reference to instruction memory 
	@param rmem Reference to result memory */
    FmdL1Timeout(RcuIMEM& imem, RcuRMEM& rmem)
      : FmdRegister("L2 timeout", 0x8, imem, rmem, false, true)
    {}
    /** Decode read data */
    void Get() { fData &= 0xffff; }
    /** Encode set data */
    void Set() { fData &= 0xffff; }
    /** @return number of clock cycles for time-out */
    unsigned int Clocks() const { return fData; }
    /** Set number of clock cycles for time-out */
    void SetClocks(unsigned int val) { fData = val; }
    /** Print contents of register */
    virtual void Print() const;
  };

  //____________________________________________________________________
  /** @struct FmdRange rcuxx/Fmd.h <rcuxx/Fmd.h>
   *  @brief Interface to FMD register Range
   *  @ingroup rcuxx_fmd
   */
  struct FmdRange : public FmdRegister 
  {
    /** Constructor 
	@param imem Reference to instruction memory 
	@param rmem Reference to result memory */
    FmdRange(RcuIMEM& imem, RcuRMEM& rmem)
      : FmdRegister("Strip range", 0x9, imem, rmem, false, true)
    {}
    /** Decode read data */
    void Get() { fMin = (fData >> 8) & 0xff; fMax = fData & 0xff; }
    /** Encode read data */
    void Set() { fData = (((fMin & 0xff) << 8) + (fMax & 0xff)); }
    /** @return minimum strip number */
    unsigned int Min() const { return fMin; }
    /** Set minimum strip number */
    void SetMin(unsigned int val){ fMin = val; }
    /** @return maximum strip number */
    unsigned int Max() const { return fMax; }
    /** Set maximum strip number */
    void SetMax(unsigned int val){ fMax = val; }
    /** Print contents of register */
    virtual void Print() const;
  protected:
    /** Minimum strip number */
    unsigned int fMin;
    /** Maximum strip number */
    unsigned int fMax;
  };

  //____________________________________________________________________
  /** @struct FmdL0Triggers rcuxx/Fmd.h <rcuxx/Fmd.h>
   *  @brief Interface to FMD register L0Triggers
   *  @ingroup rcuxx_fmd
   */
  struct FmdL0Triggers : public FmdRegister 
  {
    /** Constructor 
	@param imem Reference to instruction memory 
	@param rmem Reference to result memory */
    FmdL0Triggers(RcuIMEM& imem, RcuRMEM& rmem)
      : FmdRegister("L0 Triggers", 12, imem, rmem, false, false)
    {}
    /** Decode read data */
    void Get() { fData &= 0xfffff; }
    /** @return number of triggers */
    unsigned int Recieved() const { return fData; }
    /** Print contents of register */
    virtual void Print() const;
  };

  //____________________________________________________________________
  /** @struct FmdL1Triggers rcuxx/Fmd.h <rcuxx/Fmd.h>
   *  @brief Interface to FMD register L1Triggers
   *  @ingroup rcuxx_fmd
   */
  struct FmdL1Triggers : public FmdRegister 
  {
    /** Constructor 
	@param imem Reference to instruction memory 
	@param rmem Reference to result memory */
    FmdL1Triggers(RcuIMEM& imem, RcuRMEM& rmem)
      : FmdRegister("L1 Triggers", 13, imem, rmem, false, false)
    {}
    /** Decode read data */
    void Get() { fData &= 0xfffff; }
    /** @return number of triggers */
    unsigned int Recieved() const { return fData; }
    /** Print contents of register */
    virtual void Print() const;
  };

  //____________________________________________________________________
  /** @struct FmdStatus rcuxx/Fmd.h <rcuxx/Fmd.h>
   *  @brief Interface to FMD register Status
   *  @ingroup rcuxx_fmd
   */
  struct FmdStatus : public FmdRegister 
  {
    FmdStatus(RcuIMEM& imem, RcuRMEM& rmem)
      : FmdRegister("Status", 14, imem, rmem, false, false)
    {}
    /** Decode read valies */
    void Get();
    /** @return @c true if calibration pulse is on   */
    bool IsCalOn()       const { return fCalOn; }
    /** @return @c true if trigger FSM is busy  */
    bool IsTriggerBusy() const { return fTriggerBusy; }
    /** @return @c true if DAC FSM is busy  */
    bool IsDacBusy()     const { return fDacBusy; }
    /** @return @c true if readout FSM is busy  */
    bool IsReadoutBusy() const { return fReadoutBusy; }
    bool IsIncompleteRo() const { return fIncompleteRo; }
    /** Print contents of register */
    virtual void Print() const;
  protected:
    /** Whether cal pulse is on */
    bool fCalOn;
    /** Whether trigger FSM is busy */
    bool fTriggerBusy;
    /** Whether DAC FSM is busy */
    bool fDacBusy;
    /** Whether readout FSM is busy */
    bool fReadoutBusy;
    /** Incomplete read-out */
    bool fIncompleteRo;
  };
  

  //====================================================================
  /** @class Fmd rcuxx/Fmd.h <rcuxx/Fmd.h>
      @brief Interface to FMD 
      @ingroup rcuxx_fmd
   */
  class Fmd 
  {
  public:
    /** constructor 
	@param rcu Reference to controlling RCU  */
    Fmd(Rcu& rcu);
    /** Set broad cast mode */
    void SetBroadcast();
    /** Set Address to read/write to/from 
	@param board Board number on bus
	@param chip Chip number (ignored)
	@param channel Channel number (ignored) */
    void SetAddress(unsigned int board, unsigned int chip=0, 
		    unsigned int channel=0);
    /** Update all */
    unsigned int Update();
    /** Print all */
    void Print() const;
    /** Set the debuging stuff */
    virtual void SetDebug(bool debug);

    /** @return Pointer to ShapeBias register interface */
    FmdShapeBias*	ShapeBias() const { return fShapeBias; }
    /** @return Pointer to VFS register interface */
    FmdVFS*		VFS() const { return fVFS; }
    /** @return Pointer to VFP register interface */
    FmdVFP*		VFP() const { return fVFP; }
    /** @return Pointer to Pulser register interface */
    FmdPulser*		Pulser() const { return fPulser; }
    /** @return Pointer to ShiftClock register interface */
    FmdShiftClock*	ShiftClock() const { return fShiftClock; }
    /** @return Pointer to SampleClock register interface */
    FmdSampleClock*	SampleClock() const { return fSampleClock; }
    /** @return Pointer to HoldWait register interface */
    FmdHoldWait*	HoldWait() const { return fHoldWait; }
    /** @return Pointer to L0Timeout register interface */
    FmdL0Timeout*	L0Timeout() const { return fL0Timeout; }
    /** @return Pointer to L1Timeout register interface */
    FmdL1Timeout*	L1Timeout() const { return fL1Timeout; }
    /** @return Pointer to Range register interface */
    FmdRange*		Range() const { return fRange; }
    /** @return Pointer to L0Triggers register interface */
    FmdL0Triggers*	L0Triggers() const { return fL0Triggers; }
    /** @return Pointer to L1Triggers register interface */
    FmdL1Triggers*	L1Triggers() const { return fL1Triggers; }
    /** @return Pointer to Status register interface */
    FmdStatus*		Status() const { return fStatus; }
    // 
    /** This commands initiates the process of changing the control
	voltages for the VA1.  Note, one should set the registers, and
	then execute this command to change any of the voltages.
	Simply changing the register values does not suffice. 
	@return Pointer to Command command interface */
    AltroCommand*       ChangeDacs() const { return fChangeDacs; } 
    /** Issue a fake trigger sequence - for testing
	@return Pointer to Command command interface */
    AltroCommand*       FakeTrigger() const { return fFakeTrigger; } 
    /** Turn on calibration pulser.  Note, that this introduces a
	fixed additional delay of @f$ 1.2\mu s@f$ - corresponding to
	the L0 delay from bunch crossing. 
	@return Pointer to Command command interface */
    AltroCommand*       PulserOn() const { return fPulserOn; } 
    /** Turn calibration pulser off. 
	@return Pointer to Command command interface */
    AltroCommand*       PulserOff() const { return fPulserOff; } 
    /** Reset all FSM's in the FMD BC part. 
	@return Pointer to Command command interface */
    AltroCommand*       SoftReset() const { return fSoftReset; } 
  protected:
    // 
    /** @return Pointer to ShapeBias register interface */
    FmdShapeBias*	fShapeBias;
    /** @return Pointer to VFS register interface */
    FmdVFS*		fVFS;
    /** @return Pointer to VFP register interface */
    FmdVFP*		fVFP;
    /** @return Pointer to Pulser register interface */
    FmdPulser*		fPulser;
    /** @return Pointer to ShiftClock register interface */
    FmdShiftClock*	fShiftClock;
    /** @return Pointer to SampleClock register interface */
    FmdSampleClock*	fSampleClock;
    /** @return Pointer to HoldWait register interface */
    FmdHoldWait*	fHoldWait;
    /** @return Pointer to L0Timeout register interface */
    FmdL0Timeout*	fL0Timeout;
    /** @return Pointer to L1Timeout register interface */
    FmdL1Timeout*	fL1Timeout;
    /** @return Pointer to Range register interface */
    FmdRange*		fRange;
    /** @return Pointer to L0Triggers register interface */
    FmdL0Triggers*	fL0Triggers;
    /** @return Pointer to L1Triggers register interface */
    FmdL1Triggers*	fL1Triggers;
    /** @return Pointer to Status register interface */
    FmdStatus*		fStatus;
    // 
    /** @return Pointer to Command command interface */
    AltroCommand*       fChangeDacs; 
    /** @return Pointer to Command command interface */
    AltroCommand*       fFakeTrigger; 
    /** @return Pointer to Command command interface */
    AltroCommand*       fPulserOn; 
    /** @return Pointer to Command command interface */
    AltroCommand*       fPulserOff; 
    /** @return Pointer to Command command interface */
    AltroCommand*       fSoftReset; 
  };
}

#endif

//____________________________________________________________________
//
// EOF
//

