//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    rcuxx/Rcu.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:33:05 2006
    @brief   Implementation of RCU interface 
*/
#ifdef HAVE_CONFIG_H
# include "config.h"
#endif
#ifndef RCUXX_RCU_H
# include <rcuxx/Rcu.h>
#endif
#ifndef __IOSTREAM__
# include <iostream>
#endif
#ifndef __SSTREAM__
# include <sstream>
#endif
#ifndef __FSTREAM__
# include <fstream>
#endif
#ifndef __IOMANIP__
# include <iomanip>
#endif
#ifndef __CSTDARG__
# include <cstdarg>
#endif
#ifndef __STDEXCEPT__
# include <stdexcept>
#endif
#include "DebugGuard.h"
#ifdef HAVE_U2F
# include <rcuxx/U2F.h>
#endif
#ifdef HAVE_FEECLIENT
# include <rcuxx/Fee.h>
#endif
#ifdef HAVE_DATE
# include <rcuxx/Rorc.h>
#endif

namespace 
{
  bool fDebug = false;
  std::string gVersion = VERSION;
  std::string gPackage = PACKAGE;
  std::string gPackageString = PACKAGE_STRING;
}

//====================================================================
void
Rcuxx::Register::Print() const 
{
  std::cout << "Contents of register " << fName << std::endl;
}

//====================================================================
unsigned int
Rcuxx::RcuCommand::Commit(unsigned int arg) 
{
  DebugGuard g(fDebug, "RcuCommand::Commit(%d) <0x%x>", 
	       (fHasArgument ? arg : 0), fCmd);
  fArg = (!fHasArgument ? 0 : arg);
  return fRcu.ExecCommand(fCmd + fArg);
}


//====================================================================
unsigned int
Rcuxx::RcuRegister::Update()
{
  unsigned ret = 0, dat;
  if ((ret = fMemory.Update(fOffset, 1))) return ret;
  fMemory.Get(fOffset, 1, &dat);
  Decode(dat);
  return ret;
}

//____________________________________________________________________
unsigned int
Rcuxx::RcuRegister::Clear() 
{ 
  unsigned int ret = 0;
  if (fClear) {
    if ((ret = fClear->Commit())) return ret;
    ret = Update();
  }
  return ret;
}

//____________________________________________________________________
unsigned int
Rcuxx::RcuRegister::Commit() 
{
  if (!fSubmit) return 0;
  DebugGuard g(fDebug, "Register::Commit() [%d]", fOffset);
  unsigned int dat = Encode();
  DebugGuard::Message(fDebug, "Value is 0x%08lx", dat);
  fMemory.Set(fOffset, 1, &dat);
  return fMemory.Commit(fOffset, 1);
}

//====================================================================
void
Rcuxx::RcuERRST::Decode(unsigned int dat) 
{ 
  fPattern	= (dat & (1 << 0));
  fAbort	= (dat & (1 << 1));
  fTimeout	= (dat & (1 << 2));
  fAltro	= (dat & (1 << 3));
  fHWAdd	= (dat & (1 << 4));
  fBusy		= (dat & (1 << 31));
  fWhere    	= (dat >> 11);
}

//____________________________________________________________________
void
Rcuxx::RcuERRST::Print() const
{
  RcuRegister::Print();
  std::cout << std::boolalpha 
	    << "\tPattern:\t\t\t" << fPattern << "\n"
	    << "\tAbort:\t\t\t\t" << fAbort << "\n"
	    << "\tTimeout:\t\t\t" << fTimeout << "\n"
	    << "\tAltro:\t\t\t\t" << fAltro << "\n"
	    << "\tHWAdd:\t\t\t\t" << fHWAdd << "\n"
	    << "\tBusy:\t\t\t\t" << fBusy << "\n"
	    << "\tWhere:\t\t\t\t0x" << std::hex << fWhere << std::dec 
	    << std::endl;
}

//====================================================================
void
Rcuxx::RcuTRCNT::Decode(unsigned int dat) 
{ 
  fAccepted = dat & 0xffff;
  fReceived = (dat >> 16) & 0xffff;
}

//____________________________________________________________________
void
Rcuxx::RcuTRCNT::Print() const
{
  RcuRegister::Print();
  std::cout << "\tReceived:\t\t\t" << fReceived << "\n"
	    << "\tAccepted:\t\t\t" << fAccepted << std::endl;
}

//====================================================================
void
Rcuxx::RcuLWADD::Decode(unsigned int dat) 
{ 
  fBank1 = (dat & 0xff);
  fBank2 = (dat & 0xff00) >> 8;
}

//____________________________________________________________________
void
Rcuxx::RcuLWADD::Print() const
{
  RcuRegister::Print();
  std::cout << "\tWords in DMEM1:\t\t\t" << fBank1 << "\n"
	    << "\tWords in DMEM2:\t\t\t" << fBank2 << std::endl;
}

//====================================================================
void
Rcuxx::RcuIRADD::Decode(unsigned int dat) 
{ 
  fIRADD = (dat & 0xfffff);
}

//____________________________________________________________________
void
Rcuxx::RcuIRADD::Print() const
{
  RcuRegister::Print();
  std::cout << "\tIRADD:\t\t\t\t0x" << std::hex << fIRADD << std::dec 
	    << std::endl;
}

//====================================================================
void
Rcuxx::RcuIRDAT::Decode(unsigned int dat) 
{ 
  fIRDAT = (dat & 0xfffff);
}

//____________________________________________________________________
void
Rcuxx::RcuIRDAT::Print() const
{
  RcuRegister::Print();
  std::cout << "\tIRDAT:\t\t\t\t0x" << std::hex << fIRDAT << std::dec 
	    << std::endl;
}

//====================================================================
Rcuxx::RcuEVWORD::RcuEVWORD(RcuMemory& mem, size_t offset, RcuCommand* clear)
  : RcuRegister(mem, offset, "EVWORD", clear, false)
{
  fView1 = fView2 = 0;
}

//____________________________________________________________________
void
Rcuxx::RcuEVWORD::Decode(unsigned int dat) 
{ 
  fView1 = (dat >> 16) & 0xffff;
  fView2 = dat & 0xffff;
}

//____________________________________________________________________
void
Rcuxx::RcuEVWORD::Print() const
{
  RcuRegister::Print();
  for (size_t i = 1; i <= 2; i++) {
    std::cout << "\tBranch " << i << ":\t\t\t0x" << std::hex << View(i) 
	      << std::dec << " - ";
    if (HasTrigger(i)) std::cout << "trigger ";
    if (IsStart(i))    std::cout << "start ";
    if (IsEnd(i))      std::cout << "end ";
    if (HasData(1,i))  std::cout << "data1 ";
    if (HasData(1,i))  std::cout << "data2 ";
    std::cout << std::endl;
  }
}

//====================================================================
void
Rcuxx::RcuACTFEC::SyncToACL(const RcuMemory& acl) 
{ 
  const RcuMemory::Cache_t& mem = acl.Data();
  fValue = 0;
  for (size_t i = 0; i < sizeof(fValue)*8 && i < mem.size() / 8; i++) {
    for (size_t j = 0; j < 8; j++) {
      if (mem[i * 8 + j] != 0) {
	SetOn(i, true);
	break;
      }
    }
  }
}

//____________________________________________________________________
void
Rcuxx::RcuACTFEC::ACLSync(RcuMemory& acl, unsigned int mask) const
{
  RcuMemory::Cache_t& mem = acl.Data();
  for (size_t i = 0; i < sizeof(fValue)*8 && i < mem.size() / 8; i++) {
    if (!IsOn(i)) continue;
    for (size_t j = 0; j < 8; j++) mem[i * 8 + j] = mask;
  }
}

  
//____________________________________________________________________
void
Rcuxx::RcuACTFEC::Print() const 
{
  RcuRegister::Print();
  std::cout << "\tBranch A:\t" << std::flush;
  size_t i = 0;
  for (; i < sizeof(fValue)*8 / 2; i++) 
    std::cout << (IsOn(i) ? '*' : '-');
  std::cout << "\n\tBranch B:\t" << std::flush;
  for (; i < sizeof(fValue)*8; i++) 
    std::cout << (IsOn(i) ? '*' : '-');
  std::cout <<  std::endl;
}

//====================================================================
void
Rcuxx::RcuRDOFEC::Decode(unsigned int dat) 
{ 
  fValue = dat;
}

//____________________________________________________________________
void
Rcuxx::RcuRDOFEC::Print() const
{
  RcuRegister::Print();
  std::cout << "\tMask:\t\t\t\t0x" << std::hex 
	    << fValue << std::dec << std::endl;
}

//====================================================================
void
Rcuxx::RcuTRCFG1::Decode(unsigned int dat) 
{ 
  DebugGuard g(fDebug, "RcuTRCFG1::Decode(0x%08lx)", dat);
  fTwv = dat & 0x3fff;
  fBMD = ((dat >> 14) & 0x1 ? k8Buffers : k4Buffers);
  switch ((dat >> 15) & 0x3) {
  case 0: fMode = kInternal;  break;
  case 2: fMode = kDerivedL2; break;
  case 3: fMode = kExternal;  break;
  }
  fOpt   = (dat >> 17) & 0x1;
  fPop   = (dat >> 18) & 0x1;
  fRemb  = (dat >> 20) & 0xf;
  fEmpty = (dat >> 24) & 0x1;
  fFull  = (dat >> 25) & 0x1;
  fRptr  = (dat >> 26) & 0x3;
  fWptr  = (dat >> 29) & 0x3;
#if 0
  fOpt   = (dat >> 19) & 0x1;
  fPop   = (dat >> 18) & 0x1;
  fRemb  = (dat >> 17) & 0xf;
  fEmpty = (dat >> 21) & 0x1;
  fFull  = (dat >> 22) & 0x1;
  fRptr  = (dat >> 23) & 0x7;
  fWptr  = (dat >> 26) & 0x7;
#endif
}

//____________________________________________________________________
unsigned int 
Rcuxx::RcuTRCFG1::Encode() 
{ 
  DebugGuard g(fDebug, "RcuTRCFG1::Encode()");
  unsigned int dat = 0;
  dat += fTwv & 0x3fff;
  dat += (fBMD == k8Buffers) ? 0x1 << 14 : 0;
  switch (fMode) {
  case kInternal:  dat += (0 & 0x3) << 15; break;
  case kDerivedL2: dat += (2 & 0x3) << 15; break;
  case kExternal:  dat += (3 & 0x3) << 15; break;
  }
  dat += (fOpt  & 0x1) << 17;
  dat += (fPop  & 0x1) << 18;
  DebugGuard::Message(fDebug, "Returning the value 0x%08lx", dat);
  return dat;
}

//____________________________________________________________________
void
Rcuxx::RcuTRCFG1::Print() const
{
  RcuRegister::Print();
  std::cout << std::boolalpha
	    << "\tBC optimised:\t\t\t" << fOpt << "\n"
	    << "\tPop mode:\t\t\t"     << fPop << "\n"
	    << "\tEmpty:   \t\t\t"     << fEmpty << "\n"
	    << "\tFull:   \t\t\t"      << fFull << "\n"
	    << "\t# buffers:\t\t\t"    << (fBMD == k4Buffers ? 4 : 8) << "\n"
	    << "\tTrigger mode:\t\t\t" 
	    << (fMode == kInternal ? "Interal only" :
		(fMode == kDerivedL2 ? 	"Ext. L1, Int. L2" : 
		 "External only")) << "\n"
	    << "\t# clk. between L1 and L2:\t"  << fTwv << "\n"
	    << "\tWrite pointer:\t\t\t0x" << std::hex << fWptr << "\n"
	    << "\tRead pointer:\t\t\t0x" << std::hex << fRptr << "\n"
	    << "\tFree buffers:\t\t\t0x" << std::dec << fRemb << std::endl;
}

//====================================================================
void
Rcuxx::RcuTRCFG2::Decode(unsigned int dat) 
{ 
  fMode     = (dat & 0x1 ? kPush : kPop);
  fEnableHT = (dat & 0x2);
}

//____________________________________________________________________
unsigned int 
Rcuxx::RcuTRCFG2::Encode() 
{ 
  return (fMode == kPush ? 1 : 0) + (fEnableHT ? 2 : 0);
}

//____________________________________________________________________
void
Rcuxx::RcuTRCFG2::Print() const
{
  RcuRegister::Print();
  std::cout << std::boolalpha
	    << "\tMode:\t\t\t\t" 
	    << (fMode == kPop ? "Popped" : "Pushed") << "\n"
	    << "\tHW trigger enabled:\t\t" << fEnableHT << std::endl;
}

//====================================================================
void
Rcuxx::RcuFMIREG::Decode(unsigned int dat) 
{ 
  fData = dat & 0xff;
}

//____________________________________________________________________
unsigned int 
Rcuxx::RcuFMIREG::Encode() 
{ 
  return fData & 0xff;
}

//____________________________________________________________________
void
Rcuxx::RcuFMIREG::Print() const
{
  RcuRegister::Print();
  std::cout << "\tData:\t\t\t\t0x" << std::hex << fData 
	    << std::dec << std::endl;
}

//====================================================================
void
Rcuxx::RcuFMOREG::Decode(unsigned int dat) 
{ 
  fData = (dat & 0xff);
}

//____________________________________________________________________
unsigned int 
Rcuxx::RcuFMOREG::Encode() 
{ 
  return fData & 0xff;
}

//____________________________________________________________________
void
Rcuxx::RcuFMOREG::Print() const
{
  RcuRegister::Print();
  std::cout << "\tData:\t\t\t\t0x" << std::hex << fData << std::dec 
	    << std::endl;
}

//====================================================================
void
Rcuxx::RcuPMCFG::Decode(unsigned int dat) 
{ 
  fBegin = (dat & 0x3ff);
  fEnd   = ((dat >> 10) & 0x3ff);
}

//____________________________________________________________________
unsigned int 
Rcuxx::RcuPMCFG::Encode() 
{ 
  unsigned int val = 0;
  val += (fBegin & 0x3ff);
  val += (fEnd & 0x3ff) << 10;
  return val;
}

//____________________________________________________________________
void
Rcuxx::RcuPMCFG::Print() const
{
  RcuRegister::Print();
  std::cout << "\tBegin:\t\t\t\t0x" << std::hex << fBegin << "\n" 
	    << "\tEnd:  \t\t\t\t0x" << std::hex << fEnd << std::dec 
	    << std::endl;
}

//====================================================================
void
Rcuxx::RcuCHADD::Decode(unsigned int dat) 
{ 
  fAddress1 = (dat & 0x3ff);
  fAddress2   = ((dat >> 10) & 0x3ff);
}

//____________________________________________________________________
void
Rcuxx::RcuCHADD::Print() const
{
  RcuRegister::Print();
  std::cout << "\tAddress 1:\t\t\t0x" << std::hex << fAddress1 << "\n" 
	    << "\tAddress 2:\t\t\t0x" << std::hex << fAddress2 << std::dec 
	    << std::endl;
}

//====================================================================
Rcuxx::RcuStatusEntry::RcuStatusEntry(unsigned int dat)
{
  Decode(dat);
}

//____________________________________________________________________
Rcuxx::RcuStatusEntry::RcuStatusEntry(bool         id, 
				      unsigned int where, 
				      bool         soft, 
				      bool         ans, 
				      bool         sclk, 
				      bool         alps, 
				      bool         paps, 
				      bool         dc, 
				      bool         dv, 
				      bool         ac, 
				      bool         av, 
				      bool         temp)
  : fIsIdentified(id), 
    fWhere(where), 
    fIsSoft(soft), 
    fIsAnswering(ans),
    fMissedSclk(sclk), 
    fAlps(alps), 
    fPaps(paps), 
    fDcOverTh(dc), 
    fDvOverTh(dv), 
    fAcOverTh(ac), 
    fAvOverTh(av), 
    fTempOverTh(temp)
{}

//____________________________________________________________________
Rcuxx::RcuStatusEntry::RcuStatusEntry(const RcuStatusEntry& other)
  : fIsIdentified(other.fIsIdentified), 
    fWhere(other.fWhere), 
    fIsSoft(other.fIsSoft), 
    fIsAnswering(other.fIsAnswering),
    fMissedSclk(other.fMissedSclk), 
    fAlps(other.fAlps), 
    fPaps(other.fPaps), 
    fDcOverTh(other.fDcOverTh), 
    fDvOverTh(other.fDvOverTh), 
    fAcOverTh(other.fAcOverTh), 
    fAvOverTh(other.fDvOverTh), 
    fTempOverTh(other.fTempOverTh)
{}

//____________________________________________________________________
Rcuxx::RcuStatusEntry&
Rcuxx::RcuStatusEntry::operator=(const RcuStatusEntry& other)
{
  fIsIdentified	= other.fIsIdentified; 
  fWhere	= other.fWhere; 
  fIsSoft	= other.fIsSoft; 
  fIsAnswering	= other.fIsAnswering;
  fMissedSclk	= other.fMissedSclk; 
  fAlps		= other.fAlps; 
  fPaps		= other.fPaps; 
  fDcOverTh	= other.fDcOverTh; 
  fDvOverTh	= other.fDvOverTh; 
  fAcOverTh	= other.fAcOverTh; 
  fAvOverTh	= other.fAvOverTh; 
  fTempOverTh	= other.fTempOverTh;
  return *this;
}

//____________________________________________________________________
void
Rcuxx::RcuStatusEntry::Decode(unsigned int dat) 
{ 
  fTempOverTh	= dat & (1 << 0);
  fAvOverTh	= dat & (1 << 1);
  fAcOverTh	= dat & (1 << 2);
  fDvOverTh	= dat & (1 << 3);
  fDcOverTh	= dat & (1 << 4);
  fPaps		= dat & (1 << 5);
  fAlps		= dat & (1 << 6);
  fMissedSclk	= dat & (1 << 7);
  fIsAnswering	= !(dat & (1 << 8));
  fIsSoft	= dat & (1 << 9);
  fWhere	= (dat << 10) & 0x1f;
  fIsIdentified = dat & (1 << 15);
}

//____________________________________________________________________
void
Rcuxx::RcuStatusEntry::Print() const
{
  std::cout << std::boolalpha 
	    << "\tTemperature over:\t\t" << fTempOverTh << "\n"
	    << "\tAnalog voltage over:\t\t" << fAvOverTh << "\n"
	    << "\tAnalog current over:\t\t" << fAcOverTh << "\n"
	    << "\tDigital voltage over:\t\t" << fDvOverTh << "\n"
	    << "\tDigital current over:\t\t" << fDcOverTh << "\n"
	    << "\tPASA power supply trip:\t\t" << fPaps << "\n"
	    << "\tALTRO power supply trip:\t\t" << fAlps << "\n"
	    << "\tMissed slow clock:\t\t" << fMissedSclk << "\n"
	    << "\tAnswering:\t\t" << fIsAnswering << "\n"
	    << "\tSoft:\t\t" << fIsSoft << "\n"
	    << "\tWhere:\t\t0x" << std::hex << fWhere << std::dec << "\n"
	    << "\tIdentified:\t\t" << fIsIdentified << std::endl;
}


//====================================================================
void
Rcuxx::RcuRESREG::Decode(unsigned int dat) 
{ 
  fAddress  = (dat >> 16) & 0x1f;
  fResult   = (dat & 0xffff);
}

//____________________________________________________________________
unsigned int 
Rcuxx::RcuRESREG::Encode() 
{ 
  unsigned int val = 0;
  val += (fAddress & 0x1f) << 16;
  val += (fResult & 0xffff);
  return val;
}

//____________________________________________________________________
void
Rcuxx::RcuRESREG::Print() const
{
  RcuRegister::Print();
  std::cout << "\tAddress :\t\t\t" << std::hex << fAddress << "\n" 
	    << "\tResult  :\t\t\t" << std::hex << fResult << std::dec 
	    << std::endl;
}

//====================================================================
void
Rcuxx::RcuERRREG::Decode(unsigned int dat) 
{
  fIsNoAcknowledge = (dat & 0x2);
  fIsNotActive     = (dat & 0x1);
}

//____________________________________________________________________
void
Rcuxx::RcuERRREG::Print() const
{
  RcuRegister::Print();
  std::cout << std::boolalpha
	    << "\tNo aknowledge:\t\t\t" << fIsNoAcknowledge 
	    << "\n" 
	    << "\tNot active:\t\t\t" << fIsNotActive << std::endl;
}

//====================================================================
void
Rcuxx::RcuINTMOD::Decode(unsigned int dat) 
{ 
  fBranchA  = (dat & 0x1);
  fBranchB  = (dat & 0x2);
}

//____________________________________________________________________
unsigned int 
Rcuxx::RcuINTMOD::Encode() 
{ 
  unsigned int val = (fBranchA ? 1 : 0) + (fBranchB ? 2 : 0);
  return val;
}

//____________________________________________________________________
void
Rcuxx::RcuINTMOD::Print() const
{
  RcuRegister::Print();
  std::cout << std::boolalpha
	    << "\tBranch A :\t\t\t" << fBranchA << "\n" 
	    << "\tBranch B :\t\t\t" << fBranchB << std::dec 
	    << std::endl;
}


//====================================================================
Rcuxx::RcuMemory::RcuMemory(Rcu& rcu, const char* name, unsigned int base, 
			    unsigned int size, unsigned int max, bool commit, 
			    RcuCommand* clear, RcuCommand* exec, 
			    RcuCommand* abort)
  : fRcu(rcu), 
    fName(name), 
    fBase(base), 
    fSize(size), 
    fMax(max), 
    fWriteable(commit), 
    fData(size), 
    fClear(clear), 
    fExec(exec), 
    fAbort(abort)
{
  // fData = new unsigned int[fSize];
  Zero();
}

//____________________________________________________________________
unsigned int 
Rcuxx::RcuMemory::Commit(size_t off, size_t n)
{
  DebugGuard g(fDebug, "Memory::Commit(%d,%d)", off, n);
  unsigned int  m = std::min(unsigned(n), fSize);
  unsigned int  o = std::min(unsigned(off), fSize-1);
  unsigned int ret = fRcu.WriteMemory(fBase, o, m, &(fData[o]));
  return ret;
}
  
//____________________________________________________________________
unsigned int 
Rcuxx::RcuMemory::Update(size_t off, size_t n)
{
  DebugGuard g(fDebug, "Memory::Update(%d,%d)", off, n);
  unsigned int m = std::min(unsigned(n), fSize);
  unsigned int o = std::min(unsigned(off), fSize-1);
  unsigned int ret = fRcu.ReadMemory(fBase, o, m, &(fData[o]));
  return ret;
}

//____________________________________________________________________
unsigned int 
Rcuxx::RcuMemory::Exec(unsigned int arg) 
{
  if (!fExec) return 0;
  return fExec->Commit(arg);
}

//____________________________________________________________________
unsigned int 
Rcuxx::RcuMemory::Abort() 
{
  if (!fAbort) return 0;
  return fAbort->Commit();
}

//____________________________________________________________________
unsigned int 
Rcuxx::RcuMemory::Clear() 
{
  if (!fClear) return 0;
  return fClear->Commit();
}

//____________________________________________________________________
void
Rcuxx::RcuMemory::Zero()
{
  for (int i = 0; i < fData.size(); i++) fData[i] = 0;
}

//____________________________________________________________________
void
Rcuxx::RcuMemory::Print() const
{
  std::cout << "Contents of " << fName << ": " << std::endl;
  for (int i = 0; i < fData.size(); i++) { 
    if (i % 6 == 0 && i != 0) std::cout << std::endl;
    std::cout << "  0x" << std::setfill('0') << std::setw(8) 
	      << std::hex << fData[i] << std::flush;
  }
  std::cout << std::dec << std::endl;
}

//____________________________________________________________________
bool
Rcuxx::RcuMemory::Load(std::istream& in) 
{
  for (int i = 0; i < fData.size() && !in.eof(); i++) in >> fData[i];
  return true;
}
    
//____________________________________________________________________
bool
Rcuxx::RcuMemory::Save(std::ostream& out) 
{
  for (int i = 0; i < fData.size(); i++) 
    out << "  0x" << std::setfill('0') << std::setw(8) 
	<< std::hex << fData[i] << std::endl;
  out << std::dec;
  return true;
}

//____________________________________________________________________
void
Rcuxx::RcuMemory::Set(size_t off, size_t n, unsigned int* d) 
{
  DebugGuard g(fDebug, "RcuMemory::Set(%d, %d, %p)", off, n, d);
  for (int i = 0; i < n && i < fData.size() - off; i++) {
    fData[i + off] = (fMax & d[i]);
    DebugGuard::Message(fDebug, "\t%4d\t0x%08lx -> 0x%08lx", 
			i, d[i], fData[i+off]);
  }
}

//____________________________________________________________________
void
Rcuxx::RcuMemory::Get(size_t off, size_t n, unsigned int* d) 
{
  DebugGuard g(fDebug, "RcuMemory::Get(%d, %d, %p)", off, n, d);
  for (int i = 0; i < n && i < fData.size() - off; i++) {
    d[i] = (fMax & fData[i + off]);
    DebugGuard::Message(fDebug, "\t%4d\t0x%08lx -> 0x%08lx", 
			i, fData[i+off], d[i]);
  }
}
    
//====================================================================
Rcuxx::RcuIMEM::RcuIMEM(Rcu& rcu, size_t base, size_t n, 
			RcuCommand* exec, RcuCommand* abort) 
  : RcuMemory(rcu, "IMEM", base, n, 0xffffff, true, 0, exec, abort) 
{
  fData[0] = 0x0064000a;
  fData[1] = 0x007003a0;
  fData[2] = 0x00660011;
  fData[3] = 0x0070037f;
  fData[4] = 0x00390000;
}

//====================================================================
Rcuxx::RcuPMEM::RcuPMEM(Rcu& rcu, size_t base, size_t n) 
  : RcuMemory(rcu, "PMEM", base, n, 0xffff, true) 
{}

//====================================================================
Rcuxx::RcuRMEM::RcuRMEM(Rcu& rcu, size_t base, size_t n) 
  : RcuMemory(rcu, "RMEM", base, n, 0xfffff, true) 
{}

//====================================================================
Rcuxx::RcuDMEM::RcuDMEM(size_t which, Rcu& rcu, 
			size_t lbase, size_t hbase, size_t n, 
			RcuCommand* clear) 
  : RcuMemory(rcu, (which == 1 ? "DMEM1" : "DMEM2"), lbase, n, 0x3fff, 
	      false, clear),
    fWhich(which), fBaseHigh(hbase)
{}

//____________________________________________________________________
unsigned int 
Rcuxx::RcuDMEM::Commit(size_t off, size_t n)
{
  unsigned int m     = std::min(unsigned(n), fSize);
  unsigned int o     = std::min(unsigned(off), fSize-1);
  unsigned int ret = fRcu.WriteDataMemory(fBase, fBaseHigh, o, m, &(fData[o]));
  return ret;
}
  
//____________________________________________________________________
unsigned int 
Rcuxx::RcuDMEM::Update(size_t off, size_t n)
{
  DebugGuard g(fDebug, "Update DMEM%d at 0x%x, 0x%x, read %d words from %d", 
	       fWhich, fBase, fBaseHigh, off, n);
  unsigned int m   = std::min(unsigned(n), fSize);
  unsigned int o   = std::min(unsigned(off), fSize-1);
  unsigned int ret = fRcu.ReadDataMemory(fBase, fBaseHigh, o, m, &(fData[o]));
  return ret;
}

//====================================================================
Rcuxx::RcuACL::RcuACL(Rcu& rcu, size_t base, size_t n) 
  : RcuMemory(rcu, "ACL", base, n, 0xffff, true) 
{
  fData[0] = 0x00ff;
}

//====================================================================
Rcuxx::RcuHEADER::RcuHEADER(Rcu& rcu, size_t base, size_t n) 
  : RcuMemory(rcu, "HEADER", base, n, 0xffff, true) 
{
  fData[0] = 0x00ff;
}

//====================================================================
Rcuxx::RcuSTATUS::RcuSTATUS(Rcu& rcu, size_t base, size_t n) 
  : RcuMemory(rcu, "STATUS", base, n, 0xffff, true), 
    fEntries(0), 
    fList(32)
{}

//____________________________________________________________________
unsigned int 
Rcuxx::RcuSTATUS::Update(size_t offset, size_t n)
{
  unsigned int ret = RcuMemory::Update(0, n+1);
  if (ret) return ret;
  fEntries = fData[0];
  size_t m = std::min(n, size_t(fEntries));
  for (size_t i = 0; i < m; i++) 
    fList[i] = RcuStatusEntry(fData[i+1]);
  return 0;
}

//____________________________________________________________________
const Rcuxx::RcuStatusEntry&
Rcuxx::RcuSTATUS::Entry(size_t i) const
{
  if (i > fList.size()) throw std::runtime_error("Out of bounds");
  return fList[i];
}
  
//____________________________________________________________________
void
Rcuxx::RcuSTATUS::Print() const
{
  std::cout << "Contents of " << fName << ": " << fEntries 
	    << " cards listed" << std::endl;
  for (size_t i = 0; i < fEntries; ++i) { 
    std::cout << "  Card " << i << std::endl;
    fList[i].Print();
  }
}

  

//====================================================================
Rcuxx::Rcu::Rcu()
  :
    fERRST(0),		// ERRor and STatus register.  
    fTRCNT(0),		// TRigger CouNTers.  
    fLWADD(0),		// Last Written ADDress in the RcuDMEM1 and RcuDMEM2.  
    fIRADD(0),		// Last executed ALTRO InstRuction ADDress.  
    fIRDAT(0),		// Last executed ALTRO InstRuction DATa.
    fEVWORD(0),		// EVent WORD register.  
    fACTFEC(0),		// ACTive FrontEnd Card.  
    fRDOFEC(0),		// ReaDOut FrontEnd Card register.  
    fTRCFG1(0),		// TRigger ConFiG register.  
    fTRCFG2(0),		// ReaD-Out MODe register.  
    fFMIREG(0),		// FirMware Input ReGister.
    fFMOREG(0),		// FirMware Output ReGister 
    fPMCFG(0),		// Pedestal Memory ConFiGuration
    fCHADD(0),		// CHannel Address register 
    fINTREG(0),		// INTerrupt REGister 
    fRESREG(0),		// RESult REGister 
    fERRREG(0),		// ERRor REGister 
    fINTMOD(0),		// INTerrupt MODe register 
    fIMEM(0),		// RCU instruction memory
    fPMEM(0),		// RCU pedestal memory
    fRMEM(0),		// RCU result memory
    fDM1(0),		// RCU data memory
    fDM2(0),		// RCU data memory
    fACL(0),		// RCU Active Channel List memory
    fHEADER(0),		// RCU event HEADER memory
    fSTATUS(0),		// STATUS of BC's
    fRegisters(0),      // Register memory
    fMonitor(0), 	// Monitor memory
    fABORT(0),		// ABORT instruction execution 
    fCLR_EVTAG(0),	// CLeaR EVent TAG command
    fDCS_ON(0),		// DCS OwN bus command
    fDDL_ON(0),		// DDL OwN bus command
    fEXEC(0),		// EXECute instructions command
    fFECRST(0),		// Front-End Card ReSeT command
    fGLB_RESET(0),	// GLoBal RESET command
    fL1_CMD(0),		// L1-enable via CoMmanD command
    fL1_I2C(0),		// L1-enable via I2C command
    fL1_TTC(0),		// L1-enable via TTC command
    fRCU_RESET(0),	// RCU RESET command
    fRDABORT(0),	// ReaD-out ABORT command
    fRDFM(0),		// ReaD FirMware command
    fRS_DMEM1(0),	// ReSet DMEM1 command
    fRS_DMEM2(0),	// ReSet DMEM2 command
    fRS_STATUS(0),	// ReSet STATUS command
    fRS_TRCFG(0),	// ReSet TRigger ConFiGuration command
    fRS_TRCNT(0),	// ReSet TRigger CouNTer command
    fSCCOMMAND(0),	// Slow Control COMMAND command
    fSWTRG(0),		// SoftWare TRiGger command
    fTRG_CLR(0),	// TRiGger CLeaR command
    fWRFM(0),		// WRite FirMware command
    fRS_ERRREG(0)       // ReSet ERRor REGister command
{}

//____________________________________________________________________
Rcuxx::Rcu::Rcu(const Rcu& other)
  : 
    fERRST(other.fERRST),	  // ERRor and STatus register.  
    fTRCNT(other.fTRCNT),	  // Cache of the TRigger CouNTers.  
    fLWADD(other.fLWADD),	  // Last Written ADDress in the DMEMs
    fIRADD(other.fIRADD),	  // Last executed ALTRO InstRuction ADDress.  
    fIRDAT(other.fIRDAT),	  // Last executed ALTRO InstRuction DATa.
    fEVWORD(other.fEVWORD),	  // EVent WORD register.  
    fACTFEC(other.fACTFEC),	  // ACTive FrontEnd Card.  
    fRDOFEC(other.fRDOFEC),	  // ReaDOut FrontEnd Card register.  
    fTRCFG1(other.fTRCFG1),	  // TRigger ConFiG register.  
    fTRCFG2(other.fTRCFG2),	  // ReaD-Out MODe register.  
    fFMIREG(other.fFMIREG),	  // FirMware Input ReGister.
    fFMOREG(other.fFMOREG),	  // FirMware Output ReGister 
    fPMCFG(other.fPMCFG),	  // Pedestal Memory ConFiGuration
    fCHADD(other.fCHADD),	  // CHannel Address register 
    fINTREG(other.fINTREG),	  // INTerrupt REGister 
    fRESREG(other.fRESREG),	  // RESult REGister 
    fERRREG(other.fERRREG),	  // ERRor REGister 
    fINTMOD(other.fINTMOD),	  // INTerrupt MODe register 
    fIMEM(other.fIMEM),		  // RCU instruction memory
    fPMEM(other.fPMEM),		  // RCU pedestal memory
    fRMEM(other.fRMEM),		  // RCU result memory
    fDM1(other.fDM1),		  // RCU data memory
    fDM2(other.fDM2),		  // RCU data memory
    fACL(other.fACL),		  // RCU Active Channel List mem
    fHEADER(other.fHEADER),	  // RCU event HEADER memory
    fSTATUS(other.fSTATUS),	  // STATUS of BC's
    fRegisters(other.fRegisters), // Register memory
    fMonitor(other.fMonitor),     // Monitor memory
    fABORT(other.fABORT),	  // ABORT instruction execution 
    fCLR_EVTAG(other.fCLR_EVTAG), // CLeaR EVent TAG command
    fDCS_ON(other.fDCS_ON),	  // DCS OwN bus command
    fDDL_ON(other.fDDL_ON),	  // DDL OwN bus command
    fEXEC(other.fEXEC),		  // Interface EXECute instructions command
    fFECRST(other.fFECRST),	  // Front-End Card ReSeT command
    fGLB_RESET(other.fGLB_RESET), // GLoBal RESET command
    fL1_CMD(other.fL1_CMD),	  // Intefaceto L1-enable via CoMmanD command
    fL1_I2C(other.fL1_I2C),	  // Interface L1-enable via I2C command
    fL1_TTC(other.fL1_TTC),	  // L1-enable via TTC command
    fRCU_RESET(other.fRCU_RESET), // Interace to RCU RESET command
    fRDABORT(other.fRDABORT),	  // ReaD-out ABORT command
    fRDFM(other.fRDFM),		  // Interface ReaD FirMware command
    fRS_DMEM1(other.fRS_DMEM1),	  // ReSet DMEM1 command
    fRS_DMEM2(other.fRS_DMEM2),	  // ReSet DMEM2 command
    fRS_STATUS(other.fRS_STATUS), // ReSet STATUS command
    fRS_TRCFG(other.fRS_TRCFG),	  // ReSet TRigger ConFiGuration command
    fRS_TRCNT(other.fRS_TRCNT),	  // Inteface to ReSet TRigger CouNTer command
    fSCCOMMAND(other.fSCCOMMAND), // Slow Control COMMAND command
    fSWTRG(other.fSWTRG),	  // Interface SoftWare TRiGger command
    fTRG_CLR(other.fTRG_CLR),	  // TRiGger CLeaR command
    fWRFM(other.fWRFM),		  // WRite FirMware command
    fRS_ERRREG(other.fRS_ERRREG)  // ReSet ERRor REGister command
{}

//____________________________________________________________________
Rcuxx::Rcu::~Rcu()
{
  if (fERRST)	  delete fERRST;     // ERRor and STatus register.  
  if (fTRCNT)	  delete fTRCNT;     // Cache of the TRigger CouNTers.  
  if (fLWADD)	  delete fLWADD;     // Last Written ADDress in the DMEMs
  if (fIRADD)	  delete fIRADD;     // Last exe ALTRO InstRuction ADDress.
  if (fIRDAT)	  delete fIRDAT;     // Last exec ALTRO InstRuction DATa.
  if (fEVWORD)	  delete fEVWORD;    // EVent WORD register.  
  if (fACTFEC)	  delete fACTFEC;    // ACTive FrontEnd Card.  
  if (fRDOFEC)	  delete fRDOFEC;    // ReaDOut FrontEnd Card register.  
  if (fTRCFG1)	  delete fTRCFG1;    // TRigger ConFiG register.  
  if (fTRCFG2)	  delete fTRCFG2;    // ReaD-Out MODe register.  
  if (fFMIREG)	  delete fFMIREG;    // FirMware Input ReGister.
  if (fFMOREG)	  delete fFMOREG;    // FirMware Output ReGister 
  if (fPMCFG)	  delete fPMCFG;     // Pedestal Memory ConFiGuration
  if (fCHADD)	  delete fCHADD;     // CHannel Address register 
  if (fINTREG)	  delete fINTREG;    // INTerrupt REGister 
  if (fRESREG)	  delete fRESREG;    // RESult REGister 
  if (fERRREG)	  delete fERRREG;    // ERRor REGister 
  if (fINTMOD)	  delete fINTMOD;    // INTerrupt MODe register 
  if (fIMEM)	  delete fIMEM;	     // RCU instruction memory
  if (fPMEM)	  delete fPMEM;	     // RCU pedestal memory
  if (fRMEM)	  delete fRMEM;	     // RCU result memory
  if (fDM1)	  delete fDM1;	     // RCU data memory
  if (fDM2)	  delete fDM2;	     // RCU data memory
  if (fACL)	  delete fACL;	     // RCU Active Channel List mem
  if (fHEADER)	  delete fHEADER;    // RCU event HEADER memory
  if (fSTATUS)	  delete fSTATUS;    // STATUS of BC's
  if (fRegisters) delete fRegisters; // Register memory
  if (fMonitor)	  delete fMonitor;   // Monitor memory
  if (fABORT)	  delete fABORT;     // ABORT instruction execution 
  if (fCLR_EVTAG) delete fCLR_EVTAG; // CLeaR EVent TAG command
  if (fDCS_ON)	  delete fDCS_ON;    // DCS OwN bus command
  if (fDDL_ON)	  delete fDDL_ON;    // DDL OwN bus command
  if (fEXEC)	  delete fEXEC;	     // Interface EXECute instructions command
  if (fFECRST)	  delete fFECRST;    // Front-End Card ReSeT command
  if (fGLB_RESET) delete fGLB_RESET; // GLoBal RESET command
  if (fL1_CMD)	  delete fL1_CMD;    // L1-enable via CoMmanD command
  if (fL1_I2C)	  delete fL1_I2C;    // Interface L1-enable via I2C command
  if (fL1_TTC)	  delete fL1_TTC;    // L1-enable via TTC command
  if (fRCU_RESET) delete fRCU_RESET; // Interace to RCU RESET command
  if (fRDABORT)	  delete fRDABORT;   // ReaD-out ABORT command
  if (fRDFM)	  delete fRDFM;	     // Interface ReaD FirMware command
  if (fRS_DMEM1)  delete fRS_DMEM1;  // ReSet DMEM1 command
  if (fRS_DMEM2)  delete fRS_DMEM2;  // ReSet DMEM2 command
  if (fRS_STATUS) delete fRS_STATUS; // ReSet STATUS command
  if (fRS_TRCFG)  delete fRS_TRCFG;  // ReSet TRigger ConFiGuration command
  if (fRS_TRCNT)  delete fRS_TRCNT;  // ReSet TRigger CouNTer command
  if (fSCCOMMAND) delete fSCCOMMAND; // Slow Control COMMAND command
  if (fSWTRG)	  delete fSWTRG;     // Interface SoftWare TRiGger command
  if (fTRG_CLR)	  delete fTRG_CLR;   // TRiGger CLeaR command
  if (fWRFM)	  delete fWRFM;	     // WRite FirMware command
  if (fRS_ERRREG) delete fRS_ERRREG; // ReSet ERRor REGister command
}

//____________________________________________________________________
Rcuxx::Rcu& 
Rcuxx::Rcu::operator=(const Rcu& other)
{
  fERRST	= other.fERRST;	    // ERRor and STatus register.  
  fTRCNT	= other.fTRCNT;	    // Cache of the TRigger CouNTers.  
  fLWADD	= other.fLWADD;	    // Last Written ADDress in the DMEMs
  fIRADD	= other.fIRADD;	    // Last executed ALTRO InstRuction ADDress.
  fIRDAT	= other.fIRDAT;	    // Last executed ALTRO InstRuction DATa.
  fEVWORD	= other.fEVWORD;    // EVent WORD register.  
  fACTFEC	= other.fACTFEC;    // ACTive FrontEnd Card.  
  fRDOFEC	= other.fRDOFEC;    // ReaDOut FrontEnd Card register.  
  fTRCFG1	= other.fTRCFG1;    // TRigger ConFiG register.  
  fTRCFG2	= other.fTRCFG2;    // ReaD-Out MODe register.  
  fFMIREG	= other.fFMIREG;    // FirMware Input ReGister.
  fFMOREG	= other.fFMOREG;    // FirMware Output ReGister 
  fPMCFG	= other.fPMCFG;     // Pedestal Memory ConFiGuration
  fCHADD	= other.fCHADD;     // CHannel Address register 
  fINTREG	= other.fINTREG;    // INTerrupt REGister 
  fRESREG	= other.fRESREG;    // RESult REGister 
  fERRREG	= other.fERRREG;    // ERRor REGister 
  fINTMOD	= other.fINTMOD;    // INTerrupt MODe register 
  fIMEM		= other.fIMEM;	    // RCU instruction memory
  fPMEM		= other.fPMEM;	    // RCU pedestal memory
  fRMEM		= other.fRMEM;	    // RCU result memory
  fDM1		= other.fDM1;	    // RCU data memory
  fDM2		= other.fDM2;	    // RCU data memory
  fACL		= other.fACL;	    // RCU Active Channel List mem
  fHEADER	= other.fHEADER;    // RCU event HEADER memory
  fSTATUS	= other.fSTATUS;    // STATUS of BC's
  fRegisters	= other.fRegisters; // Register memory
  fMonitor	= other.fMonitor;   // Monitor memory
  fABORT	= other.fABORT;	    // ABORT instruction execution 
  fCLR_EVTAG	= other.fCLR_EVTAG; // CLeaR EVent TAG command
  fDCS_ON	= other.fDCS_ON;    // DCS OwN bus command
  fDDL_ON	= other.fDDL_ON;    // DDL OwN bus command
  fEXEC		= other.fEXEC;	    // Interface EXECute instructions command
  fFECRST	= other.fFECRST;    // Front-End Card ReSeT command
  fGLB_RESET	= other.fGLB_RESET; // GLoBal RESET command
  fL1_CMD	= other.fL1_CMD;    // Intefaceto L1-enable via CoMmanD command
  fL1_I2C	= other.fL1_I2C;    // Interface L1-enable via I2C command
  fL1_TTC	= other.fL1_TTC;    // L1-enable via TTC command
  fRCU_RESET	= other.fRCU_RESET; // Interace to RCU RESET command
  fRDABORT	= other.fRDABORT;   // ReaD-out ABORT command
  fRDFM		= other.fRDFM;	    // Interface ReaD FirMware command
  fRS_DMEM1	= other.fRS_DMEM1;  // ReSet DMEM1 command
  fRS_DMEM2	= other.fRS_DMEM2;  // ReSet DMEM2 command
  fRS_STATUS	= other.fRS_STATUS; // ReSet STATUS command
  fRS_TRCFG	= other.fRS_TRCFG;  // ReSet TRigger ConFiGuration command
  fRS_TRCNT	= other.fRS_TRCNT;  // ReSet TRigger CouNTer command
  fSCCOMMAND	= other.fSCCOMMAND; // Slow Control COMMAND command
  fSWTRG	= other.fSWTRG;	    // Interface SoftWare TRiGger command
  fTRG_CLR	= other.fTRG_CLR;   // TRiGger CLeaR command
  fWRFM		= other.fWRFM;	    // WRite FirMware command
  fRS_ERRREG	= other.fRS_ERRREG; // ReSet ERRor REGister command
}

//____________________________________________________________________
const std::string&
Rcuxx::Rcu::Version() 
{
  return gVersion;
}

//____________________________________________________________________
const std::string&
Rcuxx::Rcu::Package() 
{
  return gPackage;
}

//____________________________________________________________________
const std::string&
Rcuxx::Rcu::PackageString() 
{
  return gPackageString;
}

//____________________________________________________________________
void
Rcuxx::Rcu::PrintHelp(std::ostream& o)
{
  o << "Supported protocols and interfaces are\n\n";
#ifdef HAVE_U2F
  o << "\n U2F interface:\n\n"
    << "\tDEVICE\n" 
    << "\tusb:DEVICE\n" 
    << "\tu2f:DEVICE\n\n" 
    << "  where DEVICE is something like '/dev/altro0'\n\n";
#endif
#ifdef HAVE_FEECLIENT
  o << "\n  FEE client interface:\n"
    << "\tfee://HOST/SERVER\n" 
    << "\tfee://HOST/\n" 
    << "\tfee:///SERVER\n\n" 
    << "  where HOST is the address of the DIM DNS, and SERVER is "
    << "the FeeServer name\n\n";
#endif
#ifdef HAVE_DATE 
  o << "\n  RORC interface:\n" 
    << "\trorc://MINOR:CHANNEL\n\n" 
    << "  where MINOR is the minor device number, and CHANNEL is "
    << "the DDL channel number\n\n";
#endif
  o << std::flush;
}


//____________________________________________________________________
bool
Rcuxx::Rcu::ReadFile(const std::string& name, std::vector<unsigned int>& data)
{
  std::ifstream stream(name.c_str());
  if (!stream) return false;
  bool ret = ReadFile(stream, data);
  stream.close();
  return ret;
}

//____________________________________________________________________
bool
Rcuxx::Rcu::ReadFile(std::istream& stream, std::vector<unsigned int>& data)
{
  while (!stream.eof()) {
    if (stream.peek() != '#') {
      unsigned int i;
      stream >> i;
      if (stream.fail()) return false;
      data.push_back(i);
    }
    std::string line;
    std::getline(stream, line);
    if (stream.fail()) return false;
  }
  return true;
}

    

//____________________________________________________________________
Rcuxx::Rcu*
Rcuxx::Rcu::Open(const char* url, bool emul)
{
  DebugGuard g(fDebug, "Rcu::Open(%s,%s)", url, (emul ? "true" : "false"));
  std::string tmp(url);
  std::string::size_type colon = tmp.find(":");
  std::string proto((colon != std::string::npos ? tmp.substr(0, colon+1): ""));
  std::string addr((colon != std::string::npos ? tmp.substr(colon+1) : tmp));
  Rcu* ret = 0;
  try {
#ifdef HAVE_U2F
    if (addr.find("/dev") == 0 || proto == "usb:" || proto == "u2f:") 
      return new U2F(const_cast<char*>(addr.c_str()), emul);
#endif
#ifdef HAVE_FEECLIENT
    if (proto == "fee:") {
      if (addr[0] == '/') addr.erase(0,1);
      if (addr[0] == '/') addr.erase(0,1);
      DebugGuard::Message(fDebug, 
			  "Rcu::Open - Opening Fee connection to %s", url);
      return new Fee(const_cast<char*>(addr.c_str()), emul);
    }
#endif
#ifdef HAVE_DATE
    if (proto == "date:" || proto == "ddl:" || proto == "rorc:") {
      DebugGuard::Message(fDebug, 
			  "Rcu::Open - Opening Rorc connection to %s", url);
      return new Rorc(const_cast<char*>(addr.c_str()), emul);
    }
#endif
    std::cerr << "Unsupported URL: " << url << std::endl;
    PrintHelp(std::cerr);
    return 0;
  }
  catch (std::exception& e) {
    std::cerr << e.what() << std::endl;
    // if (ret) delete ret;
    return 0;
  }
  return ret;
}

//____________________________________________________________________
unsigned int
Rcuxx::Rcu::ReadDataMemory(unsigned int  lbase, unsigned int hbase, 
			   unsigned int  off, unsigned int& size, 
			   unsigned int* data) 
{
  std::vector<unsigned int> low;
  std::vector<unsigned int> high;
  size_t o = off;
  unsigned int losize = size;
  unsigned int hosize = size;
  unsigned int r;
  r = ReadMemory(lbase, o, losize, &(low[0]));
  r = ReadMemory(hbase, o, losize, &(high[0]));
  if (losize != hosize) return kInconsistentSizes;
  if (losize != size)   return kNotEnough;
  if (hosize != size)   return kNotEnough;
  size = losize;
  for (size_t i = 0; i < size; i++) {
    data[4*i+0] = low[i]          & 0x3ff;
    data[4*i+1] = (low[i]  >> 10) & 0x3ff;
    data[4*i+2] = high[i]         & 0x3ff;
    data[4*i+3] = (high[i] >> 10) & 0x3ff;
  }
  return r;
}

//____________________________________________________________________
unsigned int
Rcuxx::Rcu::WriteDataMemory(unsigned int  lbase, unsigned int hbase, 
			    unsigned int  off, unsigned int& size, 
			    unsigned int* data) 
{
  std::vector<unsigned int> low;
  std::vector<unsigned int> high;
  size_t o = off;
  unsigned int losize = size;
  unsigned int hosize = size;
  unsigned int r;
  for (size_t i = 0; i < size; i++) {
    low[i]  = (data[4 * i + 0] & 0x3ff) + ((data[4 * i + 1]) << 10);
    high[i] = (data[4 * i + 2] & 0x3ff) + ((data[4 * i + 3]) << 10);
  }
  r = WriteMemory(lbase, o, losize, &(low[0]));
  r = WriteMemory(hbase, o, hosize, &(low[0]));
  return r;
}

  
//____________________________________________________________________
unsigned int
Rcuxx::Rcu::Update() 
{
  DebugGuard g(fDebug, "Rcu::Update");
  unsigned int ret;
  if (fERRST	 && (ret = fERRST->Update()))	 return ret;
  if (fTRCNT	 && (ret = fTRCNT->Update()))	 return ret;
  if (fLWADD	 && (ret = fLWADD->Update()))	 return ret;
  if (fIRADD	 && (ret = fIRADD->Update()))	 return ret;
  if (fIRDAT	 && (ret = fIRDAT->Update()))	 return ret;
  if (fEVWORD	 && (ret = fEVWORD->Update()))	 return ret;
  //if (fACTFEC	 && (ret = fACTFEC->Update()))	 return ret;
  if (fRDOFEC	 && (ret = fRDOFEC->Update()))	 return ret;
  //if (fTRCFG1	 && (ret = fTRCFG1->Update()))	 return ret;
  if (fTRCFG2	 && (ret = fTRCFG2->Update()))	 return ret;
  if (fFMIREG	 && (ret = fFMIREG->Update()))	 return ret;
  if (fFMOREG	 && (ret = fFMOREG->Update()))	 return ret;
  if (fPMCFG	 && (ret = fPMCFG->Update()))	 return ret;
  if (fCHADD	 && (ret = fCHADD->Update()))	 return ret;
  if (fINTREG	 && (ret = fINTREG->Update()))	 return ret;
  if (fRESREG	 && (ret = fRESREG->Update()))	 return ret;
  if (fERRREG	 && (ret = fERRREG->Update()))	 return ret;
  if (fINTMOD	 && (ret = fINTMOD->Update()))	 return ret;
  if (fIMEM	 && (ret = fIMEM->Update()))	 return ret;
  if (fPMEM	 && (ret = fPMEM->Update()))	 return ret;
  if (fRMEM	 && (ret = fRMEM->Update()))	 return ret;
  //if (fDM1	 && (ret = fDM1->Update()))	 return ret;
  //if (fDM2	 && (ret = fDM2->Update()))	 return ret;
  if (fACL	 && (ret = fACL->Update()))	 return ret;
  if (fHEADER	 && (ret = fHEADER->Update()))	 return ret;
  if (fSTATUS	 && (ret = fSTATUS->Update()))	 return ret;
  return ret;
}

//____________________________________________________________________
void
Rcuxx::Rcu::Print() const
{
  DebugGuard g(fDebug, "Rcu::Print");
  if (fERRST)	 fERRST->Print();
  if (fTRCNT)	 fTRCNT->Print();
  if (fLWADD)	 fLWADD->Print();
  if (fIRADD)	 fIRADD->Print();
  if (fIRDAT)	 fIRDAT->Print();
  if (fEVWORD)	 fEVWORD->Print();
  if (fACTFEC)	 fACTFEC->Print();
  if (fRDOFEC)	 fRDOFEC->Print();
  if (fTRCFG1)	 fTRCFG1->Print();
  if (fTRCFG2)	 fTRCFG2->Print();
  if (fFMIREG)	 fFMIREG->Print();
  if (fFMOREG)	 fFMOREG->Print();
  if (fPMCFG)	 fPMCFG->Print();
  if (fCHADD)	 fCHADD->Print();
  if (fINTREG)	 fINTREG->Print();
  if (fRESREG)	 fRESREG->Print();
  if (fERRREG)	 fERRREG->Print();
  if (fINTMOD)	 fINTMOD->Print();
  if (fIMEM)	 fIMEM->Print();
  if (fPMEM)	 fPMEM->Print();
  if (fRMEM)	 fRMEM->Print();
  if (fDM1)	 fDM1->Print();
  if (fDM2)	 fDM2->Print();
  if (fACL)	 fACL->Print();
  if (fHEADER)	 fHEADER->Print();
  if (fSTATUS)	 fSTATUS->Print();
}

//____________________________________________________________________
void
Rcuxx::Rcu::SetDebug(int id, int lvl) 
{
  // if (id == 0) DebugGuard::fDebug = (lvl > 0);
  switch (id) {
  case kRcu:     fDebug = true; break;
  }
}

//____________________________________________________________________
std::string
Rcuxx::Rcu::ErrorString(unsigned int code) 
{
  switch (code) {
  case kNotEnough: 
    return std::string("Not enough data returned");
  case kInconsistentSizes:
    return std::string("Inconsistent size when reading data memory");
  case kUnknownMemory:
    return std::string("Unknown memory bank addressed");
  }
  return std::string();
}


//====================================================================
//
// EOF
//
