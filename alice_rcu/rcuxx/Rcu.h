// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    rcuxx/Rcu.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:32:49 2006
    @brief   Declaration of RCU interface 
*/
#ifndef RCUXX_RCU_H
#define RCUXX_RCU_H
#ifndef __STRING__
# include <string>
#endif
#ifndef __VECTOR__
# include <vector>
#endif

/** @defgroup rcuxx Abstract interface of the RCU communications.
 */
/** @namespace Rcuxx Namespace for Abstract interface 
    @ingroup rcuxx 
*/
namespace Rcuxx
{
  /** @defgroup rcuxx_rcu RCU interface classes  
      @ingroup rcuxx 
  */
  //__________________________________________________________________
  /** @struct Command rcuxx/Rcu.h <rcuxx/Rcu.h> 
      @brief Base class for commands 
      @ingroup rcuxx_rcu
  */
  struct Command 
  {
    Command(const char* name, unsigned int cmd) : fName(name), fCmd(cmd) {}
    /** Get the name */ 
    const std::string& Name() const { return fName; }
    /** Execute command */
    virtual unsigned int Commit() = 0;
    /** Get the cmd */ 
    unsigned int Cmd() const { return fCmd; }
  protected:
    std::string fName;
    unsigned int fCmd;
  };
  //__________________________________________________________________
  /** @struct Register rcuxx/Rcu.h <rcuxx/Rcu.h>
      @brief Base class for commands 
      @ingroup rcuxx_rcu 
  */
  struct Register
  {
    Register(const char* name, bool submit)
      : fName(name), fSubmit(submit)
    {}
    /** Get the name */ 
    const std::string& Name() const { return fName; }
    /** Whether we got Clear */
    virtual bool IsClearable() const { return false; }
    /** Whether we got Submit */
    bool IsSubmitable() const { return fSubmit; }
    /** Whether this can be broadcast */ 
    virtual bool IsBroadcastable() const { return false; }
    /** Update the cache of the register
	@return 0 on success, error code otherwise */
    virtual unsigned int Update() { return 0; }
    /** Clear the register (optional) 
	@return 0 on success, error code otherwise */
    virtual unsigned int Clear() { return 0; }
    /** Commit to the register (optional) 
	@return 0 on success, error code otherwise */
    virtual unsigned int Commit() { return 0; }
    /** Print contents of register to standard output */
    virtual void Print() const;
  protected:
    /** Name */
    std::string fName;
    /** Clear */
    bool fClear;
    /** Submit */
    bool fSubmit;
  };
  
  //____________________________________________________________________
  // Forward declaration 
  struct Rcu;
  
  //====================================================================
  /** @struct RcuCommand rcuxx/Rcu.h <rcuxx/Rcu.h>
      @brief Command interface 
      @ingroup rcuxx_rcu
  */
    struct RcuCommand : public Command
  {
  public:
    RcuCommand(Rcu& rcu, const char* name, unsigned int cmd=0, 
	       bool hasarg=false) 
      : Command(name, cmd), fRcu(rcu), fHasArgument(hasarg), fArg(0)
    {}
    virtual ~RcuCommand(){}
    /** Send command to HW
	@return 0 on success, error code otherwise */
    virtual unsigned int Commit() { return Commit(0);  }
    /** Send command to HW
	@return 0 on success, error code otherwise */
    virtual unsigned int Commit(unsigned int arg);
    /** Whether we take an argument or not */
    bool HasArgument() const { return fHasArgument; }
    /** Set argument */ 
    void SetArgument(unsigned int a) { fArg = a; }
  protected:
    /** Interface to RCU */ 
    Rcu&         fRcu;
    /** Name */ 
    std::string  fName;
    /** Command */
    unsigned int fArg; 
    /** Whether arguments make sense or not */
    bool fHasArgument;
  };
  
  //====================================================================
  /** @class RcuMemory rcuxx/Rcu.h <rcuxx/Rcu.h>
      @brief Interface to the RCU memory banks     
      @ingroup rcuxx_rcu
  */
  class RcuMemory 
  {
  public:
    /** Type of cache */
    typedef std::vector<unsigned int> Cache_t;
    /** Constructor 
	@param rcu Reference to RCU
	@param name Name of the memory
	@param base Base address
	@param size Number of words in the memory
	@param max Maximum value of words in the memory
	@param commit Whether the memory is writeable  
        @param clear Command to clear 
        @param exec Command to execute 
        @param abort Command to abort execution. */
    RcuMemory(Rcu& rcu, const char* name, unsigned int base, 
	      unsigned int size, unsigned int max, bool commit, 
	      RcuCommand* clear=0, RcuCommand* exec=0, 
	      RcuCommand* abort=0);
    /** Destructor */
    virtual ~RcuMemory() { /* delete [] fData;  */ }
    /** Update the cache
	@return 0 on success, error code otherwise */
    virtual unsigned int Update() { return Update(0, fSize); }
    /** Commit to the RCU 
	@return 0 on success, error code otherwise */
    virtual unsigned int Commit() { return Commit(0, fSize); }
    /** Update the cache (restricted)
	@param off Where to start reading
	@param n   How many words to read 
	@return 0 on success, error code otherwise */
    virtual unsigned int Update(size_t off, size_t n);
    /** Write to the hardware (restricted)
	@param off Where to start reading
	@param n   How many words to read 
	@return 0 on success, error code otherwise */
    virtual unsigned int Commit(size_t off, size_t n);
    /** Clear memory */ 
    virtual unsigned int Clear();
    /** Clear memory */ 
    virtual unsigned int Exec(unsigned int arg);
    /** Clear memory */ 
    virtual unsigned int Abort();
    /** Load a file into the cache
	@return true on success, false otherwise */
    virtual bool Load(std::istream& in);
    /** Save the cache to file
	@return true on success, false otherwise */      
    virtual bool Save(std::ostream& out);
    /** Zero the memory */
    virtual void Zero();
    /** @return Get the size */
    unsigned int Size() const { return fSize; }
    /** @return Get the data */
    const Cache_t& Data() const { return fData; }
    /** @return Get the data */
    Cache_t& Data() { return fData; }
    /** Set the cache
	@param off Starting offset
	@param n How many words (size of @a d)
	@param d The values */
    /** Set */ 
    void Set(size_t off, size_t n, unsigned int* d);
    /** Get the cache 
	@param off Starting offset
	@param n How many words (size of @a d)
	@param d On return, the values */
    void Get(size_t off, size_t n, unsigned int* d);
    /** Get the base address */ 
    unsigned int Base() const { return fBase; }
    /** Print cache to standard output */
    virtual void Print() const;
    /** Get the name */ 
    const std::string& Name() const { return fName; }
    /** Whether we got Clear */
    virtual bool IsWriteable() const { return fWriteable; }
    /** Whether we got Clear */
    virtual bool IsClearable() const { return (fClear != 0); }
    /** Whether we got Clear */
    virtual bool IsExecutable() const { return (fExec != 0); }
  protected:
    /** Reference to RCU */
    Rcu& fRcu;
    /** Name */
    std::string fName;
    /** Base address */ 
    unsigned int fBase;
    /** Data buffer */
    Cache_t fData;
    /** Size of the memory */
    unsigned int fSize;
    /** Max value of each entry in the memory */
    unsigned int fMax;
    /** Whether the memory is writeable */
    bool fWriteable;
    /** Clear command, if any */
    RcuCommand* fClear;
    /** Execute command, if any */
    RcuCommand* fExec;
    /** Abort command, if any */
    RcuCommand* fAbort;
  };

  //====================================================================
  /** @class RcuRegister rcuxx/Rcu.h <rcuxx/Rcu.h>
      @brief Register interface 
      @ingroup rcuxx_rcu
  */
    class RcuRegister : public Register
  {
  public:
    /** Constructor 
	@param mem    Base memory 
	@param offset Offset in base memory
	@param name   Name of the register 
	@param clear  Possible RCU command to clear register
	@param submit Whether we can write to this register
    */
    RcuRegister(RcuMemory& mem, size_t offset, const char* name, 
		RcuCommand* clear, bool submit) 
      : Register(name, submit), 
	fMemory(mem), 
	fOffset(offset), 
	fClear(clear)
    {}
    virtual ~RcuRegister() {}
    /** Update the cache of the register
	@return 0 on success, error code otherwise */
    virtual unsigned int Update();
    /** Clear the register (optional) 
	@return 0 on success, error code otherwise */
    virtual unsigned int Clear();
    /** Commit to the register (optional) 
	@return 0 on success, error code otherwise */
    virtual unsigned int Commit();
    /** Whether we got Clear */
    bool IsClearable() const { return fClear != 0; }
    /** Encode set value to register value */ 
    virtual unsigned int Encode() { return 0; }
    /** Decode register value to set value */
    virtual void Decode(unsigned int dat) {}
  protected:
    /** Base memory */
    RcuMemory&  fMemory;
    /** Offset in base memory */
    size_t      fOffset;
    /** Whether we can clear */
    RcuCommand* fClear;
  };

  //====================================================================
  /** @class RcuERRST rcuxx/Rcu.h <rcuxx/Rcu.h>
      @brief Interface to the ERRor and STatus register.  
      This provide the interface that concrete implementations should
      implement.   
      @ingroup rcuxx_rcu
  */
  struct RcuERRST : public RcuRegister
  {
    /** Constructor 
	@param mem reference to memory 
	@param offset Offset in memory 
	@param clear Clear command */
    RcuERRST(RcuMemory& mem, size_t offset, RcuCommand* clear) 
      : RcuRegister(mem, offset, "ERRST", clear, false),
	fPattern(false), 
	fAbort(false), 
	fTimeout(false), 
	fAltro(false),
	fHWAdd(false), 
	fBusy(false), 
	fWhere(0)
    {}

    /** @return @c true if Pattern */
    bool IsPattern() const { return fPattern; }
    /** @return @c true if Abort */
    bool IsAbort() const { return fAbort; }
    /** @return @c true if Timeout */
    bool IsTimeout() const { return fTimeout; }
    /** @return @c true if Altro */
    bool IsAltro() const { return fAltro; }
    /** @return @c true if HWAdd */
    bool IsHWAdd() const { return fHWAdd; }
    /** @return @c true if Busy */
    bool IsBusy() const { return fBusy; }
    /** @return where error occured */
    unsigned int Where() const { return fWhere; }
    /** Decode @a dat as the ERRor and STatus register  */
    virtual void Decode(unsigned int dat);
    /** Print to standard output */
    virtual void Print() const;
  protected:
    bool fPattern;
    bool fAbort;
    bool fTimeout;
    bool fAltro;
    bool fHWAdd;
    bool fBusy;
    unsigned int fWhere;
  };

  //====================================================================
  /** @class RcuTRCNT rcuxx/Rcu.h <rcuxx/Rcu.h> 
      @brief Cache of the TRigger CouNTers.  
      This provide the interface that concrete implementations should
      implement.   
      @ingroup rcuxx_rcu 
  */
  struct RcuTRCNT : public RcuRegister
  {

    /** Constructor 
	@param mem reference to memory 
	@param offset Offset in memory
	@param clear Clear command */
    RcuTRCNT(RcuMemory& mem, size_t offset, RcuCommand* clear)
      : RcuRegister(mem, offset, "TRCNT", clear, false), 
	fReceived(0), 
	fAccepted(0) 
    {}
    /** Decode @a dat as the TRigger CouNTers */
    virtual void Decode(unsigned int dat);
    /** Print to standard output */
    virtual void Print() const;
    /** @return Recieved triggers */
    unsigned int Received() const { return fReceived; }
    /** @return Accepted triggers */
    unsigned int Accepted() const { return fAccepted; }
  protected:
    unsigned int fReceived;
    unsigned int fAccepted;
  };
  //====================================================================
  /** @struct RcuLWADD rcuxx/Rcu.h <rcuxx/Rcu.h>
      @brief Cache the Last Written ADDress in the RcuDMEM1 and RcuDMEM2.  
      This provide the interface that concrete implementations should
      implement.   
      @ingroup rcuxx_rcu
  */
  struct RcuLWADD : public RcuRegister
  {
    /** Constructor 
	@param mem Memory 
	@param offset Register offset in @a mem */
    RcuLWADD(RcuMemory& mem, size_t offset)
      : RcuRegister(mem, offset, "LWADD", 0, false), 
	fBank1(0), 
	fBank2(0) 
    {}
    /** Update the cache of the Last Written ADDress in the RcuDMEM1 and
	RcuDMEM2  */
    virtual void Decode(unsigned int dat);
    /** Print to standard output */
    virtual void Print() const;
    /** @return Number of 40bit words in DMEM1 */
    unsigned int Bank1() const { return fBank1; }
    /** @return Number of 40bit words in DMEM2 */
    unsigned int Bank2() const { return fBank2; }
  protected:
    unsigned int fBank1;
    unsigned int fBank2;
  };

  //====================================================================
  /** @struct RcuIRADD rcuxx/Rcu.h <rcuxx/Rcu.h> 
      @brief Cache the last executed ALTRO InstRuction ADDress.  
      This provide the interface that concrete implementations should
      implement.   
      @ingroup rcuxx_rcu
  */
  struct RcuIRADD : public RcuRegister
  {
    /** Constructor 
	@param mem reference to memory 
	@param offset Offset in memory  */
    RcuIRADD(RcuMemory& mem, size_t offset)
      : RcuRegister(mem, offset, "IRADD", 0, false), fIRADD(0) {}
    /** Decode @a dat as the last executed ALTRO InstRuction  ADDress  */
    virtual void Decode(unsigned int dat);
    /** Print to standard output */
    virtual void Print() const;
    /** @return Last instruction address */
    unsigned int IRADD() const { return fIRADD; }
  protected:
    unsigned int fIRADD;
  };

  //====================================================================
  /** @struct RcuIRDAT rcuxx/Rcu.h <rcuxx/Rcu.h> 
      @brief Cache the last executed ALTRO InstRuction DATa.
      This provide the interface that concrete implementations should
      implement.   
      @ingroup rcuxx_rcu
  */
  struct RcuIRDAT : public RcuRegister
  {
    /** Constructor 
	@param mem reference to memory 
	@param offset Offset in memory  */
    RcuIRDAT(RcuMemory& mem, size_t offset)
      : RcuRegister(mem, offset, "IRDAT", 0, false), fIRDAT(0) {}
    /** Decode @a dat as the last executed ALTRO InstRuction  ADDress  */
    virtual void Decode(unsigned int dat);
    /** Print to standard output */
    virtual void Print() const;
    /** @return Last instruction address */
    unsigned int IRDAT() const { return fIRDAT; }
  protected:
    unsigned int fIRDAT;
  };

  //====================================================================
  /** @struct RcuEVWORD rcuxx/Rcu.h <rcuxx/Rcu.h>
      @brief Interface to the EVent WORD register.  
      This provide the interface that concrete implementations should
      implement.   
      @note This register is only available with the U2F interface 
      @ingroup rcuxx_rcu
  */
  struct RcuEVWORD : public RcuRegister
  {
    /** Constructor  
	@param mem reference to memory 
	@param offset Offset in memory 
	@param clear Clear command */
    RcuEVWORD(RcuMemory& mem, size_t offset, RcuCommand* clear);
    /** @return @c true if we have a trigger */ 
    virtual bool HasTrigger(size_t i=2) const;
    /** @return @c true if we are at start of trigger */ 
    virtual bool IsStart(size_t i=2)   const;
    /** @return @c true if we are at end of trigger */ 
    virtual bool IsEnd(size_t i=2)   const;
    /** @return @c true if we have data in DMEM1 or DMEM2 */ 
    virtual bool HasData(size_t i, size_t j=2) const;
    /** Decode @a dat as the EVent WORD register  */
    virtual void Decode(unsigned int dat);
    /** Print to standard output */
    virtual void Print() const;
    /** @return Get trigger word from view @a i */
    unsigned int View(size_t i) const { return i == 1 ? fView1 : fView2; }
    /** @return Get trigger word from view 1 */
    unsigned int View1() const { return fView1; }
    /** @return Get trigger word from view 2 */
    unsigned int View2() const { return fView2; }
  protected:
    unsigned int fView1;
    unsigned int fView2;
  };
  inline bool RcuEVWORD::HasTrigger(size_t j)  const
  {
    return (j == 1 ? (fView1 & 0x10) : (fView2 & 0x10));
  }
  inline bool RcuEVWORD::IsStart(size_t j) const
  {
    return (j == 1 ? (fView1 & 0x8) : (fView2 & 0x8));
  }
  inline bool RcuEVWORD::IsEnd(size_t j) const
  {
    return (j == 1 ? (fView1 & 0x4) : (fView2 & 0x4));
  }
  inline bool RcuEVWORD::HasData(size_t i, size_t j) const
  {
    return (j == 1 
	    ? fView1 & (i == 1 ? 0x2 : 0x1) 
	    : fView2 & (i == 1 ? 0x2 : 0x1));
  }
  
  //====================================================================
  /** @struct RcuACTFEC rcuxx/Rcu.h <rcuxx/Rcu.h> 
      @brief Interface to the ACTive FrontEnd Card.  
      This provide the interface that concrete implementations should
      implement.   @ingroup rcuxx_rcu
  */
  struct RcuACTFEC : public RcuRegister
  {
    /** Constructor  
	@param mem reference to memory 
	@param offset Offset in memory */
    RcuACTFEC(RcuMemory& mem, size_t offset)
      : RcuRegister(mem, offset, "ACTFEC", 0, true), fValue(0x1) {}

    /** Decode @a dat as ACTive Front-End-Cards mask  */
    virtual void Decode(unsigned int dat) { fValue = dat; }
    /** Encode as hardware value  */
    virtual unsigned int Encode() { return fValue; }
    /** Print to standard output */
    virtual void Print() const;
    /** @return bit mask of active front-end cards */
    unsigned int Value() const { return fValue; }
    /** Set bit mask of active front-end cards */
    void SetValue(unsigned int val) { fValue = val; }
    /** Turn on card @a i */ 
    void SetOn(unsigned int n, bool on=true) { 
      if (on) fValue |= (1 << n); 
      else    fValue &= ~(1 << n); 
    }
    /** Check if card @a i is on */ 
    bool IsOn(unsigned int n) const { return fValue & (1 << n); }
    /** Syncronize this register to the ACL memory 
	@param acl Referenc to ACL memory  */
    void SyncToACL(const RcuMemory& acl);
    /** Syncronize ACL memory to this.   Note, that this operation
	turns on all 16 channels of all 8 ALTRO's of the turned on
	cards 
	@param acl  Referenc to ACL memory  
	@param mask Bit mask of ALTRO's to read-out. */
    void ACLSync(RcuMemory& acl, unsigned int mask=0xffff) const;
  protected:
    unsigned int fValue;
  };

  //====================================================================
  /** @struct RcuRDOFEC rcuxx/Rcu.h <rcuxx/Rcu.h> 
      @brief Interface to the ReaDOut FrontEnd Card register.  
      This provide the interface that concrete implementations should
      implement.  
      @ingroup rcuxx_rcu
  */
  struct RcuRDOFEC : public RcuRegister
  {
    /** Constructor  
	@param mem reference to memory 
	@param offset Offset in memory */
    RcuRDOFEC(RcuMemory& mem, size_t offset)
      : RcuRegister(mem, offset, "RDOFEC", 0, false), fValue(0x0) {}

    /** Decode @a dat as ACTive Front-End-Cards mask  */
    virtual void Decode(unsigned int dat);
    /** Print to standard output */
    virtual void Print() const;
    /** @return bit mask of active front-end cards */
    unsigned int Value() const { return fValue; }
    /** Check if card @a i is on */ 
    bool IsOn(unsigned int n) { return fValue & (1 << n); }
  protected:
    unsigned int fValue;
  };

  //====================================================================
  /** @struct RcuTRCFG1 rcuxx/Rcu.h <rcuxx/Rcu.h> 
      @brief Interface to the TRigger ConFiG register.  
      This provide the interface that concrete implementations should
      implement.   
      @ingroup rcuxx_rcu
  */
  struct RcuTRCFG1 : public RcuRegister
  {
    /** Buffer size */
    enum BMD_t {
      /** 4 Buffers */
      k4Buffers,
      /** 8 Buffers  */
      k8Buffers
    };
    /** Mode buttons */
    enum Mode_t {
      /** Software trigger */
      kInternal = 0, 
      /** Ext. L1, internal L2 */
      kDerivedL2 = 2, 
      /** Ext. L1 and L2 */
      kExternal = 3
    };
    /** Constructor 
	@param mem reference to memory 
	@param offset Offset in memory
	@param clear Clear command */
    RcuTRCFG1(RcuMemory& mem, size_t offset, RcuCommand* clear)
      : RcuRegister(mem, offset, "TRCFG1", clear, true), 
	fBMD(k4Buffers),
	fMode(kDerivedL2), 
	fTwv(4096),
	fOpt(false),
	fPop(true)
    {}
    /** Decode @a dat as  */
    virtual void Decode(unsigned int dat);
    /** Encode set values and return hardware register value */
    virtual unsigned int Encode();
    /** Print to standard output */
    virtual void Print() const;
    /** @return BC to optimize readout */
    bool IsOpt() const { return fOpt; }
    /** Use BC to optimize readout */
    void SetOpt(bool on=true) { fOpt = on; } 
    /** @return Popped readout e.g., via U2F */
    bool IsPop() const { return fPop; }
    /** Popped readout e.g., via U2F */
    void SetPop(bool on=true) { fPop = on; }
    /** Buffer MoDe input/output */
    BMD_t BMD() const { return fBMD; }
    /** Buffer MoDe input/output */
    void SetBMD(BMD_t bmd) { fBMD = bmd; }
    /** Mode buttons */
    Mode_t Mode() const { return fMode; }
    /** Mode buttons */
    void SetMode(Mode_t mode) { fMode = mode; }
    /** @return # clock cycles between L1 and L2 */
    unsigned int Twv() const { return fTwv; }
    /** Set # clock cycles between L1 and L2 */
    void SetTwv(unsigned int val) { fTwv = val; }
    /** Write pointer */
    unsigned int Wptr() const { return fWptr; }
    /** Read pointer */
    unsigned int Rptr() const { return fRptr; }
    /** Free buffers */
    unsigned int Remb() const { return fRemb; }
    /** @return is empty? */
    bool IsEmpty() const { return fEmpty; }
    /** @return is Full? */
    bool IsFull() const { return fFull; }
  protected:
    /** Use BC to optimize readout */
    bool fOpt;
    /** Popped readout e.g., via U2F */
    bool fPop;
    /** Buffer MoDe input/output */
    BMD_t fBMD;
    /** Mode buttons */
    Mode_t fMode;
    /** # clock cycles between L1 and L2 */
    unsigned int fTwv;
    /** Write pointer */
    unsigned int fWptr;
    /** Read pointer */
    unsigned int fRptr;
    /** Free buffers */
    unsigned int fRemb;
    bool fEmpty;
    bool fFull;
  };

  //====================================================================
  /** @struct RcuTRCFG2 rcuxx/Rcu.h <rcuxx/Rcu.h>
      @brief Interface to the ReaD-Out MODe register.  
      This provide the interface that concrete implementations should
      implement.   
      @note This register is only available with the U2F interface 
      @ingroup rcuxx_rcu
  */
  struct RcuTRCFG2 : public RcuRegister
  {
    /** Read out mode */
    enum Mode_t {
      kPop, 
      kPush
    };

    /** Constructor 
	@param mem reference to memory 
	@param offset Offset in memory */
    RcuTRCFG2(RcuMemory& mem, size_t offset)
      : RcuRegister(mem, offset, "TRCFG2", 0, true), 
	fMode(kPop), 
	fEnableHT(0) 
    {}
    /** Decode @a dat as  */
    virtual void Decode(unsigned int dat);
    /** Encode set values and return hardware register value */
    virtual unsigned int Encode();
    /** Print to standard output */
    virtual void Print() const;

    /** Mode */
    Mode_t Mode() const { return fMode; }
    /** Mode */
    void SetMode(Mode_t mode) { fMode = mode; }
    /** @return hardware trigger enabled */
    bool IsEnableHT() const { return fEnableHT; }
    /** Enable hardware trigger */
    void SetEnableHT(bool on=true) { fEnableHT = on; }
  protected:
    /** Mode */
    Mode_t fMode;
    /** Enable hardware trigger */
    bool fEnableHT;
  };

  //====================================================================
  /** @struct RcuFMIREG rcuxx/Rcu.h <rcuxx/Rcu.h>
      @brief Interface to the FirMware Input ReGister.
      @ingroup rcuxx_rcu
  */
  struct RcuFMIREG : public RcuRegister
  {
    /** Constructor 
	@param mem reference to memory 
	@param offset Offset in memory */
    RcuFMIREG(RcuMemory& mem, size_t offset)
      : RcuRegister(mem, offset, "FMIREG", 0, true), 
	fData(0) 
    {}
    /** Decode @a dat as  */
    virtual void Decode(unsigned int dat);
    /** Encode set values and return hardware register value */
    virtual unsigned int Encode();
    /** Print to standard output */
    virtual void Print() const;
    /** @return Data to write */
    unsigned int Data() const { return fData; }
    /** Set data to write */
    void SetData(unsigned int val) { fData = val; }
  protected:
    unsigned int fData;

  };

  //====================================================================
  /** @struct RcuFMOREG rcuxx/Rcu.h <rcuxx/Rcu.h>
      @brief Interface to the FirMware Output ReGister 
      This provide the interface that concrete implementations should
      implement.   @ingroup rcuxx_rcu
  */
  struct RcuFMOREG : public RcuRegister
  {
    /** Constructor 
	@param mem reference to memory 
	@param offset Offset in memory */
    RcuFMOREG(RcuMemory& mem, size_t offset)
      : RcuRegister(mem, offset, "FMOREG", 0, true), 
	fData(0) 
    {}
    /** Decode @a dat as  */
    virtual void Decode(unsigned int dat);
    /** Encode set values and return hardware register value */
    virtual unsigned int Encode();
    /** Print to standard output */
    virtual void Print() const;
    /** @return Data to read */
    unsigned int Data() const { return fData; }
    /** Set data to read */
    void SetData(unsigned int val) { fData = val; }
  protected:
    unsigned int fData;
  };

  //====================================================================
  /** @struct RcuPMCFG rcuxx/Rcu.h <rcuxx/Rcu.h>
      @brief Interface to the Pedestal Memory ConFiGuration
      This provide the interface that concrete implementations should
      implement.   
      @ingroup rcuxx_rcu
  */
  struct RcuPMCFG : public RcuRegister
  {
    /** Constructor 
	@param mem reference to memory 
	@param offset Offset in memory */
    RcuPMCFG(RcuMemory& mem, size_t offset)
      : RcuRegister(mem, offset, "PMCFG", 0, true), 
	fBegin(0), fEnd(0)
    {}
    /** Decode @a dat as  */
    virtual void Decode(unsigned int dat);
    /** Encode set values and return hardware register value */
    virtual unsigned int Encode();
    /** Print to standard output */
    virtual void Print() const;
    /** @return Begin of block address */
    unsigned int Begin() const { return fBegin; }
    /** @return End of block address */
    unsigned int End() const { return fEnd; }
    /** Set start of block address */
    void SetBegin(unsigned int val) { fBegin = (val & 0x3ff); }
    /** Set end of block address */
    void SetEnd(unsigned int val) { fEnd = (val & 0x3ff); }
  protected:
    unsigned int fBegin;
    unsigned int fEnd;
  };

  //====================================================================
  /** @struct RcuCHADD rcuxx/Rcu.h <rcuxx/Rcu.h>
      @brief Interface to the CHannel Address register 
      This provide the interface that concrete implementations should
      implement.   
      @ingroup rcuxx_rcu
  */
  struct RcuCHADD : public RcuRegister
  {
    /** Constructor 
	@param mem reference to memory 
	@param offset Offset in memory */
    RcuCHADD(RcuMemory& mem, size_t offset)
      : RcuRegister(mem, offset, "CHADD", 0, false), 
	fAddress1(0), fAddress2(0) 
    {}
    /** Decode @a dat as  */
    virtual void Decode(unsigned int dat);
    /** Print to standard output */
    virtual void Print() const;
    /** @return Address of data in memory 1 */
    unsigned int Address1() const { return fAddress1; }
    /** @return Address of data in memory 2 */
    unsigned int Address2() const { return fAddress2; }
  protected:
    unsigned int fAddress1;
    unsigned int fAddress2;
  };


  //====================================================================
  /** @struct RcuStatusEntry  rcuxx/Rcu.h <rcuxx/Rcu.h>
      @brief Class to hold status data from the BC's
   */
  struct RcuStatusEntry 
  {
    /** Constructor 
	@param data The data */
    RcuStatusEntry(unsigned int data=0);
    /** Constructor 
	@param id Whether we have a valid ID
	@param where Where the interrupt came from
	@param soft If it's a soft interrupt
	@param ans Whether the address is answering 
	@param sclk If it's missed sample clocks
	@param alps If it's an ALTRO power supply error
	@param paps If it's an PASA power supply error
	@param dc If it's a digital current error
	@param dv If it's a digital voltage error
	@param ac If it's a analogue current error
	@param av If it's a analogue voltage error
	@param temp If it's temperature errror  */
    RcuStatusEntry(bool id, unsigned int where, bool soft, bool ans, 
		   bool sclk, bool alps, bool paps, bool dc, bool dv, 
		   bool ac, bool av, bool temp);
    /** Copy constructor 
	@param other Object to copy from */
    RcuStatusEntry(const RcuStatusEntry& other);
    /** Assignment operator 
	@param other Object to assign from 
	@return reference to this object  */
    RcuStatusEntry& operator=(const RcuStatusEntry& other);
    /** Decode @a dat as  */
    virtual void Decode(unsigned int dat);
    /** Print to standard output */
    virtual void Print() const;
    /** @return If the card causing the interrupt is identified */
    bool IsIdentified() const { return fIsIdentified; }
    /** @return the hardware address of the card that caused the
	interrupt.  If the 4th bit (counting from 0) is set, then it's
	on branch B */
    unsigned int Where() const { return fWhere; }
    /** @return @c true if the interrupt was soft */
    bool IsSoft() const { return fIsSoft; }
    /** @return @c true if the card is answering */
    bool IsAnswering() const { return fIsAnswering; }
    /** @return If a missed sample clocks interrupt is set */
    bool IsMissedSclk() const { return fMissedSclk; } 
    /** @return If a ALTRO power supply error interrupt is set */
    bool IsAlps() const { return fAlps; } 
    /** @return If a PASA power supply error interrupt is set */
    bool IsPaps() const { return fPaps; } 
    /** @return If a digital current over threshold interrupt is set */
    bool IsDcOverTh() const { return fDcOverTh; } 
    /** @return If a digital voltage over threshold interrupt is set */
    bool IsDvOverTh() const { return fDvOverTh; } 
    /** @return If a analog current over threshold interrupt is set */
    bool IsAcOverTh() const { return fAcOverTh; } 
    /** @return If a analog voltage over threshold interrupt is set */
    bool IsAvOverTh() const { return fAvOverTh; } 
    /** @return If a temperture over threshold interrupt is set */
    bool IsTempOverTh() const { return fTempOverTh; } 
  protected:
    /** Is the card address known */
    bool fIsIdentified;
    /** Address of the card.  If the top bit is set, then it's on
	branch B */
    unsigned int fWhere;
    /** Whether this is a soft interrupt */
    bool fIsSoft;
    /** Whether the FEC answers. */
    bool fIsAnswering;
    /** SCLK - Missed sample clocks */
    bool fMissedSclk;
    /** Alps - ALTRO power supply error */
    bool fAlps;
    /** Paps - PASA power supply error */
    bool fPaps;
    /** DC - Digital current over threshold */
    bool fDcOverTh;
    /** DV - Digital voltage over threshold */
    bool fDvOverTh;
    /** AC - Analog current over threshold */
    bool fAcOverTh;
    /** AV - Analog voltage over threshold */
    bool fAvOverTh;
    /** T - Temperture over threshold */
    bool fTempOverTh;
  };

  //====================================================================
  /** @struct RcuINTREG rcuxx/Rcu.h <rcuxx/Rcu.h>
      @brief Interface to the INTerrupt REGister 
      This provide the interface that concrete implementations should
      implement.   
      @ingroup rcuxx_rcu
  */
  struct RcuINTREG : public RcuRegister
  {
    /** Constructor 
	@param mem reference to memory 
	@param offset Offset in memory */
    RcuINTREG(RcuMemory& mem, size_t offset)
      : RcuRegister(mem, offset, "INTREG", 0, false), 
	fEntry(true, 0, false, false, false, false, false, false, 
	       false, false, false, false)
    {}
    /** Decode @a dat as  */
    virtual void Decode(unsigned int dat) { fEntry.Decode(dat); }
    /** Print to standard output */
    virtual void Print() const
    {
      RcuRegister::Print();
      fEntry.Print();
    }
    /** @return If the card causing the interrupt is identified */
    bool IsIdentified() const { return fEntry.IsIdentified(); }
    /** @return the hardware address of the card that caused the
	interrupt.  If the 4th bit (counting from 0) is set, then it's
	on branch B */
    unsigned int Where() const { return fEntry.Where(); }
    /** @return @c true if the interrupt was soft */
    bool IsSoft() const { return fEntry.IsSoft(); }
    /** @return @c true if the card is answering */
    bool IsAnswering() const { return fEntry.IsAnswering(); }
    /** @return If a missed sample clocks interrupt is set */
    bool IsMissedSclk() const { return fEntry.IsMissedSclk(); } 
    /** @return If a ALTRO power supply error interrupt is set */
    bool IsAlps() const { return fEntry.IsAlps(); } 
    /** @return If a PASA power supply error interrupt is set */
    bool IsPaps() const { return fEntry.IsPaps(); } 
    /** @return If a digital current over threshold interrupt is set */
    bool IsDcOverTh() const { return fEntry.IsDcOverTh(); } 
    /** @return If a digital voltage over threshold interrupt is set */
    bool IsDvOverTh() const { return fEntry.IsDvOverTh(); } 
    /** @return If a analog current over threshold interrupt is set */
    bool IsAcOverTh() const { return fEntry.IsAcOverTh(); } 
    /** @return If a analog voltage over threshold interrupt is set */
    bool IsAvOverTh() const { return fEntry.IsAvOverTh(); } 
    /** @return If a temperture over threshold interrupt is set */
    bool IsTempOverTh() const { return fEntry.IsTempOverTh(); } 
    const RcuStatusEntry& Entry() const { return fEntry; }
  protected:
    RcuStatusEntry fEntry;
  };

  //====================================================================
  /** @struct RcuRESREG rcuxx/Rcu.h <rcuxx/Rcu.h>
      @brief Interface to the RESult REGister 
      This provide the interface that concrete implementations should
      implement.   
      @ingroup rcuxx_rcu
  */
  struct RcuRESREG : public RcuRegister
  {
    /** Constructor 
	@param mem reference to memory 
	@param offset Offset in memory */
    RcuRESREG(RcuMemory& mem, size_t offset)
      : RcuRegister(mem, offset, "RESREG", 0, true), 
	fAddress(0), fResult(0)
    {}
    /** Decode @a dat as  */
    virtual void Decode(unsigned int dat);
    /** Encode  */
    virtual unsigned int Encode();
    /** Print to standard output */
    virtual void Print() const;
    /** @return Address of data in memory 1 */
    unsigned int Address() const { return fAddress; }
    /** @return Address of data in memory 2 */
    unsigned int Result() const { return fResult; }
    /** Set address */
    void SetAddress(unsigned int addr) { fAddress = addr & 0x1F; }
    /** Set result (?) */
    void SetResult(unsigned int res) { fAddress = res & 0xffff; }
  protected:
    unsigned int fAddress;
    unsigned int fResult;
  };

  //====================================================================
  /** @struct RcuERRREG rcuxx/Rcu.h <rcuxx/Rcu.h>
      @brief Interface to the ERRor REGister 
      This provide the interface that concrete implementations should
      implement.   
      @ingroup rcuxx_rcu
  */
  struct RcuERRREG : public RcuRegister
  {
    /** Constructor 
	@param mem reference to memory 
	@param offset Offset in memory 
	@param clear Clear command */
    RcuERRREG(RcuMemory& mem, size_t offset, RcuCommand* clear=0)
      : RcuRegister(mem, offset, "ERRREG", clear, false), 
	fIsNoAcknowledge(false), fIsNotActive(0)
    {}
    /** Decode @a dat as  */
    virtual void Decode(unsigned int dat);
    /** Print to standard output */
    virtual void Print() const;
    /** @return Address of data in memory 1 */
    unsigned int IsNoAcknowledge() const { return fIsNoAcknowledge; }
    /** @return Address of data in memory 2 */
    unsigned int IsNotActive() const { return fIsNotActive; }
  protected:
    bool fIsNoAcknowledge;
    bool fIsNotActive;
  };

  //====================================================================
  /** @struct RcuINTMOD rcuxx/Rcu.h <rcuxx/Rcu.h>
      @brief Interface to the INTerrupt MODe register 
      This provide the interface that concrete implementations should
      implement.   
      @ingroup rcuxx_rcu
  */
  struct RcuINTMOD : public RcuRegister
  {
    /** Constructor 
	@param mem reference to memory 
	@param offset Offset in memory */
    RcuINTMOD(RcuMemory& mem, size_t offset)
      : RcuRegister(mem, offset, "INTMOD", 0, true), 
	fBranchA(true), fBranchB(true)
    {}
    /** Decode @a dat as  */
    virtual void Decode(unsigned int dat);
    /** Encode  */
    virtual unsigned int Encode();
    /** Print to standard output */
    virtual void Print() const;
    /** @return BranchA of data in memory 1 */
    bool BranchA() const { return fBranchA; }
    /** @return BranchA of data in memory 2 */
    bool BranchB() const { return fBranchB; }
    /** Set BranchA */
    void SetBranchA(bool addr) { fBranchA = addr; }
    /** Set BranchB (?) */
    void SetBranchB(bool res) { fBranchB = res; }
  protected:
    bool fBranchA;
    bool fBranchB;
  };

  //====================================================================
  /** @struct RcuIMEM rcuxx/Rcu.h <rcuxx/Rcu.h>
      @brief Interface to the RCU instruction memory
      @ingroup rcuxx_rcu
  */;

  struct RcuIMEM  : public RcuMemory
  {
    /** Constructor 
	@param rcu Reference to low-level interface 
	@param base Base address
	@param n Number of words  
	@param exec Execute command 
	@param abort Abort command */
    RcuIMEM(Rcu& rcu, size_t base, size_t n, 
	    RcuCommand* exec, RcuCommand* abort);
  };

  //====================================================================
  /** @struct RcuPMEM rcuxx/Rcu.h <rcuxx/Rcu.h>
      @brief Interface to the RCU pedestal memory
      @ingroup rcuxx_rcu
  */
  struct RcuPMEM  : public RcuMemory
  {
    /** Constructor 
	@param rcu Reference to low-level interface 
	@param base Base address
	@param n Number of words  */
    RcuPMEM(Rcu& rcu, size_t base, size_t n);
  };


  //====================================================================
  /** @struct RcuRMEM rcuxx/Rcu.h <rcuxx/Rcu.h>
      @brief Interface to the RCU result memory
      @ingroup rcuxx_rcu
  */
  struct RcuRMEM  : public RcuMemory
  {
    /** Constructor 
	@param rcu Reference to low-level interface 
	@param base Base address
	@param n Number of words  */
    RcuRMEM(Rcu& rcu, size_t base, size_t n);
  };

  //====================================================================
  /** @struct RcuDMEM rcuxx/Rcu.h <rcuxx/Rcu.h>
      @brief Interface to the RCU data memory
      @ingroup rcuxx_rcu
  */
  struct RcuDMEM  : public RcuMemory
  {
    /** 
        @param which Which memory (1 or 2)
        @param rcu Reference to RCU
        @param lbase Base address for low-bits
        @param hbase Base address for high-bits
	@param n Size
	@param clear Clear command */
    RcuDMEM(size_t which, Rcu& rcu, size_t lbase, size_t hbase, size_t n, 
	    RcuCommand* clear);
    /** Commit to hard-ware */
    virtual unsigned int Commit() { return RcuMemory::Commit(); }
    /** Read from hard-ware */
    virtual unsigned int Update() { return this->Update(0, fSize); }
    /** Commit to hard-ware 
	@param offset Start address 
	@param n How many words */
    virtual unsigned int Commit(size_t offset, size_t n);
    /** Read from hard-ware 
	@param offset Start address 
	@param n How many words */
    virtual unsigned int Update(size_t offset, size_t n);
  protected:
    size_t fWhich;
    unsigned int fBaseHigh;
  };

  //====================================================================
  /** @struct RcuACL rcuxx/Rcu.h <rcuxx/Rcu.h>
      @brief Interface to the RCU Active Channel List memory
      @ingroup rcuxx_rcu
  */
  struct RcuACL  : public RcuMemory
  {
    /** Constructor 
	@param rcu Reference to low-level interface 
	@param base Base address
	@param n Number of words  */
    RcuACL(Rcu& rcu, size_t base, size_t n);
  };
  
  //====================================================================
  /** @struct RcuHEADER rcuxx/Rcu.h <rcuxx/Rcu.h>
      @brief Interface to the RCU Event Header memory
      @ingroup rcuxx_rcu
      The header has the following format 
      @verbatim 
      32     28   24       18    14    12
      +-----+-----+--------+-----+-----+--------------+
      |              Block lenght                     |
      +-----------+--------------+-----+--------------+
      |  Version  | L1 message   | MBZ | Event Id 1   |
      +-----------+--------------+-----+--------------+
      |   MBZ     |           Event Id 2              |
      +-----------+-----------------------------------+
      | Attribu   |        Sub-detector mask          |
      +-----+-----+--------------------+--------------+
      | MBZ |      Status & Error      | Mini Event   |
      +-----+--------------------------+--------------+
      |         Trigger class (low bits)              |
      +-----+--------------+--------------------------+
      | ROI |     MBZ      |   Trigger class (high)   |
      +-----+--------------+--------------------------+
      |              ROI (high bits)                  |
      +-----------------------------------------------+
      @endverbatim
  */  
  struct RcuHEADER  : public RcuMemory
  {
    typedef unsigned long long ulong64_t;

    /** Constructor 
	@param rcu Reference to low-level interface 
	@param base Base address
	@param n Number of words  */
    RcuHEADER(Rcu& rcu, size_t base, size_t n);
    /** @return block lenght (optional) */
    unsigned int BlockLength() const { return fData[0]; }
    /** @return Format version number */
    unsigned int FormatVersion() const { return (fData[1] >> 24); }
    /** @return  L1 trigger message */
    unsigned int L1Message() const { return (fData[1] >> 14) & 0x3FF; }
    /** @return  Bunch crossing number */
    unsigned int BunchCrossing() const { return (fData[1] & 0xFFF); }
    /** @return  Orbit number */
    unsigned int Orbit() const { return (fData[2] & 0xFFFFFF); }
    /** @return  Block attributes */
    unsigned int Attributes() const { return (fData[3] >> 24); }
    /** @return  Participting sub-detectors */
    unsigned int SubDetectors() const { return (fData[3] & 0xFFFFFF); }
    /** @return  Status and error bits */
    unsigned int StatusErrors() const { return (fData[4] >> 12) & 0xFFFF; } 
    /** @return  Mini event id */
    unsigned int MiniBunchCrossing() const { return fData[4] & 0xFFF; }
    /** @return  Trigger classes */
    unsigned long long TriggerClasses() const 
    { 
      return fData[5] + ((ulong64_t(fData[6]) & 0x3FFFF) << 32);
    }
    /** @return Region of intrest */
    unsigned long long RegionOfIntrest() const 
    { 
      return (fData[6] >> 28) 
	+ ((ulong64_t(fData[7]) & 0xFFFFFFFF) << 4);
    }
    /** @{ 
	@name Status and error bits */
    bool TriggerOverlap() const { return StatusErrors() & 0x1; }
    bool TriggerMissing() const { return StatusErrors() & 0x2; }
    bool DataParityError() const { return StatusErrors() & 0x4; }
    bool ControlParityError() const { return StatusErrors() & 0x8; }
    bool NoTriggerInfo() const { return StatusErrors() & 0x10; }
    bool FeeError() const { return StatusErrors() & 0x20; }
    /** @} */
     

    /** @param x block lenght (optional) */
    void  SetBlockLength(unsigned int x) { fData[0] = x; }
    /** @param x Format version number */
    void  SetFormatVersion(unsigned int x) { fData[1] += (x & 0xFF) << 24; }
    /** @param x  L1 trigger message */
    void  SetL1Message(unsigned int x) { fData[1] += (x  & 0x3FF) << 14; }
    /** @param x  Bunch crossing number */
    void  SetBunchCrossing(unsigned int x) { fData[1] += x & 0xFFF; }
    /** @param x  Orbit number */
    void  SetOrbit(unsigned int x) { fData[2] = x & 0xFFFFFF; }
    /** @param x  Block attributes */
    void  SetAttributes(unsigned int x) { fData[3] += (x & 0xFF) << 24; }
    /** @param x  Participting sub-detectors */
    void  SetSubDetectors(unsigned int x) { fData[3] += x & 0xFFFFFF; }
    /** @param x  Status and error bits */
    void  SetStatusErrors(unsigned int x) { fData[4] += (x & 0xFFFF) << 12; } 
    /** @param x  Mini event id */
    void  SetMiniBunchCrossing(unsigned int x) { fData[4] += x & 0xFFF; }
    /** @param x  Trigger classes */
    void SetTriggerClasses(unsigned long long x)
    { 
      fData[5] =  (x & 0xFFFFFFFF);
      fData[6] += (x >> 32) & 0x3FFFF;
    }
    /** @param x Region of intrest */
    void SetRegionOfIntrest(unsigned long long x) 
    { 
      fData[6] += (x & 0xF) << 28;
      fData[7] =  (x >> 8) & 0xFFFFFFFF;
    }
    
  };

  //====================================================================
  /** @struct RcuSTATUS rcuxx/Rcu.h <rcuxx/Rcu.h>
      @brief Interface to the RCU Event Header memory
      @ingroup rcuxx_rcu
  */
  struct RcuSTATUS  : public RcuMemory
  {
    /** Constructor 
	@param rcu Reference to low-level interface 
	@param base Base address
	@param n Number of words  */
    RcuSTATUS(Rcu& rcu, size_t base, size_t n);
    /** Update the memory 
	@return 0 on success, error code otherwise */
    unsigned int Update() { return Update(0, fSize); }
    /** Update @a n words from the memory starting at @a offset 
	@param offset Where to start 
	@param n How many words to read. 
	@return 0 on success, error code otherwise */
    unsigned int Update(size_t offset, size_t n);
    /** Get the @f$ i^{th}@f$ entry. 
	@param i Entry to get
	@return Reference to the @a i entry */
    const RcuStatusEntry& Entry(size_t i) const;
    /** @return number of entries set. */
    size_t Entries() const { return fEntries; }
    /** Print cache to standard output */
    virtual void Print() const;
  protected:
    unsigned int fEntries;
    typedef std::vector<RcuStatusEntry> StatusList;
    StatusList fList;
  };

  //====================================================================
  /** @class Rcu rcuxx/Rcu.h <rcuxx/Rcu.h>
      @brief Abstract interface to the RCU communications.
      @ingroup rcuxx_rcu
  */
  class Rcu
  {
  public:
    /** Static function to create interface 
	@param url URL.  This can have the forms 
	@verbatim 
	U2F interface:

        	DEVICE
        	usb:DEVICE
        	u2f:DEVICE

	where DEVICE is something like '/dev/altro0'

	FEE client interface:

        	fee://HOST/SERVER
        	fee://HOST/
		fee:///SERVER

  	where HOST is the address of the DIM DNS, and SERVER is the
       	FeeServer name. 

	RORC client interface: 

		rorc://MINOR:CHANNEL
		ddl://MINOR:CHANNEL

	where MINOR is the minor device number (0 to 8) and CHANNEL is
	the channel number on that device (0 to 8). 
	@endverbatim
	@param emul Software emulation? 
	@return Pointer to newly allocated interface, or 0 on error */
    static Rcuxx::Rcu* Open(const char* url, bool emul=false);

    enum {
      kNotEnough, 
      kInconsistentSizes, 
      kUnknownMemory
    };
    enum {
      kRcu = 10, 
      kBackend, 
      kAltro, 
      kBc, 
      kFmd
    };

    /** Print to output stream @a out the possible device options
	@param out Stream to write on. */
    static void PrintHelp(std::ostream& out);
    /** @return Get version number as a string */
    static const std::string& Version();
    /** @return Get package short name as a string */
    static const std::string& Package();
    /** @return Get package string as a string */
    static const std::string& PackageString();

    /** Read a data file 
	@param name File name 
	@param data On return, the read data 
	@return @c true on success */ 
    static bool ReadFile(const std::string& name, 
			 std::vector<unsigned int>& data);
    /** Read a data file 
	@param stream Input stream
	@param data On return, the read data 
	@return @c true on success */ 
    static bool ReadFile(std::istream& stream, 
			 std::vector<unsigned int>& data);
    
    /** Destructor */
    virtual ~Rcu();
    /** Update (some of) the register caches */
    virtual unsigned int Update();
    /** Print contents of all registers (and memories) to standard out */
    virtual void Print() const;
    /** Get an error string corresponding to an error code 
	@param code The error code 
	@return Pointer to static string */
    virtual std::string ErrorString(unsigned int code);
    /** Set the debuging stuff */
    virtual void SetDebug(int id, int lvl);
    /** Execute a command 
	@param cmd Command to execute 
	@return 0 on success, error code otherwise */
    virtual unsigned int ExecCommand(unsigned int cmd) = 0;
    /** Read a memory block 
	@param addr Base address 
	@param offset Where to start reading 
	@param size How many words to read. 
	@param data Array of data to read into
	@return 0 on success, error code otherwise */ 
    virtual unsigned int ReadMemory(unsigned int addr, 
				    unsigned int offset, 
				    unsigned int& size, 
				    unsigned int* data) = 0;
    /** Write to a memory block 
	@param addr Base address 
	@param offset Where to start writing
	@param size How many words to write 
	@param data Array of data to write into
	@return 0 on success, error code otherwise */ 
    virtual unsigned int WriteMemory(unsigned int addr, 
				     unsigned int offset, 
				     unsigned int& size, 
				     unsigned int* data) = 0;
    /** Read a data memory block 
	@param lbase Base address for low bits
	@param hbase Base address for high bits
	@param offset Where to start reading 
	@param size How many words to read. 
	@param data Array of data to read into
	@return 0 on success, error code otherwise */ 
    virtual unsigned int ReadDataMemory(unsigned int lbase, 
					unsigned int hbase, 
					unsigned int offset, 
					unsigned int& size, 
					unsigned int* data);
    /** Write a data memory block 
	@param lbase Base address for low bits
	@param hbase Base address for high bits
	@param offset Where to start writing 
	@param size How many words to write 
	@param data Array of data to write
	@return 0 on success, error code otherwise */ 
    virtual unsigned int WriteDataMemory(unsigned int lbase, 
					 unsigned int hbase, 
					 unsigned int offset, 
					 unsigned int& size, 
					 unsigned int* data);

    /** @{
	@name Access to registers */
    /** @return ERRor and STatus register.   */
    RcuERRST*	ERRST()		{ return fERRST; }
    /** @return TRigger CouNTers.   */
    RcuTRCNT*	TRCNT()		{ return fTRCNT; }
    /** @return Last Written ADDress in the RcuDMEM1 and RcuDMEM2.   */
    RcuLWADD*	LWADD()		{ return fLWADD; }
    /** @return Last executed ALTRO InstRuction ADDress.   */
    RcuIRADD*	IRADD()		{ return fIRADD; }
    /** @return Last executed ALTRO InstRuction DATa. */
    RcuIRDAT*	IRDAT()		{ return fIRDAT; }
    /** @return EVent WORD register.   */
    RcuEVWORD*	EVWORD()	{ return fEVWORD; }
    /** @return ACTive FrontEnd Card.   */
    RcuACTFEC*	ACTFEC()	{ return fACTFEC; }
    /** @return ReaDOut FrontEnd Card register.   */
    RcuRDOFEC*	RDOFEC()	{ return fRDOFEC; }
    /** @return TRigger ConFiG register.   */
    RcuTRCFG1*	TRCFG1()	{ return fTRCFG1; }
    /** @return ReaD-Out MODe register.   */
    RcuTRCFG2*	TRCFG2()	{ return fTRCFG2; }
    /** @return FirMware Input ReGister. */
    RcuFMIREG*	FMIREG()	{ return fFMIREG; }
    /** @return FirMware Output ReGister  */
    RcuFMOREG*	FMOREG()	{ return fFMOREG; }
    /** @return Pedestal Memory ConFiGuration */
    RcuPMCFG*	PMCFG()		{ return fPMCFG; }
    /** @return CHannel Address register  */
    RcuCHADD*	CHADD()		{ return fCHADD; }
    /** @return INTerrupt REGister  */
    RcuINTREG*	INTREG()	{ return fINTREG; }
    /** @return RESult REGister  */
    RcuRESREG*	RESREG()	{ return fRESREG; }
    /** @return ERRor REGister  */
    RcuERRREG*	ERRREG()	{ return fERRREG; }
    /** @return INTerrupt MODe register  */
    RcuINTMOD*	INTMOD()	{ return fINTMOD; }
    /** @} */

    /** @{
	@name Access to the memories */
    /** @return RCU instruction memory */
    RcuIMEM*	IMEM()	{ return fIMEM; } 
    /** @return RCU pedestal memory */
    RcuPMEM*	PMEM()	{ return fPMEM; } 
    /** @return RCU result memory */
    RcuRMEM*	RMEM()	{ return fRMEM; } 
    /** @return RCU data memory */
    RcuDMEM*	DM1()	{ return fDM1; } 
    /** @return RCU data memory */
    RcuDMEM*	DM2()	{ return fDM2; } 
    /** @return RCU Active Channel List memory */
    RcuACL*	ACL()	{ return fACL; } 
    /** @return RCU event HEADER memory */
    RcuHEADER*	HEADER(){ return fHEADER; } 
    /** @return Status memory */
    RcuSTATUS*  STATUS() { return fSTATUS; }
    /** @} */

    /** @{
	@name Access to the commands */
    /** @return ABORT instruction execution  */
    RcuCommand* ABORT()		{ return fABORT; } 
    /** @return CLeaR EVent TAG command */
    RcuCommand* CLR_EVTAG()	{ return fCLR_EVTAG; } 
    /** @return DCS OwN bus command */
    RcuCommand* DCS_ON()	{ return fDCS_ON; } 
    /** @return DDL OwN bus command */
    RcuCommand* DDL_ON()	{ return fDDL_ON; } 
    /** @return EXECute instructions command */
    RcuCommand* EXEC()		{ return fEXEC; } 
    /** @return Front-End Card ReSeT command */
    RcuCommand* FECRST()	{ return fFECRST; } 
    /** @return GLoBal RESET command */
    RcuCommand* GLB_RESET()	{ return fGLB_RESET; } 
    /** @return L1-enable via CoMmanD command */
    RcuCommand* L1_CMD()	{ return fL1_CMD; } 
    /** @return L1-enable via I2C command */
    RcuCommand* L1_I2C()	{ return fL1_I2C; } 
    /** @return L1-enable via TTC command */
    RcuCommand* L1_TTC()	{ return fL1_TTC; } 
    /** @return RCU RESET command */
    RcuCommand* RCU_RESET()	{ return fRCU_RESET; } 
    /** @return ReaD-out ABORT command */
    RcuCommand* RDABORT()	{ return fRDABORT; } 
    /** @return FirMware command */
    RcuCommand* RDFM()		{ return fRDFM; } 
    /** @return ReSet DMEM1 command */
    RcuCommand* RS_DMEM1()	{ return fRS_DMEM1; } 
    /** @return ReSet DMEM2 command */
    RcuCommand* RS_DMEM2()	{ return fRS_DMEM2; } 
    /** @return ReSet STATUS command */
    RcuCommand* RS_STATUS()	{ return fRS_STATUS; } 
    /** @return ReSet TRigger ConFiGuration command */
    RcuCommand* RS_TRCFG()	{ return fRS_TRCFG; } 
    /** @return ReSet TRigger CouNTer command */
    RcuCommand* RS_TRCNT()	{ return fRS_TRCNT; } 
    /** @return Slow Control COMMAND command */
    RcuCommand* SCCOMMAND()	{ return fSCCOMMAND; } 
    /** @return SoftWare TRiGger command */
    RcuCommand* SWTRG()		{ return fSWTRG; } 
    /** @return TRiGger CLeaR command */
    RcuCommand* TRG_CLR()	{ return fTRG_CLR; } 
    /** @return WRite FirMware command */
    RcuCommand* WRFM()		{ return fWRFM; } 
    /** @return ReSet ERRor REGister */
    RcuCommand* RS_ERRREG()     { return fRS_ERRREG; }
    /** @} */
  protected:
    /** Create the RCU interface */
    Rcu();
    /** Create the RCU interface */
    Rcu(const Rcu& other);
    /** Assignment 
	@param o Other */
    Rcu& operator=(const Rcu& o);


    /** @{
	@name Access to registers */
    /** ERRor and STatus register.   */
    RcuERRST*	fERRST;
    /** TRigger CouNTers.   */
    RcuTRCNT*	fTRCNT;
    /** Last Written ADDress in the RcuDMEM1 and RcuDMEM2.   */
    RcuLWADD*	fLWADD;
    /** Last executed ALTRO InstRuction ADDress.   */
    RcuIRADD*	fIRADD;
    /** Last executed ALTRO InstRuction DATa. */
    RcuIRDAT*	fIRDAT;
    /** EVent WORD register.   */
    RcuEVWORD*	fEVWORD;
    /** ACTive FrontEnd Card.   */
    RcuACTFEC*	fACTFEC;
    /** ReaDOut FrontEnd Card register.   */
    RcuRDOFEC*	fRDOFEC;
    /** TRigger ConFiG register.   */
    RcuTRCFG1*	fTRCFG1;
    /** ReaD-Out MODe register.   */
    RcuTRCFG2*	fTRCFG2;
    /** FirMware Input ReGister. */
    RcuFMIREG*	fFMIREG;
    /** FirMware Output ReGister  */
    RcuFMOREG*	fFMOREG;
    /** Pedestal Memory ConFiGuration */
    RcuPMCFG*	fPMCFG;
    /** CHannel Address register  */
    RcuCHADD*	fCHADD;
    /** INTerrupt REGister  */
    RcuINTREG*	fINTREG;
    /** RESult REGister  */
    RcuRESREG*	fRESREG;
    /** ERRor REGister  */
    RcuERRREG*	fERRREG;
    /** INTerrupt MODe register  */
    RcuINTMOD*	fINTMOD;
    /** @} */

    /** @{
	@name Access to the memories */
    /** RCU instruction memory */
    RcuIMEM*	fIMEM;
    /** RCU pedestal memory */
    RcuPMEM*	fPMEM;
    /** RCU result memory */
    RcuRMEM*	fRMEM;
    /** RCU data memory */
    RcuDMEM*	fDM1;
    /** RCU data memory */
    RcuDMEM*	fDM2;
    /** RCU Active Channel List memory */
    RcuACL*	fACL;
    /** RCU event HEADER memory */
    RcuHEADER*	fHEADER;
    /** Status memory */
    RcuSTATUS*  fSTATUS; 
    /** RCU event registers memory */
    RcuMemory*	fRegisters;
    /** RCU event monitor memory */
    RcuMemory*	fMonitor;
    /** @} */


    /** @{
	@name Access to the commands */
    /** ABORT instruction execution  */
    RcuCommand* fABORT;
    /** CLeaR EVent TAG command */
    RcuCommand* fCLR_EVTAG;
    /** DCS OwN bus command */
    RcuCommand* fDCS_ON;
    /** DDL OwN bus command */
    RcuCommand* fDDL_ON;
    /** EXECute instructions command */
    RcuCommand* fEXEC;
    /** Front-End Card ReSeT command */
    RcuCommand* fFECRST;
    /** GLoBal RESET command */
    RcuCommand* fGLB_RESET;
    /** L1-enable via CoMmanD command */
    RcuCommand* fL1_CMD;
    /** L1-enable via I2C command */
    RcuCommand* fL1_I2C;
    /** L1-enable via TTC command */
    RcuCommand* fL1_TTC;
    /** RCU RESET command */
    RcuCommand* fRCU_RESET;
    /** ReaD-out ABORT command */
    RcuCommand* fRDABORT;
    /** ReaD FirMware command */
    RcuCommand* fRDFM;
    /** ReSet DMEM1 command */
    RcuCommand* fRS_DMEM1;
    /** ReSet DMEM2 command */
    RcuCommand* fRS_DMEM2;
    /** ReSet STATUS command */
    RcuCommand* fRS_STATUS;
    /** ReSet TRigger ConFiGuration command */
    RcuCommand* fRS_TRCFG;
    /** ReSet TRigger CouNTer command */
    RcuCommand* fRS_TRCNT;
    /** Slow Control COMMAND command */
    RcuCommand* fSCCOMMAND;
    /** SoftWare TRiGger command */
    RcuCommand* fSWTRG;
    /** TRiGger CLeaR command */
    RcuCommand* fTRG_CLR;
    /** WRite FirMware command */
    RcuCommand* fWRFM;
    /** ReSet ERRor REGister */
    RcuCommand* fRS_ERRREG;
    /** @} */
  };
}

#endif
//====================================================================
//
// EOF
//
