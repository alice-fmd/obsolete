//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    rcuxx/Rorc.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 01:03:24 2006
    @brief   Implementation of RorcClient back-end 
*/
#ifdef HAVE_CONFIG_H
# include "config.h"
#endif
#ifndef RCUXX_Rorc
# include <rcuxx/Rorc.h>
#endif
#ifndef RCUXX_Constants
# include <rcuxx/Constants.h>
#endif
#include "DebugGuard.h"
// This is needed, because `rorc_lib.h' does not have 
// the necessary protection. 
extern "C" {
#ifndef _RORC_LIB_H_
# include <rorc_lib.h>
#endif
}
#ifndef _RORC_DDL_H_
# include <rorc_ddl.h>
#endif
#include <stdexcept>
#include <cstdarg>
//#include <iostream>
//#include <iomanip>
//#include <sstream>
#include <string>
//#include <numeric>

//====================================================================
#define MAX_PHYSMEM (1024*1024*8)
#define DDL_PARAM_MASK 0x7FFFF
namespace 
{
  bool fDebug = false;
}

//==================================================================== 
namespace Rcuxx
{    
  //====================================================================
  /** @class RorcClient
      @brief Interface to a memory via RORC layer
      @ingroup rorcxx
  */
  class RorcClient 
  {
  public:
    /** Constructor  
	Make client 
	@param minor Device minor number 
	@param channel DDL channel on device */    
    RorcClient(int minor, int channel) 
      : fMinor(minor), 
	fChannel(channel), 
	fUsTimeout(DDL_RESPONSE_TIME), 
	fIsOpen(false)
    {}
    /** Destructor  */
    virtual ~RorcClient() 
    {
      if (fIsOpen) Close();
      fIsOpen = false;
    }
    /** open connection to RORC.  
	@return  0 on success, error code otherwise */
    virtual int Open() 
    {
      Rcuxx::DebugGuard g(fDebug, "RorcClient::Open");
      if (fIsOpen) return 0;
      // Open the channel 
      // printf("Opening rorc://%d:%d\n", fMinor, fChannel);
      int ret = rorcOpenChannel(&fRorc, fMinor, fChannel);
      if (ret) return ret;
      fTimeout = fUsTimeout * fRorc.loop_per_usec;
      Rcuxx::DebugGuard::Message(fDebug, "Time out set to %dus (=%d*%d)", 
				 fUsTimeout, fRorc.loop_per_usec, fTimeout);
      volatile unsigned long *addr_user;
      // Open the physmem. 
      ret = physmemOpen(&fPhysMem, &addr_user,
			&fPhysMemAddress, &fPhysMemSize);
      if (ret) return ret;
      fUserAddress = const_cast<long unsigned int*>(addr_user);
      fPhysMemOffset = (fMinor * MAX_CHANNEL + fChannel) * MAX_PHYSMEM;
      Rcuxx::DebugGuard::Message(fDebug, 
				 "Allocated physical memory @ 0x%08lx of size" 
				 " %ld (%ld MB) bytes",  fPhysMemAddress, 
				 fPhysMemSize,  fPhysMemSize / 1024 / 1024); 
      if ((fPhysMemOffset + MAX_PHYSMEM) > fPhysMemSize) {
	fprintf(stderr, 
		"Usable memory offset (%ld MB) + size (%ld MB) must be less " 
		"than the physmem area size (%ld MB)\n", 
		fPhysMemOffset / 1024 / 1024, MAX_PHYSMEM / 1024 / 1024, 
		fPhysMemSize / 1024 / 1024);
	return 1;
      }

      fPhysMemAddress += fPhysMemOffset;
      fUserAddress += fPhysMemOffset / 4;

      // Reset the RORC 
      rorcReset(&fRorc, RORC_RESET_RORC);
      
      fIsOpen = true;
      // Return exit code 
      return ret;
    }
    /** close connection to RORC.  
	@return  0 on success, error code otherwise */
    virtual int Close() 
    {
      DebugGuard g(fDebug, "RorcClient::Close() <%s>", 
		   fIsOpen ? "open" : "closed");
      if (!fIsOpen) return 0;
      // Close the physmem 
      physmemClose(fPhysMem, fUserAddress, fPhysMemSize);
      // Close rorc 
      rorcClose(&fRorc);
      // Flag as closed
      fIsOpen = false;
      // Return 0 for success. 
      return 0;
    }
    /** Write @a size words of data to the FEE at the address @a
	addr. 
	@param addr Address to write to
	@param size Size of data to write 
	@param data The data to write 
	@return  0 on success, error code otherwise  */
    virtual int WriteBlock(unsigned long addr, unsigned long offset, 
			   int size, unsigned int* data)
    {
      Rcuxx::DebugGuard g(fDebug, "RorcClient::WriteBlock(0x%0x,%d,%p)", 
			  addr, size, data);
      unsigned long  returnPhys = fPhysMemAddress;
      unsigned long* returnAddr = fUserAddress;
      unsigned long  bufferPhys = returnPhys + 0x40 + DDL_MAX_BYTE + 4;
      unsigned long* bufferAddr = returnAddr + 0x10 + DDL_MAX_WORD + 1;
      int            step       = 0;
      unsigned long  raddr      = addr+offset;      
      unsigned long  tmp        = raddr & DDL_PARAM_MASK;
      static char buffer[128];
      if (tmp != raddr) {
	sprintf(buffer, "In write, address 0x%08lx too large (> 0x%08lx)\n",
		raddr, DDL_PARAM_MASK);
	fErrStr = buffer;
	return 1;
      }
      // Copy the data from user into physmem 
      size = std::min(size, DDL_MAX_WORD);
      Rcuxx::DebugGuard::Message(fDebug, "Write %d words", size);
      for (size_t i = 0; i < size; i++) { 
	bufferAddr[i] = data[i];
	Rcuxx::DebugGuard::Message(fDebug, "\t%4d\t0x%08lx -> 0x%08lx", 
				   i, data[i], bufferAddr[i]);
      }
      int ret = ddlWriteDataBlock(&fRorc, bufferPhys, size, returnPhys, 
				  returnAddr, raddr, fTimeout, fStw, 
				  &fNReply, &step);
      if (!DecodeError(ret, "WriteBlock")) return ret;
      return ret;
    }
    /** Send a command @a addr to the FEE
	@param addr Command to send 
	@return 0 on success, error code otherwise */
    virtual int SendCommand(unsigned long addr) 
    {
      Rcuxx::DebugGuard g(fDebug, "RorcClient::SendCommand(0x%x)", addr);
      int ret    = ddlSendCommandAndWaitReply(&fRorc, FECTRL, addr, 
					      fTimeout, fStw, 1, &fNReply);
      if (!DecodeError(ret, "SendCommand")) return ret;
      return ret;
    }
    
    /** Read @a size words of data from Fee at address @a addr. 
	@param addr Address to read from 
	@param size Number of words to read.  On return, this contains
	the actual number of words read. 
	@param data Data read. 
	@return 0 on success, error code otherwise  */
    virtual int ReadBlock(unsigned long addr, unsigned long offset, 
			  int& size, unsigned int* data)
    {
      Rcuxx::DebugGuard g(fDebug, "RorcClient::ReadBlock(0x%0x,%d,%d,%p)", 
			  addr, offset, size, data);
      
      unsigned long    returnPhys = fPhysMemAddress;
      unsigned long*   returnAddr = fUserAddress;
      rorcReadyFifo_t* rf         = (rorcReadyFifo_t*)(returnAddr);
      unsigned long    dataPhys   = returnPhys + 0x40/* 8*/;
      unsigned long*   dataAddr   = returnAddr + 0x10/* 2*/;
      int              step       = 0;
      int              ret        = 0;
      unsigned long    raddr      = addr+offset;
      unsigned long    tmp        = raddr & DDL_PARAM_MASK;
      static char buffer[128];
      if (tmp != raddr) {
	sprintf(buffer, "In read, address 0x%08lx too large (> 0x%08lx)\n",
		addr, DDL_PARAM_MASK);
	fErrStr = buffer;
	return 1;
      }

      ret = SendCommand(0x5000 + size);
      if (ret) return ret;

      ret = ddlReadDataBlock(&fRorc, dataPhys, returnPhys, rf, 
			     raddr, fTimeout, fStw, &fNReply, &step);
      if (!DecodeError(ret, "ReadBlock")) return ret;

      // Decode error 
      // if (ret == RORC_STATUS_OK) return ret;
      
      // std::ostringstream s(fErrStr);
      if (rf->status && ((rf->status & 0xFF) != DTSW)) {
	sprintf(buffer, "Protocol error: status!=DTSW (0x%x!=0x%x)", 
		(rf->status & 0xF), DTSW);
	fErrStr = buffer;
	return 1;
      }
      if ((rf->length < 0) || (rf->length > DDL_MAX_TX_WORD)) {
	sprintf(buffer, "Protocol error: data length=%d", rf->length);
	fErrStr = buffer;
	return 1;
      }

      // Copy data from physmem to user
      //  size = std::min(size, rf->length);      
      size = rf->length;
      Rcuxx::DebugGuard::Message(fDebug, "Got %d word%sback", size, 
				 (size == 1 ? " " : "s "));
      for (size_t i = 0; i < size; i++) {
	data[i] = dataAddr[i];
	Rcuxx::DebugGuard::Message(fDebug, "\t%3d: 0x%08lx (0x%08lx)", 
				   i, dataAddr[i], data[i]);
      }
      return ret;
    }

    /** Decode an error number, and store result in internal buffer. 
	@param ret Error code.
	@param prefix What to put in front. 
	@return @c true if there's an error, @c false otherwise */
    virtual bool DecodeError(int ret, const char* prefix) 
    {
      fErrStr.erase();
      
      if (ret == RORC_STATUS_OK) return true;

      static char buffer[128];
      // std::ostringstream s(fErrStr);
      switch (ret) {
      case RORC_LINK_NOT_ON: 
      case RORC_TIMEOUT:
      case RORC_NOT_ABLE:
	sprintf(buffer, "%s: %s", prefix,  rorcInterpretReturnCode(ret));
	fErrStr = buffer;
	// s << prefix  << ": " << rorcInterpretReturnCode(ret);
	break;
      case RORC_NOT_ENOUGH_REPLY:
	sprintf(buffer, "%s: Not enough (%d) replies in %dus", 
		prefix, fNReply, fUsTimeout);
	fErrStr = buffer;
	// s << prefix << ". Not enough (" << fNReply 
	//   << ") replies in " << std::dec << fUsTimeout << " us";
	break;
      case RORC_TOO_MANY_REPLY:
	sprintf(buffer, "%s: Too many (%d) replies recieved", 
		prefix, fNReply);
	fErrStr = buffer;
	// s << prefix << ". Too many (" << fNReply << ") replies recieved";
	break;
      }
      for (size_t i = 0; i < fNReply; i++) {
	sprintf(buffer, "\n  0x%x", fStw[i].stw);
	fErrStr.append(buffer);
	// s << "\n  0x" << std::hex << fStw[i].stw;
      }
      return false;
    }

    /** @return String of last error.  It is empty if there's no error. */
    std::string ErrorString() const { return fErrStr; }
  protected:
    /** Minor device number */
    int fMinor;
    /** Channel number */
    int fChannel;
    /** Timeout in @f$ \mu s@f$ */
    int fUsTimeout;
    /** Whether we're opened yet */
    bool fIsOpen;
    /** Timeout in RORC loops */
    int fTimeout;
    /** Device structure */
    rorc_pci_dev_t fRorc;
    /** File descriptor for physmem */
    int fPhysMem;
    /** User address for PhysMem */
    unsigned long* fUserAddress;
    /** PhysMem address */ 
    unsigned long fPhysMemAddress;
    /** Size of PhysMem */
    unsigned long fPhysMemSize;
    /** Offset in PhysMem */
    unsigned long fPhysMemOffset;
    /** Status array */ 
    stword_t fStw[DDL_MAX_REPLY];
    /** Available status words */ 
    int fNReply;
    /** Last error string */
    std::string fErrStr;
  };
}

//====================================================================
Rcuxx::Rorc::Rorc()
{
  throw std::runtime_error("Cannot use defualt Rcuxx::Rorc constructor");
}

//____________________________________________________________________
Rcuxx::Rorc::Rorc(const Rorc& other)
{
  throw std::runtime_error("Cannot use Rcuxx::Rorc copy constructor");
}

//____________________________________________________________________
void
Rcuxx::Rorc::ParseUrl(char* node) 
{
  DebugGuard g(fDebug, "Parsing node %s", node);
  std::string url(node);
  std::string::size_type colon = url.find(':');
  if (colon != std::string::npos) {
    std::string sminor(url.substr(0, colon));
    if (!sminor.empty()) {
      // std::stringstream sm(sminor);
      // sm >> fMinor;
      sscanf(sminor.c_str(), "%d", &fMinor);
    }
    std::string schannel(url.substr(colon+1, url.size()-colon-1));
    if (!schannel.empty()) {
      // std::stringstream sc(schannel);
      // sc >> fChannel;
      sscanf(schannel.c_str(), "%d", &fChannel);
    }
  }
  DebugGuard::Message(fDebug, "Minor is %d and Channel is %d", 
		      fMinor, fChannel);
}

//____________________________________________________________________
Rcuxx::Rorc::Rorc(char* node, bool emul) 
  : fMinor(0), fChannel(0)
{
  DebugGuard g(fDebug, "Opening RORC server %s", node);

  if (emul) 
    throw std::runtime_error("emulation not supported by Rorc interface");
  
  ParseUrl(node);
  fClient = new RorcClient(fMinor, fChannel);
  int isOpened = fClient->Open();
  DebugGuard::Message(fDebug, "Open returned %d\n",isOpened);
  if (isOpened) 
    throw std::runtime_error("Failed to open channel");
  
  fABORT	 = new RcuCommand(*this, "ABORT",	 C_ABORT);
  //fCLR_EVTAG	 = new RcuCommand(*this, "CLR_EVTAG",	 C_CLR_EVTAG);
  fDCS_ON	 = new RcuCommand(*this, "DCS_ON",	 C_DCS_ON);
  fDDL_ON	 = new RcuCommand(*this, "DDL_ON",	 C_DDL_ON);
  fEXEC		 = new RcuCommand(*this, "EXEC",	 C_EXEC,	true);
  fFECRST	 = new RcuCommand(*this, "FECRST",	 C_FECRST);
  fGLB_RESET	 = new RcuCommand(*this, "GLB_RESET",	 C_GLB_RESET);
  fL1_CMD	 = new RcuCommand(*this, "L1_CMD",	 C_L1_CMD);
  fL1_I2C	 = new RcuCommand(*this, "L1_I2C",	 C_L1_I2C);
  fL1_TTC	 = new RcuCommand(*this, "L1_TTC",	 C_L1_TTC);
  fRCU_RESET	 = new RcuCommand(*this, "RCU_RESET",	 C_RCU_RESET);
  //fRDABORT	 = new RcuCommand(*this, "RDABORT",	 C_RDABORT);
  //fRDFM	 = new RcuCommand(*this, "RDFM",	 C_RDFM);
  fRS_DMEM1	 = new RcuCommand(*this, "RS_DMEM1",	 C_RS_DMEM1);
  fRS_DMEM2	 = new RcuCommand(*this, "RS_DMEM2",	 C_RS_DMEM2);
  fRS_STATUS	 = new RcuCommand(*this, "RS_STATUS",	 C_RS_STATUS);
  fRS_TRCFG	 = new RcuCommand(*this, "RS_TRCFG",	 C_RS_TRCFG);
  fRS_TRCNT	 = new RcuCommand(*this, "RS_TRCNT",	 C_RS_TRCNT);
  //fSCCOMMAND	 = new RcuCommand(*this, "SCCOMMAND",	 C_SCCOMMAND);
  fSWTRG	 = new RcuCommand(*this, "SWTRG",	 C_SWTRG);
  //fTRG_CLR	 = new RcuCommand(*this, "TRG_CLR",	 C_TRG_CLR);
  //fWRFM	  = new RcuCommand(*this, "WRFM",	 C_WRFM);
  fRS_ERRREG	 = new RcuCommand(*this, "RS_ERRREG",	 C_RS_ERRREG);
  
  fIMEM		 = new RcuIMEM(*this, A_IMEM, S_IMEM, fEXEC, fABORT);
  fRMEM		 = new RcuRMEM(*this, A_RMEM, S_RMEM);
  fPMEM		 = new RcuPMEM(*this, A_PMEM, B_PMEM);
  fDM1		 = new RcuDMEM(1, *this, A_DML1,  A_DMH1, S_DM, fRS_DMEM1);
  fDM2		 = new RcuDMEM(2, *this, A_DML2,  A_DMH2, S_DM, fRS_DMEM2);
  fACL		 = new RcuACL(*this, A_ACL,  S_ACL);
  fHEADER	 = new RcuHEADER(*this, A_HEADER, S_HEADER);
  fSTATUS	 = new RcuSTATUS(*this, A_STATUS, S_STATUS);
  fRegisters	 = new RcuMemory(*this, "Registers", A_REGISTERS, S_REGISTERS, 
				 0xffffffff, true);
  fMonitor	 = new RcuMemory(*this, "Monitor", A_MONITOR, S_MONITOR,
				 0xffffffff, true);
  
  fERRST	 = new RcuERRST(*fRegisters,	O_ERRST,	fRS_STATUS);
  fTRCNT	 = new RcuTRCNT(*fRegisters,	O_TRCNT,	fRS_TRCNT);
  fLWADD	 = new RcuLWADD(*fRegisters,	O_LWADD);
  fIRADD	 = new RcuIRADD(*fRegisters,	O_IRADD);
  fIRDAT	 = new RcuIRDAT(*fRegisters,	O_IRDAT);
  //fEVWORD	 = new RcuEVWORD(*fRegisters,	O_EVWORD);
  fACTFEC	 = new RcuACTFEC(*fMonitor,	O_ACTFEC);
  fRDOFEC	 = new RcuRDOFEC(*fMonitor,	O_RDOFEC);
  fTRCFG1	 = new RcuTRCFG1(*fRegisters,	O_TRCFG1,	fRS_TRCFG);
  //fTRCFG2	 = new RcuTRCFG2(*fRegisters,	O_TRCFG2);
  //fFMIREG	 = new RcuFMIREG(*fRegisters,	O_FMIREG);
  //fFMOREG	 = new RcuFMOREG(*fRegisters,	O_FMOREG);
  fPMCFG	 = new RcuPMCFG(*fRegisters,	O_PMCFG);
  fCHADD	 = new RcuCHADD(*fRegisters,	O_CHADD);
  //fINTREG	 = new RcuINTREG(*fMonitor,	O_INTREG);
  fRESREG	 = new RcuRESREG(*fMonitor,	O_RESREG);
  fERRREG	 = new RcuERRREG(*fMonitor,	O_ERRREG,	fRS_ERRREG);
  fINTMOD	 = new RcuINTMOD(*fMonitor,	O_INTMOD);
}

//____________________________________________________________________
Rcuxx::Rorc::~Rorc()
{
  DebugGuard g(fDebug, "Rorc::~Rorc");
  if (fClient) fClient->Close();
}


//____________________________________________________________________
unsigned int 
Rcuxx::Rorc::ExecCommand(unsigned int cmd)
{
  DebugGuard g(fDebug, "Rorc::ExecCommand(0x%x)", cmd);
  assert(fClient);
  return fClient->SendCommand(cmd);
}

//____________________________________________________________________
unsigned int 
Rcuxx::Rorc::ReadMemory(unsigned int base, unsigned int offset,  
		       unsigned int& isize, unsigned int* data)
{
  assert(fClient);
  DebugGuard g(fDebug, "Rorc::ReadMemory(0x%x,%d,%d,%p)", 
	       base, offset, isize, data);
  int osize = isize;
  unsigned int ret = fClient->ReadBlock(base, offset, osize, data);
  isize = osize;
  return ret;
}

//____________________________________________________________________
unsigned int 
Rcuxx::Rorc::WriteMemory(unsigned int base, unsigned int offset,  
			unsigned int& osize, unsigned int* data)
{
  DebugGuard g(fDebug, "Rorc::WriteMemory(%p,%d,%d,%p)",
	       base,offset,osize,data);
  assert(fClient);
  return fClient->WriteBlock(base, offset, osize, data);
}

//____________________________________________________________________
void
Rcuxx::Rorc::SetDebug(int id, int lvl)
{
  Rcu::SetDebug(id, lvl);
  switch (id) {
  case kBackend: fDebug = lvl;  break;
  default:       break;
  }
}

//____________________________________________________________________
std::string
Rcuxx::Rorc::ErrorString(unsigned int code)
{
  // Rcuxx::Rcu::ErrorString(code);
  assert(fClient);
  return fClient->ErrorString();
}

//====================================================================
//
// EOF
//
