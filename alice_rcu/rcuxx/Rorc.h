// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USAq
//
/** @file    rcuxx/Rorc.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:34:01 2006
    @brief   Declaration of RORC concrete interface  
*/
#ifndef RCUXX_RORC_H
#define RCUXX_RORC_H
#ifndef RCUXX_RCU_H
# include <rcuxx/Rcu.h>
#endif

namespace Rcuxx 
{
  class RorcClient;

  //====================================================================
  /** @class Rorc rcuxx/Rorc.h <rcuxx/Rcu.h> 
      @brief This provides a concrete implementation of the
      communication protocol to the RCU via the Rorc driver and library.  
      @ingroup rcuxx_backend
  */
  class Rorc : public Rcu
  {
  public:
    ~Rorc();
    /** Set the debuging stuff */
    virtual void SetDebug(int id, int lvl);
    /** Get an error string corresponding to an error code 
	@param code The error code 
	@return Pointer to static string */
    std::string ErrorString(unsigned int code);
    /** Execute a command 
	@param cmd Command to execute 
	@return 0 on success, error code otherwise */
    virtual unsigned int ExecCommand(unsigned int cmd);
    /** Read a memory block 
	@param addr Base address 
	@param offset Where to start reading 
	@param size How many words to read. 
	@param data Array of data to read into
	@return 0 on success, error code otherwise */ 
    virtual unsigned int ReadMemory(unsigned int addr, 
				    unsigned int offset, 
				    unsigned int& size, 
				    unsigned int* data);
    /** Write to a memory block 
	@param addr Base address 
	@param offset Where to start writing
	@param size How many words to write 
	@param data Array of data to write into
	@return 0 on success, error code otherwise */ 
    virtual unsigned int WriteMemory(unsigned int addr, 
				     unsigned int offset, 
				     unsigned int& size, 
				     unsigned int* data);
  protected:
    friend class Rcu;
    /** Default ctor */
    Rorc();
    /** Copy ctor */
    Rorc(const Rorc& other);
    /** Create the register interface tab */
    Rorc(char* node, bool emulate=false);
    /** */
    void ParseUrl(char* node);
    int fChannel;
    int fMinor;
    /** Client inferface */ 
    RorcClient* fClient;
  };
}

#endif

//====================================================================
//
// EOF
//
