//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    rcuxx/U2F.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:33:37 2006
    @brief   Implementation of U2F concrete RCU interface 
*/
#ifdef HAVE_CONFIG_H
# include "config.h"
#endif
#ifndef RCUXX_U2F_H
# include <rcuxx/U2F.h>
#endif
#ifndef __IOSTREAM__
# include <iostream>
#endif
#ifndef __IOMANIP__
# include <iomanip>
#endif
#ifndef __STDEXCEPT__
# include <stdexcept>
#endif
#ifndef __U2F_H
# include <altro/u2f.h>
#endif
#include "DebugGuard.h"

extern int packageId;
extern int traceLevel;
#define A_MONITOR 	A_REGISTERS
#define S_REGISTERS 	15
#define A_STATUS	0x8100
#define S_STATUS	32  

namespace 
{
  bool fDebug = false;
}

//====================================================================
Rcuxx::U2F::U2F()
{
  throw std::runtime_error("Cannot use defualt Rcuxx::U2F constructor");
}

//____________________________________________________________________
Rcuxx::U2F::U2F(const U2F& other)
{
  throw std::runtime_error("Cannot use Rcuxx::U2F copy constructor");
}

//____________________________________________________________________
Rcuxx::U2F::U2F(char* node, bool emulate) 
{
  Rcuxx::DebugGuard g(fDebug, "Opening %s in %s mode", node, 
		      (emulate ? "emulation" : "hardware"));
  int ret = U2F_Open(const_cast<char*>(node ? node : DEFAULT_NODE), 
		     &fHandle, emulate ? SW : HW);
  if (ret) throw std::runtime_error("Failed to open U2F interface");
  
  fABORT	 = new RcuCommand(*this, "ABORT",	 C_ABORT);
  fCLR_EVTAG	 = new RcuCommand(*this, "CLR_EVTAG",	 C_CLR_EVTAG);
  //fDCS_ON	 = new RcuCommand(*this, "DCS_ON",	 C_DCS_ON);
  //fDDL_ON	 = new RcuCommand(*this, "DDL_ON",	 C_DDL_ON);
  fEXEC		 = new RcuCommand(*this, "EXEC",	 C_EXEC,	true);
  fFECRST	 = new RcuCommand(*this, "FECRST",	 C_FECRST);
  //fGLB_RESET	 = new RcuCommand(*this, "GLB_RESET",	 C_GLB_RESET);
  //fL1_CMD	 = new RcuCommand(*this, "L1_CMD",	 C_L1_CMD);
  //fL1_I2C	 = new RcuCommand(*this, "L1_I2C",	 C_L1_I2C);
  //fL1_TTC	 = new RcuCommand(*this, "L1_TTC",	 C_L1_TTC);
  //fRCU_RESET	 = new RcuCommand(*this, "RCU_RESET",	 C_RCU_RESET);
  fRDABORT	 = new RcuCommand(*this, "RDABORT",	 C_RDABORT);
  fRDFM		 = new RcuCommand(*this, "RDFM",	 C_RDFM);
  fRS_DMEM1	 = new RcuCommand(*this, "RS_DMEM1",	 C_RS_DMEM1);
  fRS_DMEM2	 = new RcuCommand(*this, "RS_DMEM2",	 C_RS_DMEM2);
  fRS_STATUS	 = new RcuCommand(*this, "RS_STATUS",	 C_RS_STATUS);
  fRS_TRCFG	 = new RcuCommand(*this, "RS_TRCFG",	 C_RS_TRCFG);
  fRS_TRCNT	 = new RcuCommand(*this, "RS_TRCNT",	 C_RS_TRCNT);
  fSCCOMMAND	 = new RcuCommand(*this, "SCCOMMAND",	 C_SCCOMMAND);
  fSWTRG	 = new RcuCommand(*this, "SWTRG",	 C_SWTRG);
  fTRG_CLR	 = new RcuCommand(*this, "TRG_CLR",	 C_TRG_CLR);
  fWRFM	 	 = new RcuCommand(*this, "WRFM",	 C_WRFM);
  // fRS_ERRREG	 = new RcuCommand(*this, "RS_ERRREG",	 C_RS_ERRREG);
  
  fIMEM		 = new RcuIMEM(*this, A_IMEM, S_IMEM-1, fEXEC, fABORT);
  fRMEM		 = new RcuRMEM(*this, A_RMEM, S_RMEM-1);
  fPMEM		 = new RcuPMEM(*this, A_PMEM, B_PMEM-1);
  fACL		 = new RcuACL(*this,  A_ACL,  S_ACL-1);
  // Four times bigger.  One factor 2 comes from the division in
  // low/high, and another factor 2 comes from that the U2F stores two
  // 10bit words in each 20bit word of DML and DMH
  fDM1		 = new RcuDMEM(1, *this, A_DML1,  A_DMH1, 4*S_DM, fRS_DMEM1);
  fDM2		 = new RcuDMEM(2, *this, A_DML2,  A_DMH2, 4*S_DM, fRS_DMEM2);
  // fHEADER	 = new RcuHEADER(*this, A_HEADER, S_HEADER);
  // fSTATUS	 = new RcuSTATUS(*this, A_STATUS, S_STATUS);
  fRegisters	 = new RcuMemory(*this, "Registers", A_REGISTERS, S_REGISTERS, 
				 0xffffffff, false);
  // fMonitor	 = new RcuMemory(*this, "Monitor", A_MONITOR, S_MONITOR,
  //                             0xffffffff, false);
  fMonitor	 = fRegisters;
  
  fERRST	 = new RcuERRST(*fRegisters,	O_ERRST,	fRS_STATUS);
  fTRCNT	 = new RcuTRCNT(*fRegisters,	O_TRCNT,	fRS_TRCNT);
  fLWADD	 = new RcuLWADD(*fRegisters,	O_LWADD);
  fIRADD	 = new RcuIRADD(*fRegisters,	O_IRADD);
  fIRDAT	 = new RcuIRDAT(*fRegisters,	O_IRDAT);
  fEVWORD	 = new RcuEVWORD(*fRegisters,	O_EVWORD,	fTRG_CLR);
  fACTFEC	 = new RcuACTFEC(*fMonitor,	O_ACTFEC);
  // fRDOFEC	 = new RcuRDOFEC(*fMonitor,	O_RDOFEC);
  fTRCFG1	 = new RcuTRCFG1(*fRegisters,	O_TRCFG1,	fRS_TRCFG);
  fTRCFG2	 = new RcuTRCFG2(*fRegisters,	O_TRCFG2);
  fFMIREG	 = new RcuFMIREG(*fRegisters,	O_FMIREG);
  fFMOREG	 = new RcuFMOREG(*fRegisters,	O_FMOREG);
  // fPMCFG	 = new RcuPMCFG(*fRegisters,	O_PMCFG);
  // fCHADD	 = new RcuCHADD(*fRegisters,	O_CHADD);
  fINTREG	 = new RcuINTREG(*fMonitor,	O_INTREG);
  fRESREG	 = new RcuRESREG(*fMonitor,	O_RESREG);
  fERRREG	 = new RcuERRREG(*fMonitor,	O_ERRREG);
  //fERRREG	 = new RcuERRREG(*fMonitor,	O_ERRREG,	fRS_ERRREG);
  fINTMOD	 = new RcuINTMOD(*fMonitor,	O_INTMOD);

  fMonitor       = 0;
}

//____________________________________________________________________
Rcuxx::U2F::~U2F()
{
  U2F_Close(fHandle);
}


//____________________________________________________________________
unsigned int 
Rcuxx::U2F::ExecCommand(unsigned  int cmd) 
{
  Rcuxx::DebugGuard g(fDebug, "ExecCommand(%p)", cmd);
  unsigned int ret = 0;
  ret = U2F_Exec_Command(fHandle, cmd, 0);
  return ret;
}

//____________________________________________________________________
unsigned int 
Rcuxx::U2F::ReadMemory(unsigned int base, unsigned int offset, 
		       unsigned int& size, unsigned int* data) 
{
  Rcuxx::DebugGuard g(fDebug, "ReadMemory(%p, %d, %d, %p)", 
		      base, offset, size, data);
  unsigned int ret = 0, s = size;
  switch (base) {
  case A_IMEM: ret = U2F_IMEM_Read(fHandle, size, &s, offset, data); break;
  case A_RMEM: ret = U2F_RMEM_Read(fHandle, size, &s, offset, data); break;
  case A_PMEM: ret = U2F_PMEM_Read(fHandle, size, &s, offset, data); break;
  case A_ACL:  ret = U2F_ACL_Read(fHandle,  size, &s, offset, data); break;
  case A_REGISTERS:
    // case A_MONITOR:
    Rcuxx::DebugGuard::Message(fDebug, "Reading from register %p", offset);
    ret = U2F_Reg_Read(fHandle, offset, &(data[0]));
    break;
  default:
    return kUnknownMemory;
  }
  Rcuxx::DebugGuard::Message(fDebug, "Returned %d", ret);
  return ret;
}

//____________________________________________________________________
unsigned int 
Rcuxx::U2F::WriteMemory(unsigned int base, unsigned int offset, 
			unsigned int& size, unsigned int* data) 
{
  Rcuxx::DebugGuard g(fDebug, "WriteMemory(%p, %d, %d, %p)", 
		      base, offset, size, data);
  unsigned int ret = 0;
  switch (base) {
  case A_IMEM: ret = U2F_IMEM_Write(fHandle, size, offset, data); break;
  case A_RMEM: ret = U2F_RMEM_Write(fHandle, size, offset, data); break;
  case A_PMEM: ret = U2F_PMEM_Write(fHandle, size, offset, data); break;
  case A_ACL:  ret = U2F_ACL_Write(fHandle,  size, offset, data); break;
  case A_REGISTERS:
    // case A_MONITOR:
    Rcuxx::DebugGuard::Message(fDebug, "Writing to register %p", offset);
    ret = U2F_Reg_Write(fHandle, offset, data[0]);
    break;
  default:
    return kUnknownMemory;
  }
  Rcuxx::DebugGuard::Message(fDebug, "Returned %d", ret);
  return ret;
}

//____________________________________________________________________
unsigned int 
Rcuxx::U2F::ReadDataMemory(unsigned int lbase, unsigned hbase, 
			   unsigned int offset, unsigned int& size, 
			   unsigned int* data) 
{
  Rcuxx::DebugGuard g(fDebug, "ReadDataMemory(%p, %p, %d, %d, %p)", 
		      lbase, hbase, offset, size, data);
  unsigned int ret = 0, s = size;
  if (s < 1) return 0;
  if      (lbase == A_DML1 && hbase == A_DMH1) 
    ret = U2F_DM1_Read(fHandle, size, &s, offset, data);
  else if (lbase == A_DML2 && hbase == A_DMH2) 
    ret = U2F_DM2_Read(fHandle, size, &s, offset, data);
  else 
    ret = kUnknownMemory;
  size = s;
  Rcuxx::DebugGuard::Message(fDebug, "Returned %d", ret);
  return ret;
}

//____________________________________________________________________
unsigned int 
Rcuxx::U2F::WriteDataMemory(unsigned int lbase, unsigned hbase, 
			    unsigned int offset, unsigned int& size, 
			    unsigned int* data) 
{
  Rcuxx::DebugGuard g(fDebug, "WriteDataMemory(%p, %p, %d, %d, %p)", 
		      lbase, hbase, offset, size, data);
  unsigned int ret = 0, s;
  if      (lbase == A_DML1 && hbase == A_DMH1) 
    ret = U2F_DM1_Write(fHandle, offset, size, data);
  else if (lbase == A_DML2 && hbase == A_DMH2) 
    ret = U2F_DM2_Write(fHandle, offset, size, data);
  else 
    ret = kUnknownMemory;
  Rcuxx::DebugGuard::Message(fDebug, "Returned %d", ret);
  return ret;
}


#define U2F_NOT_OPEN "No connection to U2F"
//____________________________________________________________________
std::string
Rcuxx::U2F::ErrorString(unsigned int code)
{
  switch (code) {
  case kNotEnough: 
    return std::string("Not enough data returned");
  case kInconsistentSizes:
    return std::string("Inconsistent size when reading data memory");
  case kUnknownMemory:
    return std::string("Unknown memory bank addressed");
  }
  err_pack err = code;
  err_str  pid;
  err_str  str;
  if (code == U2F_NOTOPEN) 
    strcpy(str, U2F_NOT_OPEN);
  else 
    U2F_err_get(err, pid, str);
  return std::string(str);
}

//____________________________________________________________________
void
Rcuxx::U2F::SetDebug(int id, int lvl) 
{
  Rcu::SetDebug(id, lvl);
  switch (id) {
  case 1:        packageId = P_ID_U2F;   traceLevel = lvl; break;
  case 2:        packageId = P_ID_ALTRO; traceLevel = lvl; break;
  case kBackend: fDebug = true;  break;
  default:       break;
  }
}

//====================================================================
//
// EOF
//
