// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    rcuxx/U2F.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:34:01 2006
    @brief   Declaration of U2F concrete interface  
*/
#ifndef RCUXX_U2F_H
#define RCUXX_U2F_H
#ifndef RCUXX_RCU_H
# include <rcuxx/Rcu.h>
#endif

namespace Rcuxx 
{
  //====================================================================
  /** @defgroup rcuxx_backend Implementation of the RCU communications
      protocol
      @ingroup rcuxx
  */

  //====================================================================
  /** @class U2F rcuxx/U2F.h <rcuxx/Rcu.h> 
      @brief This provides a concrete implementation of the
      communication protocol to the RCU via the U2F (USB to Front end)
      driver and library. 
      @ingroup rcuxx_backend
  */
  class U2F : public Rcu
  {
  public:
    ~U2F();
    /** Get an error string corresponding to an error code 
	@param code The error code 
	@return Pointer to static string */
    std::string ErrorString(unsigned int code);
    /** Set the debuging stuff */
    virtual void SetDebug(int id, int lvl);
    /** Execute a command 
	@param cmd Command to execute 
	@return 0 on success, error code otherwise */
    virtual unsigned int ExecCommand(unsigned int cmd);
    /** Read a memory block 
	@param addr Base address 
	@param offset Where to start reading 
	@param size How many words to read. 
	@param data Array of data to read into
	@return 0 on success, error code otherwise */ 
    virtual unsigned int ReadMemory(unsigned int addr, 
				    unsigned int offset, 
				    unsigned int& size, 
				    unsigned int* data);
    /** Write to a memory block 
	@param addr Base address 
	@param offset Where to start writing
	@param size How many words to write 
	@param data Array of data to write into
	@return 0 on success, error code otherwise */ 
    virtual unsigned int WriteMemory(unsigned int addr, 
				     unsigned int offset, 
				     unsigned int& size, 
				     unsigned int* data);
    /** Read a data memory block 
	@param lbase Base address for low bits
	@param hbase Base address for high bits
	@param offset Where to start reading 
	@param size How many words to read. 
	@param data Array of data to read into
	@return 0 on success, error code otherwise */ 
    virtual unsigned int ReadDataMemory(unsigned int lbase, 
					unsigned int hbase, 
					unsigned int offset, 
					unsigned int& size, 
					unsigned int* data);
    /** Write a data memory block 
	@param lbase Base address for low bits
	@param hbase Base address for high bits
	@param offset Where to start writing 
	@param size How many words to write 
	@param data Array of data to write
	@return 0 on success, error code otherwise */ 
    virtual unsigned int WriteDataMemory(unsigned int lbase, 
					 unsigned int hbase, 
					 unsigned int offset, 
					 unsigned int& size, 
					 unsigned int* data);
  protected:
    friend class Rcu;
    /** Handle to U2F */
    int fHandle;
    /** Default ctor */
    U2F();
    /** Copy ctor */
    U2F(const U2F& other);
    /** Create the register interface tab */
    U2F(char* node, bool emulate=false);
  };
}

#endif

//====================================================================
//
// EOF
//
