//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUXX_RCU_H
# include <rcuxx/Rcu.h>
#endif
#ifndef RCUXX_ALTRO_H
# include <rcuxx/Altro.h>
#endif
#ifndef RCUXX_BC_H
# include <rcuxx/Bc.h>
#endif
#ifndef RCUXX_FMD_H
# include <rcuxx/Fmd.h>
#endif
#ifndef RCUXX_ACQ_H
# include <rcuxx/Acq.h>
#endif
#include <iostream>
#include <iomanip>
#include <sstream>
#include <stdexcept>

template <typename T>
T 
str2val(const char* str)
{
  std::stringstream s(str);
  T v;
  s >> (str[0]=='0' ? (str[1]=='x'||str[1]=='X' ? std::hex : std::oct) 
	: std::dec) >> v;
  return v;
}

int
main(int argc, char** argv)
{
  std::string           device  = "/dev/altro0";
  bool                  emul    = false;
  bool                  debug   = false;
  int                   samples = 255;
  int                   events  = 10;
  Rcuxx::Acq::Trigger_t mode    = Rcuxx::Acq::kSoftwareTrigger;
  int                   package = 0;
  int                   level   = 0;
  int                   run     = 0;
  for (int i = 1; i < argc; i++) {
    if (argv[i][0] == '-') {
      switch(argv[i][1]) {
      case 'h':
	std::cout << "Usage: " << argv[0] << " [OPTIONS] [DEVICE]\n\n"
		  << "Options:\n\n"
		  << "\t-h\tThis help\n" 
		  << "\t-t N\tNumber of ALTRO timebins\n"
		  << "\t-n N\tNumber of events\n"
		  << "\t-r N\tRun identifier\n"
		  << "\t-d N\tSet debug level to N\n"
		  << "\t-p N\tSet debug package ID N\n"
		  << "\t-s\tSoftware trigger\n"
		  << "\t-x\tExternal trigger\n"
		  << "\t-e\tEmulation mode\n"
		  << std::endl;
	return 0;
      case 'e': emul    = true; break;
      case 's': mode    = Rcuxx::Acq::kSoftwareTrigger; break;
      case 'x': mode    = Rcuxx::Acq::kExternalTrigger; break;
      case 'd': level   = str2val<int>(argv[i+1]); i++; break;
      case 'p': package = str2val<int>(argv[i+1]); i++; break;
      case 't': samples = str2val<int>(argv[i+1]); i++; break;
      case 'n': events  = str2val<int>(argv[i+1]); i++; break;
      case 'r': run     = str2val<int>(argv[i+1]); i++; break;
      default:  device  = argv[i+1]; i++; break;
      }
    }
  }
  
  // Rcuxx::DebugGuard::fDebug = level;
  Rcuxx::Acq::InstallSignalHandler();
  Rcuxx::Rcu* rcu = Rcuxx::Rcu::Open(device.c_str(), emul);
  try {
    if (!rcu) throw std::runtime_error("Failed to open device");
    rcu->SetDebug(package, level);
    Rcuxx::Altro altro(*rcu);
    Rcuxx::Bc    bc(*rcu);
    Rcuxx::Acq   acq(*rcu);

    rcu->ACTFEC()->SetValue(0x1);
    unsigned int imem[] = { 0x64000a, 0x700000, 0x390000 };
    imem[1]          += samples;
    rcu->IMEM()->Set(0, 3, imem);
    unsigned int acl[] = { 0x3, 0 };
    rcu->ACL()->Set(0, 1, acl);
    
    rcu->TRCFG1()->SetPop(true);
    rcu->TRCFG1()->SetBMD(Rcuxx::RcuTRCFG1::k4Buffers);
    rcu->TRCFG1()->SetMode(Rcuxx::RcuTRCFG1::kDerivedL2);
    rcu->TRCFG1()->SetTwv(4*(samples+1));

    unsigned int ret;
    acq.Setup(run, events, mode);
    if ((ret = acq.Run())) throw ret;
  }
  catch (std::exception& e) {
    std::cerr << e.what() << std::endl;
    return 1;
  }
  catch (unsigned int ret) {
    std::cerr << "Error # " << std::setw(3) << ret << ": ";
    if (rcu) std::cerr << rcu->ErrorString(ret);
    std::cerr << std::endl;
    return 1;
  }
  
  return 0;
}
//____________________________________________________________________
//
// EOF
// 
