//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    rcuexec.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 01:03:48 2006
    @brief   Program to interactively execute instructions in the RCU
    IMEM 
*/
#ifndef RCUXX_RCU_H
# include <rcuxx/Rcu.h>
#endif
#include <iostream>
#include <iomanip>
#include <sstream>
#include <stdexcept>
#include <fstream>
#ifdef HAVE_ALICE_RCUC_COMPILER_H
# include <alice_rcuc/compiler.h>
static const struct RCUC_set* program = 0;
#endif

template <typename T>
T 
str2val(const char* str)
{
  std::stringstream s(str);
  T v;
  s >> (str[0]=='0' ? (str[1]=='x'||str[1]=='X' ? std::hex : std::oct) 
	: std::dec) >> v;
  return v;
}

int
main(int argc, char** argv)
{
  std::string           device     = "/dev/altro0";
  std::string           file       = "";
  std::string           label      = "";
  int                   offset     = 0;
  bool                  compile    = false;
  bool                  emul       = false;
  int                   rcuxxdebug = 0;
  int                   u2fdebug   = 0;
  int                   altrodebug = 0;
  int                   rsize      = 0;
  
  for (int i = 1; i < argc; i++) {
    if (argv[i][0] == '-') {
      switch(argv[i][1]) {
      case 'h':
	std::cout << "Usage: " << argv[0] << " [OPTIONS] [DEVICE] \n\n"
		  << "Options:\n\n"
		  << "\t-h\tThis help\n" 
		  << "\t-d N\tSet Rcu++ debug level to N [" << rcuxxdebug 
		  << "]\n"
		  << "\t-u N\tSet U2F debug level to N   [" << u2fdebug   
		  << "]\n"
		  << "\t-a N\tSet ALTRO debug level to N [" << altrodebug 
		  << "]\n"
		  << "\t-e\tEmulation mode               [" << emul << "]\n"
		  << "\t-i FILE\tInput file              [" << file << "]\n"
		  << "\t-o N\tExecution offset           [" << offset << "]\n" 
#ifdef HAVE_ALICE_RCUC_COMPILER_H
		  << "\t-c\tCompile input                [" << compile << "]\n"
		  << "\t-l NAME\tExecution label         [" << label << "]\n"
#endif
		  << "\t-r N\tNumber of words to read from RMEM [" << rsize 
		  << "]\n" 
		  << std::endl;
	Rcuxx::Rcu::PrintHelp(std::cout);
	std::cout << std::endl;
	return 0;
      case 'e': emul       = true;                         break;
      case 'o': offset     = str2val<int>(argv[i+1]); i++; break;
#ifdef HAVE_ALICE_RCUC_COMPILER_H
      case 'c': compile    = true;                         break;
      case 'l': label      = argv[i+1];               i++; break;
#endif
      case 'i': file       = argv[i+1];               i++; break;
      case 'r': rsize      = str2val<int>(argv[i+1]); i++; break;
      case 'd': rcuxxdebug = str2val<int>(argv[i+1]); i++; break;
      case 'u': u2fdebug   = str2val<int>(argv[i+1]); i++; break;
      case 'a': altrodebug = str2val<int>(argv[i+1]); i++; break;
      default:  device     = argv[i+1]; i++; break;
      }
    }
  }
#ifndef HAVE_ALICE_RCUC_COMPILER_H
  if (compile || !label.empty()) {
    std::cerr << "Compilation or labels not supported without "
	      << "compiler interface library" << std::endl;
    return 1;
  }
#endif

  // Rcuxx::DebugGuard::fDebug = rcuxxdebug;
  Rcuxx::Rcu* rcu = Rcuxx::Rcu::Open(device.c_str(), emul);
  try {
    if (!rcu) throw std::runtime_error("Failed to open device");
    if (u2fdebug)   rcu->SetDebug(1, u2fdebug);
    if (altrodebug) rcu->SetDebug(2, altrodebug);

    unsigned int imem[rcu->IMEM()->Size()];
    
    size_t i = 0;
#ifdef HAVE_ALICE_RCUC_COMPILER_H
    if (compile) {
      if (file.empty()) 
	throw std::runtime_error("Cannot compile command line");
      program = RCUC_compile(file.c_str(), 0, 0);
      if (!program) throw std::runtime_error(RCUC_get_error());
      for (i = 0; i < program->size; i++) {
	char* lab = program->labels[i];
	if (!label.empty() && label == lab) offset = i;
	imem[i] = program->data[i];
      }
    }
#endif
    if (!compile) {
      std::ifstream* finput = 0;
      if (!file.empty()) {
	finput = new std::ifstream(file.c_str());
	if (!finput && !(*finput)) 
	  throw std::runtime_error("Failed to open input file");
      }
      else 
	std::cout << "Please input 32bit instructions, one per line " 
		  << "and finish with Ctrl-D:\n" << std::endl;
      std::istream& input = (finput ? *finput : std::cin);
      while (!input.eof() && i < rcu->IMEM()->Size()) {
	if (!finput) std::cout << "> " << std::flush;
	input >> imem[i++];
	i++;
	if (input.bad()) 
	  throw std::runtime_error("Failed to read input");
      }
      if (finput) finput->close();
    }

    unsigned int ret = 0;
    rcu->IMEM()->Set(0, i, imem);
    if ((ret = rcu->IMEM()->Commit())) throw ret;
    if ((ret = rcu->IMEM()->Exec(offset))) throw ret;

    if (rsize > 0) {
      if (rsize > rcu->RMEM()->Size()) rsize = rcu->RMEM()->Size();
      unsigned int rmem[rcu->RMEM()->Size()];
      rcu->RMEM()->Update();
      rcu->RMEM()->Get(0, rsize, rmem);
      std::cout << "The first " << rsize << " words of RMEM are: " 
		<< std::endl;
      for (size_t j = 0; j < rsize; j++) 
	std::cout << std::setw(4) << j << ": 0x" << std::setw(8) 
		  << std::setfill('0') << std::hex << rmem[j] 
		  << std::dec << std::setfill(' ') << std::endl;
    }
  }
  catch (std::exception& e) {
    std::cerr << e.what() << std::endl;
    return 1;
  }
  catch (unsigned int ret) {
    std::cerr << "Error # " << std::setw(3) << ret << ": ";
    if (rcu) std::cerr << rcu->ErrorString(ret);
    std::cerr << std::endl;
    return 1;
  }
  
  return 0;
}

//
// EOF
//
