dnl
dnl $Id: acinclude.m4,v 1.5 2006-06-03 19:52:39 cholm Exp $
dnl
dnl  Copyright (C) 2002 Christian Holm Christensen <cholm@nbi.dk>
dnl
dnl  This library is free software; you can redistribute it and/or
dnl  modify it under the terms of the GNU Lesser General Public License
dnl  as published by the Free Software Foundation; either version 2.1
dnl  of the License, or (at your option) any later version.
dnl
dnl  This library is distributed in the hope that it will be useful,
dnl  but WITHOUT ANY WARRANTY; without even the implied warranty of
dnl  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
dnl  Lesser General Public License for more details.
dnl
dnl  You should have received a copy of the GNU Lesser General Public
dnl  License along with this library; if not, write to the Free
dnl  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
dnl  02111-1307 USA
dnl
dnl ------------------------------------------------------------------
AC_DEFUN([AC_DEBUG],
[
  AC_REQUIRE([AC_PROG_CC])
  AC_MSG_CHECKING(whether to make debug objects)
  AC_ARG_ENABLE(debug,
    [AC_HELP_STRING([--enable-debug],[Enable debugging symbols in objects])])
  if test "x$enable_debug" = "xno" ; then
    CFLAGS=`echo $CFLAGS | sed 's,-g,,'`
  else
    case $CFLAGS in
    *-g*) ;;
    *)    CFLAGS="$CFLAGS -g" ;;
    esac
  fi
  AC_MSG_RESULT($enable_debug 'CFLAGS=$CFLAGS')
])

dnl ------------------------------------------------------------------
AC_DEFUN([AC_OPTIMIZATION],
[
  AC_REQUIRE([AC_PROG_CC])

  AC_ARG_ENABLE(optimization,
    [AC_HELP_STRING([--enable-optimization],[Enable optimization of objects])])

  AC_MSG_CHECKING(for optimiztion level)

  changequote(<<, >>)dnl
  if test "x$enable_optimization" = "xno" ; then
    CFLAGS=`echo   $CFLAGS   | sed 's,-O\([0-9][0-9]*\|\),,'`
  elif test "x$enable_optimization" = "xyes" ; then
    case $CFLAGS in
    *-O*) ;;
    *)    CFLAGS="$CXXFLAGS -O2" ;;
    esac
  else
    CFLAGS=`echo $CFLAGS | sed "s,-O\([0-9][0-9]*\|\),-O$enable_optimization,"`
  fi
  changequote([, ])dnl
  AC_MSG_RESULT($enable_optimization 'CFLAGS=$CFLAGS')
])


dnl ==================================================================
dnl
dnl Group of macro's to enable `normal' Java language support.
dnl
dnl 
dnl ------------------------------------------------------------------
dnl
dnl Set up Java variables. 
dnl
m4_define([AC_LANG(Java)],
[
  ac_ext=java
  # ac_objext=class
  ac_cpp=true
  ac_compile='if test ! "x$CLASSPATH" = "x" ; then CLASSPATH=$CLASSPATH $JAVAC $JAVACFLAGS conftest.$ac_ext ; else $JAVAC $JAVACFLAGS conftest.$ac_ext ; fi ; if test conftest.class ; then mv conftest.class conftest.${ac_objext} ; fi >&AS_MESSAGE_LOG_FD'
  ac_link='$JAVAC $JAVACFLAGS conftest.$ac_ext >&AS_MESSAGE_LOG_FD'
  ac_compiler_gnu=$ac_cv_javac_compiler_gnu
])
m4_define([AC_LANG_JAVA],          [AC_LANG(Java)])
m4_define([_AC_LANG_ABBREV(Java)], [java])
m4_define([_AC_LANG_PREFIX(Java)], [JAVA])

dnl ------------------------------------------------------------------
dnl
dnl Enables
dnl
dnl AC_LANG_PUSH(Java)
dnl AC_LANG_SOURCE([public class Test {}])
dnl AC_LANG_POP(Java)
dnl
m4_define([AC_LANG_SOURCE(Java)],
[_ACEOF
cat << _ACEOF > conftest.java
/* [#]line __oline__ "configure" */
$1])

dnl ------------------------------------------------------------------
dnl
dnl Enables
dnl
dnl AC_LANG_PUSH(Java)
dnl AC_LANG_PROGRAM([import package.*],[public class Test {}])
dnl AC_LANG_POP(Java)
dnl
m4_define([AC_LANG_PROGRAM(Java)],
[$1
public class conftest 
{
$2
}])
m4_copy([AC_LANG_PROGRAM(Java)],[AC_LANG_CALL(Java)])
m4_copy([AC_LANG_PROGRAM(Java)],[AC_LANG_FUNC_LINK_TRY(Java)])
m4_copy([AC_LANG_PROGRAM(Java)],[AC_LANG_BOOL_COMPILE_TRYE(Java)])


dnl ------------------------------------------------------------------
dnl
dnl Compiles and such
dnl 
m4_define([AC_LANG_PREPROC(Java)],  
	  [true
	   AC_PROVIDE([AC_LANG_PREPROC(Java)])])
m4_define([AC_LANG_COMPILER(Java)], 
	  [dnl AC_REQUIRE([AC_PROG_JAVAC])
	   AC_PROVIDE([AC_LANG_COMPILER(Java)])])

dnl ------------------------------------------------------------------
dnl 
dnl
dnl Checks if we can find a Java byte-code compiler. 
dnl
AC_DEFUN([AC_PROG_JAVAC],
[
   AC_REQUIRE([AC_EXEEXT])dnl
   AC_LANG_PUSH(Java)dnl
   AC_ARG_VAR(JAVAC,[Java bytecode compiler])
   AC_ARG_VAR(JAVAFLAGS,[Java bytecode compiler flags])
   candidates="guavac javac jikes "
   m4_ifval([$1],
            [AC_CHECK_TOOLS(JAVAC, [$1])],
            [AC_CHECK_TOOL(JAVAC, "gcj${EXEEXT}")
             if test -z "$JAVAC" ; then 
               if test -n "${ac_tool_prefix}" ; then
		 for c in $candidates ; do 
	           AC_CHECK_PROG(JAVAC, [${ac_tool_prefix}${c}${EXEEXT}],
	                                [${ac_tool_prefix}${c}${EXEEXT}])
	           if test -n "$JAVAC" ; then break ; fi
                 done
               fi
               if test -z "$JAVAC" ; then 
	         for c in $candidates ; do 
	           AC_CHECK_PROG(JAVAC, [${c}${EXEEXT}],[${c}${EXEEXT}])
	           if test -n "$JAVAC" ; then break ; fi
                 done
	       fi
             else 
	        ac_cv_javac_compiler_gnu=$JAVAC
		JAVACFLAGS="$JAVACFLAGS -C"
             fi])
   test "x$JAVAC" = x && \
     AC_MSG_ERROR([no acceptable Java compiler found in \$PATH])
   AC_PROG_JAVAC_WORKS
   AC_LANG_POP(Java)
   AC_PROVIDE([$0])dnl
])

dnl ------------------------------------------------------------------
dnl 
dnl Check if the Java compiler actually works. 
dnl
AC_DEFUN([AC_PROG_JAVAC_WORKS],
[
   AC_CACHE_CHECK([if $JAVAC works], ac_cv_prog_javac_works, [
     AC_COMPILE_IFELSE([AC_LANG_PROGRAM([],[])],
		       [ac_cv_prog_javac_works=yes],
                       [ac_cv_prog_javac_works=no])  
  
  ])
  AC_PROVIDE([$0])dnl
])

dnl ------------------------------------------------------------------
dnl 
dnl User-level macro to set the class path for Java. 
dnl
AC_DEFUN([AC_JAVA_CLASSPATH],
[
  AC_ARG_WITH([classpath],
	      [AC_HELP_STRING([--with-classpath=DIRS_OR_JARS],
                       [Set class path to directories and/or archives])],
              [if test "x$withval" = "x" ; then
	         :
               else
		 if test "x$CLASSPATH" = "x" ; then 
		   CLASSPATH="$withval"
         	 else
                   CLASSPATH="$withval:$CLASSPATH"
		 fi
               fi])
  AC_ARG_VAR(CLASSPATH, 
             [Path of directories and/or archives to search for classes])
  AC_ARG_VAR(JAVACFLAGS,
             [Flags passed to the Java compiler])
  if test "x$CLASSPATH" = "x" ; then 
      :
  else 
      JAVACFLAGS="-classpath $CLASSPATH $JAVACFLAGS"
  fi
])

dnl ------------------------------------------------------------------
dnl
dnl Search for Java archiver
dnl
AC_DEFUN([AC_PROG_JAR],
[
  AC_PATH_PROGS(JAR, [jar fastjar])
  AC_ARG_VAR(JAR, [Java object archiver])
])

dnl ==================================================================
dnl
dnl Check if we have org.jdesktop.layout
dnl
AC_DEFUN([HAVE_ORG_JDESKTOP_LAYOUT],
[
    AC_MSG_CHECKING(whether we have org.jdesktop.layout)
    have_org_jdesktop_layout=no
    AC_LANG_PUSH(Java)
    AC_COMPILE_IFELSE(AC_LANG_PROGRAM([import javax.swing.*;
                                   import org.jdesktop.layout.*;],
[private static class Widget extends JFrame 
    {
	public Widget(String msg)
	{
	    JLabel  label   = new JLabel(msg);
	    // Testing group layout. 
	    GroupLayout layout = new GroupLayout(getContentPane());
	    getContentPane().setLayout(layout);
	    layout.setAutocreateContainerGaps(true);
	    layout.setVerticalGroup(layout.createParallelGroup().add(label));
	    layout.setHorizontalGroup(layout.createSequentialGroup().add(label));
	    pack();
	    setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
	}
    }
    private static class RunIt implements Runnable 
    {
	public RunIt()
	{}
	public void run() {
	    try {
		UIManager.setLookAndFeel(UIManager.
					 getSystemLookAndFeelClassName());
	    } catch (Exception ex) {
		ex.printStackTrace();
	    }
	    new Widget("Hello").setVisible(true);
	}
    }
    public static void main(String args[]) 
    {
        java.awt.EventQueue.invokeLater(new RunIt());
    }]),[have_org_jdesktop_layout=yes],[have_org_jdesktop_layout=no])
    AC_MSG_RESULT($have_org_jdesktop_layout)
    AC_LANG_POP(Java)
    if test "x$have_org_jdesktop_layout" = "xyes" ; then 
        ifelse([$1], , :, [$1])
    else
        ifelse([$2], , :, [$2])
    fi
])

dnl ==================================================================
dnl
dnl Check if we have org.jdesktop.layout
dnl
AC_DEFUN([HAVE_ORG_NETBEANS_LIB_AWTEXTRA],
[
    AC_MSG_CHECKING(whether we have org.netbeans.lib.awtextra)
    have_org_netbeans_lib_awtextra=no
    AC_LANG_PUSH(Java)
    AC_COMPILE_IFELSE(AC_LANG_PROGRAM([import javax.swing.*;
import org.netbeans.lib.awtextra.*;],
[   private static class Widget extends JFrame 
    {
	Widget(String msg) 
	{
	    /* org.netbeans.lib.awtextra. */
	    JLabel label = new JLabel(msg);
	    getContentPane().setLayout(new AbsoluteLayout());
	    
	    getContentPane().add(label, new AbsoluteConstraints(30,20,-1,-1));
	    pack();
	    setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
	}
    }
    private static class RunIt implements Runnable
    {
        public RunIt()
        {}
        public void run() {
            try {
                UIManager.setLookAndFeel(UIManager.
                                         getSystemLookAndFeelClassName());
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            new Widget("Hello").setVisible(true);
        }
    }
    public static void main(String args[])
    {
        java.awt.EventQueue.invokeLater(new RunIt());
    }]),
    [have_org_netbeans_lib_awtextra=yes],
    [have_org_netbeans_lib_awtextra=no])
    AC_MSG_RESULT($have_org_netbeans_lib_awtextra)
    AC_LANG_POP(Java)
    if test "x$have_org_netbeans_lib_awtextra" = "xyes" ; then 
        ifelse([$1], , :, [$1])
    else
        ifelse([$2], , :, [$2])
    fi
])

dnl ==================================================================
dnl
dnl Macro to set LabView prefix 
dnl
AC_DEFUN([AC_PREFIX_LABVIEW],
[
    AC_ARG_WITH([labview-prefix],
                [AC_HELP_STRING([--with-labview-prefix],
                                [Where LabVIEW is installed])])
    AC_CACHE_CHECK([for LabVIEW installation prefix],[ac_cv_labview_prefix],
    [if test "x$with_labview_prefix" = "x" ; then 
	ac_cv_labview_prefix=$with_labview_prefix
     fi
     if test "x$ac_cv_labview_prefix" = "x" && \
       test -f /etc/natinst/labview/labview.dir ; then 
	ac_cv_labview_prefix=$(cat /etc/natinst/labview/labview.dir)
     fi])
    AC_PROVIDE([AC_PREFIX_LABVIEW])
])

dnl ------------------------------------------------------------------
dnl
dnl
dnl
AC_DEFUN([AC_PROG_LVSBUTIL],
[
    AC_REQUIRE([AC_PREFIX_LABVIEW])
    save_PATH=$PATH
    if test ! "x$ac_cv_labview_prefix" = "x" ; then 
	PATH=${ac_cv_labview_prefix}/cintools:$PATH
    fi
    AC_PATH_PROG(LVSBUTIL, [lvsbutil])
    PATH=$save_PATH
    AC_PROVIDE([AC_PROG_LVSBUTIL])
])

dnl ------------------------------------------------------------------
dnl
dnl
dnl
AC_DEFUN([AC_CHECK_LIBCIN],
[
    AC_REQUIRE([AC_PREFIX_LABVIEW])
    save_LDFLAGS="$LDFLAGS"
    ac_tmp_have_libcin=no
    if test ! "x$ac_cv_labview_prefix" = "x" ; then 
	LDFLAGS="-L${ac_cv_labview_prefix}/cintools $LDFLAGS"
    fi
    AC_CHECK_LIB(cin, CINLoad, [ac_tmp_have_libcin=yes])
    LDFLAGS=$save_LDFLAGS
    if test "x$ac_tmp_have_libcin" = "xyes" && \
       test ! "x$ac_cv_labview_prefix" = "x" ; then
	CIN_LDFLAGS="-L${ac_cv_labview_prefix}/cintools"
	CIN_LIBS="-lcin"
    fi
    AC_SUBST(CIN_LDFLAGS)
    AC_SUBST(CIN_LIBS)
    if test "x$ac_tmp_have_libcin" = "xyes" ; then 
        ifelse([$1], , :, [$1])
    else
        ifelse([$2], , :, [$2])
    fi
    AC_PROVIDE([AC_CHECK_LIBCIN])
])

dnl ------------------------------------------------------------------
dnl
dnl
dnl
AC_DEFUN([AC_CHECK_CIN_O],
[
    AC_REQUIRE([AC_PREFIX_LABVIEW])
    save_LDFLAGS="$LDFLAGS"
    ac_tmp_have_cin_o=no
    AC_CHECK_FILE($ac_cv_labview_prefix/cintools/cin.o,[ac_tmp_have_cin_o=yes])
    if test "x$ac_tmp_have_cin_o" = "xyes" && \
       test ! "x$ac_cv_labview_prefix" = "x" ; then
	CIN_O=$ac_cv_labview_prefix/cintools/cin.o
    fi
    AC_SUBST(CIN_O)
    if test "x$ac_tmp_have_cin_o" = "xyes" ; then 
        ifelse([$1], , :, [$1])
    else
        ifelse([$2], , :, [$2])
    fi
    AC_PROVIDE([AC_CHECK_CIN_O])
])
	  
dnl ------------------------------------------------------------------
dnl
dnl
dnl
AC_DEFUN([AC_CHECK_CIN_EXTCODE],
[
    AC_REQUIRE([AC_PREFIX_LABVIEW])
    save_CPPFLAGS="$CPPFLAGS"
    ac_tmp_have_cin_extcode=no
    if test ! "x$ac_cv_labview_prefix" = "x" ; then 
	CPPFLAGS="-I${ac_cv_labview_prefix}/cintools $CPPFLAGS"
    fi
    AC_CHECK_HEADER(extcode.h,[ac_tmp_have_cin_extcode=yes])
    if test "x$ac_tmp_have_cin_ext_code" = "xyes" ; then
	AC_MSG_CHECKING(if extcode.h defines CIN_VERS)
	AC_COMPILE_IFELSE([AC_LANG_PROGRAM([#include <extcode.h>
],
[#ifndef CIN_VERS
choke me
#endif
])], , [ac_tmp_have_cin_ext_code=no])
	AC_MSG_RESULT($ac_tmp_have_cin_ext_code)
    fi
    CPPFLAGS=$save_CPPFLAGS
    if test "x$ac_tmp_have_cin_extcode" = "xyes" && \
       test ! "x$ac_cv_labview_prefix" = "x" ; then
	CIN_CPPFLAGS="-I${ac_cv_labview_prefix}/cintools"
    fi
    AC_SUBST(CIN_CPPFLAGS)
    if test "x$ac_tmp_have_cin_extcode" = "xyes" ; then 
        ifelse([$1], , :, [$1])
    else
        ifelse([$2], , :, [$2])
    fi
    AC_PROVIDE([AC_CHECK_CIN_EXTCODE])
])

dnl ------------------------------------------------------------------
dnl
dnl
dnl
AC_DEFUN([AC_LABVIEW_CIN],
[
    ac_tmp_have_labview_cin=yes
    AC_REQUIRE([AC_PREFIX_LABVIEW])
    AC_REQUIRE([AC_CHECK_CIN_EXTCODE],[],[ac_tmp_have_labview_cin=no])
    AC_REQUIRE([AC_CHECK_CIN_O],[],[ac_tmp_have_labview_cin=no])
    AC_REQUIRE([AC_CHECK_LIBCIN],[],[ac_tmp_have_labview_cin=no])
    AC_REQUIRE([AC_PROG_LVSBUTIL],[],[ac_tmp_have_labview_cin=no])
    if test "x$ac_tmp_have_labview_cin" = "xyes" ; then 
        ifelse([$1], , :, [$1])
    else
        ifelse([$2], , :, [$2])
    fi
])	  


dnl ==================================================================
dnl
dnl
dnl 
AC_DEFUN([AC_KERNEL_SOURCES],
[
  AC_REQUIRE([AC_CANONICAL_HOST])
  dnl Check that we're compiling for Linux
  case "$host" in
  *-*-linux*)  ;;
  *) AC_MSG_ERROR([librcu is only supported on Linux for the time being]);;
  esac
  dnl Enable some command line options 
  AC_ARG_WITH([kernel-version],
	      [AC_HELP_STRING([--with-kernel-version=VERS],
	                      [Compile driver for kernel version VERS])])
  AC_ARG_WITH([kernel-sources],
	      [AC_HELP_STRING([--with-kernel-sources=DIR],
	                      [Compile driver for kernel sources in DIR])])
  dnl Check if we got an argument that specifies the kernel source location. 
  if test "x$with_kernel_sources" != "x" && 
    test "x$with_kernel_sources" != "xno" ;then
    KSRC=$with_kernel_sources
  fi
  dnl Check if we got an argument that specifies the kernel version
  if test "x$with_kernel_version" != "x" && 
     test "x$with_kernel_version" != "xno" ;then
    KSRC=/lib/modules/${with_kernel_version}/build
  fi
  dnl If the kernel source location wasn't defined, then try to deduce 
  dnl it from the kernel version. 
  if test "x$KSRC" = "x" ; then 
    dnl If no kernel version was specified, deduce it from the currently
    dnl running kernel - but only if we're on a Linux host 
    case $build in 
    *-*-linux-*) KVERS=`uname -r` ;;
    *) ;;
    esac
    KSRC=/lib/modules/$KVERS/build
  fi
  dnl check that we found some valid sources. 
  AC_MSG_CHECKING(for kernel sources)
  if test ! -d  $KSRC/include/linux ; then 
    KSRC=
    KVERS=
    AC_MSG_RESULT(no)
    AC_MSG_WARN(Kernel sources not found in $KSRC)
  else 
    AC_MSG_RESULT($KSRC)
    dnl Check that the sources are actually configured. 
    if test ! "x$KSRC" = "x" && test ! -f $KSRC/.config ; then 
      AC_MSG_WARN([Kernel sources in $KSRC not configured])
      KSRC=
      KVERS=
    fi
    dnl Now check for the kernel version, if one wasn't already specified. 
    AC_MSG_CHECKING(kernel version of sources i $KSRC)
    if test "x$KVERS" = "x" ; then 
      if test -f $KSRC/include/linux/version.h ; then 
        KVERS=$(grep UTS_RELEASE $KSRC/include/linux/version.h | \
	        sed -e 's/.*UTS_RELEASE *"//' -e 's/".*//')
      elif test -f $KSRC/.kernelrelease ; then 
        KVERS=$(cat $KSRC/.kernelrelease)
      else
        KSRC=
        KVERS=
      fi	 
    fi
    AC_MSG_RESULT([$KVERS])
    if test "x$KVERS" = "x" ; then 
      AC_MSG_WARN([Could not determine kernel version])
    fi
  fi
  dnl Substitute variables 
  AC_SUBST(KVERS)
  AC_SUBST(KSRC)
])
	        
#
# EOF
#
