/************************************************************************/
/*									*/
/*  This is the secondary header file for the ALTRO library		*/
/*									*/
/*  7. Sep. 04  MAJO  created						*/
/*									*/
/*******C 2004 - The software with that certain something****************/

#ifndef _ALTRO_COMMON_H
#define _ALTRO_COMMON_H

#ifndef __KERNEL__
# ifdef ALTRO_BUILD
#  include <tools/rcc_error.h>
# else
#  include <altro/rcc_error.h>
# endif
# ifndef __SYS_TYPES_H
#  include <sys/types.h>
# endif
#else 
# define P_ID_ALTRO 1 /* MUST be in sync with tools/rcc_error.h */
#endif

/*************/
/* Constants */
/*************/
#define BULK_OUT_SIZE      1024
#define BULK_IN_SIZE       1024
#define MAX_ALTRO_DEVICES  16
#define MAX_PROC_TEXT_SIZE 0x10000 /*The output of "more /proc/altro"
				     must not generate more characters
				     than that*/ 

/*********/
/* Types */
/*********/
typedef struct altro_bulk_out
{
  u_int nbytes;                /* Number of bytes in buffer */
  u_char data[BULK_OUT_SIZE];  /* The data in the buffer as bytes */
} altro_bulk_out_t;

typedef struct altro_bulk_in
{
  u_int nbytes;               /* Number of bytes in buffer */
  u_char data[BULK_IN_SIZE];  /* The data in the buffer as bytes */
} altro_bulk_in_t;

typedef struct altro_control
{
  u_char bRequest;       /* Request byte */
  u_char bmRequestType;  /* Request type */
  u_char data[100];      /* Data of the request */
  u_short wValue;        /* value */
  u_short wIndex;        /* index */
  u_short wLength;       /* length */
} altro_control_t;

enum altro_ioctl_codes 
{
  SEND_DATA = 1, /* Send Data IO control code */
  GET_DATA,      /* Get Data IO control code */
  CONTROL        /* Control IO control code */
};

enum altro_error_codes
{
  ALTRO_SUCCESS = 0,                /* Function successfully executed */
  ALTRO_FILE = (P_ID_ALTRO <<8) + 1,  /* Failed to open / close device file */
  ALTRO_NOTOPEN,                    /* The library has not yet been opened */
  ALTRO_NO_CODE,                    /* Unknown error */
  ALTRO_EFAULT,                     /* Error from the driver */
  ALTRO_ERROR_FAIL                  /* Failure */
};
 
#define ALTRO_SUCCESS_STR        "Function successfully executed"
#define ALTRO_FILE_STR           "Failed to open / close device file"
#define ALTRO_NOTOPEN_STR        "The library has not yet been opened"
#define ALTRO_EFAULT_STR         "Error from the driver"
#define ALTRO_NO_CODE_STR        "Unknown error"

#endif
