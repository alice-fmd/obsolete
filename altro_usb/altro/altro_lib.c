/************************************************************************/
/*									*/
/*  This is the ALTRO USB library    				        */
/*									*/
/*  16. Mar. 04  MAJO  created						*/
/*									*/
/*******C 2004 - The software with that certain something****************/

#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include "altro.h"


/***********/
/* Globals */
/***********/
static int fd[MAX_ALTRO_DEVICES] = {0};
static int altro_is_open[MAX_ALTRO_DEVICES] = {0};
int inthandle;
extern int traceLevel, packageId;


/***************************************/
u_int ALTRO_Open(char *node, int* handle)
/***************************************/
{  
  u_int ret;
  
  DEBUG_TEXT(P_ID_ALTRO, 15 ,("  ALTRO_Open: called for node %s\n", node));
  
  /* open the error package */
  ret = rcc_error_init((err_pid)P_ID_ALTRO, ALTRO_err_get);
  if (ret) 
  {
    DEBUG_TEXT(P_ID_ALTRO, 5 ,("ALTRO_Open: Failed to open error package\n"));
    return(RCC_ERROR_RETURN(0, ALTRO_ERROR_FAIL)); 
  }
  DEBUG_TEXT(P_ID_ALTRO, 20 ,("ALTRO_Open: error package opened\n"));

  /* Which card are we dealing with*/
  /* convert from ASCII to decimal */
  sscanf(&node[strlen(node) - 1], "%d", handle);  
  DEBUG_TEXT(P_ID_ALTRO, 20 ,("  ALTRO_Open: Opening library for card %d\n", 
			      *handle));
  
  if (altro_is_open[*handle]) 
  {
    /* keep track of multiple open calls */
    altro_is_open[*handle]++;                     
    DEBUG_TEXT(P_ID_ALTRO, 15 ,("  ALTRO_Open: done\n"));
    return(RCC_ERROR_RETURN(0, ALTRO_SUCCESS));
  }
  DEBUG_TEXT(P_ID_ALTRO, 20 ,("ALTRO_Open: Opening %s\n", node));
  if ((fd[*handle] = open(node, O_RDWR)) < 0) 
  {
    perror("open");
    DEBUG_TEXT(P_ID_ALTRO, 20 ,("ALTRO_Open: failed to open %s\n", node));
    return(RCC_ERROR_RETURN(0, ALTRO_FILE)); 
  }
  DEBUG_TEXT(P_ID_ALTRO, 20 ,("ALTRO_Open: %s is open\n", node));
  altro_is_open[*handle] = 1;
  return(ALTRO_SUCCESS);
}


/***************************/
u_int ALTRO_Close(int handle)
/***************************/
{
  DEBUG_TEXT(P_ID_ALTRO, 20 ,("  ALTRO_Close: called for card %d\n", handle));
 
  if (!altro_is_open[handle])
  {
    DEBUG_TEXT(P_ID_ALTRO, 5 ,
	       ("  ALTRO_Close: this card has not yet been opened\n"));
    return(RCC_ERROR_RETURN(0, ALTRO_NOTOPEN));
  }

  if (altro_is_open[handle] > 1)
    altro_is_open[handle]--;
  else 
  {  
    close(fd[handle]);
    altro_is_open[handle] = 0;
  }
  
  DEBUG_TEXT(P_ID_ALTRO, 20 ,("  ALTRO_Close: done\n"));
  return(ALTRO_SUCCESS);
}


/*************************************************/
u_int ALTRO_Send(int handle, altro_bulk_out_t *out)
/*************************************************/
{
  int ret, pid, tl, loop, ndump;

  DEBUG_TEXT(P_ID_ALTRO, 15 ,("  ALTRO_Send: called\n"));
  DEBUG_TEXT(P_ID_ALTRO,20,("  ALTRO_Send: Called for card %d\n", handle));
  if (!altro_is_open[handle]) 
    return(RCC_ERROR_RETURN(0, ALTRO_NOTOPEN));
 
  DEBUG_TEXT(P_ID_ALTRO,20,("  ALTRO_Send: out.nbytes = %d \n", out->nbytes));

  rcc_error_get_debug(&pid, &tl);
  if ((pid == P_ID_ALTRO || pid == 0) && tl >= 20)   /*debug*/
  {
    if (out->nbytes < 20)
      ndump = out->nbytes;
    else
      ndump = 20;
    printf("  ALTRO_Send: Dumping %d bytes from out.data:\n", ndump);
    for (loop = 0; loop < ndump; loop++)
      printf("  ALTRO_Send: byte %d = 0x%02x\n", loop, out->data[loop]); 
  }

  ret = ioctl(fd[handle], SEND_DATA, out);
  if (ret) 
  {
    DEBUG_TEXT(P_ID_ALTRO,5,("  ALTRO_Send: Error from ioctl\n"));
    return(RCC_ERROR_RETURN(0, errno));
  }

  DEBUG_TEXT(P_ID_ALTRO, 15 ,("  ALTRO_Send: done\n"));
  return(ALTRO_SUCCESS);
}


/**********************************************/
u_int ALTRO_Get(int handle, altro_bulk_in_t *in)
/**********************************************/
{
  int ret, pid, tl, loop, ndump;

  DEBUG_TEXT(P_ID_ALTRO, 15 ,("  ALTRO_Get: Called for card %d\n", handle));
  if (!altro_is_open[handle]) 
    return(RCC_ERROR_RETURN(0, ALTRO_NOTOPEN));
    
  ret = ioctl(fd[handle], GET_DATA, in);
  if (ret) 
  {
    DEBUG_TEXT(P_ID_ALTRO, 5 ,("  ALTRO_Get: Error from ioctl\n"));
    return(RCC_ERROR_RETURN(0, errno));
  }

  DEBUG_TEXT(P_ID_ALTRO, 5 ,
	     ("  ALTRO_Get: %d bytes received from the USB endpoint\n", 
	      in->nbytes));

  rcc_error_get_debug(&pid, &tl);

  if ((pid == P_ID_ALTRO || pid == 0) && tl >= 20)   /*debug*/

  {
    if (in->nbytes < 20)
      ndump = in->nbytes;
    else
      ndump = 20;
    printf("  ALTRO_Get: Dumping %d bytes from in.data:\n", ndump);
    for (loop = 0; loop < ndump; loop++)
      printf("  ALTRO_Get: byte %d = 0x%02x\n", loop, in->data[loop]); 
  }

  DEBUG_TEXT(P_ID_ALTRO, 15 ,("  ALTRO_Get: done\n"));
  return(ALTRO_SUCCESS);
}

  
/****************************************************/
u_int ALTRO_Control(int handle, altro_control_t *ctrl)
/****************************************************/
{
  int ret;

  DEBUG_TEXT(P_ID_ALTRO, 15 ,
	     ("  ALTRO_Control: Called for card %d\n", handle));
  if (!altro_is_open[handle]) 
    return(RCC_ERROR_RETURN(0, ALTRO_NOTOPEN));
    
  ret = ioctl(fd[handle], CONTROL, ctrl);
  if (ret) 
  {
    DEBUG_TEXT(P_ID_ALTRO, 5 , 
	       ("  ALTRO_Control: Error from ioctl, errno = 0x%08x\n", errno));
    return(RCC_ERROR_RETURN(0, errno));
  }
  
  DEBUG_TEXT(P_ID_ALTRO, 15 ,("  ALTRO_Control: done\n"));
  return(ALTRO_SUCCESS);
}


/**********************************************************/
u_int ALTRO_err_get(err_pack err, err_str pid, err_str code)
/**********************************************************/
{ 
  strcpy(pid, P_ID_ALTRO_STR);

  switch (RCC_ERROR_MINOR(err)) 
  {  
    case ALTRO_SUCCESS:  strcpy(code, ALTRO_SUCCESS_STR);   break;
    case ALTRO_FILE:     strcpy(code, ALTRO_FILE_STR);      break;
    case ALTRO_EFAULT:   strcpy(code, ALTRO_EFAULT_STR);    break;
    default:             strcpy(code, ALTRO_NO_CODE_STR);   
      return(RCC_ERROR_RETURN(0, ALTRO_NO_CODE)); break;
  }
  return(RCC_ERROR_RETURN(0, ALTRO_SUCCESS));
}
