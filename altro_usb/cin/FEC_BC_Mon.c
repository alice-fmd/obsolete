/* CIN source file */

#include "extcode.h"
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/file.h>
#include <signal.h>
#include "fec/fec.h"

MgErr CINRun(uInt32 *Handle, uInt32 *fec_addr, float32 *temp, float32 *ana_v, float32 *ana_i, float32 *dig_v, float32 *dig_i, uInt32 *error);

MgErr CINRun(uInt32 *Handle, uInt32 *fec_addr, float32 *temp, float32 *ana_v, float32 *ana_i, float32 *dig_v, float32 *dig_i, uInt32 *error)
	{
        float a=0,b=0,c=0,d=0,e=0;

        *error = FEC_BC_Mon(*Handle, *fec_addr, &a, &b, &c, &d, &e);
	*temp = a;
	*ana_v = b;
	*ana_i = c;
	*dig_v = d;
	*dig_i = e;
	return noErr;
	}

