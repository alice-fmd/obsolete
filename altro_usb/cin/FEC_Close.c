/* CIN source file */

#include "extcode.h"
#include "fec/fec.h"

MgErr CINRun(uInt32 *Handle, uInt16 *Error);

MgErr CINRun(uInt32 *Handle, uInt16 *Error)
	{
        *Error = FEC_Close(*Handle);
	return noErr;
	}
