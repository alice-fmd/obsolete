/* CIN source file */

#include "extcode.h"
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/file.h>
#include <signal.h>
#include "fec/fec.h"

MgErr CINRun(uInt32 *Handle, uInt32 *fec_addr, uInt32 *trg_mode, uInt32 *rdo_mode, uInt32 *nevents, uInt32 *nsamples, uInt32 *pre_trig, LStrHandle arg1, int32 *arg2, uInt16 *Error);

MgErr CINRun(uInt32 *Handle, uInt32 *fec_addr, uInt32 *trg_mode, uInt32 *rdo_mode, uInt32 *nevents, uInt32 *nsamples, uInt32 *pre_trig, LStrHandle arg1, int32 *arg2, uInt16 *Error)
	{
        char *file_name;

        file_name = (char*)LStrBuf(*arg1);
	file_name[*arg2] = '\0';
        *Error = FEC_Event_Readout(*Handle, *fec_addr, *trg_mode, *rdo_mode, *nevents, *nsamples, *pre_trig, file_name);
	return noErr;
	}
