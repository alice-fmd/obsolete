/* CIN source file */

#include "extcode.h"
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/file.h>
#include <signal.h>
#include "fec/fec.h"

MgErr CINRun(uInt32 *Handle, uInt32 *arg1, uInt32 *RegisterAddress, uInt32 *Data, uInt16 *Error);

MgErr CINRun(uInt32 *Handle, uInt32 *arg1, uInt32 *RegisterAddress, uInt32 *Data, uInt16 *Error)
	{
	*Error = FEC_Read_ALTRO_Reg(*Handle, *arg1, *RegisterAddress, Data);
	return noErr;
	}
