/* CIN source file */

#include "extcode.h"
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/file.h>
#include <signal.h>
#include "fec/fec.h"

MgErr CINRun(uInt32 *Handle, uInt32 *fec_addr, uInt32 *nwords, LStrHandle arg1, int32 *arg2, uInt32 *ErrorCode);

MgErr CINRun(uInt32 *Handle, uInt32 *fec_addr, uInt32 *nwords, LStrHandle arg1, int32 *arg2, uInt32 *ErrorCode)
	{
	char *new_file_name;
	
	new_file_name = (char*)LStrBuf(*arg1);
	new_file_name[*arg2] = '\0';  
        *ErrorCode = FEC_FM_Read(*Handle, *fec_addr, *nwords, new_file_name);
	return noErr;
	}
