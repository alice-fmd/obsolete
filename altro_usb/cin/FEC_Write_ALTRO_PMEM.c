/* CIN source file */

#include "extcode.h"
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/file.h>
#include <signal.h>
#include "fec/fec.h"

MgErr CINRun(uInt32 *Handle, uInt32 *bcast, uInt32 *ch_addr, uInt32 *nwords, LStrHandle fname, int32 *arg1, uInt16 *PMEMERROR);

MgErr CINRun(uInt32 *Handle, uInt32 *bcast, uInt32 *ch_addr, uInt32 *nwords, LStrHandle fname, int32 *arg1, uInt16 *PMEMERROR)
	{
	char *new_file_name;
	
	new_file_name = (char*)LStrBuf(*fname);
        new_file_name[*arg1] = '\0';
	*PMEMERROR = FEC_Write_ALTRO_PMEM(*Handle, *bcast, *ch_addr, *nwords, new_file_name);
	return noErr;
	}
