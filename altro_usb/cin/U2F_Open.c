/* CIN source file */

#include "extcode.h"
#include "u2f/u2f.h"

MgErr CINRun(uInt32 *Handle, uInt16 *Error);

MgErr CINRun(uInt32 *Handle, uInt16 *Error)
	{
	  int h;
	*Error = U2F_Open(DEFAULT_NODE, &h,HW);
        *Error += U2F_Reg_Write(*Handle, O_TRCFG2, 0x0);
	*Handle = h;
	return *Error;
	}
