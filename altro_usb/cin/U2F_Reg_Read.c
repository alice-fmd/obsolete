/* CIN source file */

#include "extcode.h"
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/file.h>
#include <signal.h>
#include "u2f/u2f.h"

MgErr CINRun(uInt32 *Handle, uInt32 *Register, uInt32 *Data, uInt32 *Error);

MgErr CINRun(uInt32 *Handle, uInt32 *Register, uInt32 *Data, uInt32 *Error)
	{
	*Error = U2F_Reg_Read(*Handle, *Register, Data);
	return noErr;
	}
