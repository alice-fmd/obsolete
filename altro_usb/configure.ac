dnl -*- mode: autoconf -*- 
dnl
dnl $Id: configure.ac,v 1.5 2006-06-03 19:52:39 cholm Exp $
dnl
dnl ------------------------------------------------------------------
AC_INIT([ALICE RCU/U2F], [0.6], [Luciano.Musa@cern.ch], altro)
AC_PREREQ(2.53)
AC_COPYRIGHT(GNU Lesser General Public License)
AC_REVISION($Revision: 1.5 $)
AC_CONFIG_SRCDIR(u2f/u2f_lib.c)
AC_CONFIG_AUX_DIR(config)
AC_PREFIX_DEFAULT(${HOME})
AM_INIT_AUTOMAKE([$PACKAGE_TARNAME], [$PACKAGE_VERSION])

dnl ------------------------------------------------------------------
dnl 
dnl Check host type
dnl  
AC_CANONICAL_HOST

dnl ------------------------------------------------------------------
dnl 
dnl Check for kernel sources 
dnl  
AC_KERNEL_SOURCES
if ! test "x$KVERS" = "x" ; then
  case x$KVERS in 
  x)
    KSRC=
    KVERS=
    MODEXT=
    ;;
  2.2*|2.4*)
    old_build=yes
    MODEXT=o
    ;;
  *)
   old_build=no
   MODEXT=ko
   ;;
  esac
fi
AM_CONDITIONAL(OLD_MODULE_BUILD, test "x$old_build" = "xyes")
AM_CONDITIONAL(MODULE_BUILD, test ! "x$KSRC" = "x")
AC_SUBST(MODEXT)
   
dnl ------------------------------------------------------------------
dnl
dnl Minor node number 
dnl
AC_MSG_CHECKING(for minor node number of /dev/rcu)
MINOR=192
AC_ARG_WITH([rcu-minor],
	    [AC_HELP_STRING([--with-rcu-minor=NUMBER],
	                    [Minor node number of /dev/usb/rcu])])
if test "x$with_rcu_minor" != "x" && 
   test "x$with_rcu_minor" != "xno" ;then
   MINOR=$with_rcu_minor
fi
AC_SUBST(MINOR)
AC_MSG_RESULT($MINOR)

dnl ------------------------------------------------------------------
dnl
dnl Dummy RCU library
dnl
AH_TEMPLATE([RCU_BANKS_HAVE_DATA], [Whether to include a data member in rcu_banks_t])
AC_MSG_CHECKING(whether to use dummy RCU library)
AC_ARG_ENABLE([dummy], 
	    [AC_HELP_STRING([--enable-dummy],
	                    [Make dummy RCU library])])
if test "x$enable_dummy" = "xyes" ; then
   AC_DEFINE([RCU_BANKS_HAVE_DATA])
fi
AM_CONDITIONAL(USE_DUMMY, test "x$enable_dummy" = "xyes")
AC_MSG_RESULT($enable_dummy)
		       

dnl ------------------------------------------------------------------
dnl
dnl Check for C compiler 
dnl
AC_PROG_CC


dnl ------------------------------------------------------------------
dnl
dnl Check for libtool
dnl
AC_PROG_LIBTOOL 


dnl ------------------------------------------------------------------
dnl
dnl Check for Java compiler 
dnl
AC_ARG_ENABLE([java],
	      [AC_HELP_STRING([--enable-java], 
			      [Enable Java GUI interface])],
	      [], [enable_java=yes])
if test "x$enable_java" = "xyes" ; then 
   AC_JAVA_CLASSPATH
   AC_PROG_JAVAC
else
   JAVAC=
fi
if test ! "x$JAVAC" = "x" ; then 
    enable_java=yes
else
    enable_java=no
fi
if test "x$enable_java" = "xyes" ; then 
    AC_PROG_JAR
    if test "x$JAR" = "x" ; then enable_java=no ; fi
fi
if test "x$enable_java" = "xyes" ; then
    HAVE_ORG_JDESKTOP_LAYOUT([],[enable_java=no])
fi
if test "x$enable_java" = "xyes" ; then
    HAVE_ORG_NETBEANS_LIB_AWTEXTRA([], [enable_java=no])
fi
AC_MSG_CHECKING([whether to make Java GUI])
AC_MSG_RESULT([$enable_java])
AM_CONDITIONAL(HAVE_JAVA, test "x$enable_java" = "xyes")

dnl
dnl __________________________________________________________________
AC_ARG_ENABLE([jni],
              [AC_HELP_STRING([--enable-jni],
                              [Make the Java Native Interface ])])
AC_CHECK_HEADER([jni.h], [], [enable_jni=no])
AC_MSG_CHECKING([whether make Java Native Interface])
if test ! "x$enable_jni" = "xno" ; then
   enable_jni=yes
fi
AC_MSG_RESULT([$enable_jni])
AM_CONDITIONAL([HAVE_JNI],test "x$enable_jni" = "xyes")
dnl AC_SUBST(enable_jni)


dnl ------------------------------------------------------------------
dnl
dnl Check for LabVIEW
dnl
AC_ARG_ENABLE([labview],
	      [AC_HELP_STRING([--enable-labview],
			      [Enable LabVIEW interface])],
              [],[enable_labview=yes])
if test "x$enable_labview" = "xyes" ; then 
    AC_LABVIEW_CIN([enable_labview=yes])
fi
AC_MSG_CHECKING([whether to make LabVIEW interface])
AC_MSG_RESULT([$enable_labview])
AM_CONDITIONAL(HAVE_LABVIEW, test "x$enable_labview" = "xyes")

dnl ------------------------------------------------------------------
dnl
dnl Various flags
dnl
AC_DEBUG
AC_OPTIMIZATION
CFLAGS="$CFLAGS -Wall -Werror"



dnl ------------------------------------------------------------------
AC_OUTPUT([Makefile 
           driver/Makefile
	   altro/Makefile
	   tools/Makefile
	   u2f/Makefile
	   goofie/Makefile
	   fec/Makefile
	   java/Makefile
	   java/jni/Makefile
	   java/ju2f/Makefile
	   cin/Makefile
	   debian/Makefile
	   scripts/Makefile
	   scripts/altro
	   scripts/altro-config
	   scripts/altro_usb.spec], [])

dnl
dnl EOF
dnl
