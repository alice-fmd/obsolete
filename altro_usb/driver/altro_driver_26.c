/*
 * This diver based on:
 (
 * USB Skeleton driver - 1.1
 *
 * Copyright (C) 2001-2003 Greg Kroah-Hartman (greg@kroah.com)
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as
 *	published by the Free Software Foundation, version 2.
 */
/** @file    altro_driver_26.c
 *  @author  Markus Joos, CERN-PH-ESS 15.3.2004
 *  @date    Mon Jan 17 22:28:58 2005
 *  @brief   This is the driver for the ALICE RCU card
 *
 *  This driver works for kernels from 2.4 onwards
 *
 *  Currently only one RCU card is supported. Extending the driver for
 *  multiple cards would require to introduce a "minor" array as done
 *  in the usb-skeleton driver 
 *
 *  @par open issues:  
 *
 *  1) The memory read functions do not work because the IN
 *  transaction is executed before the OUT transaction 
 *
 *  2) If one repeats the same memory write two times
 *  (e.g. IMEM_Write) the first transaction has an ACK,   
 *  the second a NYet, the third URB does not complete and the
 *  fourth creates the error mentioned in issue 1) above  
 *
 *  3) The memory read/write calls in the U2F library need an offset
 *  parameter relative to the respective memory
*/
/** @defgroup altro_driver RCU driver code  */
#include <linux/config.h>
#include <linux/kernel.h>
#include <linux/version.h>
#include <linux/errno.h>
#include <linux/init.h>
#include <linux/slab.h>
#include <linux/module.h>
#include <linux/smp_lock.h>
#include <linux/completion.h>
#include <asm/uaccess.h>
#include <linux/usb.h>
#include <linux/proc_fs.h>
#include "altro_common.h"

/********************************************************************/
/** @name Debug 
 *  @ingroup altro_driver 
 */
#ifdef CONFIG_USB_DEBUG
/** @var debug 
 *  @brief Debug flag 
 */
static int debug = 1;
#else
static int debug;
#endif

#undef kdebug
/** @def kdebug 
 *  @brief macro to print debug information to system log */
#define kdebug(x) \
  do { if (debug) { printk("ALTRO [%s]: ", __FUNCTION__); printk x; } } while (0)

/********************************************************************/
/*  @name Version Information 
 *  @ingroup altro_driver 
 */
/** @def DRIVER_VERSION
 *  @ingroup altro_driver 
 *  @brief v1.0
 */
#define DRIVER_VERSION "v1.0"
/** @def DRIVER_AUTHOR
 *  @ingroup altro_driver 
 *  @brief Markus Joos, CERN/PH
 */
#define DRIVER_AUTHOR  "Markus Joos, CERN/PH"
/** @def DRIVER_DESC
 *  @ingroup altro_driver 
 *  @brief USB driver for the ALICE ALTRO card
 */
#define DRIVER_DESC    "USB driver for the ALICE ALTRO card"
/** @def DRIVER_LICENSE
 *  @ingroup altro_driver 
 *  @brief Private: Contact markus.joos@cern.ch
 */
#define DRIVER_LICENSE "Private: Contact markus.joos@cern.ch"

/********************************************************************/
/*  @name Module parameters
 *  @ingroup altro_driver 
 */
MODULE_PARM(debug, "i");
MODULE_PARM_DESC(debug, "Debug enabled or not");

/********************************************************************/
/*  @name Device information
 *  @ingroup altro_driver 
 */
/** @def USB_ALTRO_VENDOR_ID
 *  @ingroup altro_driver 
 *  @brief The vendor identifier 
 */
#define USB_ALTRO_VENDOR_ID	 0x1556
/** @def USB_ALTRO_PRODUCT_ID
 *  @ingroup altro_driver 
 *  @brief The product identifier 
 */
#define USB_ALTRO_PRODUCT_ID 	0x0350
/** @def USB_ALTRO_U2F_ID 
 *  @ingroup altro_driver
 *  @brief Product identifier for the U2F board 
 */
#define USB_ALTRO_U2F_ID          0x15e
/** @def USB_ALTRO_GOFFIE_ID 
 *  @ingroup altro_driver
 *  @brief Product identifier for the GOFFIE board 
 */
#define USB_ALTRO_GOFFIE_ID       0x15f
/** @def USB_ALTRO_MPT_ID 
 *  @ingroup altro_driver
 *  @brief Product identifier for the MPT board 
 */
#define USB_ALTRO_MPT_ID          0x163


/** @var altro_table
 *  @ingroup altro_driver 
 *  @brief  table of devices that work with this driver 
 */
static struct usb_device_id altro_table [] = {
  { USB_DEVICE(USB_ALTRO_VENDOR_ID, USB_ALTRO_PRODUCT_ID),
    driver_info: (unsigned long)"altro_by_vendor (Old id)" },
  { USB_DEVICE(USB_ALTRO_VENDOR_ID, USB_ALTRO_U2F_ID), 
    driver_info: (unsigned long)"ALICE U2F" },
  { USB_DEVICE(USB_ALTRO_VENDOR_ID, USB_ALTRO_GOFFIE_ID), 
    driver_info: (unsigned long)"ALICE GOFFIE" },
  { USB_DEVICE(USB_ALTRO_VENDOR_ID, USB_ALTRO_MPT_ID), 
    driver_info: (unsigned long)"ALICE MPT" },
  { } /* Terminating entry */
};

MODULE_DEVICE_TABLE (usb, altro_table);


/********************************************************************/
/*  @name Driver information 
 *  @ingroup altro_driver 
 */ 
#ifndef USB_ALTRO_MINOR_BASE
/** @def  USB_ALTRO_MINOR_BASE
 *  @ingroup altro_driver 
 *  @brief Get a minor range for your devices from the usb maintainer
 */ 
# define USB_ALTRO_MINOR_BASE	192
#endif

//____________________________________________________________________
/** @struct altro_device 
 *  @brief Structure to hold all of our device specific stuff 
 *  @ingroup altro_driver 
 */
struct altro_device {
  /** save off the usb device pointer */ 
  struct usb_device*	udev;			
  /** the interface for this device */ 
  struct usb_interface*	interface;		
  /** the starting minor number for this device */ 
  unsigned char minor;			
  /** the number of ports this device has */ 
  unsigned char	num_ports;		
  /** number of interrupt in endpoints we have */ 
  char num_interrupt_in;	
  /** number of bulk in endpoints we have */
  char num_bulk_in;		
  /** number of bulk out endpoints we have */ 
  char	num_bulk_out;		
  
  /** the buffer to receive data */ 
  unsigned char* bulk_in_buffer;		
  /** the size of the receive buffer */ 
  size_t bulk_in_size;		
  /** the urb used to recieve data */ 
  struct urb* read_urb;		
  /** the address of the bulk in endpoint */
  __u8	bulk_in_endpointAddr;	

  /** the buffer to send data */ 
  unsigned char* bulk_out_buffer;	
  /** the size of the send buffer */ 
  size_t bulk_out_size;		
  /** the urb used to send data */ 
  struct urb* write_urb;		
  /** the address of the bulk out endpoint */ 
  __u8	bulk_out_endpointAddr;	
  /** true iff write urb is busy */ 
  atomic_t write_busy;		
  /** wait for the write to finish */ 
  struct completion write_finished;		

  /** if the port is open or not */ 
  int open;			
  /** if the device is not disconnected */ 
  int present;		
  /** locks this structure */ 
  struct semaphore sem;			

  /** Test data */ 
  int test;
  /** Name */
  char* name;
};

static u_int    n_our_devices = 0;
static struct altro_device* our_devices[10] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

static void add_our_device(struct altro_device* dev) 
{
  if (n_our_devices >= 10) {
    kdebug(("Cannot add the 11th device to internal table"));
    return;
  }
  our_devices[n_our_devices] = dev;
  kdebug(("Added device %s at %d", dev->name, n_our_devices));
  n_our_devices++;
}

static void remove_our_device(struct altro_device* dev) 
{
  u_int i = 0;
  for (i = 0; i < n_our_devices; i++) {
    if (dev == our_devices[i]) {
      our_devices[i] = 0;
      n_our_devices--;
      kdebug(("Removed device %s at %d", dev->name, i));
      break;
    }
  }
  for (; i < n_our_devices + 1; i++) 
    our_devices[i] = our_devices[i+1];
  if (i >=  n_our_devices + 1)
    our_devices[i] = 0;
}
  

/** @brief Semaphore to prevent races between open() and disconnect() 
 *  @ingroup altro_driver 
 */
static DECLARE_MUTEX (disconnect_sem);

/* static struct altro_device*     _altro = NULL; */

/********************************************************************/
/*  @name PROC node
 *  @ingroup altro_driver 
 */ 
/** @struct altro_proc_data_t
 *  @ingroup altro_driver 
 *  @brief Data structure for the @c /proc file system node 
 */
struct altro_proc_data_t
{
  /** Name */
  char name[10];
  /** Values */
  char value[100];
};
/** @var altro_proc_file
 *  @ingroup altro_driver 
 *  @brief @c /proc file system node 
 */
static struct proc_dir_entry* altro_proc_file;
/** @var altro_proc_data
 *  @ingroup altro_driver 
 *  @brief Data of the @c /proc file system node 
 */
static struct altro_proc_data_t altro_proc_data;

/********************************************************************/
/* local function prototypes  - the functions are documented at thier
 * definition 
 */
static int altro_ioctl(struct inode *inode, struct file *file, 
			   unsigned int cmd, unsigned long arg);
static int altro_open(struct inode *inode, struct file *file);
static int altro_release(struct inode *inode, struct file *file);
static int altro_probe(struct usb_interface *interface, 
		     const struct usb_device_id *id);
static void altro_disconnect(struct usb_interface *interface);
static ssize_t altro_read(struct file *file, char *buffer, 
			size_t count, loff_t *ppos);
static ssize_t altro_write(struct file *file, const char *buffer, 
			 size_t count, loff_t *ppos);
static void altro_write_bulk_callback(struct urb *urb, 
				    struct pt_regs *regs);

static int  altro_proc_write(struct file *file, const char *buffer, 
			   unsigned long count, void *data);
static int  altro_proc_read(char *page, char **start, off_t off, 
			  int count, int *eof, void *data);


/********************************************************************/
/*  @name Driver structures used by USB layer in kernel
 *  @ingroup altro_driver 
 */ 
/** @var altro_fops
 *  @brief Call-backs when doing I/O on this device 
 *  @ingroup altro_driver 
 *
 *  File operations needed when we register this driver.  This assumes
 *  that this driver @e needs file operations, of course, which means
 *  that the driver is expected to have a node in the @c /dev
 *  directory. If the USB device were for a network interface then the
 *  driver would use @c struct @c net_driver instead, and a serial
 *  device would use @c struct @c tty_driver.
 */
static struct file_operations altro_fops = {
  /*
   * The owner field is part of the module-locking mechanism. The idea
   * is that the kernel knows which module to increment the
   * use-counter of BEFORE it calls the device's open() function.
   * This also means that the kernel can decrement the use-counter
   * again before calling release() or should the open() function
   * fail.
   */
  .owner =	THIS_MODULE, 
  .ioctl =	altro_ioctl,
  .open =	altro_open,
  .release =	altro_release,
  .read =	altro_read,
  .write =	altro_write
};

/** @var altro_class 
 *  @brief The type of the USB device 
 *  @ingroup altro_driver 
 * 
 *  USB class driver info in order to get a minor number from the USB
 *  core, and to have the device registered with devfs and the driver
 *  core
 */
static struct usb_class_driver altro_class = {
  .name = "altro%d",
  .fops = &altro_fops,
  /* .mode = S_IFCHR | S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH
     | S_IWOTH,  */
  .minor_base =	USB_ALTRO_MINOR_BASE,
};

/** @var altro_driver 
 *  @brief Driver call backs 
 *  @ingroup altro_driver 
 * 
 *  USB specific object needed to register this driver with the USB
 *  subsystem
 */ 
static struct usb_driver altro_driver = {
  /* .owner =	THIS_MODULE, */
  .name =	"altro",
  .probe =	altro_probe,
  .disconnect =	altro_disconnect,
  .id_table =	altro_table,
};

/********************************************************************/
/*  @name Functions
 *  @ingroup altro_driver 
 */ 
/*__________________________________________________________________*/
/** Delete the ALTRO driver structures 
 *  @param dev The device to free memory for 
 *  @ingroup altro_driver 
 */
static inline void 
altro_delete(struct altro_device *dev)
{
  kfree (dev->bulk_in_buffer);
  usb_buffer_free(dev->udev, dev->bulk_out_size,
		  dev->bulk_out_buffer,
		  dev->write_urb->transfer_dma);
  usb_free_urb(dev->write_urb);
  kfree(dev);
}

/*__________________________________________________________________*/
/** Call-back for @c open system call 
 *  @param inode The node opened by the user 
 *  @param file  The file pointer to open
 *  @return 0 on success, error code otherwise
 *  @ingroup altro_driver 
 */
static int 
altro_open(struct inode *inode, struct file *file)
{
  struct altro_device*    dev = NULL;
  struct usb_interface* interface;
  int                   subminor;
  int                   retval = 0;
  kdebug(("called"));

  // the below is if we have more than one USB device.
  subminor = iminor(inode);
  kdebug(("minor number = %d\n", subminor));

  /* prevent disconnects */
  down(&disconnect_sem);

  interface = usb_find_interface (&altro_driver, subminor);
  if (!interface) {
    err ("%s - error, can't find device for minor %d", __FUNCTION__, subminor);
    retval = -ENODEV;
    goto exit_no_device;
  }

  dev = usb_get_intfdata(interface);
  if (!dev) {
    retval = -ENODEV;
    goto exit_no_device;
  }

  /* lock this device */
  down(&dev->sem);
  
  /* increment our usage count for the driver */
  ++dev->open;

  /* save our object in the file's private structure */
  file->private_data = dev;

  /* unlock this device */
  up (&dev->sem);

 exit_no_device:
  up (&disconnect_sem);
  return retval;
}

/*__________________________________________________________________*/
/** Call-back for @c close system call 
 *  @param inode the node to close
 *  @param file  The file descriptor to close 
 *  @return 0 on success, error code otherwise
 *  @ingroup altro_driver 
 */
static int 
altro_release(struct inode *inode, struct file *file)
{
  int retval = 0;
  // The below is if we have more than one device 
  struct altro_device* dev;

  dev = (struct altro_device *)file->private_data;
  if (dev == NULL) {
    dbg ("%s - object is NULL", __FUNCTION__);
    return -ENODEV;
  }
  kdebug(("minor %d\n", dev->minor));

  /* lock our device */
  down (&dev->sem);
  if (dev->open <= 0) {
    kdebug(("device not opened"));
    retval = -ENODEV;
    goto exit_not_opened;
  }

  /* wait for any bulk writes that might be going on to finish up */
  if (atomic_read (&dev->write_busy))
    wait_for_completion (&dev->write_finished);

  --dev->open;

  if (!dev->present && !dev->open) {
    /* the device was unplugged before the file was released */
    up (&dev->sem);
    altro_delete (dev);
    return 0;
  }

 exit_not_opened:
  up (&dev->sem);
  return retval;
}

/*__________________________________________________________________*/
/** Print out debug data. 
 *  @param function The function that called this 
 *  @param size the size of @a data 
 *  @param data The data to write out
 *  @ingroup altro_driver 
 */
static inline void 
altro_debug_data(const char *function, int size, const unsigned char *data)
{
  int i;
  if (!debug) return;
  printk(KERN_DEBUG __FILE__": %s - length = %d, data = ", function, size);
  for (i = 0; i < size; ++i) printk ("%.2x ", data[i]);
  printk ("\n");
}

/*__________________________________________________________________*/
/** Call-back of @c read system call 
 *
 *   @param file File the user is reading from 
 *   @param buffer On return, the data read
 *   @param count On input, the size of @a buffer in bytes. 
 *   @param ppos Not used. 
 *   @return The actual number of bytes read, or negative error
 *   code on failure
 *  @ingroup altro_driver 
 */
static ssize_t 
altro_read(struct file *file, char *buffer, size_t count, loff_t *ppos)
{
  struct altro_device *dev;
  int retval = 0;

  dev = (struct altro_device *)file->private_data;
  kdebug(("minor %d, count = %d", dev->minor, count));

  /* lock this object */
  down (&dev->sem);

  /* verify that the device wasn't unplugged */
  if (!dev->present) {
    kdebug(("Error: Device was unplugged!\n"));
    up (&dev->sem);
    return -ENODEV;
  }

  kdebug(("function called for end-point 0x%08x\n"
	  "  bulk_in_size: %d", dev->bulk_in_endpointAddr, dev->bulk_in_size));
  
  /* do a blocking bulk read to get data from the device */ 
  retval = usb_bulk_msg(dev->udev,
			usb_rcvbulkpipe (dev->udev, 
					 dev->bulk_in_endpointAddr),
			dev->bulk_in_buffer,
			min (dev->bulk_in_size, count),
			&count, HZ*10);

  /* if the read was successful, copy the data to userspace */
  if (!retval) {
    if (copy_to_user (buffer, dev->bulk_in_buffer, count)) {
      kdebug(("error from copy_to_user"));
      retval = -EFAULT;
    }
    else
      retval = count;
  }
  else 
    kdebug(("No data recivied"));
  
  /* unlock the device */
  up (&dev->sem);
  return retval;
}


/*__________________________________________________________________*/
/** Call-back of @c write system call 
 * 
 *  A device driver has to decide how to report I/O errors back to the
 *  user.  The safest course is to wait for the transfer to finish
 *  before returning so that any errors will be reported reliably.
 *  altro_read() works like this.  But waiting for I/O is slow, so many
 *  drivers only check for errors during I/O initiation and do not
 *  report problems that occur during the actual transfer.  That's
 *  what we will do here.
 *
 *  A driver concerned with maximum I/O throughput would use double-
 *  buffering: Two urbs would be devoted to write transfers, so that
 *  one URB could always be active while the other was waiting for the
 *  user to send more data.
 *
 *  @param file File the user is writing to 
 *  @param buffer The data the user is writing 
 *  @param count The size of @a buffer in bytes 
 *  @param ppos Not used. 
 *  @return The actual number of bytes written, or negative error code
 *  on failure 
 *  @ingroup altro_driver 
 */
static ssize_t 
altro_write(struct file *file, const char *buffer, size_t count, loff_t *ppos) 
{
  struct altro_device* dev;
  ssize_t            bytes_written = 0;
  int                retval        = 0;

  dev = (struct altro_device *)file->private_data;
  kdebug(("minor %d, count = %d\n", dev->minor, count)); 

  /* lock this object */ 
  down (&dev->sem); 

  /* verify that the device wasn't unplugged */ 
  if (!dev->present) { 
    kdebug(("Error: Device was unplugged!\n")); 
    retval = -ENODEV; 
    goto exit; 
  }

  kdebug(("function called for end-point 0x%08x\n"
	  "  bulk_in_size: %d", dev->bulk_in_endpointAddr, dev->bulk_in_size));

  /* verify that we actually have some data to write */ 
  if (count == 0) { 
    kdebug(("write request of 0 bytes\n")); 
    goto exit; 
  } 

  /* wait for a previous write to finish up; we don't use a timeout
   * and so a nonresponsive device can delay us indefinitely.
   */
  if (atomic_read (&dev->write_busy))
    wait_for_completion (&dev->write_finished);

  /* we can only write as much as our buffer will hold */
  bytes_written = min (dev->bulk_out_size, count);

  /* copy the data from userspace into our transfer buffer;
   * this is the only copy required.
   */
  if (copy_from_user(dev->write_urb->transfer_buffer, buffer, bytes_written)) {
    kdebug(("error from copy_from_user"));
    retval = -EFAULT;
    goto exit;
  }

  altro_debug_data (__FUNCTION__, bytes_written, 
		  dev->write_urb->transfer_buffer);

  /* this urb was already set up, except for this write size */
  dev->write_urb->transfer_buffer_length = bytes_written;

  /* send the data out the bulk port */
  /* a character device write uses GFP_KERNEL, unless a spinlock is held */
  init_completion (&dev->write_finished);
  atomic_set (&dev->write_busy, 1);
  retval = usb_submit_urb(dev->write_urb, GFP_KERNEL);
  if (retval) {
    atomic_set (&dev->write_busy, 0);
    kdebug(("No data sent"));
    err("%s - failed submitting write urb, error %d", __FUNCTION__, retval); 
  } else {
    kdebug(("%d bytes sent", bytes_written));
    retval = bytes_written; 
  }

 exit:
  /* unlock the device */
  up (&dev->sem);

  return retval;
}

/*__________________________________________________________________*/

// Turn Jiffies into milliseconds.  Since 2.6.12, the time-out
// parameter of usb_bulk_msg should be in seconds, not jiffies. 
#if HZ == 1000
# define JIFFIES_TO_MSECS(x) (x)
# define MSECS_TO_JIFFIES(x) (x)
#elif HZ == 100
# define JIFFIES_TO_MSECS(x) ((x) * 10)
# define MSECS_TO_JIFFIES(x) (((x) + 9) / 10)
#else
# define JIFFIES_TO_MSECS(x) (((x) * 1000) / HZ)
# define MSECS_TO_JIFFIES(x) (((x) * HZ + 999) / 1000)
#endif
#if  LINUX_VERSION_CODE > KERNEL_VERSION(2,6,11) 
# if HZ == 1000
#  define TIMEOUT(x) (x)
# elif HZ == 100
#  define TIMEOUT(x) ((x) * 10)
# else 
#  define TIMEOUT(x) (((x)  * 1000) / HZ)
# endif
#else 
# define  TIMEOUT(x) (x)
#endif

/** Call-back for the @c ioctl system call.  This driver understands
 *  three commands 
 *  <dl>
 *    <dt> @c SEND_DATA </dt>
 *    <dd> Send data to the ALTRO device.  In this case, the input
 *      parameter @a arg is taken to be a pointer to an @c
 *      altro_bulk_out_t variable allocated by the user. </dd>
 *    <dt> @c GET_DATA </dt>
 *    <dd> Get data from the ALTRO device.  In this case, the input
 *      parameter @a arg is taken to be a pointer to an @c
 *      altro_bulk_in_t variable allocated by the user. </dd>
 *    <dt> @c CONTROL </dt>
 *    <dd> Control message to the ALTRO device.  In this case, the input
 *      parameter @a arg is taken to be a pointer to an @c
 *      altro_control_t variable allocated by the user. </dd>
 *  </dl>
 *  @param inode Node doing the I/O control on 
 *  @param file File doing the I/O control on 
 *  @param cmd The command word 
 *  @param arg Arguments to the command 
 *  @return 0 on success, error code otherwise 
 *  @ingroup altro_driver 
 */
static int 
altro_ioctl(struct inode *inode, struct file *file, 
	  unsigned int cmd, unsigned long arg)
{
  struct altro_device *dev;
  int retval = 0, pipe;
  kdebug(("function called %s\n", 
	  (cmd == GET_DATA ? "GET_DATA" :
	   (cmd == SEND_DATA ? "SEND_DATA" : 
	    (cmd == CONTROL ? "CONTROL" : "unknown")))));
  
  // Get the device 
  dev = (struct altro_device*)file->private_data;
  kdebug(("test is %d\n", dev->test));
  
  /* lock this object */
  down (&dev->sem);

  /* verify that the device wasn't unplugged */
  if (!dev->present) {
    kdebug(("Error: Device was unplugged!\n"));
    retval = -ENODEV;
    goto ioctl_return;
  }
  kdebug(("minor %d, cmd 0x%.4x, arg %ld\n", dev->minor, cmd, arg));

  /* fill in your device specific stuff here */
  switch (cmd) {
  case GET_DATA: {
    altro_bulk_in_t params;
    kdebug(("GET_DATA called for endpoint 0x%08x\n", 
	    dev->bulk_in_endpointAddr));
    kdebug(("in bulk size is %d\n", dev->bulk_in_size));

#if 0
    /* see if we alread in the middle of a write */
    if (dev->read_urb->status == -EINPROGRESS) {
      kdebug(("The bulk in URB is current in use\n"));
      up(&dev->sem);
      return (-ALTRO_EFAULT);
    }
#endif

    /* Set up our URB */
    pipe = usb_rcvbulkpipe(dev->udev, dev->bulk_in_endpointAddr);
    kdebug(("writing on pipe %d\n", pipe));
    
    /* do an immediate bulk read to get data from the device */
    retval = usb_bulk_msg(dev->udev, pipe, &(params.data[0]), 
			  dev->bulk_in_size, &(params.nbytes), 
			  TIMEOUT(HZ * 10));
    kdebug(("USB bulk message returns %d\n",retval));
    if (retval) {
      kdebug(("no data recieved!\n"));
      params.nbytes = 0;
    }
    else 
      kdebug(("%d bytes recieved\n", params.nbytes));
    
    /** Copy data to user */
    if (copy_to_user((void*)arg, &params, sizeof(altro_bulk_in_t)) != 0){
      kdebug(("error when copying to user\n"));
      retval = -ALTRO_EFAULT;
    }
    break;
  }
  case SEND_DATA: {
    altro_bulk_out_t params;
    int maxp, ndone;
    kdebug(("SEND_DATA called for endpoint 0x%08x\n", 
	    dev->bulk_out_endpointAddr));
    kdebug(("in bulk size is %d\n", dev->bulk_out_size));

    /** Copy data from user */
    if (copy_from_user(&params, (void*)arg, sizeof(altro_bulk_out_t))!= 0){
      kdebug(("error when copying from user\n"));
      retval = -ALTRO_EFAULT;
      goto ioctl_return;
    }

    /* Set up our URB */
    pipe = usb_sndbulkpipe(dev->udev, dev->bulk_out_endpointAddr); 
    kdebug(("reading from pipe %d\n", pipe));
    maxp = usb_maxpacket(dev->udev, pipe, usb_pipeout(pipe));
    if (maxp > dev->bulk_out_size) maxp = dev->bulk_out_size;
    kdebug(("it is possible to send %d bytes\n", maxp));
    
    /* Check if we would write more data than possible */
    if (params.nbytes > maxp) {
      kdebug(("cannot write %d bytes to pipe of %d bytes\n", 
		params.nbytes, maxp));
      retval = -ALTRO_EFAULT;
      goto ioctl_return;
    }

    kdebug(("trying to sen %d bytes\n", params.nbytes));
    /* do an immediate bulk write to send data to the device */
    retval = usb_bulk_msg(dev->udev, pipe, &(params.data[0]), 
			  params.nbytes, &ndone, 
			  TIMEOUT(HZ * 10));
    kdebug(("USB bulk message returns %d\n",retval));
    if (retval) {
      kdebug(("no data sent!\n"));
      params.nbytes = 0;
    }
    else 
      kdebug(("%d bytes sent\n", params.nbytes));
  } // SEND_DATA
    break;
  case CONTROL: {
    altro_control_t params;
    kdebug(("CONTROL called"));
    
    /** Copy data from user */
    if (copy_from_user(&params, (void*)arg, sizeof(altro_control_t)) != 0){
      kdebug(("error when copying from user\n"));
      retval = -ALTRO_EFAULT;
      goto ioctl_return;
    }
    
    kdebug(("CONTROL: bRequest      = %d\n", params.bRequest));   
    kdebug(("CONTROL: bmRequestType = %d\n", params.bmRequestType));
    kdebug(("CONTROL: wValue        = %d\n", params.wValue));
    kdebug(("CONTROL: wIndex        = %d\n", params.wIndex));
    kdebug(("CONTROL: data[0]       = %d\n", params.data[0]));
    kdebug(("CONTROL: wLength       = %d\n", params.wLength));
    
    retval = usb_control_msg(dev->udev, 
			     usb_rcvctrlpipe(dev->udev, 0),
			     params.bRequest, 
			     params.bmRequestType,
			     params.wValue,
			     params.wIndex,
			     &(params.data[0]),  
			     params.wLength, TIMEOUT(HZ));	  
    if (retval) {
      kdebug(("error when sending control word: %d\n", retval));
      retval = -ALTRO_EFAULT;
    }
    break;
  }// case CONTROL:
  default:
    retval = -ENOTTY;
    break;
  } // switch (cmd)
  
 ioctl_return:  
  /* unlock the device */
  up (&dev->sem);

  /* return that we did not understand this ioctl call */
  return retval; /* (retval ? -ENOTTY : 0); */
}


/*__________________________________________________________________*/
/** Call back when writing and URB bulk message 
 *  @param urb The URB (USB Request Block) that was sent 
 *  @param regs 
 *  @ingroup altro_driver 
 */
static void
altro_write_bulk_callback (struct urb *urb, struct pt_regs *regs)
{
  struct altro_device *dev = (struct altro_device *)urb->context;

  kdebug(("minor %d\n", dev->minor));

  /* sync/async unlink faults aren't errors */
  if (urb->status && !(urb->status == -ENOENT ||
		       urb->status == -ECONNRESET)) {
    kdebug(("nonzero write bulk status received: %d", urb->status));
  }
  
  /* notify anyone waiting that the write has finished */
  atomic_set (&dev->write_busy, 0);
  complete (&dev->write_finished);
}

/*__________________________________________________________________*/
/** Called by the usb core when a new device is connected that it
 *  thinks this driver might be interested in. 
 * 
 *  @param interface The interface that the driver may want to handle 
 *  @param id USB device ID of the device recently plugged in.
 *  @return 0 if the driver want's to handle the device plugged in;
 *  otherwise an error code. 
 *  @ingroup altro_driver 
 */
static int 
altro_probe(struct usb_interface *interface, const struct usb_device_id *id)
{
  struct usb_device *udev                   = interface_to_usbdev(interface);
  struct altro_device *dev                    = NULL;
  struct usb_host_interface *iface_desc     = 0;
  struct usb_endpoint_descriptor *endpoint  = 0;
  size_t buffer_size;
  int i;
  int retval = -ENOMEM;

  kdebug(("checking if we want to handle this device %d:%d (%d:%d)",
	  udev->descriptor.idVendor, udev->descriptor.idProduct,
	  USB_ALTRO_VENDOR_ID, USB_ALTRO_PRODUCT_ID));
  /* See if the device offered us matches what we can accept */
  if ((udev->descriptor.idVendor != USB_ALTRO_VENDOR_ID)) 
    return -ENODEV;
  /* See if the device offered us matches what we can accept */
  switch (udev->descriptor.idProduct) {
  case USB_ALTRO_PRODUCT_ID: break; // Old card
  case USB_ALTRO_U2F_ID:     break;
  case USB_ALTRO_GOFFIE_ID:  break; 
  case USB_ALTRO_MPT_ID:     break;
  default: 
    return -ENODEV;
  }
  kdebug(("got a %s card (%X,%X)", 
	  (char*)(id->driver_info),
	  udev->descriptor.idVendor, udev->descriptor.idProduct));
  
  /* allocate memory for our device state and initialize it */
  dev = kmalloc (sizeof(struct altro_device), GFP_KERNEL);
  if (dev == NULL) {
    err ("Out of memory");
    return -ENOMEM;
  }
  memset (dev, 0x00, sizeof (*dev));

  dev->name = (char*)id->driver_info;
  init_MUTEX (&dev->sem);
  dev->udev      = udev;
  dev->interface = interface;

  /* set up the endpoint information */
  iface_desc = &interface->altsetting[0];
  if (iface_desc->desc.bNumEndpoints != 2) {
    kdebug(("Error: number of endpoints is %d (should be 2)", 
	    iface_desc->desc.bNumEndpoints));
    goto error;
  }
  
  /* check out the endpoints */
  /* use only the first bulk-in and bulk-out endpoints */
  for (i = 0; i < iface_desc->desc.bNumEndpoints; ++i) {
    endpoint = &iface_desc->endpoint[i].desc;

    if (/* !dev->bulk_in_endpointAddr && */
	(endpoint->bEndpointAddress & USB_DIR_IN) &&
	((endpoint->bmAttributes & USB_ENDPOINT_XFERTYPE_MASK)
	 == USB_ENDPOINT_XFER_BULK)) {
      /* we found a bulk in endpoint */
      if (!dev->bulk_in_endpointAddr) {
	kdebug(("we found first bulk in endpoint\n"));
	buffer_size               = endpoint->wMaxPacketSize;
	dev->bulk_in_size         = buffer_size;
	dev->bulk_in_endpointAddr = endpoint->bEndpointAddress;
	dev->bulk_in_buffer       = kmalloc(buffer_size, GFP_KERNEL);
	kdebug(("bulk in endpoint address: %d\n", dev->bulk_in_endpointAddr));
	if (!dev->bulk_in_buffer) {
	  err("Couldn't allocate bulk_in_buffer");
	  goto error;
	}
      }
      else 
	kdebug(("we found yet another bulk in endpoint!\n"));
    }
    else if (/* !dev->bulk_out_endpointAddr && */
	     !(endpoint->bEndpointAddress & USB_DIR_IN) &&
	     ((endpoint->bmAttributes & USB_ENDPOINT_XFERTYPE_MASK)
	      == USB_ENDPOINT_XFER_BULK)) {
      /* we found a bulk out endpoint */
      if (!dev->bulk_out_endpointAddr) {
	/* a probe() may sleep and has no restrictions on memory
	   allocations */ 
	dev->write_urb             = usb_alloc_urb(0, GFP_KERNEL);
	if (!dev->write_urb) {
	  err("No free urbs available");
	  goto error;
	}
	dev->bulk_out_endpointAddr     = endpoint->bEndpointAddress;
	buffer_size                    = endpoint->wMaxPacketSize;
	dev->bulk_out_size             = buffer_size;
	dev->write_urb->transfer_flags = (URB_NO_TRANSFER_DMA_MAP 
					  /* | URB_ASYNC_UNLINK*/);
	dev->bulk_out_buffer           = 
	  usb_buffer_alloc(udev, buffer_size, GFP_KERNEL,
			   &dev->write_urb->transfer_dma);
	if (!dev->bulk_out_buffer) {
	  err("Couldn't allocate bulk_out_buffer");
	  goto error;
	}
	usb_fill_bulk_urb(dev->write_urb, udev,
			  usb_sndbulkpipe(udev, endpoint->bEndpointAddress),
			  dev->bulk_out_buffer, buffer_size,
			  altro_write_bulk_callback, dev);
      }
      else 
	kdebug(("we found yet another bulk out endpoint!\n"));
    }
    else 
      kdebug(("we found an unidentifiable endpoint!\n"));
  }
  if (!(dev->bulk_in_endpointAddr && dev->bulk_out_endpointAddr)) {
    err("Couldn't find both bulk-in and bulk-out endpoints");
    goto error;
  }

  /* allow device read, write and ioctl */
  dev->present = 1;

  /* we can register the device now, as it is ready */
  usb_set_intfdata(interface, dev);
  retval = usb_register_dev(interface, &altro_class);
  if (retval) {
    /* something prevented us from registering this driver */
    err ("Not able to get a minor for this device.");
    usb_set_intfdata (interface, NULL);
    goto error;
  }

  dev->minor = interface->minor;
  /* _altro = 0; */
  add_our_device(dev);
  /* let the user know what node this device is now attached to */
  info ("USB ALICE ALTRO device now attached to USB ALICE ALTRO-%d", 
	dev->minor);
  return 0;

 error:
  altro_delete (dev);
  return retval;
}


/*__________________________________________________________________*/
/** Called by the Usb core when the device is removed from the
 *  system (the plug is pulled out, or the reset switch is pushed on
 *  the board, or the power is cut to the board). 
 *
 *  This routine guarantees that the driver will not submit any more
 *  URBS by clearing dev->udev.  It is also supposed to terminate any
 *  currently active urbs.  Unfortunately, @c usb_bulk_msg(), used in
 *  @c altro_read(), does not provide any way to do this.  But at least
 *  we can cancel an active write.
 *
 *  @param interface The interface that was disconnected. 
 *  @ingroup altro_driver 
 */
static void 
altro_disconnect(struct usb_interface *interface)
{
  struct altro_device *dev;
  int minor;

  /* prevent races with open() */
  down(&disconnect_sem);

  dev = usb_get_intfdata(interface);
  usb_set_intfdata(interface, NULL);

  down(&dev->sem);
  remove_our_device(dev);
  
  minor = dev->minor;

  /* give back our minor */
  usb_deregister_dev(interface, &altro_class);

  /* terminate an ongoing write */
  if (atomic_read(&dev->write_busy)) {
    usb_unlink_urb(dev->write_urb);
    wait_for_completion(&dev->write_finished);
  }

  /* prevent device read, write and ioctl */
  dev->present = 0;

  up(&dev->sem);

  /* if the device is opened, altro_release will clean this up */
  if (!dev->open)
    altro_delete(dev);

  up (&disconnect_sem);

  info("USB ALICE ALTRO #%d now disconnected", minor);
}

/*********************************************************************/
/** User writing to the ALTRO proc entry 
 *  @param file The file descriptor
 *  @param buffer Buffer of @a count size 
 *  @param count The size of buffer (max 99)
 *  @param data The data 
 *  @ingroup altro_driver 
 *  @return  
 */
static int 
altro_proc_write(struct file *file, const char *buffer, 
	       unsigned long count, void *data)
{
  int len;
  struct altro_proc_data_t *fb_data = (struct altro_proc_data_t *)data;
  
  kdebug(("user is writing to /proc/altro\n"));

  if(count > 99) len = 99;
  else           len = count;

  if (copy_from_user(fb_data->value, buffer, len)) {
    kdebug(("error when copying from user\n"));
    return(-EFAULT);
  }

  kdebug(("length is %d\n", len));
  fb_data->value[len - 1] = '\0';
  kdebug(("text passed is \"%s\"\n", fb_data->value));
 
  if (!strcmp(fb_data->value, "debug")) {
    debug = 1;
    kdebug(("debugging enabled\n")); 
  }
 
  if (!strcmp(fb_data->value, "nodebug")) {
    kdebug(("debugging disabled\n")); 
    debug = 0; 
  }
  
  if (!strcmp(fb_data->value, "dec")) {
    module_put(THIS_MODULE);
    /* MOD_DEC_USE_COUNT; */
    kdebug(("Use count decremented\n"));
  }

  if (!strcmp(fb_data->value, "inc")) {
    try_module_get(THIS_MODULE);
    /* MOD_INC_USE_COUNT; */
    kdebug(("Use count incremented\n"));
  }
  return len;
}

/*__________________________________________________________________*/
/** User Reading from the ALTRO proc entry
 *  @param page   Page to read into 
 *  @param start  Points to begining of text on output
 *  @param off    Points to offset of the text in the page 
 *  @param count  Size of page 
 *  @param eof    Wheter to set end of file 
 *  @param data   The data 
 *  @ingroup altro_driver 
 *  @return The length 
 */
static int
altro_proc_read(char *page, char **start, off_t off, 
	      int count, int *eof, void *data)
{
  int len;
  u_int i;
  
  kdebug(("user is reading /proc/altro\n"));

  len =  sprintf(page, "ALICE USB-ALTRO driver\n\n");
  len += sprintf(page + len, "Minor device number:\t0x%x\n", 
		 USB_ALTRO_MINOR_BASE);
  len += sprintf(page + len, "Vendor/product ID:\t0x%x/0x%x\n\n", 
		 USB_ALTRO_VENDOR_ID, USB_ALTRO_PRODUCT_ID);
  for (i = 0; i < n_our_devices; i++) {
    len += sprintf(page + len, "\tDevice:\t %s (0x%x)\n", 
		   our_devices[i]->name, our_devices[i]->minor);
  }
  len += sprintf(page + len, " \n");
  len += sprintf(page + len, "The command 'echo <action> > /proc/altro', "
		 "executed as root,\n");
  len += sprintf(page + len, "allows you to interact with the driver. "
		 "Possible actions are:\n");
  len += sprintf(page + len, "debug   -> enable debugging\n");
  len += sprintf(page + len, "nodebug -> disable debugging\n");
  len += sprintf(page + len, "dec     -> Decrement use count\n");
  len += sprintf(page + len, "inc     -> Increment use count\n");

  *eof = 1;
  *start = page + off;
  len -= off;

  if (len > count) len = count;
  return (len);
}

/*__________________________________________________________________*/
/** Function called when the driver is loaded. 
 *  @return 0 on success or error code otherwise 
 *  @ingroup altro_driver 
 */
static int __init 
usb_altro_init(void)
{
  int result;
  // Install `/proc' entry 
  altro_proc_file = create_proc_entry("altro", 0644, NULL);
  if (altro_proc_file == NULL) {
    err("create_proc_entry failed");
    return -ENOMEM;
  }
  strcpy(altro_proc_data.name, "altro");
  strcpy(altro_proc_data.value, "altro");
  altro_proc_file->data       = &altro_proc_data;
  altro_proc_file->read_proc  = altro_proc_read;
  altro_proc_file->write_proc = altro_proc_write;
  altro_proc_file->owner      = THIS_MODULE;

  /* register this driver with the USB subsystem */
  result = usb_register(&altro_driver);
  if (result) {
    err("usb_register failed. Error number %d", result);
    return result;
  }
  
  info(DRIVER_DESC " " DRIVER_VERSION);
  info("ALTRO: debug is %d", debug);
  return 0;
}


/*__________________________________________________________________*/
/** Function called when the driver is unloaded via @c rmmod or
 *  otherwise.
 *  @ingroup altro_driver 
 */
static void __exit 
usb_altro_exit(void)
{
  /* deregister this driver with the PROC subsystem */
  remove_proc_entry("altro", NULL);
  
  /* deregister this driver with the USB subsystem */
  usb_deregister(&altro_driver);
}
  
/*__________________________________________________________________*/
module_init(usb_altro_init);
module_exit(usb_altro_exit);
// int init_module() 
// {
//   return usb_altro_init();
// }
// void cleanup_module() 
// {
//   usb_altro_exit();
// }


/*__________________________________________________________________*/
MODULE_AUTHOR(DRIVER_AUTHOR);
MODULE_DESCRIPTION(DRIVER_DESC);
MODULE_LICENSE(DRIVER_LICENSE);

/*__________________________________________________________________
 *
 * EOF
 */
