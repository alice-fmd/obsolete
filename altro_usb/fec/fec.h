/************************************************************************/
/*									*/
/*  This is the header file for the FEC library				*/
/*									*/
/*  09. Jun. 05  L. Musa & H. Santos  created				*/
/*                                                                      */
/*  Last update: 07. Oct. 05                                            */
/*									*/
/************************************************************************/

#include <sys/types.h>
#include <tools/get_input.h>
#include <tools/rcc_time_stamp.h>
#include <altro/altro.h>
#include <u2f/u2f.h>

/*************/
/* Constants */
/*************/

#define MAX_NR_OF_FECS 16

/*********/ 
/* Macros*/
/*********/ 
#define CHECK_RET(ecode)\
  if (ecode)\
  {\
    rcc_error_print(stdout, ecode);\
    return ecode;\
  }\

#define FECISOPEN(handle) {if(!fec_is_open[handle]) return(FEC_NOTOPEN);} 


/*************/
/*error codes*/
/*************/
enum
{
  FEC_SUCCESS = 0,  
  FEC_U2F_FAIL = (P_ID_FEC << 8) + 1,
  FEC_NOTOPEN,
  FEC_NO_CODE,
  FEC_ERROR_FAIL
};


/***************/
/*error strings*/
/***************/
#define FEC_SUCCESS_STR      "Function successfully executed"
#define FEC_U2F_FAIL_STR     "Error from U2F library"
#define FEC_NOTOPEN_STR      "The library has not been opened yet"
#define FEC_NO_CODE_STR      "Unknown error"

/***********************************/ 
/*Official functions of the library*/
/***********************************/ 
u_int FEC_Open(char *node, int *handle);
u_int FEC_Close(int handle);
u_int FEC_Write_ALTRO_Reg(int handle, u_int ch_addr, u_int reg_addr, u_int reg_data);
u_int FEC_Read_ALTRO_Reg(int handle, u_int ch_addr, u_int reg_addr, u_int *reg_data);
u_int FEC_Write_ALTRO_PMEM(int handle, u_int bcast, u_int ch_addr, u_int nwords, char *fname);
u_int FEC_Read_ALTRO_PMEM(int handle, u_int ch_addr, u_int nwords, char fname[]); 
u_int FEC_Power_Reset(int handle, u_int fecs_addr);
u_int FEC_All_Reset(int handle);
u_int FEC_All_Status(int handle, int *fec_status);
u_int FEC_BC_Reg_Write(int handle, u_int fec_addr, u_int reg_addr, u_int data);
u_int FEC_BC_Reg_Read(int handle, u_int fec_addr, u_int reg_addr, u_int *reg_data);
u_int FEC_BC_Mon(int handle, u_int fec_addr, float *temp, float *ana_v,  float *ana_i, float *dig_v, float *dig_i);
u_int FEC_FM_Write(int handle, u_int fec_addr, u_int nwords, char fname[]);
u_int FEC_FM_Read(int handle, u_int fec_addr, u_int nwords, char fname[]);
u_int FEC_Event_Readout(int handle, u_int fec_addr, u_int trg_mode, u_int rdo_mode, u_int nevents, u_int nsamples, u_int n_before_trg, char fname[]); 
/********************/ 
/*Internal functions*/
/********************/ 
u_int FEC_err_get(err_pack err, err_str pid, err_str code);
