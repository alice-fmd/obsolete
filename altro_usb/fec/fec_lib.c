/************************************************************************/
/*									*/
/*  This is the FEC library 	   				        */
/*									*/
/*  25. Jul. 05  L. Musa         					*/
/*									*/
/************************************************************************/

#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/file.h>
#include <fcntl.h>
#include <math.h>
#include "fec.h"

/***********/
/* Globals */
/***********/
static u_int fec_is_open[MAX_U2F_DEVICES] = {0};


/*************************************/
u_int FEC_Open(char *node, int *handle)
/*************************************/
{  
  u_int ret;
 
  DEBUG_TEXT(P_ID_FEC, 15 ,("FEC_Open: called\n"));
  /* open the error package */
  ret = rcc_error_init((err_pid)P_ID_FEC, FEC_err_get);
  if (ret) 
  {
    DEBUG_TEXT(P_ID_FEC, 5 ,("FEC_Open: Failed to open error package\n"));
    return(RCC_ERROR_RETURN(0, FEC_ERROR_FAIL)); 
  }
  DEBUG_TEXT(P_ID_FEC, 20 ,("FEC_Open: error package opened\n"));
  
  ret = U2F_Open(node, handle, HW);
  if (ret) 
  {
    DEBUG_TEXT(P_ID_FEC, 5 ,("FEC_Open: Failed to open the U2F library\n"));
    return(RCC_ERROR_RETURN(ret, FEC_U2F_FAIL)); 
  }
  DEBUG_TEXT(P_ID_FEC, 10 ,("FEC_Open: U2F library opened\n"));
  DEBUG_TEXT(P_ID_FEC, 10 ,("FEC_Open: handle = %d\n", *handle));

  if (fec_is_open[*handle]) 
  {
    fec_is_open[*handle]++;             /* keep track of multiple open calls */
    return(RCC_ERROR_RETURN(0, FEC_SUCCESS));
  }

  /* Open the rcc_time_stamp library */
  ret = ts_open(1, TS_DUMMY);
  CHECK_RET(ret)
  
  fec_is_open[*handle] = 1;
  DEBUG_TEXT(P_ID_FEC, 10 ,("FEC_Open: FEC library opened for handle %d\n", *handle));
  DEBUG_TEXT(P_ID_FEC, 15 ,("FEC_Open: done\n"));
  return(FEC_SUCCESS);
}


/*************************/
u_int FEC_Close(int handle)
/*************************/
{
  u_int ret = 0;
  
  DEBUG_TEXT(P_ID_FEC, 15 ,("FEC_Close: called\n"));
  FECISOPEN(handle);
    
  if (fec_is_open[handle] > 1) 
    fec_is_open[handle]--;
  else 
  {  
    ret = U2F_Close(handle);
    if (ret) 
    {
      DEBUG_TEXT(P_ID_FEC, 5,("FEC_Close: Failed to close the U2F library for handle %d\n", handle));
      return(RCC_ERROR_RETURN(ret, FEC_U2F_FAIL)); 
    }
    DEBUG_TEXT(P_ID_FEC, 10 ,("FEC_Close: U2F library closed for handle %d\n", handle));
    ret = ts_close(TS_DUMMY);
    CHECK_RET(ret)
    
    fec_is_open[handle] = 0;
  }
  
  DEBUG_TEXT(P_ID_FEC, 15 ,("FEC_Close: done\n"));
  return(FEC_SUCCESS);
}

/**********************************************************************************/
u_int FEC_Write_ALTRO_Reg(int handle, u_int ch_addr, u_int reg_addr, u_int reg_data) 
/**********************************************************************************/
{
  u_int ret, memdata[256];

  DEBUG_TEXT(P_ID_FEC, 15 ,("FEC_Write_ALTRO_Reg: called\n"));
  FECISOPEN(handle);

  /* Preparing the Instruction Memory data */
  DEBUG_TEXT(P_ID_FEC, 20 ,("FEC_Write_ALTRO_Reg: Preparing the Instruction Memory data\n"));
  memdata[0] = 0x00600000 + (ch_addr << 5) + reg_addr;	
  memdata[1] = 0x00700000 + reg_data;
  memdata[2] = 0x00390000; 
 
  DEBUG_TEXT(P_ID_FEC, 20 ,("FEC_Write_ALTRO_Reg: Memory Data Block:\n %x\n %x\n %x\n", memdata[0], memdata[1],memdata[2]));
     
  /* Writing the Instruction Memory */
  DEBUG_TEXT(P_ID_FEC, 20 ,("FEC_Write_ALTRO_Reg: Writing into the Instruction Memory\n"));
  ret = U2F_IMEM_Write(handle, 3, 0, memdata);
  CHECK_RET(ret)

  /* Execute the Instruction Memory */
  DEBUG_TEXT(P_ID_FEC, 20 ,("FEC_Write_ALTRO_Reg: Executing the Instruction Memory\n"));
  ret = U2F_Exec_Command(handle, C_EXEC, 0);
  CHECK_RET(ret)

  DEBUG_TEXT(P_ID_FEC, 15 ,("FEC_Write_ALTRO_Reg: done\n"));
  return (0);
}

/**********************************************************************************/
u_int FEC_Read_ALTRO_Reg(int handle, u_int ch_addr, u_int reg_addr, u_int *reg_data) 
/**********************************************************************************/
{
  u_int ret, osize, memdata[256];
  DEBUG_TEXT(P_ID_FEC, 15 ,("FEC_Read_ALTRO_Reg: called\n"));
  FECISOPEN(handle);

  /* Preparing the Instruction Memory data */
  DEBUG_TEXT(P_ID_FEC, 20 ,("FEC_Read_ALTRO_Reg: Preparing the Instruction Memory data\n"));
  memdata[0] = 0x00500000 + (ch_addr << 5) + reg_addr;	
  memdata[1] = 0x00390000; 
 
  DEBUG_TEXT(P_ID_FEC, 20 ,("FEC_Read_ALTRO_Reg: Memory Data Block:\n %x\n %x\n", memdata[0], memdata[1]));
     
  /* Writing the Instruction Memory */
  DEBUG_TEXT(P_ID_FEC, 20 ,("FEC_Read_ALTRO_Reg: Writing into the Instruction Memory\n"));
  ret = U2F_IMEM_Write(handle, 2, 0, memdata);
  CHECK_RET(ret)
     
  /* Execute the Instruction Memory */
  DEBUG_TEXT(P_ID_FEC, 20 ,("FEC_Read_ALTRO_Reg: Executing the Instruction Memory\n"));
  ret = U2F_Exec_Command(handle, C_EXEC, 0);
  CHECK_RET(ret)

  /* Reading the Result memory */
  DEBUG_TEXT(P_ID_FEC, 20 ,("FEC_Read_ALTRO_Reg: Reading the Result Memory\n"));  
  ret = U2F_RMEM_Read(handle, 1, &osize, 0, reg_data);  
  CHECK_RET(ret)
  DEBUG_TEXT(P_ID_FEC, 20 ,("FEC_Read_ALTRO_Reg: Result Memory: %x\n", *reg_data)); 

  DEBUG_TEXT(P_ID_FEC, 15 ,("FEC_Read_ALTRO_Reg: done\n"));
  return (0);
}

/*******************************************************************************************/
u_int FEC_Write_ALTRO_PMEM(int handle, u_int bcast, u_int ch_addr, u_int nwords, char *fname) 
/*******************************************************************************************/
{
  u_int ret, loop, memdata[1024], fdata[1024];

  DEBUG_TEXT(P_ID_FEC, 15 ,("FEC_Write_ALTRO_PMEM: called\n"));
  FECISOPEN(handle);

  /* Reading the Pedestal Memory Pattern from file */
  DEBUG_TEXT(P_ID_FEC, 20 ,("FEC_Write_ALTRO_PMEM: Reading the Pedestal Memory Pattern from file:\n%s\n", fname));
  ret = U2F_File_Read(fname, nwords, fdata);
  CHECK_RET(ret)
    
  /* Preparing the Pattern Memory Block */
  DEBUG_TEXT(P_ID_FEC, 20 ,("FEC_Write_ALTRO_PMEM: Preparing the Pattern Memory Block\n"));
  for(loop=0; loop < nwords; loop++)
  {
    memdata[loop] = fdata[loop];
    DEBUG_TEXT(P_ID_FEC, 20 ,("FEC_Write_ALTRO_PMEM: Data word %d = 0x%08x\n", loop, memdata[loop]));
  }
  ret = U2F_PMEM_Write(handle, nwords, 0, memdata);
  CHECK_RET(ret)

  /* Preparing the Instruction Memory data */
  DEBUG_TEXT(P_ID_FEC, 20 ,("FEC_Write_ALTRO_PMEM: Preparing the Instruction Memory data\n"));
  memdata[0] = 0x00380000 + (bcast << 12) + ch_addr;	
  memdata[1] = 0x00390000; 
  DEBUG_TEXT(P_ID_FEC, 20 ,("FEC_Write_ALTRO_PMEM: Memory Data Block:\n %x\n %x\n", memdata[0], memdata[1]));
     
  /* Writing the Instruction Memory */
  DEBUG_TEXT(P_ID_FEC, 20 ,("FEC_Write_ALTRO_PMEM: Writing into the Instruction Memory\n"));
  ret = U2F_IMEM_Write(handle, 2, 0, memdata);
  CHECK_RET(ret)
     
  /* Execute the Instruction Memory */
  DEBUG_TEXT(P_ID_FEC, 20 ,("FEC_Write_ALTRO_PMEM: Executing the Instruction Memory\n"));
  ret = U2F_Exec_Command(handle, C_EXEC, 0);
  CHECK_RET(ret)

  DEBUG_TEXT(P_ID_FEC, 15 ,("FEC_Write_ALTRO_PMEM: done\n"));
  return (0);
}


/******************************************************************************/
u_int FEC_Read_ALTRO_PMEM(int handle, u_int ch_addr, u_int nwords, char fname[]) 
/******************************************************************************/
{
  u_int ret, loop, osize, reg_data[256], memdata[1024], fdata[1024];

  DEBUG_TEXT(P_ID_FEC, 15 ,("FEC_Read_ALTRO_PMEM: called\n"));
  FECISOPEN(handle);

  for(loop = 0; loop < nwords; loop++)
  {
    /* Preparing the Instruction Memory data */
    DEBUG_TEXT(P_ID_FEC, 20 ,("FEC_Read_ALTRO_PMEM: Preparing the Instruction Memory data\n"));
    memdata[0] = 0x0060000d + (ch_addr << 5);	
    memdata[1] = 0x00700000 + loop;
    memdata[2] = 0x00500007 + (ch_addr << 5);
    memdata[3] = 0x00390000; 
    DEBUG_TEXT(P_ID_FEC, 20 ,("FEC_Read_ALTRO_PMEM: Memory Data Block:\n %x\n %x\n %x\n %x\n", memdata[0], memdata[1], memdata[2], memdata[3]));    
 
    /* Writing the Instruction Memory */
    DEBUG_TEXT(P_ID_FEC, 20 ,("FEC_Read_ALTRO_PMEM: Writing into the Instruction Memory\n"));
    ret = U2F_IMEM_Write(handle, 4, 0, memdata);
    CHECK_RET(ret)
    
    /* Execute the Instruction Memory */
    DEBUG_TEXT(P_ID_FEC, 20 ,("FEC_Read_ALTRO_PMEM: Executing the Instruction Memory\n"));
    ret = U2F_Exec_Command(handle, C_EXEC, 0); 
    CHECK_RET(ret)

    /* Reading the Result memory */
    DEBUG_TEXT(P_ID_FEC, 20 ,("FEC_Read_ALTRO_PMEM: Reading the Result Memory\n"));  
    ret = U2F_RMEM_Read(handle, 1, &osize, 0, reg_data); 
    CHECK_RET(ret)
    DEBUG_TEXT(P_ID_FEC, 20 ,("FEC_Read_ALTRO_PMEM: Result Memory: %x\n", *reg_data));
      
    /* copying the content of the result memory into fdata */
    fdata[loop] = *reg_data;
    DEBUG_TEXT(P_ID_FEC, 20 ,("FEC_Read_ALTRO_PMEM: Pedestal Data:\n %x\n", fdata[loop]));
  }
  
  /* Writing the pedestal pattern into file fname */   
  DEBUG_TEXT(P_ID_FEC, 20 ,("FEC_Read_ALTRO_PMEM: Writing Pedestal Data into file\n"));
  ret = U2F_File_Write(fname, nwords, fdata);
  CHECK_RET(ret)
    
  DEBUG_TEXT(P_ID_FEC, 15 ,("FEC_Read_ALTRO_PMEM: done\n"));
  return (0);
}

  
/************************************************/
u_int FEC_Power_Reset(int handle, u_int fecs_addr)
/************************************************/
{
  u_int ret;

  DEBUG_TEXT(P_ID_FEC, 15 ,("FEC_Power_Reset: called\n"));
  FECISOPEN(handle);

  /* Switch ON and OFF the FECs according to the fecs_addr value */
  ret = U2F_Reg_Write(handle, O_ACTFEC, fecs_addr);
  DEBUG_TEXT(P_ID_FEC, 20 ,("FEC_Power_Reset: FECs Power set to %d\n", fecs_addr));
  CHECK_RET(ret)

  /* delay 1 sec */
  ts_delay(1000000);

  /* Reset FECs */
  ret = U2F_Exec_Command(handle, C_FECRST, 0x0);
  CHECK_RET(ret)

  DEBUG_TEXT(P_ID_FEC, 20 ,("FEC_Power_Reset: All FECs have been reset\n"));

  /* delay 10 us */
  ts_delay(10);

  DEBUG_TEXT(P_ID_FEC, 15 ,("FEC_Power_Reset: done\n"));
  return (0);
}

/******************************/
u_int FEC_All_Reset(int handle)
/******************************/
{
  /* OK */
  u_int ret;
  
  /* Reset FECs */
  ret = U2F_Exec_Command(handle, C_FECRST, 0x0);
  CHECK_RET(ret)

  DEBUG_TEXT(P_ID_FEC, 20 ,("FEC_Power_Reset: All FECs have been reset\n"));

  /* delay 1 us */
  ts_delay(10);

  DEBUG_TEXT(P_ID_FEC, 15 ,("FEC_Power_Reset: done\n"));
  return (0);
}


/************************************************/
u_int FEC_All_Status(int handle, int *fec_status)
/************************************************/
{
  /* OK */
  u_int ret, status, loop, rdata=0; 
	
  /* Reading back the ACTFEC Register */
  ret = U2F_Reg_Read(handle, O_ACTFEC, &rdata);
  CHECK_RET(ret)
  
  printf("\nThe FECs have the following power status:\n");
  for(loop = 0; loop < MAX_NR_OF_FECS ; loop++)
  {
   status = (rdata >> loop) & 0x1;
   if(status)
   {
	printf("FEC(0x%x)=ON\n", loop);
	*(fec_status+1) = 1;
   }
   else
   {
    printf("FEC(0x%x)=OFF\n", loop);
	*(fec_status+1) = 0;
   }
  }
  
  /* delay 10 us */
  ts_delay(10);
  
  return 0;
}
/********************************************************************************/
u_int FEC_BC_Reg_Write(int handle, u_int fec_addr, u_int reg_addr, u_int reg_data) 
/********************************************************************************/
{
  u_int ret, memdata[256];

  DEBUG_TEXT(P_ID_FEC, 15 ,("FEC_BC_Reg_Write: called\n"));
  FECISOPEN(handle);

  /* Preparing the Instruction Memory data */
  DEBUG_TEXT(P_ID_FEC, 20 ,("FEC_BC_Reg_Write: Preparing the Instruction Memory data\n"));
	
  memdata[0] = 0x00620000 + ((fec_addr & 0x1f) << 12) + (reg_addr & 0x1f);	
  memdata[1] = 0x00700000 + (reg_data & 0xfffff);
  memdata[2] = 0x00390000; 
 
  DEBUG_TEXT(P_ID_FEC, 20 ,("FEC_BC_Reg_Write: Memory Data Block:\t %x\t %x\t %x\n", memdata[0], memdata[1],memdata[2]));
     
  /* Writing the Instruction Memory */
  DEBUG_TEXT(P_ID_FEC, 20 ,("FEC_BC_Reg_Write: Writing into the Instruction Memory\n"));
  ret = U2F_IMEM_Write(handle, 3, 0, memdata);
  CHECK_RET(ret)
     
  /* Execute the Instruction Memory */
  DEBUG_TEXT(P_ID_FEC, 20 ,("FEC_BC_Reg_Write: Executing the Instruction Memory\n"));
  ret = U2F_Exec_Command(handle, C_EXEC, 0);
  CHECK_RET(ret)

  DEBUG_TEXT(P_ID_FEC, 15 ,("FEC_BC_Reg_Write: done\n"));
  return (0);
}


/********************************************************************************/
u_int FEC_BC_Reg_Read(int handle, u_int fec_addr, u_int reg_addr, u_int *reg_data) 
/********************************************************************************/
{
  u_int ret, osize, memdata[256], csr3_data = 0, conv_done = 0, conv_cycle = 0;

  DEBUG_TEXT(P_ID_FEC, 15 ,("\nFEC_BC_Reg_Read: called\n"));
  FECISOPEN(handle);

  /* Reading Registers that require  to be updated by running the mADC */
  if((reg_addr == 0x06) || (reg_addr == 0x07) || (reg_addr == 0x08) || (reg_addr == 0x09) || (reg_addr == 0x0a))
  {
    ret = FEC_BC_Reg_Write(handle, fec_addr, 0x1b, 0);
    CHECK_RET(ret)
    
    while(!conv_done)
    {
      /* delay 1ms */
      ts_delay(1000);

      ret = FEC_BC_Reg_Read(handle, fec_addr, 0x14, &csr3_data);  
      CHECK_RET(ret)

      conv_done = csr3_data & 0x8000;
      conv_cycle++;
      
      if(conv_cycle == 10){
        exit(0);  /*MJ*/
	}
    }
    
    /* Preparing the Instruction Memory data */
    DEBUG_TEXT(P_ID_FEC, 20 ,("FEC_BC_Reg_Read: Preparing the Instruction Memory data\n"));
    memdata[0] = 0x00520000 + ((fec_addr & 0x1f) << 12) + (reg_addr & 0x1f);	
    memdata[1] = 0x00390000; 
    DEBUG_TEXT(P_ID_FEC, 20 ,("FEC_BC_Reg_Read: Memory Data Block:\n %x\n %x\n", memdata[0], memdata[1]));
     
    /* Writing the Instruction Memory */
    DEBUG_TEXT(P_ID_FEC, 20 ,("FEC_BC_Reg_Read: Writing into the Instruction Memory\n"));
    ret = U2F_IMEM_Write(handle, 2, 0, memdata);
    CHECK_RET(ret)
     
    /* Execute the Instruction Memory */
    DEBUG_TEXT(P_ID_FEC, 20 ,("FEC_BC_Reg_Read: Executing the Instruction Memory\n"));
    ret = U2F_Exec_Command(handle, C_EXEC, 0);
    CHECK_RET(ret)

    /* Reading the Result memory */
    DEBUG_TEXT(P_ID_FEC, 20 ,("FEC_BC_Reg_Read: Reading the Result Memory\n"));  
    ret = U2F_RMEM_Read(handle, 1, &osize, 0, reg_data);  
    CHECK_RET(ret)

    DEBUG_TEXT(P_ID_FEC, 20 ,("FEC_BC_Reg_Read: Result Memory: %x\n", *reg_data)); 
  }    
 
  /*  Reading any other BC Register  */
  else 
  {
    /* Preparing the Instruction Memory data */
    DEBUG_TEXT(P_ID_FEC, 20 ,("FEC_BC_Reg_Read: Preparing the Instruction Memory data\n"));
    memdata[0] = 0x00520000 + ((fec_addr & 0x1f) << 12) + (reg_addr & 0x1f);	
    memdata[1] = 0x00390000; 
    DEBUG_TEXT(P_ID_FEC, 20 ,("FEC_BC_Reg_Read: Memory Data Block:\n %x\n %x\n", memdata[0], memdata[1]));
     
    /* Writing the Instruction Memory */
    DEBUG_TEXT(P_ID_FEC, 20 ,("FEC_BC_Reg_Read: Writing into the Instruction Memory\n"));
    ret = U2F_IMEM_Write(handle, 2, 0, memdata);
    CHECK_RET(ret)
     
    /* Execute the Instruction Memory */
    DEBUG_TEXT(P_ID_FEC, 20 ,("FEC_BC_Reg_Read: Executing the Instruction Memory\n"));
    ret = U2F_Exec_Command(handle, C_EXEC, 0);
    CHECK_RET(ret)

    /* Reading the Result memory */
    DEBUG_TEXT(P_ID_FEC, 20 ,("FEC_BC_Reg_Read: Reading the Result Memory\n"));  
    ret = U2F_RMEM_Read(handle, 1, &osize, 0, reg_data);  
    CHECK_RET(ret)

    DEBUG_TEXT(P_ID_FEC, 20 ,("FEC_BC_Reg_Read: Result Memory: %x\n", *reg_data)); 
  }      
  DEBUG_TEXT(P_ID_FEC, 15 ,("FEC_BC_Reg_Read: done\n"));
  return (0);
}


/****************************************************************************************************************/
u_int FEC_BC_Mon(int handle, u_int fec_addr, float *temp, float *ana_v,  float *ana_i, float *dig_v, float *dig_i) 
/****************************************************************************************************************/
{
  u_int ret, reg_temp = 0, reg_ana_v = 0, reg_ana_i = 0, reg_dig_v = 0, reg_dig_i = 0;

  DEBUG_TEXT(P_ID_FEC, 15 ,("FEC_BC_Mon: called\n"));
  FECISOPEN(handle);

  ret = FEC_BC_Reg_Read(handle, fec_addr, 0x06, &reg_temp);
  CHECK_RET(ret)

  ret = FEC_BC_Reg_Read(handle, fec_addr, 0x07, &reg_ana_v);
  CHECK_RET(ret)

  ret = FEC_BC_Reg_Read(handle, fec_addr, 0x08, &reg_ana_i);
  CHECK_RET(ret)

  ret = FEC_BC_Reg_Read(handle, fec_addr, 0x09, &reg_dig_v);
  CHECK_RET(ret)

  ret = FEC_BC_Reg_Read(handle, fec_addr, 0x0a, &reg_dig_i);
  CHECK_RET(ret)

  *temp  = reg_temp / 4.0;
  *ana_v = reg_ana_v * 4.43;
  *ana_i = reg_ana_i * 17.0;
  *dig_v = reg_dig_v * 4.43;
  *dig_i = reg_dig_i * 30.0;
   
  DEBUG_TEXT(P_ID_FEC, 20 ,("FEC_BC_Mon: \nFEC Monitoring ADC:\n"));
  DEBUG_TEXT(P_ID_FEC, 20 ,("FEC_BC_Mon: Temperature =\t%.1fC\n", *temp));
  DEBUG_TEXT(P_ID_FEC, 20 ,("FEC_BC_Mon: Analogue Voltage =\t%.0f mV\n", *ana_v));
  DEBUG_TEXT(P_ID_FEC, 20 ,("FEC_BC_Mon: Analogue Current =\t%.0f mA\n", *ana_i));
  DEBUG_TEXT(P_ID_FEC, 20 ,("FEC_BC_Mon: Digital Voltage =\t%.0f mV\n", *dig_v));
  DEBUG_TEXT(P_ID_FEC, 20 ,("FEC_BC_Mon: Digital Current =\t%.0f mA\n", *dig_i));
  DEBUG_TEXT(P_ID_FEC, 15 ,("FEC_BC_Mon: done\n"));
  return (0);
}  


/************************************************************************/
u_int FEC_FM_Write(int handle, u_int fec_addr, u_int nwords, char fname[])
/************************************************************************/
{
  u_int ret, loop, fm_address, fm_data, fdata[128];
  int pid, tl;
  
  DEBUG_TEXT(P_ID_FEC, 15 ,("FEC_FM_Write: called\n"));
  FECISOPEN(handle);

  /*** Reading file fname ***/
  ret = U2F_File_Read(fname, nwords, fdata);
  CHECK_RET(ret)

  /* Writing in the FM */   
  rcc_error_get_debug(&pid, &tl);
  if(pid == P_ID_FEC && tl == 20)   /*debug*/
  {
    printf("\nFEC_FM_Read:  Writing %d words in the FM of FEC %d\n", nwords, fec_addr);
    for(loop = 0; loop < nwords; loop++)
    {
      fm_address = (fdata[loop] >> 8) & 0xff;
      fm_data = fdata[loop] & 0xff;
      printf("FEC_FM_Read: Address = %d\t\t Data = %d\n", fm_address, fm_data);
    }       
  }    
  
  for(loop = 0; loop < nwords; loop++)
  {
    /* Writing FM Address and Data in FMIREG */
    ret = U2F_Reg_Write(handle, O_FMIREG, fdata[loop]);
    CHECK_RET(ret)

    /* Execute the WRFM Command */
    ret = U2F_Exec_Command(handle, C_WRFM, 0);
    CHECK_RET(ret)
  
    /* delay 5 ms */
    ts_delay(5000);
   }
  
  DEBUG_TEXT(P_ID_FEC, 20 ,("FEC_FM_Write: FEC's Flash Memory Written\n"));
  DEBUG_TEXT(P_ID_FEC, 15 ,("FEC_FM_Write: done\n"));
  return (0);
}


/***********************************************************************/
u_int FEC_FM_Read(int handle, u_int fec_addr, u_int nwords, char fname[])
/***********************************************************************/
{
  u_int ret, loop, fm_address, rdata = 0, fdata[128];
  
  DEBUG_TEXT(P_ID_FEC, 15 ,("FEC_FM_Read: called\n"));
  FECISOPEN(handle);

  /* Reading in the FM */   
  DEBUG_TEXT(P_ID_FEC, 20 ,("FEC_FM_Read: Reading %d words in the FM of FEC %d\n", nwords, fec_addr));
  
  for(loop = 0; loop < nwords; loop++)
  {
    /* Writing FM Address in FMIREG */
    fm_address = (loop << 8) & 0xffff;
    ret = U2F_Reg_Write(handle, O_FMIREG, fm_address);
    CHECK_RET(ret)
  
    /* Execute the RDFM Command */
    ret = U2F_Exec_Command(handle, C_RDFM, 0);
    CHECK_RET(ret)
    
    /* delay 5 ms */
    ts_delay(5000);

    /* Reading FMOREG */
    ret = U2F_Reg_Read(handle, O_FMOREG, &rdata);
    CHECK_RET(ret)

    fdata[loop] = (((loop << 8) & 0xff00) | rdata) & 0xffff ;
    
    DEBUG_TEXT(P_ID_FEC, 20 ,("FEC_FM_Read: Address = %d\t\t Data = %d\n", loop, rdata & 0xff));
  }
    
  /* Writing FM data in file fname */
  DEBUG_TEXT(P_ID_FEC, 20 ,("FEC_FM_Read: Writing Flash Memory Data into file\n"));
  ret = U2F_File_Write(fname, nwords, fdata);
  CHECK_RET(ret)

  DEBUG_TEXT(P_ID_FEC, 15 ,("FEC_FM_Read: done\n"));
  return (0);
}


/************************************************************************************************/
u_int FEC_Event_Readout(int handle, u_int fec_addr, u_int trg_mode, u_int rdo_mode, u_int nevents, 
                        u_int nsamples, u_int n_before_trg, char fname[]) 
/************************************************************************************************/
{
  int pid, tl, fecnr;
  double int_nwords = 0.0, float_nwords = 0.0, frac_nwords = 0.0;
  u_int mdc, ret, loop, nev, chnum = 0, tick= 0, point = 0, rdata = 0, osize = 0, rsize = 0, memdata[200000];
  FILE *file;
  
  /*************************************************************************/
  /*				FEC INITIALIZATION			   */
  /*************************************************************************/

  DEBUG_TEXT(P_ID_FEC, 15 ,("FEC_Event_Readout: called\n"));
  FECISOPEN(handle);

  /* Preparing the Instruction Memory data */
  DEBUG_TEXT(P_ID_FEC, 20 ,("FEC_Event_Readout: \nPreparing the Instruction Memory Data\n"));
	
  mdc = 0;
  memdata[mdc++] = 0x0064000a;		
  memdata[mdc++] = 0x00700000 + nsamples;
  memdata[mdc++] = 0x0064000c;
  memdata[mdc++] = 0x00700000 + n_before_trg;
  memdata[mdc++] = 0x0064000b; 
  if(rdo_mode)
    memdata[mdc++] = 0x0070000a;
  else
    memdata[mdc++] = 0x00700000;
  memdata[mdc++] = 0x00390000; 
 
  rcc_error_get_debug(&pid, &tl);
  if(pid == P_ID_FEC && tl == 20)   /*debug*/
  {
    printf("FEC_Event_Readout: \nMemory Data Block:\n");
    
    for(loop = 0; loop < mdc; loop++)

      printf("FEC_Event_Readout: Memory Data[%d] =\t%d\n", loop, memdata[loop]);
  }
     
  /* Writing the Instruction Memory */
  DEBUG_TEXT(P_ID_FEC, 20 ,("FEC_Event_Readout: Writing %d words into the Instruction Memory\n", mdc));
  ret = U2F_IMEM_Write(handle, mdc, 0, memdata);
  CHECK_RET(ret)
   
  /* Execute the Instruction Memory */
  DEBUG_TEXT(P_ID_FEC, 20 ,("FEC_Event_Readout: \nExecuting the Instruction Memory\n"));
  ret = U2F_Exec_Command(handle, C_EXEC, 0);
  CHECK_RET(ret)

  /* delay 1ms */
  ts_delay(1000);

  /****************************************************************/
  /*			INITIALIZING the U2F			  */
  /****************************************************************/
  DEBUG_TEXT(P_ID_FEC, 20 ,("FEC_Event_Readout: Initializing the Channel Readout List\n"));
  /* Initializing the Readout List */

  for (fecnr = 0; fecnr < MAX_NR_OF_FECS; fecnr++)   
  {
    if(fec_addr & (1 << fecnr))
    {
      for(loop = 0; loop < 128; loop++)
        memdata[loop] = 0x0;
      ret = U2F_ACL_Write(handle, 128, fecnr * 8, memdata);
      CHECK_RET(ret)
    }
  }
    
  /* Initializing the Active Channel List */
  mdc = 0;
  memdata[mdc++] = 0xffff;
  memdata[mdc++] = 0xffff;
  memdata[mdc++] = 0xffff;
  memdata[mdc++] = 0xffff;
  memdata[mdc++] = 0xffff;
  memdata[mdc++] = 0xffff;
  memdata[mdc++] = 0xffff;
  memdata[mdc++] = 0xffff;    
  
  for (fecnr = 0; fecnr < MAX_NR_OF_FECS; fecnr++)   
  {
    if(fec_addr & (1 << fecnr))
    {
      ret = U2F_ACL_Write(handle, mdc, fecnr * 8, memdata);
      CHECK_RET(ret)        
    }
  }
  
  /* Configuring the Trigger Configuration Register */
  DEBUG_TEXT(P_ID_FEC, 20 ,("FEC_Event_Readout: \nConfiguring the Trigger Configuration Register\n"));
  ret = U2F_Reg_Write(handle, O_TRCFG1, 0x51000);
  CHECK_RET(ret)

  /*************************************************************************/
  /*			Sending a TRIGGER				   */
  /*************************************************************************/

  /*** Opening the Data File ***/
  file = fopen(fname, "w"); 

  for(nev = 0; nev < nevents; nev++)
  { 
    point = 0;
    chnum = 0;  
    /* Send a software trigger */
    if(trg_mode)
    {
      ret = U2F_Exec_Command(handle, C_SWTRG, 0);
      CHECK_RET(ret)
      DEBUG_TEXT(P_ID_FEC, 20 ,("FEC_Event_Readout: \n\nSOFTWARE TRIGGER ISSUED\n\n"));
    }
    /*** Waiting for an external Trigger ***/
    /***  Read EVWORD Register  ***/
    else
    {
      ret = U2F_Reg_Write(handle, O_TRCFG2, 0x2); 
      CHECK_RET(ret)

      ret = U2F_Reg_Read(handle, O_TRCFG2, &rdata); 
      CHECK_RET(ret)
      DEBUG_TEXT(P_ID_FEC, 20 ,("FEC_Event_Readout: External Trigger Enabled = %x\n", rdata));

      ret = U2F_Reg_Read(handle, O_EVWORD, &rdata); 
      CHECK_RET(ret)
      DEBUG_TEXT(P_ID_FEC, 20 ,("FEC_Event_Readout: EVWORD = %x\n", rdata));

      while(!(rdata & 0x00000010))
      {
	tick++;

	/***  Read EVWORD Register  ***/
	ret = U2F_Reg_Read(handle, O_EVWORD, &rdata); 
        CHECK_RET(ret)

	if(tick == 5000)
	{
          DEBUG_TEXT(P_ID_FEC, 20 ,("FEC_Event_Readout: Waiting for external trigger, point = %d, tick=%d\n", point, tick));
	  point++;
	  tick = 0;
	} 

	if(point == 10)
	{
          DEBUG_TEXT(P_ID_FEC, 10 ,("FEC_Event_Readout: point = %d, tick=%d\n", point, tick));
          ret = U2F_Reg_Write(handle, O_TRCFG2, 0x0); 
          CHECK_RET(ret)
          ret = U2F_Reg_Read(handle, O_TRCFG2, &rdata); 
          CHECK_RET(ret)
          DEBUG_TEXT(P_ID_FEC, 20 ,("FEC_Event_Readout: External Trigger Disabled = %x\n", rdata));
          return(0);  		
	}
      }
    }


    /*************************************************************************/
    /*			Perform the Readout			 	     */
    /*************************************************************************/
    ts_delay(150);
      
    /***  Read EVWORD Register  ***/
    ret = U2F_Reg_Read(handle, O_EVWORD, &rdata); 
    CHECK_RET(ret)
    DEBUG_TEXT(P_ID_FEC, 20 ,("FEC_Event_Readout: EVWORD = %x\n", rdata));

    if((rdata & 0x00000010) && (rdata & 0x00000008)) 
    {
      DEBUG_TEXT(P_ID_FEC, 20 ,("FEC_Event_Readout: STARTING EVENT READOUT\n"));  	
      if(rdata & 0x00000004)
        DEBUG_TEXT(P_ID_FEC, 10 ,("FEC_Event_Readout: EVENT READOUT WAS CONCLUDED WITHOUT DATA TRANSFER\n"));

      /*** Calculating Data Block Size ***/  
      float_nwords = (nsamples + n_before_trg + 2) / 4.0;
      
      frac_nwords = modf(float_nwords, &int_nwords);
      if(frac_nwords)
	rsize = int_nwords + 2;
      else
	rsize = int_nwords + 1; 
      DEBUG_TEXT(P_ID_FEC, 20 ,("FEC_Event_Readout: \nData Block Size = %d\n", rsize));

      while(!(rdata & 0x00000004))
      {
	/******************************/
	/***  Read EVWORD Register  ***/
	/******************************/
	ret = U2F_Reg_Read(handle, O_EVWORD, &rdata); 
        CHECK_RET(ret)

	/*****************************/
	/***  Reading Data  	   ***/
	/*****************************/
	/*** Reading DMEM1  ***/
	if (rdata & 0x00000002)
	{
	  DEBUG_TEXT(P_ID_FEC, 20 ,("FEC_Event_Readout: \n\nReading Channel #%d from DMEM1\n", chnum));
	  ret = U2F_DM1_Read(handle, rsize, &osize, 0, memdata);
          CHECK_RET(ret)
  
	  for (loop = 0; loop < osize; loop++) 
	  {
	    fprintf(file, "%d\n", memdata[loop * 4]);
	    fprintf(file, "%d\n", memdata[loop * 4 + 1]);
	    fprintf(file, "%d\n", memdata[loop * 4 + 2]);
	    fprintf(file, "%d\n", memdata[loop * 4 + 3]);	
	  }

	  /* Read EVWORD */ 
	  ret = U2F_Reg_Read(handle, O_EVWORD, &rdata); 
          CHECK_RET(ret)
	  DEBUG_TEXT(P_ID_FEC, 20 ,("FEC_Event_Readout: EVWORD = %x\n", rdata));

	  /***  Reset DMEM1 flag    ***/
	  ret = U2F_Exec_Command(handle, C_RS_DMEM1, 0);
          CHECK_RET(ret)

	  /* Read EVWORD */ 
	  ret = U2F_Reg_Read(handle, O_EVWORD, &rdata); 
          CHECK_RET(ret)
	  DEBUG_TEXT(P_ID_FEC, 20 ,("FEC_Event_Readout: EVWORD = %x\n", rdata));
	}

	/***  Reading DMEM2  ***/
	if (rdata & 0x00000001)
	{
	  chnum++;
	  DEBUG_TEXT(P_ID_FEC, 20 ,("FEC_Event_Readout: \n\nReading Channel #%d from DMEM2\n", chnum));

	  ret = U2F_DM2_Read(handle, rsize, &osize, 0, memdata); 
          CHECK_RET(ret)

	  for (loop = 0; loop < osize; loop++) 
	  {
	    fprintf(file, "%d\n", memdata[loop * 4]);
	    fprintf(file, "%d\n", memdata[loop * 4 + 1]);
	    fprintf(file, "%d\n", memdata[loop * 4 + 2]);
	    fprintf(file, "%d\n", memdata[loop * 4 + 3]);	
	  }

	  /* Read EVWORD */ 
	  ret = U2F_Reg_Read(handle, O_EVWORD, &rdata); 
          CHECK_RET(ret)
	  DEBUG_TEXT(P_ID_FEC, 20 ,("FEC_Event_Readout: EVWORD = %x\n", rdata));

	  /***  Reset DMEM2 flag    ***/
	  ret = U2F_Exec_Command(handle, C_RS_DMEM2, 0);
          CHECK_RET(ret)

	  /* Read EVWORD */ 
	  ret = U2F_Reg_Read(handle, O_EVWORD, &rdata); 
          CHECK_RET(ret)
	  DEBUG_TEXT(P_ID_FEC, 20 ,("FEC_Event_Readout: EVWORD = %x\n", rdata));
	}
	chnum++;
      }

      /*** Reset EVWORD ***/
      ret = U2F_Exec_Command(handle, C_TRG_CLR, 0);
      CHECK_RET(ret)

      /*** Read EVWORD ***/ 
      ret = U2F_Reg_Read(handle, O_EVWORD, &rdata); 
      CHECK_RET(ret)
      DEBUG_TEXT(P_ID_FEC, 20 ,("FEC_Event_Readout: EVWORD = %x\n", rdata));
    }  
    DEBUG_TEXT(P_ID_FEC, 20 ,("FEC_Event_Readout: Event %d completed\n", nev));
  }
  fclose(file); 

  ret = U2F_Reg_Write(handle, O_TRCFG2, 0x0); 
  CHECK_RET(ret)

  ret = U2F_Reg_Read(handle, O_TRCFG2, &rdata); 
  CHECK_RET(ret)

  DEBUG_TEXT(P_ID_FEC, 20 ,("FEC_Event_Readout: External Trigger Disabled = %x\n", rdata));
  DEBUG_TEXT(P_ID_FEC, 15 ,("FEC_Event_Readout: done\n"));
  return (0);
}


/********************************************************/
u_int FEC_err_get(err_pack err, err_str pid, err_str code)
/********************************************************/
{ 
  strcpy(pid, P_ID_FEC_STR);

  switch (RCC_ERROR_MINOR(err))
  {  
    case FEC_SUCCESS:    strcpy(code, FEC_SUCCESS_STR);     break;
    case FEC_U2F_FAIL:   strcpy(code, FEC_U2F_FAIL_STR);    break;
    case FEC_NOTOPEN:    strcpy(code, FEC_NOTOPEN_STR);     break;
    default:             strcpy(code, FEC_NO_CODE_STR);     return(RCC_ERROR_RETURN(0, FEC_NO_CODE)); break;
  }
  return(RCC_ERROR_RETURN(0, FEC_SUCCESS));
}






