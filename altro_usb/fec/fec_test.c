/************************************************************************/
/*									*/
/*  This is the FEC library 	   				        */
/*									*/
/*  09. Jun. 05  L. Musa & H. Santos					*/
/*									*/
/************************************************************************/

#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/file.h>
#include <fcntl.h>
#include "fec.h"

int main(void)
{
  u_int ret, rcode, rdata, ccode, cdata, reg_data = 0;
  u_int system_init = 1, write_read_reg = 0, write_read_pmem = 0, readout = 0, write_read_bc = 0;
  u_int read_FEC_mADC = 1, usb_test = 0, endurance = 0;
  u_int tick=0, point=0;
  int handle;
  float temp = 0.0, ana_v = 0.0, ana_i = 0.0, dig_v = 0.0, dig_i = 0.0;
  static char fname[200] = "/home/rcu/NEW_U2F/altro_usb/data_files/test_data";
  static char finame[200] = "/home/rcu/NEW_U2F/altro_usb/pedestal/ped_in";
  static char foname[200] = "/home/rcu/NEW_U2F/altro_usb/pedestal/ped_out";

  rcc_error_set_debug(0, 0);

  /***********************************************************
      System Initialization 
  ***********************************************************/
  if(system_init)   
  {  
    /* Open FEC_LIB */
    ret = FEC_Open(DEFAULT_NODE, &handle);
    if (ret)
    {
      rcc_error_print(stdout, ret);
      return ret;
    }

    /* Open the rcc_time_stamp library */
    ret = ts_open(1, TS_DUMMY);
    if (ret)
    {
      rcc_error_print(stdout, ret);
      return ret;
    }

    /* Switch on FEC with Address = 0  */
    rcode = O_ACTFEC;
    rdata = 0x1;
    ret = U2F_Reg_Write(handle, rcode, rdata);
    if (ret)
    {
      rcc_error_print(stdout, ret);
      return ret;
    }

    /* delay 1000000 us */
    ts_delay(1000000);

    /* Reset FEC */
    ccode = C_FECRST;
    cdata = 0;
    ret = U2F_Exec_Command(handle, ccode, cdata);
    if (ret)
    {
      rcc_error_print(stdout, ret);
      return ret;
    }
    ccode = C_FECRST;
    cdata = 0;
    ret = U2F_Exec_Command(handle, ccode, cdata);
    if (ret)
    {
      rcc_error_print(stdout, ret);
      return ret;
    }

    /* delay 100 us */
    ts_delay(100);
  }


  /***********************************************************/
  /*    WRITE & READ  ALTRO REGISTER			     */	
  /***********************************************************/
  if(write_read_reg)
  {
    ret = FEC_Write_ALTRO_Reg(handle, 0, 0, 0xabcd);
    if (ret)
    {
      rcc_error_print(stdout, ret);
      return ret;
    }

    ret = FEC_Read_ALTRO_Reg(handle, 0x0, 0x0, &reg_data);
    if (ret)
    {
      rcc_error_print(stdout, ret);
      return ret;
    }

    printf("Reg 0\t of Channel 0\t of Chip 0\t has the value:\t%x\n", reg_data);
  }

  /*********************************************************/
  /*    WRITE & READ  Pedestal Memory			   */	
  /*********************************************************/
  if(write_read_pmem)
  {
    printf("opening file: \n%s\n", finame);
    ret = FEC_Write_ALTRO_PMEM(handle, 1, 0, 128, finame);
    if (ret)
    {
      rcc_error_print(stdout, ret);
      return ret;
    }

    /* delay 1s */
    ts_delay(1000000);

    ret = FEC_Read_ALTRO_PMEM(handle, 0, 128, foname);
    if (ret)
    {
      rcc_error_print(stdout, ret);
      return ret;
    }
  }


  /***********************************************************/
  /*          Data Readout multiple in FEC bus               */
  /***********************************************************/
  if(readout)
  {
    printf("\nCalling FEC_Event_Readout\n");
    ret = FEC_Event_Readout(handle, 1, 1, 0, 1, 256, 0, fname);
    if (ret)
    {
      rcc_error_print(stdout, ret);
      return ret;
    }
  }

  /*********************************************************/
  /*    WRITE & READ  ALTRO REGISTER			   */	
  /*********************************************************/
  if(write_read_bc)
  {
    ret = FEC_BC_Reg_Write(handle, 0, 0x10, 0xabcd);
    if (ret)
    {
      rcc_error_print(stdout, ret);
      return ret;
    }

    ret = FEC_BC_Reg_Read(handle, 0, 0x10, &reg_data) ;
    if (ret)
    {
      rcc_error_print(stdout, ret);
      return ret;
    }
    printf("\nBC Reg 0x06 = %x\n", reg_data);
  }


  /*********************************************************/
  /*    READ  FEC Monitoring ADC  			   */	
  /*********************************************************/
  if(read_FEC_mADC)
  {
    ret = FEC_BC_Mon(handle, 0, &temp, &ana_v, &ana_i, &dig_v, &dig_i);
    if (ret)
    {
      printf("\nFEC_BC_Mon failed\n");
      rcc_error_print(stdout, ret);
      return ret;
    }
    printf("\nTemp: %f\nAna_V: %f\nAna_I: %f\nDig_V: %f\nDig_I: %f\n", temp, ana_v, ana_i, dig_v, dig_i);
  }


  /*********************************************************/
  /*    Test I2C Communication				   */	
  /*********************************************************/
  if(usb_test)
  {
    while(usb_test)
    {
      ret = U2F_Reg_Write(handle, O_FMIREG, 0xabcd);
      if (ret)
      {
	rcc_error_print(stdout, ret);
	return ret;
      }

      ret = U2F_Reg_Read(handle, O_FMIREG, &reg_data);
      if (ret)
      {
	rcc_error_print(stdout, ret);
	return ret;
      }

      printf("U2F Register O_FMIREG:\t%x\n", reg_data);
    }
  }


  /*********************************************************/
  /*    Endurance Test            			   */	
  /*********************************************************/
  if(endurance)
  {
    tick =0;
    while(endurance)
    {
      tick++;

      if(tick == 5000)
      {
	printf("point = %d, tick=%d\n", point, tick);
	point++;
	tick = 0;
      } 
      if(point == 10)
      {
	printf("point = %d, tick=%d\n", point,tick);
	return(0);  		
      }
      ret = U2F_Reg_Write(handle, O_ACTFEC, 0x0); 
      if (ret)
      {
	rcc_error_print(stdout, ret);
	return ret;
      }
    }
  }

  ret = FEC_Close(handle);
  if (ret)
  {
    rcc_error_print(stdout, ret);
    return ret;
  }
  ret = ts_close(TS_DUMMY);  
  if (ret)
  {
    rcc_error_print(stdout, ret);
    return ret;
  }
  exit(0);
}

