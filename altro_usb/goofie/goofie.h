/************************************************************************/
/*									*/
/*  This is the header file for the GOOFIE library			*/
/*									*/
/*  16. Nov. 04  MAJO  created						*/
/*									*/
/*******C 2004 - The software with that certain something****************/

#ifndef _GOOFIE_H
#define _GOOFIE_H

#include <altro/altro.h>

/*************/
/* Constants */
/*************/

/* Instruction codes */
#define I_WRREG    0
#define I_RDREG    1
#define I_WRPDM    2
#define I_RDPDM    3
#define I_RDDTM    4
#define I_RDEVL    5
#define I_SWTRG    6
#define I_SWRST    7
#define I_TWRST    8

/* Registers */
#define O_BSLCT 1
#define O_TMASK 2
#define O_ACTCH 3
#define O_ZSCTL 4
#define O_ZSTHD 5
#define O_ZSPED 6
#define O_TATHD 7
#define O_TBTHD 8
#define O_NSEVT 9
#define O_TRGEV 10
#define O_PRTRG 11

/* Sizes */
#define S_DMEM 128
#define S_PMEM 512

/* Channel offsets */
#define O_CHANNEL     0x0400
	

/*********/
/* Types */
/*********/
typedef struct
{
  u_int pattern_error; /* Possible values are TRUE and FALSE */
  u_int abort;         /* Possible values are TRUE and FALSE */
  u_int timeout;       /* Possible values are TRUE and FALSE */
  u_int altro_error;   /* Possible values are TRUE and FALSE */
} errst_t;

typedef struct
{
  u_int tw;    /* Legal values are 0 - 0x3fff     */
  u_int bmd;   /* Legal values are 0 or 1         */
  u_int mode;  /* Legal values are 0, 2 or 3      */
  u_int remb;  /* Legal values are 0 - 0xf        */
  u_int empty; /* Legal values are TRUE and FALSE */
  u_int full;  /* Legal values are TRUE and FALSE */
  u_int rd_pt; /* Legal values are 0 to 7         */
  u_int wr_pt; /* Legal values are 0 to 7         */
} trcfg_t;

typedef struct
{
  u_int ntr; /* Possible values are 0 - 0xffff */
  u_int nta; /* Possible values are 0 - 0xffff */
} trcnt_t;


/*************/
/*error codes*/
/*************/
enum
{
  GOOFIE_SUCCESS = 0,
  GOOFIE_FILE = (P_ID_GOOFIE<<8) + 1,
  GOOFIE_FILE2,
  GOOFIE_NOTOPEN,
  GOOFIE_ALTRO_FAIL,
  GOOFIE_RANGE,
  GOOFIE_ODD_BYTES,
  GOOFIE_NODATA,
  GOOFIE_ERROR_FAIL,
  GOOFIE_NO_CODE
};

/***************/
/*error strings*/
/***************/
#define GOOFIE_SUCCESS_STR        "Function successfully executed"
#define GOOFIE_FILE_STR           "Failed to open / close data file"
#define GOOFIE_FILE2_STR          "A file with the specified name does already exist"
#define GOOFIE_NOTOPEN_STR        "The library has not been opened yet"
#define GOOFIE_ALTRO_FAIL_STR     "Error from ALTRO library"
#define GOOFIE_RANGE_STR          "A parameter is out of range"
#define GOOFIE_ODD_BYTES_STR      "Wrong number of bytes received"
#define GOOFIE_NO_CODE_STR        "Unknown error"
#define GOOFIE_NODATA_STR         "The files you are reading does not contain enought data"
 
/********/
/*Macros*/
/********/
#define GOOFIEISOPEN {if(!goofie_is_open) return(GOOFIE_NOTOPEN);}

#ifdef __cplusplus
extern "C" 
{
#endif

/***********************************/ 
/*Official functions of the library*/
/***********************************/ 
u_int Goofie_Open(char *node);
u_int Goofie_Close(void);
u_int Goofie_File_Read(char *name, u_int size, u_int data[]);
u_int Goofie_File_Write(char *name, u_int size, u_int data[]);
u_int Goofie_CSR_Read(u_int reg, u_int *data);
u_int Goofie_CSR_Write(u_int reg, u_int data);
u_int Goofie_PMEM_Read(u_int channel, u_int offset, u_short *data);
u_int Goofie_PMEM_Write(u_int channel, u_int offset, u_short data);
u_int Goofie_DMEM_Read(u_int channel, u_int isize, u_int *osize, u_int data[]);
u_int Goofie_EVLEN_Read(u_int channel, u_short *data);
u_int Goofie_Exec_Command(u_int command);


/***********************************/ 
/*Internal functions of the library*/
/***********************************/ 
u_int Goofie_err_get(err_pack err, err_str pid, err_str code);

#ifdef __cplusplus
}
#endif

#endif
