/************************************************************************/
/*									*/
/*  This is the GOOFIE library 	   				        */
/*									*/
/*  16. Nov. 04  MAJO  created						*/
/*									*/
/*******C 2004 - The software with that certain something****************/

#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/file.h>
#include <fcntl.h>
#include "goofie.h"


/***********/
/* Globals */
/***********/
static u_int goofie_is_open = 0;
static int handle;


/***************************/
u_int Goofie_Open(char *node)
/***************************/
{  
  u_int ret;
  
  if (goofie_is_open)
  {
    goofie_is_open++;             /* keep track of multiple open calls */
    return(RCC_ERROR_RETURN(0, GOOFIE_SUCCESS));
  }

  /* open the error package */
  ret = rcc_error_init((err_pid)P_ID_GOOFIE, Goofie_err_get);
  if (ret)
  {
    DEBUG_TEXT(P_ID_GOOFIE, 5 ,("Goofie_Open: Failed to open error package\n"))
    return(RCC_ERROR_RETURN(0, GOOFIE_ERROR_FAIL)); 
  }
  DEBUG_TEXT(P_ID_GOOFIE, 20 ,("Goofie_Open: error package opened\n")) 

  ret = ALTRO_Open(node, &handle);
  if (ret)
  {
    DEBUG_TEXT(P_ID_GOOFIE, 5 ,("Goofie_Open: Failed to open the ALTRO library\n"))
    return(RCC_ERROR_RETURN(ret, GOOFIE_ALTRO_FAIL)); 
  }
  DEBUG_TEXT(P_ID_GOOFIE, 10 ,("Goofie_Open: ALTRO library opened\n")) 
  
  goofie_is_open = 1;
  return(GOOFIE_SUCCESS);
}


/**********************/
u_int Goofie_Close(void)
/**********************/
{
  u_int ret;

  GOOFIEISOPEN
  
  if (goofie_is_open > 1)
    goofie_is_open--;
  else
  {  
    ret = ALTRO_Close(handle);
    if (ret)
    {
      DEBUG_TEXT(P_ID_GOOFIE, 5 ,("Goofie_Close: Failed to close the ALTRO library\n"))
      return(RCC_ERROR_RETURN(ret, GOOFIE_ALTRO_FAIL)); 
    }
    DEBUG_TEXT(P_ID_GOOFIE, 10 ,("Goofie_Close: ALTRO library closed\n")) 
    goofie_is_open = 0;
  }
  
  return(GOOFIE_SUCCESS);
}


/***************************************************/
u_int Goofie_EVLEN_Read(u_int channel, u_short *data)
/***************************************************/
{
  int ret;
  altro_bulk_out_t out;
  altro_bulk_in_t in;
  u_short *msg;
  
  GOOFIEISOPEN
 
  if (channel < 1 || channel > 3)
  {    
    DEBUG_TEXT(P_ID_GOOFIE, 5 ,("Goofie_EVLEN_Read: The channel address is out of range\n"))
    return(RCC_ERROR_RETURN(0, GOOFIE_RANGE)); 
  }

  /* Assemble the message */
  msg = (u_short *)&out.data[0];
  msg[0] = I_RDEVL; 
  msg[1] = (channel - 1) * O_CHANNEL;
  out.nbytes = 4;
  
  /* Tell the GOOFIE that we want to read a register  */
  ret = ALTRO_Send(handle, &out);
  if (ret)
  {      
    DEBUG_TEXT(P_ID_GOOFIE, 5 ,("Goofie_EVLEN_Read: Error from ALTRO_Send\n"))
    return(RCC_ERROR_RETURN(ret, GOOFIE_ALTRO_FAIL)); 
  }    

  /* Now read the data from IMEM */
  ret = ALTRO_Get(handle, &in);
  if (ret)
  {
    DEBUG_TEXT(P_ID_GOOFIE, 5 ,("Goofie_EVLEN_Read: Error from ALTRO_Get\n"))
    return(RCC_ERROR_RETURN(ret, GOOFIE_ALTRO_FAIL)); 
  }

  DEBUG_TEXT(P_ID_GOOFIE, 20 ,("Goofie_EVLEN_Read: %d bytes received from the GOOFIE\n", in.nbytes))
  if (in.nbytes != 2)
  {
     DEBUG_TEXT(P_ID_GOOFIE, 5 ,("Goofie_EVLEN_Read: Wrong number of bytes received\n"))
     return(RCC_ERROR_RETURN(0, GOOFIE_ODD_BYTES)); 
  }

  *data = in.data[0] + (in.data[1] << 8);

  return(GOOFIE_SUCCESS);
} 


/****************************************************************************/
u_int Goofie_DMEM_Read(u_int channel, u_int isize, u_int *osize, u_int data[])
/****************************************************************************/
{
  altro_bulk_out_t out;
  altro_bulk_in_t in;
  u_short *msg, *usp, us1, us2, us3, us4;
  u_int dcount, ret, loop;
  
  GOOFIEISOPEN
 
  if (isize > S_DMEM)
  {    
    DEBUG_TEXT(P_ID_GOOFIE, 5 ,("Goofie_DMEM_Read: Parameter isize is out of range\n")) 
    return(RCC_ERROR_RETURN(0, GOOFIE_RANGE));
  } 
 
  /* Construct a message */
  msg = (u_short *)&out.data[0];
  msg[0] = (isize << 5) + I_RDDTM; 
  msg[1] = (channel - 1) * O_CHANNEL;
  out.nbytes = 4;

  /* Tell the GOOFIE that we want to read the DMEM */
  ret = ALTRO_Send(handle, &out);
  if (ret)
  {      
    DEBUG_TEXT(P_ID_GOOFIE, 5 ,("Goofie_DMEM_Read: Error from ALTRO_Send\n"))
    return(RCC_ERROR_RETURN(ret, GOOFIE_ALTRO_FAIL)); 
  }    

  /* Now read the data from DMEM */
  ret = ALTRO_Get(handle, &in);
  if (ret)
  {
    DEBUG_TEXT(P_ID_GOOFIE, 5 ,("Goofie_DMEM_Read: Error from ALTRO_Get\n"))
    return(RCC_ERROR_RETURN(ret, GOOFIE_ALTRO_FAIL)); 
  }

  DEBUG_TEXT(P_ID_GOOFIE, 20 ,("Goofie_DMEM_Read: %d bytes received from the GOOFIE\n", in.nbytes))
  if (in.nbytes != (8 * isize))
  {
    DEBUG_TEXT(P_ID_GOOFIE, 5 ,("Goofie_DMEM_Read: Wrong number of bytes received\n"))
    *osize = 0;
    return(RCC_ERROR_RETURN(0, GOOFIE_ODD_BYTES)); 
  }
  
  *osize = isize;
  dcount = 0;
  usp = (u_short *) &in.data[0];
  for(loop = 0; loop < isize; loop++)
  {
    us1 = (*usp++) & 0x3ff;
    us2 = (*usp++) & 0x3ff;
    us3 = (*usp++) & 0x3ff;
    us4 = (*usp++) & 0x3ff;
    DEBUG_TEXT(P_ID_GOOFIE, 5 ,("Goofie_DMEM_Read: loop %d: us1=0x%04x  us2=0x%04x  us3=0x%04x  us4=0x%04x\n", loop, us1, us2, us3, us4));
    data[dcount++] = us1 + (us2 << 10) + (us3 << 20) + ((us4 && 0x3) << 30); 
    data[dcount++] = us4 >> 2; 
  }
  
  return(GOOFIE_SUCCESS); 
}


/*****************************************************************/
u_int Goofie_PMEM_Write (u_int channel, u_int offset, u_short data)
/*****************************************************************/
{
  altro_bulk_out_t out;
  u_short *msg;
  u_int ret;
  
  GOOFIEISOPEN

  /* Construct a message */
  msg = (u_short *)&out.data[0];
  msg[0] = I_WRPDM; 
  msg[1] = (channel - 1) * O_CHANNEL + offset;
  msg[2] = data;
  out.nbytes = 6;  /*  16 bit word to byte */

  /* Tell the GOOFIE that we want to write the PMEM  */
  ret = ALTRO_Send(handle, &out);
  if (ret)
  {      
    DEBUG_TEXT(P_ID_GOOFIE, 5 ,("Goofie_PMEM_Write: Error from ALTRO_Send\n"))
    return(RCC_ERROR_RETURN(ret, GOOFIE_ALTRO_FAIL)); 
  }    

  return(GOOFIE_SUCCESS);   
}


/****************************************************************/
u_int Goofie_PMEM_Read(u_int channel, u_int offset, u_short *data)
/****************************************************************/
{
  altro_bulk_out_t out;
  altro_bulk_in_t in;
  u_short *msg, *ptr;
  u_int ret;
  
  GOOFIEISOPEN
 
  /* Construct a message */
  msg = (u_short *)&out.data[0];
  msg[0] = I_RDPDM; 
  msg[1] = (channel - 1) * O_CHANNEL + offset;
  out.nbytes = 4;

  /* Tell the GOOFIE that we want to read the PMEM  */
  ret = ALTRO_Send(handle, &out);
  if (ret)
  {      
    DEBUG_TEXT(P_ID_GOOFIE, 5 ,("Goofie_PMEM_Read: Error from ALTRO_Send\n"))
    return(RCC_ERROR_RETURN(ret, GOOFIE_ALTRO_FAIL)); 
  }    

  /* Now read the data from PMEM */
  ret = ALTRO_Get(handle, &in);
  if (ret)
  {
    DEBUG_TEXT(P_ID_GOOFIE, 5 ,("Goofie_PMEM_Read: Error from ALTRO_Get\n"))
    return(RCC_ERROR_RETURN(ret, GOOFIE_ALTRO_FAIL)); 
  }

  DEBUG_TEXT(P_ID_GOOFIE, 20 ,("Goofie_PMEM_Read: %d bytes received from the GOOFIE\n", in.nbytes))
  if (in.nbytes != 2)
  {
    DEBUG_TEXT(P_ID_GOOFIE, 5 ,("Goofie_PMEM_Read: Wrong number of bytes received\n"))
    return(RCC_ERROR_RETURN(0, GOOFIE_ODD_BYTES)); 
  }
  
  /* copy the data from the altro_bulk_in_t structure to the data variable */
  ptr = (u_short *) &in.data[0];
  *data = *ptr;

  return(GOOFIE_SUCCESS); 
}
		
  
/*******************************************/
u_int Goofie_CSR_Write(u_int reg, u_int data)
/*******************************************/
{
  int ret;
  altro_bulk_out_t out;
  u_short *msg;

  DEBUG_TEXT(P_ID_GOOFIE, 20 ,("Goofie_CSR_Write: reg = %d  data = %d\n", reg, data))

  GOOFIEISOPEN
  
  if ((reg < O_BSLCT) || (reg > O_PRTRG))
  {    
    DEBUG_TEXT(P_ID_GOOFIE, 5 ,("Goofie_CSR_Write: The parameter reg is out of range (reg = %d)\n", reg))
    return(RCC_ERROR_RETURN(0, GOOFIE_RANGE)); 
  } 
  
  /* Assemble the message */
  msg = (u_short *)&out.data[0];
  msg[0] = I_WRREG; 
  msg[1] = reg;
  msg[2] = data & 0x3ff;  /*longest register has 10 bits*/
  out.nbytes = 6;

  /* Tell the GOOFIE that we want to write a register  */
  ret = ALTRO_Send(handle, &out);
  if (ret)
  {      
    DEBUG_TEXT(P_ID_GOOFIE, 5 ,("Goofie_CSR_Write: Error from ALTRO_Send\n"))
    return(RCC_ERROR_RETURN(ret, GOOFIE_ALTRO_FAIL)); 
  }    
  
  return(GOOFIE_SUCCESS);
}


/*******************************************/
u_int Goofie_CSR_Read(u_int reg, u_int *data)
/*******************************************/
{
  int ret;
  altro_bulk_out_t out;
  altro_bulk_in_t in;
  u_short *msg;
  
  GOOFIEISOPEN
 
  if ((reg < O_BSLCT) || (reg > O_PRTRG))
  {    
   DEBUG_TEXT(P_ID_GOOFIE, 5 ,("Goofie_CSR_Read: The parameter reg is out of range (reg = %d)\n", reg))
   return(RCC_ERROR_RETURN(0, GOOFIE_RANGE)); 
  }

  /* Assemble the message */
  msg = (u_short *)&out.data[0];
  msg[0] = I_RDREG; 
  msg[1] = reg;
  out.nbytes = 4;
  
  /* Tell the GOOFIE that we want to read a register  */
  ret = ALTRO_Send(handle, &out);
  if (ret)
  {      
    DEBUG_TEXT(P_ID_GOOFIE, 5 ,("Goofie_CSR_Read: Error from ALTRO_Send\n"))
    return(RCC_ERROR_RETURN(ret, GOOFIE_ALTRO_FAIL)); 
  }    

  /* Now read the content of the CSR */
  ret = ALTRO_Get(handle, &in);
  if (ret)
  {
    DEBUG_TEXT(P_ID_GOOFIE, 5 ,("Goofie_CSR_Read: Error from ALTRO_Get\n"))
    return(RCC_ERROR_RETURN(ret, GOOFIE_ALTRO_FAIL)); 
  }

  DEBUG_TEXT(P_ID_GOOFIE, 20 ,("Goofie_CSR_Read: %d bytes received from the GOOFIE\n", in.nbytes))
  if (in.nbytes != 2)
  {
     DEBUG_TEXT(P_ID_GOOFIE, 5 ,("Goofie_CSR_Read: Wrong number of bytes received\n"))
     return(RCC_ERROR_RETURN(0, GOOFIE_ODD_BYTES)); 
  }

  *data = in.data[0] + (in.data[1] << 8);

  return(GOOFIE_SUCCESS);
}


/**********************************************************/
u_int Goofie_File_Read(char *name, u_int size, u_int data[])
/**********************************************************/
{
  u_int loop;
  FILE *inf;
  
  GOOFIEISOPEN
  
  /* open the input file */
  inf = fopen(name, "r");
  if (inf == 0)
  {
    DEBUG_TEXT(P_ID_GOOFIE, 20 ,("Goofie_File_Read:  Can't open input file\n")) 
    return(RCC_ERROR_RETURN(0, GOOFIE_FILE));
  }

  /* read  the file */
  for (loop = 0; loop < size; loop++)
  {
    fscanf(inf,"%x",&data[loop]);
    if (feof(inf))
    {  
      DEBUG_TEXT(P_ID_GOOFIE, 20 ,("Goofie_File_Read: The file is too short\n")) 
      return(RCC_ERROR_RETURN(0, GOOFIE_NODATA));
    }
  }
  fclose(inf);
  return(GOOFIE_SUCCESS);
}

  
/***********************************************************/
u_int Goofie_File_Write(char *name, u_int size, u_int data[])
/***********************************************************/
{
  u_int loop;
  FILE *outf;

  GOOFIEISOPEN

  /*Check if the file exists*/
  outf = fopen(name, "r");
  if (outf!=0)
  {
    DEBUG_TEXT(P_ID_GOOFIE, 5 ,("Goofie_File_Write:  File does already exist\n")) 
    return(RCC_ERROR_RETURN(0, GOOFIE_FILE2));
  }

  outf = fopen(name, "w");
  if (outf==0)
  {
    DEBUG_TEXT(P_ID_GOOFIE, 5 ,("Goofie_File_Write:  Can't open output file\n")) 
    return(RCC_ERROR_RETURN(0, GOOFIE_FILE));
  }
  
  /* write  the file */
  for (loop = 0; loop < size; loop++)
    fprintf(outf,"%x\n",data[size]);

  fclose(outf);
  return(GOOFIE_SUCCESS);  
}


/**************************************/
u_int Goofie_Exec_Command(u_int command)
/**************************************/
{
  altro_bulk_out_t out;
  u_short *msg;
  u_int ret;
  
  GOOFIEISOPEN

  if ((command != I_SWTRG) && (command != I_SWRST) && (command != I_TWRST))
  {    
    DEBUG_TEXT(P_ID_GOOFIE, 5 ,("Goofie_Exec_Command:  The parameter <command> is out of range\n")) 
    return(RCC_ERROR_RETURN(0, GOOFIE_RANGE));
  } 

  /* Construct a message */
  msg = (u_short *)&out.data[0];
  msg[0] = command; 
  out.nbytes = 2;
   
  /* Send the message  */
  ret = ALTRO_Send(handle, &out);
  if (ret)
  {      
    DEBUG_TEXT(P_ID_GOOFIE, 5 ,("Goofie_Exec_Command: Failed to close the ALTRO library\n"))
    return(RCC_ERROR_RETURN(ret, GOOFIE_ALTRO_FAIL)); 
  }      

  return(GOOFIE_SUCCESS);  
}


/******************************************************************/
unsigned int Goofie_err_get(err_pack err, err_str pid, err_str code)
/*************************s****************************************/
{ 
  strcpy(pid, P_ID_ALTRO_STR);

  switch (RCC_ERROR_MINOR(err))
  {  
     case GOOFIE_SUCCESS:    strcpy(code, GOOFIE_SUCCESS_STR);     break;
     case GOOFIE_FILE:       strcpy(code, GOOFIE_FILE_STR);        break;
     case GOOFIE_FILE2:      strcpy(code, GOOFIE_FILE2_STR);       break;
     case GOOFIE_ALTRO_FAIL: strcpy(code, GOOFIE_ALTRO_FAIL_STR);  break;
     case GOOFIE_RANGE:      strcpy(code, GOOFIE_RANGE_STR);       break;
     case GOOFIE_ODD_BYTES:  strcpy(code, GOOFIE_ODD_BYTES_STR);   break;
     case GOOFIE_NODATA:     strcpy(code, GOOFIE_NODATA_STR);      break;
     case GOOFIE_NOTOPEN:    strcpy(code, GOOFIE_NOTOPEN_STR);     break;
     default:                strcpy(code, GOOFIE_NO_CODE_STR);     return(RCC_ERROR_RETURN(0, GOOFIE_NO_CODE)); break;
  }
  return(RCC_ERROR_RETURN(0, GOOFIE_SUCCESS));
}

