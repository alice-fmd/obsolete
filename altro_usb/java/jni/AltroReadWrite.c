#include <jni.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/file.h>
#include <signal.h>
#include <fec/fec.h>
#include "JniAltroReadWrite.h"
#include "AltroReadWrite.h"


JNIEXPORT jint JNICALL Java_core_jni_fec_AltroReadWrite_initialiseFECs
  (JNIEnv *env, jobject obj, jint fecAddress, jintArray statusArray)
  {
	  jint ret, /* index, */ handle=0;
	  jint *carr = NULL;
	  
	  rcc_error_set_debug(0, 15);
	  
	  /* Open FEC_LIB */
      ret = FEC_Open(DEFAULT_NODE, &handle);  
      CHECK_RET(ret);
      
      /* Open U2F_LIB */
	  ret = U2F_Open(DEFAULT_NODE, &handle, HW);
	  CHECK_RET(ret);
	 
	  if(carr== NULL)
     	return (-1); /* exception occured */
     	
	  /* Get the pointer to the primitive array */
      carr = (*env)->GetIntArrayElements (env, statusArray, NULL);
      
      /* Power On the FECs */
      ret = FEC_Power_Reset(handle, fecAddress);
      CHECK_RET(ret);
      
      /* Read the Status of FECs*/
      ret = FEC_All_Status(handle, &carr[0]);
      CHECK_RET(ret);
      
  	  /* This will release the pointer to the primitive array */
      (*env) -> ReleaseIntArrayElements(env, statusArray, carr,0);
      
      /* Close the U2F */
      ret = U2F_Close(handle);
      CHECK_RET(ret);
      
      /* Close FEC_LIB */
	  ret = FEC_Close(handle);
	  CHECK_RET(ret);
      
      return 0;
  }

JNIEXPORT jint JNICALL Java_core_jni_fec_AltroReadWrite_multiReadAltroReg
  (JNIEnv *env, jobject obj, jint fecAddr, jint channAddr, jint regAddr, jintArray regArray, jint loop)
  {
  	  jint ret, index=0, handle=0;
	  jint *carr = NULL;
       /* Read the register */
        u_int ch_addr,reg_data = 0xab;

      printf("\nC: Entering multiReadAltroReg");
	  
	  /* Open FEC_LIB */
      ret = FEC_Open(DEFAULT_NODE, &handle);  
      CHECK_RET(ret);
      
      /* Open U2F_LIB */
	  ret = U2F_Open(DEFAULT_NODE, &handle, HW);
	  CHECK_RET(ret);
	 
	  if(carr== NULL)
     	return (-1); /* exception occured */
     	
	  /* Get the pointer to the primitive array */
      carr = (*env)->GetIntArrayElements (env, regArray, NULL);
        
        ch_addr = channAddr;
        
        printf("\n 1 - Values for Channel Address[%x], Reg Address [%x], Reg Data[%x] ",ch_addr,regAddr,reg_data);
        
        ret= fecAddressToChannelAddressMapping(fecAddr, &ch_addr);
		CHECK_RET(ret)	
		
		printf("\n 2 - Values for Channel Address[%x], Reg Address [%x], Reg Data[%x] ",ch_addr,regAddr,reg_data);
		
		/* ret=FEC_Write_ALTRO_Reg(handle, ch_addr,regAddr, reg_data);
		   CHECK_RET(ret) */
		
		reg_data=0;
		
		printf("\n 3 - Values for Channel Address[%x], Reg Address [%x], Reg Data[%x] ",ch_addr,regAddr,reg_data);
     
      /* Read the register for N times */
      for (/*index*/; index < loop; index++)
      {
      	
      	ret=FEC_Read_ALTRO_Reg(handle, ch_addr,regAddr, &reg_data);
      	/*ret = Read_ALTRO_Reg(handle,fecAddr, ch_addr, regAddr, &reg_data);*/
      	CHECK_RET(ret)
      	
      	carr[index] = reg_data;
      	printf("\n 4 - Reg %x, of FEC %d, has value: 0x%x\n", regAddr, fecAddr, carr[index]);
      }
       
      /* This will release the pointer to the primitive array */
      (*env) -> ReleaseIntArrayElements(env, regArray, carr,0);
      
      /* Close the U2F */
      ret = U2F_Close(handle);
      CHECK_RET(ret);
  
       /* Close FEC_LIB */
	  ret = FEC_Close(handle);
	  CHECK_RET(ret);
	       
      printf("\nC: Leaving multiReadAltroReg");
      
      return ret;
  
  }
  
  JNIEXPORT jint JNICALL Java_core_jni_fec_AltroReadWrite_readAltroReg
  (JNIEnv *env, jobject obj, jint fecAddr, jint channAddr, jint regAddr, jintArray data)
  {
  
  	jint ret, /*index=0,*/ handle=0;
	  jint *carr=NULL;
      /* Read the register */
        u_int ch_addr,reg_data = 0;
        

    printf("\nC: Entering readAltroReg");
	  
	  /* Open FEC_LIB */
      ret = FEC_Open(DEFAULT_NODE, &handle);  
      CHECK_RET(ret);
      
      /* Open U2F_LIB */
	  ret = U2F_Open(DEFAULT_NODE, &handle, HW);
	  CHECK_RET(ret);
	 
	  if(carr== NULL)
     	return (-1); /* exception occured */
     	
	  /* Get the pointer to the primitive array */
      carr = (*env)->GetIntArrayElements (env, data, NULL);
      
        ch_addr = channAddr;
        
        printf("\n 1 - Values for Channel Address[%x], Reg Address [%x], Reg Data[%x] ",ch_addr,regAddr,reg_data);
        
        ret= fecAddressToChannelAddressMapping(fecAddr, &ch_addr);
		CHECK_RET(ret)	
		
		printf("\n 2 - Values for Channel Address[%x], Reg Address [%x], Reg Data[%x] ",ch_addr,regAddr,reg_data);
		
		ret=FEC_Read_ALTRO_Reg(handle, ch_addr,regAddr, &reg_data);
		CHECK_RET(ret) 
      	
      	/*ret = Read_ALTRO_Reg(handle,fecAddr, ch_addr, regAddr, &reg_data);*/
      	
      	carr[0] = reg_data;
      	printf("\n Done - Reg %x, of FEC %d, has value: 0x%x\n", regAddr, fecAddr, carr[0]);
      	CHECK_RET(ret)
       
      /* This will release the pointer to the primitive array */
      (*env) -> ReleaseIntArrayElements(env, data, carr,0);
      
      /* Close the U2F */
      ret = U2F_Close(handle);
      CHECK_RET(ret);
      
      /* Close FEC_LIB */
	  ret = FEC_Close(handle);
	  CHECK_RET(ret);
	        
      printf("\nC: Leaving readAltroReg");
      return ret;
  }

JNIEXPORT jint JNICALL Java_core_jni_fec_AltroReadWrite_writeAltroReg
  (JNIEnv *env, jobject obj, jint fecAddr, jint channAddr, jint regAddr, jint data, jint loop)
  {
  	  jint ret, index=0, handle=0;
	  /* jint *carr = NULL; */
	  
       /* Read the register */
        u_int ch_addr,reg_data = 0;
        
      printf("\nC: Entering writeAltroReg");
	  /* Open FEC_LIB */
      ret = FEC_Open(DEFAULT_NODE, &handle);  
      CHECK_RET(ret);
      
      /* Open U2F_LIB */
	  ret = U2F_Open(DEFAULT_NODE, &handle, HW);
	  CHECK_RET(ret);
	 
        ch_addr = channAddr;
        
        printf("\n 1 - Values for Channel Address[%x], Reg Address [%x], Reg Data[%x] ",ch_addr,regAddr,reg_data);
        
        ret= fecAddressToChannelAddressMapping(fecAddr, &ch_addr);
		CHECK_RET(ret)	
		
		printf("\n 2 - Values for Channel Address[%x], Reg Address [%x], Reg Data[%x] ",ch_addr,regAddr,reg_data);
		
		/* Copy the data from JVM to C */	
		reg_data = data;		
		
      /* Read the register for N times */
      for (/*index*/; index < loop; index++)
      {
      	ret=FEC_Write_ALTRO_Reg(handle, ch_addr,regAddr, reg_data);
      	CHECK_RET(ret)
      	
      	printf("\n Done - Reg %x, of FEC %d, has value: 0x%x\n", regAddr, fecAddr, reg_data);
      }
       
      /* This will release the pointer to the primitive array 
      (*env) -> ReleaseIntArrayElements(env, dataArray, carr,0);
      */
      
      /* Close the U2F */
      ret = U2F_Close(handle);
      CHECK_RET(ret);

      /* Close FEC_LIB */
	  ret = FEC_Close(handle);
	  CHECK_RET(ret);
	        
      printf("\nC: Leaving writeAltroReg");
      
      return 0;
  }

/**************************************************************************************************/
u_int Read_ALTRO_Reg(int handle,u_int fec_addr, u_int ch_addr, u_int reg_addr, u_int *reg_data) 
/**************************************************************************************************/
{
  u_int ret;
  
  ret= fecAddressToChannelAddressMapping(fec_addr,&ch_addr);
  CHECK_RET(ret)	
  /*ret=FEC_Read_ALTRO_Reg(handle, ch_addr,reg_addr, &reg_data);*/
  CHECK_RET(ret) 
  return ret;
}

/* Omer Khalid: Function overloading with FEC address input parameter */
/***************************************************************************************************/
u_int Write_ALTRO_Reg(int handle, u_int fec_addr, u_int ch_addr, u_int reg_addr, u_int reg_data) 
/***************************************************************************************************/
{
  u_int ret;
  ret= fecAddressToChannelAddressMapping(fec_addr,&ch_addr);
  CHECK_RET(ret)
  ret=FEC_Write_ALTRO_Reg(handle, ch_addr, reg_addr, reg_data);
  CHECK_RET(ret)
  return ret;
}

/***************************************/
u_int fecAddressToChannelAddressMapping(u_int fec_addr, u_int *ch_addr)
/***************************************/
{
  /* OK: Channel address based on the FEC address */
  *ch_addr = ((fec_addr << 7) & 0xf80) | (*ch_addr & 0x7f);
  DEBUG_TEXT(P_ID_FEC, 15 ,("Channel Address = %x\n", *ch_addr));
  return 0;
}
