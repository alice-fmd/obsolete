#include <jni.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/file.h>
#include <signal.h>
#include <fec/fec.h>
#include "BCReadWrite.h"

JNIEXPORT jint JNICALL Java_core_jni_fec_BCReadWrite_readBCRegister (JNIEnv *env, jobject obj, jint fecAddr, jint regAddr, jintArray data)
{
  	  jint ret = 0;
  	  /* u_int index=0; */
  	  int handle;
  	  
	  jint *carr = NULL;
	  
        u_int fec_addr;
        u_int reg_data=0;
        
	printf("\nC: Entering BCReadWrite_readBCRegister");
	
	  /* Open FEC_LIB */
      ret = FEC_Open(DEFAULT_NODE, &handle);  
      CHECK_RET(ret);
      
      /* Open U2F_LIB */
	  ret = U2F_Open(DEFAULT_NODE, &handle, HW);
	  CHECK_RET(ret);
	 
	  if(carr== NULL)
     	return (-1); /* exception occured */
     	
	  /* Get the pointer to the primitive array */
      carr = (*env)->GetIntArrayElements (env, data, NULL);
      
       /* Read the register */
        fec_addr = fecAddr;
        
        printf("\n 1 - Values for FEC Address[%x], Reg Address [%x], Reg Data[%x] ",fec_addr,regAddr,reg_data);

      	ret = FEC_BC_Reg_Read(handle, fec_addr,regAddr, &reg_data);
      	/*ret = Read_ALTRO_Reg(handle,fecAddr, ch_addr, regAddr, &reg_data);*/
      	CHECK_RET(ret)
      	
      	carr[0] = reg_data;
      	printf("\n 2 - Reg %x, of FEC %d, has value: 0x%x\n", regAddr, fec_addr, carr[0]);
       
      /* This will release the pointer to the primitive array */
      (*env) -> ReleaseIntArrayElements(env, data, carr,0);
      
      /* Close the U2F */
      ret = U2F_Close(handle);
      CHECK_RET(ret);
      
      /* Close FEC_LIB */
	  ret = FEC_Close(handle);
	  CHECK_RET(ret);
	  
      printf("\nC: Leaving BCReadWrite_readBCRegister");
      
      return ret;
}

JNIEXPORT jint JNICALL Java_core_jni_fec_BCReadWrite_readAllBCRegister (JNIEnv *env, jobject obj, jint fecAddr, jintArray regAddr, jintArray data, jint loop)
{
  	  jint ret = 0;
  	  int index=0;
  	  int handle;
  	  
	  jint *carr = NULL;
	  jint *reg_addr = NULL;
	  
        u_int fec_addr;
        u_int reg_data=0;
        
        
	printf("\nC: Entering BCReadWrite_readAllBCRegister");
	
	  /* Open FEC_LIB */
      ret = FEC_Open(DEFAULT_NODE, &handle);  
      CHECK_RET(ret);
      
      /* Open U2F_LIB */
	  ret = U2F_Open(DEFAULT_NODE, &handle, HW);
	  CHECK_RET(ret);
	 
	  if((carr== NULL) || (reg_addr== NULL))
     	return (-1); /* exception occured */
     	
	  /* Get the pointer to the primitive array */
      carr = (*env)->GetIntArrayElements (env, data, NULL);
      reg_addr = (*env)->GetIntArrayElements (env, regAddr, NULL);
      
       /* Read the register */
        fec_addr = fecAddr;
        
        printf("\n 1 - Values for FEC Address[%x], Reg Address [%p], Reg Data[%x] ",fec_addr,regAddr,reg_data);
 
      /* Read the register for N times */
      for (/* index */; index < loop; index++)
      {
      	
      	ret = FEC_BC_Reg_Read(handle, fec_addr,reg_addr[index], &reg_data);
      	/*ret = Read_ALTRO_Reg(handle,fecAddr, ch_addr, regAddr, &reg_data);*/
      	CHECK_RET(ret)
      	
      	carr[index] = reg_data;
      	printf("\n 2 - Reg %p, of FEC %d, has value: 0x%x\n", regAddr, fec_addr, carr[index]);
      }
       
      /* This will release the pointer to the primitive array */
      (*env) -> ReleaseIntArrayElements(env, data, carr,0);
      (*env) -> ReleaseIntArrayElements(env, regAddr, reg_addr,0);
      
      /* Close the U2F */
      ret = U2F_Close(handle);
      CHECK_RET(ret);
      
      /* Close FEC_LIB */
	  ret = FEC_Close(handle);
	  CHECK_RET(ret);
	  
      printf("\nC: Leaving BCReadWrite_readAllBCRegister");
      
      return ret;
}
  
JNIEXPORT jint JNICALL Java_core_jni_fec_BCReadWrite_writeBCRegister (JNIEnv *env, jobject obj, jint fecAddr, jint regAddr, jint data)
{
  	jint ret;
  	jint handle=0;
	  
    /* Read the register */
    u_int reg_data = 0;
 
  	printf("\nC: Entering BCReadWrite_writeBCRegister");
	/* Open FEC_LIB */
    ret = FEC_Open(DEFAULT_NODE, &handle);  
    CHECK_RET(ret);
      
    /* Open U2F_LIB */
	ret = U2F_Open(DEFAULT_NODE, &handle, HW);
	CHECK_RET(ret);
	 
    printf("\n 1 - Values for Fec Address[%x], Reg Address [%x], Reg Data[%x] ",fecAddr,regAddr,reg_data);
		
	/* Copy the data from JVM to C */	
	reg_data = data;		
		
    /* Read the register for N times */
    ret = FEC_BC_Reg_Write(handle, fecAddr,regAddr, reg_data);
    CHECK_RET(ret)
      	
    printf("\n 2 Done - Reg %x, of FEC %d, written value: 0x%x\n", regAddr, fecAddr, reg_data);
      	
    /* Close the U2F */
    ret = U2F_Close(handle);
    CHECK_RET(ret);

    /* Close FEC_LIB */
	ret = FEC_Close(handle);
	CHECK_RET(ret);
	        
    printf("\nC: Leaving BCReadWrite_writeBCRegister");
      
    return ret;
}
