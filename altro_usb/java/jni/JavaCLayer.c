#include <jni.h>
#include <stdio.h>
#include <sys/types.h>
#include "JavaCLayer.h"

JNIEXPORT jint JNICALL Java_core_jni_JavaCLayer_readBoardController
  (JNIEnv *env, jobject obj, jint regCode)
  {
    jfloat temp=0.0, ana_v=0.0, ana_i=0.0, dig_v=0.0, dig_i=0.0;
    jint ret,handle=0, read_FEC_mADC=0;
	u_int pid=0, level=5;
	/* pid=0; level=5; */
	u_int rcode;
	u_int rdata;
	u_int data_mask;
	rcode=0, rdata = 0;
	rcc_error_set_debug(pid, level);
	
	/* Open FEC_LIB */
    ret = FEC_Open(DEFAULT_NODE, &handle);
    if (ret)
    {
      rcc_error_print(stdout, ret);
      return ret;
    }
    		
	/*********************************************************/
    /*    Write and Read U2F Register                        */
    /*********************************************************/
    /* This code is taken from U2FScope */
    
   	ret = ts_open(1, TS_DUMMY);
  	if (ret)
    rcc_error_print(stdout, ret);
    
    ret = U2F_Open(DEFAULT_NODE, &handle, HW);
      if (ret)
      {
        rcc_error_print(stdout, ret);
        return(-1);
      }
      
      /* Set the register code */
      rcode = O_TRCFG1; 
	  data_mask = 0xffffffff;
      rdata = 0xabcd;
      /* write to a register */
      printf("The data to be written to register contains 0x%08x\n", rdata & data_mask);  
      ret = U2F_Reg_Write(handle, regCode, rdata);
        if (ret)
        {
           rcc_error_print(stdout, ret);
           return(-1);
        }
        
      /* Read from the register */
      ret = U2F_Reg_Read(handle, regCode, &rdata);
        if (ret)
        {
           rcc_error_print(stdout, ret);
           return(-1);
        } 

        printf("The register contains 0x%08x\n", rdata & data_mask);  
        
      /* Close the U2F */
      ret = U2F_Close(handle);
      if (ret)
      {
        rcc_error_print(stdout, ret);
        return(-1);
      }
      
	/*********************************************************/
    /*    READ  FEC Monitoring ADC                           */
    /*********************************************************/
  if(read_FEC_mADC)
  {
    printf("\n\nAbout to Call FEC_BC_Mon!\n");
  	printf("-Inside the if statement\n");
    ret = FEC_BC_Mon(handle, 1, &temp, &ana_v, &ana_i, &dig_v, &dig_i);
    printf("-Executed FEC_BC_Mon and going to validate 'ret'\n");
    if (ret)
    {
      printf("\nFEC_BC_Mon failed\n");
      rcc_error_print(stdout, ret);
      return ret;
    }
    printf("-NO ret Error, next of screen should be the data\n");
    printf("\nTemp: %f\nAna_V: %f\nAna_I: %f\nDig_V: %f\nDig_I: %f\n", temp, ana_v, ana_i, dig_v, dig_i);
  }
  	return rdata;
    /* ts_close(TS_DUMMY); */
    /* exit(0); */
  	/* return; */
  }
 
 /*
 * This method will return an array which will hold the registers values of a U2F
 */
JNIEXPORT jint JNICALL Java_core_jni_JavaCLayer_readU2FRegisters
  (JNIEnv *env, jobject obj, jintArray rData)
  {
  	 jint ret, index, handle=0;
  	 jint pid=0, level=5; /*pid=0; level=5;*/
     /* static jint rData[11]; */
     jint *carr = NULL;
     u_int data_mask = 0xffffffff, data;
     
     /* Set the debug parameters */
     rcc_error_set_debug(pid, level);
     
     /* Get the pointer to the primitive array */
     carr = (*env)->GetIntArrayElements (env, rData, NULL);
     
     if(carr== NULL)
     	return (-1); /* exception occured */
     	
  	/*********************************************************/
    /*    Write and Read U2F Register                        */
    /*********************************************************/
    /* This code is taken from U2FScope */
    
   	ret = ts_open(1, TS_DUMMY);
  	if (ret)
    rcc_error_print(stdout, ret);
    
    ret = U2F_Open(DEFAULT_NODE, &handle, HW);
      if (ret)
      {
        rcc_error_print(stdout, ret);
        return(-1);
      }
      
      for( index=0; index < 11; index++)
      {
      	/* Read from the register */
      	ret = U2F_Reg_Read(handle, index , &data); /* &carr[index]); */
	carr[index] = data;
        if (ret)
        {
           rcc_error_print(stdout, ret);
           return(-1);
        } 

        printf("The register contains 0x%08x\n", carr[index] & data_mask);  
      }
      
      /* Close the U2F */
      ret = U2F_Close(handle);
      if (ret)
      {
        rcc_error_print(stdout, ret);
        return(-1);
      }
      
      /* This will release the pointer to the primitive array */
      (*env) -> ReleaseIntArrayElements(env, rData, carr,0);
      return 0;
  }
