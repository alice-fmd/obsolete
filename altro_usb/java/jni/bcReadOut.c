#include <jni.h>
#include <stdio.h>
#include <fec/fec.h>
#include "BoardControlReadOut.h"


JNIEXPORT void JNICALL Java_BoardControlReadOut_readBoardController
  (JNIEnv *env, jobject obj)
  {
    jfloat temp=0, ana_v=0, ana_i=0, dig_v=0, dig_i=0;
    jint ret;
	
	printf("About to Call FEC_BC_Mon!\n");
    ret = FEC_BC_Mon(1, 0, &temp, &ana_v, &ana_i, &dig_v, &dig_i);
	printf("\nTemp: %f\nAna_V: %f\nAna_I: %f\nDig_V: %f\nDig_I: %f\n", temp, ana_v, ana_i, dig_v, dig_i);
  	return;
  }
