/**
 * 
 */
package core.database;

import java.sql.*;

import core.database.data.U2FData;
import core.global.GlobalVariables;

/**
 * @author okhalid
 *
 */
public class ReadDataFromDatabase {

	Connection connection = null;					//For the connection to database
	String url = GlobalVariables.mySQLUrl;  		// The URL string is set from the global variables
	String mySQLUser = GlobalVariables.mySQLUser;	// MySQL username
	String mySQLPass = GlobalVariables.mySQLPass;   // MySQL password
	PreparedStatement stmt;							// The statement object would be required to execute statments
	ResultSet resultSet = null;							// The resultset object will fetch the data from the DB
	String error;									// Error string to store error information
	U2FData u2fDataObject = null;				// Local copy of the U2FData object
	String[] channelNum = null;
	
	/* Following are the SQL Statements. ? reprensents the places where values would be injected at the later stage
	 * from the statements are called. 
	 */
	String selectAltroChannels = " SELECT ?,?,?,?,?,?,? FROM ALTRO WHERE ID = ?";
	
	public ReadDataFromDatabase() {}
	
	public void getConfiguration()
	{
	  try
		{
		  /* connect to the database */
		  connectToMySQL();
		
		  /* get the configuration from the database */
		
		  /* first create a local instance of the U2F Data object */
		  u2fDataObject = new U2FData();	
		
		  /* get altro configurations from the database */
		  getAltroConfiguration();
		
		  /* get FEC configurations from the database */ 
		  getFECConfiguration();
		
		  /* get the U2F configurations from the database */
		  getU2FConfiguration();
		
		  /* now copy the local copy of U2FData object to the static and global copy in global variables */ 
		 // GlobalVariables.u2fDatafromDB = u2fDataObject;
		  //u2fDataObject = null;
		}
	  	catch (Exception e)
		{
			error = e.toString();
			System.out.println("getConfiguration():" + error);
			closeConnection();
		}
	  	
	  	/* Close the connection */
	  	closeConnection();
	}

	private void getU2FConfiguration()
	{
		
	}
	
	private void getFECConfiguration()
	{
		
	}
	
	private void getAltroConfiguration()
	{
		int[] array = new int[GlobalVariables.MaxPerFecAltros];
		
		int perAltroChannelIndex=0;
		
		for (int altroCount=0; altroCount < GlobalVariables.MaxPerFecAltros; altroCount++)
		{
			for (int channelCount=0; channelCount < GlobalVariables.MaxPerAltroChannels; channelCount++ )
			{
				perAltroChannelIndex=0;
				
				/* set the correct channel number string */
				channelNumString(channelCount);
				
				try
				{
					stmt = connection.prepareStatement(selectAltroChannels);
					
					stmt.setString(1,channelNum[perAltroChannelIndex]);
					stmt.setString(2,channelNum[perAltroChannelIndex++] );
					stmt.setString(3,channelNum[perAltroChannelIndex++] );
					stmt.setString(4,channelNum[perAltroChannelIndex++] );
					stmt.setString(5,channelNum[perAltroChannelIndex++] );
					stmt.setString(6,channelNum[perAltroChannelIndex++] );
					stmt.setString(7,channelNum[perAltroChannelIndex++] );
					
					stmt.setInt(8,altroCount + 1);
					
					/* Executes the above query */
					resultSet = stmt.executeQuery();	
					
					/* But before getting the results, resets this integer */
					perAltroChannelIndex=0;
					
					while(resultSet.next())
					{
						array[perAltroChannelIndex]  = resultSet.getInt("K1_CHANNEL00");	
						array[perAltroChannelIndex++]  = resultSet.getInt("K2_CHANNEL00");	
						array[perAltroChannelIndex++]  = resultSet.getInt("K3_CHANNEL00");	
						array[perAltroChannelIndex++]  = resultSet.getInt("L1_CHANNEL00");	
						array[perAltroChannelIndex++]  = resultSet.getInt("L2_CHANNEL00");	
						array[perAltroChannelIndex++]  = resultSet.getInt("L3_CHANNEL00");	
						array[perAltroChannelIndex++]  = resultSet.getInt("VFPED_CHANNEL00");
						
						System.out.println("\nK1= "+ array[0] + "\nK2= " + array[1] +"\nK3= " + array[2] + "\nL1= " + array[3] + "\nL2= " + array[4] + "\nL3=" +
								" " + array[5] + "\nVFPED= " + array[6]);
					}
				}
				catch (Exception e)
				{
					error = e.toString();
					System.out.println(this.toString() +": getAltroConfiguration(): " + error);
					closeConnection();
				}
				
				/* close the connection */
				closeConnection();
			} /* closing Channel FOR loop */
		} /* closing Altro FOR loop */
	}/* method end */
	
	private void connectToMySQL()
	{
		try
		{
			if ((connection == null) || (connection.isClosed() == true))
			{
				Class.forName("com.mysql.jdbc.Driver").newInstance();		//Open connection to database
				connection = DriverManager.getConnection(url,mySQLUser, mySQLPass);
			}
		}
			catch (Exception e)
			{
				error = e.toString();
				System.out.println(error);
			}
	}
	
	private void closeConnection()
	{
		try
		{
			connection.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	private void channelNumString(int counter)
	{
		if ((counter >= 0) && (counter <= 9))
		{
			for (int index=0; index < GlobalVariables.MaxPerAltroChannelRegisters; index++)
			{
				channelNum[index]= "0" + String.valueOf(counter);
			}
		}
		else
		{
			for (int index=0; index < GlobalVariables.MaxPerAltroChannelRegisters; index++)
			{
				channelNum[index]= String.valueOf(counter);
			}
		}
	}
}
