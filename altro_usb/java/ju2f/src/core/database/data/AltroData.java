package core.database.data;

import core.global.GlobalVariables;

public class AltroData {

	
	public int[] globalRegisters = new  int[GlobalVariables.MaxPerAltroGlobalRegisters];
	
	public ChannelData[] channels = new ChannelData[GlobalVariables.MaxPerAltroChannels];
	
	public AltroData()
	{	}
	
	public ChannelData[] getChannels()
	{
		return channels;
	}
	
	public void setChannels(ChannelData[] array)
	{
		channels = array;
	}
	
	public int[] getGlobalRegisters()
	{
		return globalRegisters;
	}
	
	public void setGlobalRegisters(int[] array)
	{
		globalRegisters = array;
	}
}
