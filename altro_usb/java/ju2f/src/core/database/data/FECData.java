package core.database.data;

import core.global.GlobalVariables;

public class FECData {

	public AltroData[] altro;
	
	public FECData()
	{
		altro = new AltroData[GlobalVariables.MaxPerFecAltros];
	}
	
	public AltroData getAltro(int altroNum)
	{
		return altro[altroNum];
	}
	
	public void setAltro(int altroNum, AltroData tempAltro)
	{
		altro[altroNum] = tempAltro;
	}
}

