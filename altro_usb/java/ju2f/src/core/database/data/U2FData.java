package core.database.data;

import core.global.GlobalVariables;

public class U2FData {

	public FECData[] fec; 
	
	public U2FData()
	{
		fec = new FECData[GlobalVariables.MaxPerU2Ffecs];
	}
	
	public FECData getFec(int fecNum)
	{
		return fec[fecNum];
	}
	
	public void setFec(int fecNum, FECData tempFec)
	{
		fec[fecNum] = tempFec;
	}
}
