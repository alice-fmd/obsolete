package core.jni.fec;

import core.global.GlobalVariables;
import javax.swing.JOptionPane;

public class AltroReadWrite 
{

	public AltroReadWrite() {}

	private native int writeAltroReg(int fecAddr, int chanAddr, int regAdd, int data, int loop);
	private native int multiReadAltroReg(int fecAddr, int chanAddr, int regAdd, int[] data, int loop);
	private native int initialiseFECs(int fecAddress, int[] statusArray);
	
	/*J in the end only represent that this method would be called by the java objects*/
	public void readAltroGlobalReg()
	{
		try
		{
			GlobalVariables.RegDataArray = new int[GlobalVariables.RegLoopCount];
			System.out.println("\nJava: About to Call native readAltroGlobalReg ");
			int ret= multiReadAltroReg(GlobalVariables.FecAddr,GlobalVariables.ChannelAddr, GlobalVariables.RegAddr,GlobalVariables.RegDataArray,GlobalVariables.RegLoopCount);
			System.out.println("\nJava: Call finished for native readAltroGlobalReg with ret: " + ret);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public void readAltroReg()
	{
		try
		{
			/* Initalise the array based on the loop count */
			GlobalVariables.RegDataArray = new int[GlobalVariables.RegLoopCount];
			System.out.println("\nJava: About to call native readAltroRegMultiple");
			int ret= multiReadAltroReg(GlobalVariables.FecAddr,GlobalVariables.ChannelAddr, GlobalVariables.RegAddr,GlobalVariables.RegDataArray,GlobalVariables.RegLoopCount);
			System.out.println("\nJava: Call finished for native readAltroRegMultiple with ret:" + ret);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public void writeAltroReg()
	{
			try
			{
				System.out.println("\nJava: About to call native readAltroRegMultiple");
				
				/* Call the native function */
				int ret= writeAltroReg(GlobalVariables.FecAddr,GlobalVariables.ChannelAddr, GlobalVariables.RegAddr,GlobalVariables.RegData,GlobalVariables.RegLoopCount);
				
				if (ret !=0 )
				{
					JOptionPane.showMessageDialog(null,"An error has occured while writting to the register,\nPlease try again!","Error !",JOptionPane.ERROR_MESSAGE);
				}
				
				System.out.println("\nJava: Call finished for native readAltroRegMultiple with ret:" + ret);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
	}
	
	public void initialiseFEC()
	{
		/* Format the Fec address, before its sent to U2F */
		formatFecAddress();
		
		/* Initialised the requested FECs by calling the native function */
		initialiseFECs(GlobalVariables.FecAddress,GlobalVariables.FEC_STATUS);
	}
	
	/* When ever FEC has to be initialised, this function must be called */
	private void formatFecAddress()
	{
		for (int index=0; index < GlobalVariables.MaxPerU2Ffecs; index++)
		{		
			if (GlobalVariables.FEC_STATUS[index] == 1)
			{
				/* This shift the 1 into left by Index value, thus allowing multiple FEC
				 * to be switched on through a single integer value */
				GlobalVariables.FecAddress |= (1 << index);
			}
		}
	}
	
	public String getInputValue()
	{	
		String inputValue = "";
		
		inputValue = JOptionPane.showInputDialog("Please input a value to be written!");
		
		/* Makesure that a value is entered */
		while(inputValue.length() == 0) 
		{
			inputValue = JOptionPane.showInputDialog("Please input a value to be written!");
		}
		
		return inputValue;
	}
	
	static
	{
		System.loadLibrary("jniu2f");
	}
}
