package core.jni.fec;

import core.global.GlobalVariables;

public class BCReadWrite {
	
	private static boolean isExecutable=true;
	
	private native int readBCRegister(int fecAddr,int regAdd, int[] data);
	private native int readAllBCRegister(int fecAddr,int[] regAdd, int[] data, int loop);
	private native int writeBCRegister(int fecAddr,int regAdd, int data);
	
	public synchronized void read_BC_Reg()
	{
		int ret;
		
		if(isExecutable == false) {  // not the readFromClient's thread turn

	         // thread that called this method must wait 
	         try {
	            wait();  
	         }

	         // process Interrupted exception while thread waiting
	         catch ( InterruptedException exception ) {
	            exception.printStackTrace();
	         }
	      }
		else
		{
			isExecutable = false;
			
			GlobalVariables.RegDataArray = new int[GlobalVariables.RegLoopCount];
			ret = readBCRegister(GlobalVariables.FecAddr, GlobalVariables.RegAddr, GlobalVariables.RegDataArray);
		
			//preventing read_BC_Reg thread to run twice
			isExecutable = true;

			// tell a waiting thread to become ready
			notify();
		}
	}
	
	public synchronized void read_All_BC_Reg()
	{
		int ret;
		
		if(isExecutable == false) {  // not the readFromClient's thread turn

	         // thread that called this method must wait 
	         try {
	            wait();  
	         }

	         // process Interrupted exception while thread waiting
	         catch ( InterruptedException exception ) {
	            exception.printStackTrace();
	         }
	      }
		else
		{
			isExecutable = false;
			
			GlobalVariables.RegDataArray = new int[GlobalVariables.RegLoopCount];
			ret = readAllBCRegister(GlobalVariables.FecAddr, GlobalVariables.RegAddrArray, GlobalVariables.RegDataArray,GlobalVariables.RegLoopCount );
		
			//preventing read_BC_Reg thread to run twice
			isExecutable = true;

			// tell a waiting thread to become ready
			notify();
		}
	}
	
	public synchronized void write_BC_Reg()
	{
		if(isExecutable == false) {  // not the readFromClient's thread turn

	         // thread that called this method must wait 
	         try {
	            wait();  
	         }

	         // process Interrupted exception while thread waiting
	         catch ( InterruptedException exception ) {
	            exception.printStackTrace();
	         }
	      }
		else
		{
			isExecutable = false;
		
			writeBCRegister(GlobalVariables.FecAddr, GlobalVariables.RegAddr, GlobalVariables.RegData);
		
			//preventing read_BC_Reg thread to run twice
			isExecutable = true;

			// tell a waiting thread to become ready
			notify();
		}
	}
	
	static
	{
		System.loadLibrary("jniu2f");
	}
}
