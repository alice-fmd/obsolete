/**
 * 
 */
package core.modules.fec;

import core.global.GlobalVariables;
import core.jni.fec.AltroReadWrite;
import core.modules.fec.tools.FecToolBar;

/**
 * @author okhalid
 *
 */
public class FecManager implements Runnable
{
	private FecToolBar fecTools;
	
	public FecManager(){}
	public void run()
	{
		initComponents();
	}
	
	public void initComponents()
	{
		/*create an instance of AltroReadWrite Object, which would then be used by the FecToolBar and AltroGui to
		 * execute C code */
		//jniAltroCom = new AltroReadWrite();
		
		/* For the moment, this is only initialise toolbar */
		initGuiComponents();
	}
	
	public void initGuiComponents()
	{
		fecTools = new FecToolBar();
		//fecTools = new FecToolBar(getAltroReadWrite());
	}
	
	public FecToolBar getFecToolBar()
	{
		return fecTools;
	}
	/*
	private AltroReadWrite getAltroReadWrite()
	{
		//return jniAltroCom;
	}*/
}

