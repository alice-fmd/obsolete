/**
 * 
 */
package core.modules.fec.altro;

/**
 * @author okhalid
 * 
 */
public class Altro 
{
	/* Following are the list of Global Altro Registers, they must be decoded into integers before passing on the hardware */
	public final static String ZSTHR ="0x08";
	public final static String BCTHR ="0x09";
	public final static String TRCFG ="0x0A";
	public final static String DPCFG ="0x0B";
	public final static String BFNPT ="0x0C";
	public final static String ERSTR ="0x10";
	public final static String TRCNT ="0x12";
	public final static String PMADD ="0x0D";
	public final static String WPINC ="0x18";
	public final static String RPINC ="0x19";
	public final static String CHRDO ="0x1A";
	public final static String SWTRG ="0x1B";
	public final static String TRCLR ="0x1C";
	public final static String ERCLR ="0x1D";
        
        /* Following are the list of Channel specific Altro Registers, they must be decoded into integers before passing on the hardware */
	public final static String K1 ="0x00";
	public final static String K2 ="0x01";
	public final static String K3 ="0x02";
	public final static String L1 ="0x03";
	public final static String L2 ="0x04";
	public final static String L3 ="0x05";
	public final static String VFPED ="0x06";
	public final static String PMDTA ="0x07";
	public final static String ADEVL ="0x11";
	
	/* Following are the access rights for the registers */
	/*  * READ_ONLY variable is used to set read only access level of the register. */
	public static final int READ_ONLY  = 0;
	/* * READ_WRITE varaible is used to set read/write access level of the register. */
	public static final int READ_WRITE = 1;
	/* * WRITE_ONLY variable is used only to write to a register */
	public static final int WRITE_ONLY = 2;
	
	/* Following is the list of access rights for the global registers */
	
	public final static int ZSTHR_Access = READ_WRITE;
	public final static int BCTHR_Access = READ_WRITE;
	public final static int TRCFG_Access = READ_WRITE;
	public final static int DPCFG_Access = READ_WRITE;
	public final static int BFNPT_Access = READ_WRITE;
	public final static int WPINC_Access = WRITE_ONLY;
	public final static int RPINC_Access = WRITE_ONLY;
	public final static int CHRDO_Access = WRITE_ONLY;
	public final static int SWTRG_Access = WRITE_ONLY;
	public final static int TRCLR_Access = WRITE_ONLY;
	public final static int ERCLR_Access = WRITE_ONLY;

}
