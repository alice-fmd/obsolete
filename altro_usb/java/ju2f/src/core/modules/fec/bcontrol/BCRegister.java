/**
 * 
 */
package core.modules.fec.bcontrol;

import javax.swing.JButton;
import javax.swing.JTextField;
import core.jni.fec.AltroReadWrite;

/**
 * @author okhalid
 *
 */
public class BCRegister 
{
	private String address;
	private int bitLenght;
	private int accessType;
	private String value;
	private JTextField textFieldComponent;
	private JButton readButtonComponent;
	private JButton writeButtonComponent;
	
	public BCRegister()
	{}
	
	public BCRegister(String Address, int BitLength, int AccessType, JTextField jText, JButton jReadButton, JButton jWriteButton)
	{
		setAddress(Address);
		setBitLenght(BitLength);
		setAccessType(AccessType);
		setTextFieldComponent(jText);
		setButtons(jReadButton, jWriteButton);
	}
        
    public BCRegister(String Address, int BitLength, int AccessType, JTextField jText)
	{
		setAddress(Address);
		setBitLenght(BitLength);
		setAccessType(AccessType);
		setTextFieldComponent(jText);
	}
    
    public BCRegister(String Address, int BitLength, int AccessType)
	{
		setAddress(Address);
		setBitLenght(BitLength);
		setAccessType(AccessType);
	}
	
	private void setAddress(String Address)
	{
		address = Address;
	}
	
	public int getAddress()
	{
		return Integer.decode(address).intValue();
	}
	
	private void setBitLenght(int BitLenght)
	{
		bitLenght = BitLenght;
	}
	
	private void setAccessType(int AccessType)
	{
		accessType = AccessType;
	}
	
	private int getAccessType()
	{
		return accessType;
	}
	
	public void setValue(String Value)
	{
		value = "0x" + Value;
		setValueInTextField();
	}
	
	public void setValue(int Value)
	{
		value = "0x" + Integer.toHexString(Value);
		setValueInTextField();
	}
	
	private String getValue()
	{
		value = textFieldComponent.getText();
		return value;
	}
	
	/* The reason why this function returns a int value rather than a String is because this value is only used to write
	 * to BC Register and it have to be integer. If not, then the translation from Hex String to Integer would have to take 
	 * place before a request is made to Jni function. To simplify, this is implemented at this level. */
	public int getValueToWriteToHardware()
	{
		value = textFieldComponent.getText();
		
		/* Verify if the user has entered a hex string or an ASCII, if ASCII then convert it to Hex */
		if(!(value.startsWith("0x")))
		{
			value = "0x" + value;
		}
		
		return Integer.decode(value).intValue();
	}
	
	private void setTextFieldComponent(JTextField jText )
	{
		textFieldComponent = jText;
		
		textFieldComponent.addMouseListener(new java.awt.event.MouseAdapter() {
	        public void mouseEntered(java.awt.event.MouseEvent evt) {
	            MouseEnteredListener(evt);
	        }
	        public void mouseExited(java.awt.event.MouseEvent evt) {
	            MouseExitedListener(evt);
	        }
	    });
	}
	
	private void setValueInTextField()
	{
		textFieldComponent.setText(value);
	}
	
	private void setButtons( JButton jReadButton, JButton jWriteButton)
	{
		readButtonComponent = jReadButton;
		writeButtonComponent= jWriteButton;
	}
	
    private void MouseExitedListener(java.awt.event.MouseEvent evt) 
    {
    	//readButtonComponent.setEnabled(true);
    	//writeButtonComponent.setEnabled(true);
    }

    private void MouseEnteredListener(java.awt.event.MouseEvent evt) 
    {
    	if(getAccessType() == BoardControl.READ_ONLY)
    	{
    		//writeButtonComponent.setEnabled(false);
    	}
    	
    	if(getAccessType() == BoardControl.WRITE_ONLY)
    	{
    		//readButtonComponent.setEnabled(false);
    	}
    	
    	if(getAccessType() == BoardControl.READ_WRITE)
    	{
    		//readButtonComponent.setEnabled(true);
        	//writeButtonComponent.setEnabled(true);
    	}
    }
    
    
}
