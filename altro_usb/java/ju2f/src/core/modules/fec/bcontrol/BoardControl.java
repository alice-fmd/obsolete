package core.modules.fec.bcontrol;

public class BoardControl 
{
	
	public final static int MaxBCRegisters = 20;
	public final static int MaxMiscBCRegisters = 6;
	
	/* Following are the list of Global Altro Registers, they must be decoded into integers before passing on the hardware */
	public final static String T_TH 		="0x01";
	public final static String AV_TH 		="0x02";
	public final static String AC_TH 		="0x03";
	public final static String DV_TH 		="0x04";
	public final static String DC_TH 		="0x05";
	public final static String TEMP 		="0x06";
	public final static String AV 			="0x07";
	public final static String AC 			="0x08";
	public final static String DV 			="0x09";
	public final static String DC 			="0x0A";
	public final static String L1CNT 		="0x0B";
	public final static String L2CNT 		="0x0C";
	public final static String SCLKCNT 		="0x0D";
	public final static String DSTBCNT 		="0x0E";
	public final static String TSMWORD 		="0x0F";
	public final static String USRATIO 		="0x10";
	public final static String CSR0 		="0x11";
	public final static String CSR1 		="0x12";
	public final static String CSR2 		="0x13";
	public final static String CSR3 		="0x14";
	
	/* Rest of the Registers are not used yet */
	public final static String FREE 		="0x15";
	public final static String CNTLAT 		="0x16";
	public final static String CNTCLR 		="0x17";
	public final static String CSR1CLR 		="0x18";
	public final static String ALRST 		="0x19";
	public final static String BCRST 		="0x1A";
	public final static String STCNV 		="0x1B";
	
	public final static String SCEVL 		="0x1C";
	public final static String EVLRDO 		="0x1D";
	public final static String STTSM 		="0x1E";
	public final static String ACQRDO 		="0x1F";
	
	/* Following is the bit length of the registers */
	public final static int T_TH_Length		=10;
	public final static int AV_TH_Length 	=10;
	public final static int AC_TH_Length 	=10;
	public final static int DV_TH_Length 	=10;
	public final static int DC_TH_Length 	=10;
	public final static int TEMP_Length 	=10;
	public final static int AV_Length 		=10;
	public final static int AC_Length 		=10;
	public final static int DV_Length 		=10;
	public final static int DC_Length 		=10;
	
	public final static int L1CNT_Length 	=16;
	public final static int L2CNT_Length 	=16;
	public final static int SCLKCNT_Length 	=16;
	public final static int DSTBCNT_Length 	=8;
	public final static int TSMWORD_Length 	=9;
	public final static int USRATIO_Length 	=16;
	public final static int CSR0_Length 	=16;
	public final static int CSR1_Length 	=16;
	public final static int CSR2_Length 	=16;
	public final static int CSR3_Length 	=16;
	
	public final static int FREE_Length 	=0;
	
	public final static int CNTLAT_Length 	=0;
	public final static int CNTCLR_Length 	=0;
	public final static int CSR1CLR_Length 	=0;
	public final static int ALRST_Length 	=0;
	public final static int BCRST_Length 	=0;
	public final static int STCNV_Length 	=0;
	
	public final static int SCEVL_Length 	=0;
	public final static int EVLRDO_Length 	=0;
	public final static int STTSM_Length 	=0;
	public final static int ACQRDO_Length 	=0;
	
	/* Following are the access rights for the registers */
	/*  * READ_ONLY variable is used to set read only access level of the register. */
	public static final int READ_ONLY  = 0;
	/* * READ_WRITE varaible is used to set read/write access level of the register. */
	public static final int READ_WRITE = 1;
	/* * WRITE_ONLY variable is used only to write to a register */
	public static final int WRITE_ONLY = 2;
	
	/* Following is the list of access rights for the global registers */
	
	public final static int T_TH_Access 	= READ_WRITE;
	public final static int AV_TH_Access 	= READ_WRITE;
	public final static int AC_TH_Access 	= READ_WRITE;
	public final static int DV_TH_Access 	= READ_WRITE;
	public final static int DC_TH_Access 	= READ_WRITE;
	
	public final static int TEMP_Access 	= READ_ONLY;
	public final static int AV_Access 		= READ_ONLY;
	public final static int AC_Access 		= READ_ONLY;
	public final static int DV_Access 		= READ_ONLY;
	public final static int DC_Access 		= READ_ONLY;
	
	public final static int L1CNT_Access 	= READ_ONLY;
	public final static int L2CNT_Access 	= READ_ONLY;
	public final static int SCLKCNT_Access 	= READ_ONLY;
	public final static int DSTBCNT_Access 	= READ_ONLY;
	
	public final static int TSMWORD_Access 	= READ_WRITE;
	public final static int USRATIO_Access 	= READ_WRITE;
	public final static int CSR0_Access 	= READ_WRITE;
	public final static int CSR1_Access 	= READ_WRITE;
	public final static int CSR2_Access 	= READ_WRITE;
	public final static int CSR3_Access 	= READ_WRITE;
	
	public final static int FREE_Access 	= WRITE_ONLY; /* This register doesnot have any name or dscription yet */
	
	/* These registers are used globally to reset certain features of the BoardControl */
	public final static int CNTLAT_Access 	= WRITE_ONLY;
	public final static int CNTCLR_Access 	= WRITE_ONLY;
	public final static int CSR1CLR_Access 	= WRITE_ONLY;
	public final static int ALRST_Access 	= WRITE_ONLY;
	public final static int BCRST_Access 	= WRITE_ONLY;
	public final static int STCNV_Access 	= WRITE_ONLY;
	
	/* These registers are not documented yet based on Reg_Table_v2.3.pdf */
	public final static int SCEVL_Access 	= WRITE_ONLY;
	public final static int EVLRDO_Access 	= WRITE_ONLY;
	public final static int STTSM_Access 	= WRITE_ONLY;
	public final static int ACQRDO_Access 	= WRITE_ONLY;
}
