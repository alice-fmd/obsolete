package core.modules.fec.tools;

import java.awt.Toolkit;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JToolBar;
import core.global.GlobalVariables;
import core.jni.fec.AltroReadWrite;

public class FecToolBar extends JToolBar {

	public FecToolBar() 
	{
		initComponents();
	}
	
	public FecToolBar(AltroReadWrite altroJniObj) 
	{
		initComponents();
	}
	
	public void initComponents()
	{
		initPowerButton();
	}
	
	private void initPowerButton()
	{
		fecPowerOn = new JButton();
		/*Add the button to tool bar */
        String path = GlobalVariables.imagePath + "Stop24.gif"; 
        // String path = "/home/rcu/java/ju2f/Stop24.gif";
        ImageIcon icon = new ImageIcon(Toolkit.getDefaultToolkit().getImage(path));
        //try
        //{
        	fecPowerOn.setIcon(icon);
       /* }
        catch (Exception e)
        {
        	e.printStackTrace();
        }
        */
        fecPowerOn.setToolTipText("Power Reset FEC's");
        fecPowerOn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                powerHandler(evt);
            }
        });
        add(fecPowerOn);
	}
	
	 private void powerHandler(java.awt.event.ActionEvent evt) 
	 {
		 /* Link the AltroReadWrite Object */
		fecAltroObject = new AltroReadWrite();
			
	    /* Power On the Fec's */
		fecAltroObject.initialiseFEC();
		
		/* Display the status of current FEC's*/
		for (int index=0; index < GlobalVariables.MaxPerU2Ffecs; index++)
		{
			System.out.println("\nFEC Nr. "+ (index +1) + " Status =" + GlobalVariables.FEC_STATUS[index]);
		}
	  }
	
	private JButton fecPowerOn;
	private AltroReadWrite fecAltroObject;
}


 /*
public class MetalEdit extends javax.swing.JFrame {

    /** 
     * The resource bundle for the default locale 
     *
    private ResourceBundle resources = null;
  
    /** 
     * Path to the image resources in the resource bundle (in this 
     * case, .gif and .jpg files)
     *
    private String imagePath = null;
    

    /**
     * Loads locale-specific resources: strings, images, et cetera
     *
    private void initResources() {
        Locale locale = Locale.getDefault();
        resources = ResourceBundle.getBundle(
            "samples.resources.bundles.MetalEditResources", locale);
        imagePath = resources.getString("images.path");
    }


    
    
   
	 

    toolBar.setName (resources.getString("toolbarName"));

      buttonNew.setIcon(new ImageIcon(getClass().getResource(imagePath+resources.getString("imageNew"))));
      buttonNew.setToolTipText(resources.getString("toolTipImageNew"));
      buttonNew.getAccessibleContext().setAccessibleName(resources.getString("accessibleNameImageNew"));
  
      toolBar.add(buttonNew);
      */
  