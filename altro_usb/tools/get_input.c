/********************************************************/
/*  file : get_input.c					*/
/*							*/
/*  get_input functions					*/
/*							*/
/*  970122 J.O.Petersen + M.Joos 			*/
/********************************************************/

#include <stdio.h> 
#include <string.h>


/* get ONE decimal integer */
/**************/
int getdec(void)  
/**************/
{
  char sbuf[20];   /* max 19 chars */
  int nfield, nint;

  do 
  {
    fgets(sbuf, 20, stdin);
    nfield = sscanf(sbuf, "%d", &nint);
    if (nfield < 1) 
      printf(" ??? : ");
  } 
  while (nfield < 1);

  return(nint);
}


/* get ONE hex integer */
/***********************/
unsigned int gethex(void)  
/***********************/
{
char sbuf[20];   /* max 19 chars */
int nfield;
unsigned int nint;

  do 
  {
    fgets(sbuf, 20, stdin);
    nfield = sscanf(sbuf, "%x", &nint);
    if (nfield < 1) 
      printf(" ??? : ");
  } 
  while (nfield < 1);

  return(nint);
}


/* get first character */
/*******************/
char getfstchar(void)  
/*******************/
{
  char sbuf[20];   /* max 19 chars */

  fgets(sbuf, 20, stdin);

  return(sbuf[0]);
}


/* get ONE decimal integer */
/*******************/ 
int getdecd(int defa)  
/*******************/ 
{
  char sbuf[20];   /* max 19 chars */
  int nfield, nint;

  printf("[%d] :", defa); 
  do
  {
    fgets(sbuf, 20, stdin);
    if (strlen(sbuf) == 1) 
      return(defa);
    nfield = sscanf(sbuf, "%d", &nint);
    if (nfield <1 ) 
      printf(" ??? : ");
  } 
  while (nfield < 1);
  return(nint);
}


/* get ONE hex integer */
/*************************************/ 
unsigned int gethexd(unsigned int defa)  
/*************************************/ 
{
  char sbuf[20];   /* max 19 chars */
  int nfield;
  unsigned int nint;
 
  printf("[0x%08x] :", defa); 
  do
  {
    fgets(sbuf, 20, stdin);
    if (strlen(sbuf) == 1) 
      return(defa);
    nfield = sscanf(sbuf, "%x", &nint);
    if (nfield < 1) 
      printf(" ??? : ");
  } 
  while (nfield < 1);
  return(nint);
}


/* get ONE hex float */
/*************************/
float getfloatd(float defa)  
/*************************/
{
  char sbuf[20];   /* max 19 chars */
  int nfield;
  float nfloat;

  printf("[%f] :", defa);
  do
  {
    fgets(sbuf, 20, stdin);
    if (strlen(sbuf) == 1) 
      return(defa);
    nfield = sscanf(sbuf, "%f", &nfloat);
    if (nfield < 1) 
      printf(" ??? : ");
  }
  while (nfield < 1);
  return(nfloat);
}


/* get a string */
/*********************************/ 
void getstrd(char* str, char* defa)  
/*********************************/ 
{
 char sbuf[256];   /* max 255 chars, HP Beck 16-02-99 */
 
  printf("[%s] :", defa); 
  fgets(sbuf, 256, stdin);

  /* chop the NewLine character, HP Beck 16-02-1999 */
  if (sbuf[strlen(sbuf) - 1] == '\n') 
    sbuf[strlen(sbuf)-1] = '\0';

  if (strlen(sbuf) == 0) 
    strcpy(str,defa);
  else
    strcpy(str,sbuf);
  return;
}
