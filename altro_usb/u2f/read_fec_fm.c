/****************************************************************/
/*                                                              */
/*  file: read_fec_fm.c                                         */
/*                                                              */
/* This program allows to write a data block into the           */
/* FEC Flash Memory						*/
/*                                                              */
/*  Author: Luciano Musa, CERN-PH                               */
/*                                                              */
/*  13. Feb. 05  LM  created                                    */
/*                                                              */
/****************************************************************/

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <signal.h>
#include <tools/get_input.h>
#include <altro/altro.h>
#include <tools/rcc_time_stamp.h>
#include "u2f.h"


/******************************/
int main(int argc, char *argv[])
/******************************/
{  
  static u_int ret, rcode = 0, rdata = 0, ccode = 0, cdata = 0;
  static u_int Nword = 16;
  static int handle;
  static char fname[200] = "./INIT/fm_outdata";
  static u_int testmode = 0;
  u_int loop;
  FILE *file;

  for (loop = 1; loop < argc; loop++) 
  {
    if (argv[loop][0] == '-') 
    {
      switch (argv[loop][1]) 
      {
      case 'h': 
	printf("Usage: %s [OPTIONS]\n\n", argv[0]); 
	printf("Options:\n"); 
	printf("  -h\t\tHelp:\n"); 
	printf("  -d LEVEL\tSet the debug level to LEVEL [%d]\n", testmode); 
	printf("  -i FILE\tRead instructions from FILE [%s]\n", fname); 
	printf("  -n WORDS\tRead NWORDS from the FLASH MEMORY [%d]\n",Nword);
	return 0;
	break;
      case 'd': testmode = strtol(argv[loop+1], NULL, 0); loop++; break;
      case 'i': strcpy(fname, argv[loop+1]); loop++; break;
      case 'n': Nword    = strtol(argv[loop+1], NULL, 0); loop++; break;
        break;
      default:
	fprintf(stderr, "Unknown option %s, try %s -h", argv[loop], argv[0]);
	return 1;
      }
    }
  }
  
  /* Open U2F_LIB */
  ret = U2F_Open(DEFAULT_NODE, &handle, HW);
  if (ret)
    rcc_error_print(stdout, ret);

  /* Open the rcc_time_stamp library */
  ret = ts_open(1, TS_DUMMY);
  if (ret)
    rcc_error_print(stdout, ret);
    
  /* Switch on FEC with Address = 0  */
  rcode = O_ACTFEC;
  rdata = 0x1;
  ret = U2F_Reg_Write(handle, rcode, rdata);
  if (ret)
    rcc_error_print(stdout, ret);
  if(testmode)	
	printf("FEC ON\n");

  ts_delay(5);  /* delay 5 us */
  
  /* Reset FEC */
  ccode = C_FECRST;
  cdata = 0;
  ret = U2F_Exec_Command(handle, ccode, cdata);
  if (ret)
    rcc_error_print(stdout, ret);
  ret = U2F_Exec_Command(handle, ccode, cdata);
  if (ret)
    rcc_error_print(stdout, ret);
  if(testmode)
	printf("Reset FEC\n");

  /********************************************/
  /* Read N words from the FEC's Flash Memory */
  /********************************************/
    
  if(testmode)
  	printf("Reading from the FEC's Flash Memory\n");

  file = fopen(fname,"w");	
	
  for(loop=0; loop<Nword; loop++)
  { 
    /* Write FM Address in FMIREG */    
    rcode = O_FMIREG;
    rdata = loop << 8;
    ret = U2F_Reg_Write(handle, rcode, rdata &0xffff);
    if (ret)
      rcc_error_print(stdout, ret);

    /* Execute the RDFM command */
    ccode = C_RDFM;
    cdata = 0;
    ret = U2F_Exec_Command(handle, ccode, cdata);
    if (ret)
      rcc_error_print(stdout, ret);

    ts_delay(5000);  /* delay 5ms */

    /* Execute the RDFM command */
    ccode = C_RDFM;
    cdata = 0;
    ret = U2F_Exec_Command(handle, ccode, cdata);
    if (ret)
      rcc_error_print(stdout, ret);

    ts_delay(5000);  /* delay 5mfs */

    /* Read FMOREG */    
    rcode = O_FMOREG;
    ret = U2F_Reg_Read(handle, rcode, &rdata);
    if (ret)
      rcc_error_print(stdout, ret);

    /* Writing FMOREF into fm_outdata file */
    if(testmode == 2)
      printf("Address %d\t Data %x\n", loop, rdata & 0xff);
  }
	
  ret = U2F_Close(handle);
  ret = ts_close(TS_DUMMY);
  exit(0);
}

