/****************************************************************/
/*                                                              */
/*  file: u2f_acq.c                                             */
/*                                                              */
/* This program allows to issue a software or hardware trigger  */
/* and, subsequently, to perform the data readout               */
/*                                                              */
/*  Author: Luciano Musa, CERN-PH                               */
/*                                                              */
/*  18. Nov. 04  LM  created                                    */
/*                                                              */
/****************************************************************/

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <signal.h>
#include <tools/get_input.h>
#include <altro/altro.h>
#include <tools/rcc_time_stamp.h>
#include "u2f.h"


/******************************/
int main(int argc, char *argv[])
/******************************/
{  
  static u_int ret, rcode = 0, rdata = 0, fdata[20048] = {0}, fsize = 3, ccode = 0, cdata = 0;
  static u_int offset = 0, rsize = 10, memdata[20048] = {0};
  static int handle;
  static char fname[200] = "/home/rcu/NEW_U2F/altro_usb/init_files/ginit2";
  static char fname2[200]= "/home/rcu/NEW_U2F/altro_usb/data_files/test_data";
  static u_int testmode = 1, readout=1, soft_trigger =1, ext_trigger=0;
  static u_int chnum=0, point=0, tick=0, initonly=0;
  u_int loop, osize, emode = HW;
  FILE *file;

  for (loop = 1; loop < argc; loop++) 
  {
    if (argv[loop][0] == '-') 
    {
      switch (argv[loop][1]) 
      {
        case 'h': 
	  printf("Usage: %s [OPTIONS]\n\n", argv[0]); 
	  printf("Options:\n"); 
	  printf("  -h\t\tThis help:\n"); 
	  printf("  -d LEVEL\tSet the debug level to LEVEL [%d]\n", testmode); 
	  printf("  -i FILE\tRead instructions from FILE [%s]\n", fname); 
	  printf("  -n LINES\tRead LINES of instructions from input file [%d]\n", fsize);
	  printf("  -o FILE\tWrite output to FILE [%s]\n", fname2); 
	  printf("  -r\t\tEnable readout %s\n", (readout ? "(default)" : "")); 
	  printf("  -R\t\tDisable readout %s\n", (!readout ? "(default)" : "")); 
	  printf("  -t MODE\tSet trigger to use [%s]\n\n",(soft_trigger ? "software" : "external"));
	  printf("  -x Just initialize the H/W and exit\n");
	  printf("  -e Use emulation mode\n");
	  return 0;
	  break;
	case 'd': testmode = strtol(argv[loop+1], NULL, 0); loop++; break;
	case 'i': strcpy(fname, argv[loop+1]); loop++; break;
	case 'n': fsize    = strtol(argv[loop+1], NULL, 0); loop++; break;
	case 'o': strcpy(fname2, argv[loop+1]); loop++; break;
	case 'r': readout = 1; break;
	case 'R': readout = 0; break;
	case 'x': initonly = 1; break;
        case 'e': emode    = SW; break;
	case 't': ret  = strtol(argv[loop+1], NULL, 0); loop++;
	switch (ret) 
	{
	  case 1: soft_trigger = 1; ext_trigger = 0; break;
	  case 2: soft_trigger = 0; ext_trigger = 1; break;
	  default: 
	    fprintf(stderr, "Invalid trigger mode: %d, should be 1 (software) or 2 (external)", ret);
	    return 1;
	}
	break;
      default:
	fprintf(stderr, "Unknown option %s, try %s -h", argv[loop], argv[0]);
	return 1;
      }
    }
  }
  
  /* Open U2F_LIB */
  ret = U2F_Open(DEFAULT_NODE, &handle, emode);
  if (ret)
    rcc_error_print(stdout, ret);

  /* Open the rcc_time_stamp library */
  ret = ts_open(1, TS_DUMMY);
  if (ret)
    rcc_error_print(stdout, ret);

  /* Switch on FEC with Address = 0  */
  rcode = O_ACTFEC;
  rdata = 0x1;
  ret = U2F_Reg_Write(handle, rcode, rdata);
  if (ret)
    rcc_error_print(stdout, ret);
    
  /* delay 1000000 us */
  ts_delay(1000000);

  /* Reset FEC */
  ccode = C_FECRST;
  cdata = 0;
  ret = U2F_Exec_Command(handle, ccode, cdata);
  ccode = C_FECRST;
  cdata = 0;
  ret = U2F_Exec_Command(handle, ccode, cdata);
  if (ret)
    rcc_error_print(stdout, ret);

    
  /* delay 100 us */
  ts_delay(100);


  /* Load the Instruction Memory data */
  printf("Reading instructions from file %s\n", fname);
  ret = U2F_File_Read(fname, fsize, fdata);
  if (ret)
    rcc_error_print(stdout, ret);
    
  /* write into the Instruction Memory */
  rsize = fsize;
  offset = 0;
  printf("Initializing instruction memory\n");
  for(loop=0; loop<fsize; loop++)
  {
    memdata[loop] = fdata[loop];
    printf("Data word %d = 0x%08x\n", loop, memdata[loop]);
  }
  ret = U2F_IMEM_Write(handle, rsize, offset, memdata);
  if (ret)
    rcc_error_print(stdout, ret);
    
  /* initialize the Channel Readout List  */
  rsize = 8;
  offset = 0;  
  
  memdata[0] = 0xffff;
  memdata[1] = 0xffff;
  memdata[2] = 0xffff;
  memdata[3] = 0xffff;
  memdata[4] = 0xffff;
  memdata[5] = 0xffff;
  memdata[6] = 0xffff;
  memdata[7] = 0xffff;
  
  ret = U2F_ACL_Write(handle, rsize, offset, memdata);
  if (ret)
    rcc_error_print(stdout, ret);
  
  /* Execute the Instruction Memory */
  ccode = C_EXEC;
  cdata = 0;
  ret = U2F_Exec_Command(handle, ccode, cdata);
  if (ret)
    rcc_error_print(stdout, ret);
    
  /* delay 1000 us */
  ts_delay(1000);
	
  /* Configure the Trigger Configuration Register */
  rcode = O_TRCFG1;
  rdata = 0x50800;
  ret = U2F_Reg_Write(handle, rcode, rdata);
  if (ret)
    rcc_error_print(stdout, ret);
    
  if (initonly)
  {
    printf("U2F H/W is now initialized\n");
    exit(0);
  }  
    
    
  /* Send a software trigger */
  if(soft_trigger)
  {
    ccode = C_SWTRG;
    cdata = 0;
    ret = U2F_Exec_Command(handle, ccode, cdata);
    if (ret)
      rcc_error_print(stdout, ret);
    if(testmode)
      printf("\n\nSOFTWARE TRIGGER ISSUED\n\n");
  }

  /*** Waiting for an external Trigger ***/
  /***  Read EVWORD Register  ***/
  if(ext_trigger)
  {
    rcode = O_EVWORD;
    ret = U2F_Reg_Read(handle, rcode, &rdata); 
    if (ret)
      rcc_error_print(stdout, ret);
    if(testmode)
      printf("EVWORD = %x\n", rdata);

    while(!(rdata & 0x00000010))
    {
      tick++;

      /***  Read EVWORD Register  ***/
      rcode = O_EVWORD;
      ret = U2F_Reg_Read(handle, rcode, &rdata); 
      if (ret)
        rcc_error_print(stdout, ret);

      if(testmode)
      {
	if(tick == 5000)
	{
	  printf("Waiting for external trigger\n");
	  point++;
	  tick = 0;
	} 
	if(point == 10)
	  return(0); 
      } 		
    }
  }

  if(readout)
  {
    /****************************************************************/
    /************************* PERFORM THE READOUT ******************/
    /****************************************************************/
    rsize = 66;
    offset = 0;
    rdata = 0;

    /***  Read EVWORD Register  ***/
    rcode = O_EVWORD;
    ret = U2F_Reg_Read(handle, rcode, &rdata); 
    if (ret)
      rcc_error_print(stdout, ret);
    if(testmode)
      printf("EVWORD = %x\n", rdata);

    if((rdata & 0x00000010) && (rdata & 0x00000008)) 
    {
      if(testmode)
      {
	printf("STARTING EVENT READOUT\n");  	
	if(rdata & 0x00000004)
  	  printf("EVENT READOUT WAS CONCLUDED WITHOUT DATA TRANSFER\n");
      }

      while(!(rdata & 0x00000004))
      {
	/*******************************/
	/***  Read EVWORD Register  ***/
	/******************************/
	rcode = O_EVWORD;
	ret = U2F_Reg_Read(handle, rcode, &rdata); 
        if (ret)
          rcc_error_print(stdout, ret);

	/*******************************/
	/***  Reading Data  	     ***/
	/*******************************/

	/*** Reading DMEM1  ***/

	if (rdata & 0x00000002)
	{
	  if(testmode)
	    printf("\n\nReading Channel #%d from DMEM1\n", chnum);

	  ret = U2F_DM1_Read(handle, rsize, &osize, offset, memdata);
	  if (ret)
	    rcc_error_print(stdout, ret);

	  file = fopen(fname2,"a");
	  for (loop = 0; loop < osize; loop++) 
	  {
	    fprintf(file, "%d\n", memdata[loop * 4]);
	    fprintf(file, "%d\n", memdata[loop * 4 +1]);
	    fprintf(file, "%d\n", memdata[loop * 4 +2]);
	    fprintf(file, "%d\n", memdata[loop * 4 +3]);	
	  }

	  /* Read EVWORD */ 
	  rcode = O_EVWORD;
	  ret = U2F_Reg_Read(handle, rcode, &rdata); 
	  if (ret)
	    rcc_error_print(stdout, ret);
	  if(testmode)
	    printf("EVWORD = %x\n", rdata);

	  /***  Reset DMEM1 flag    ***/
	  ccode = C_RS_DMEM1;
	  cdata = 0;
	  ret = U2F_Exec_Command(handle, ccode, cdata);
	  if (ret)
	    rcc_error_print(stdout, ret);

	  /* Read EVWORD */ 
	  rcode = O_EVWORD;
	  ret = U2F_Reg_Read(handle, rcode, &rdata); 
	  if (ret)
	    rcc_error_print(stdout, ret);
	  if(testmode)
	    printf("EVWORD = %x\n", rdata);

	}

	/***  Reading DMEM2  ***/
	if (rdata & 0x00000001)
	{
	  chnum++;
	  if(testmode)	
	    printf("\n\nReading Channel #%d from DMEM2\n", chnum);

	  ret = U2F_DM2_Read(handle, rsize, &osize, offset, memdata);
	  if (ret)
	    rcc_error_print(stdout, ret);

	  file = fopen(fname2,"a");
	  for (loop = 0; loop < osize; loop++) 
	  {
	    fprintf(file, "%d\n", memdata[loop * 4]);
	    fprintf(file, "%d\n", memdata[loop * 4 +1]);
	    fprintf(file, "%d\n", memdata[loop * 4 +2]);
	    fprintf(file, "%d\n", memdata[loop * 4 +3]);	
	  }

	  /* Read EVWORD */ 
	  rcode = O_EVWORD;
	  ret = U2F_Reg_Read(handle, rcode, &rdata); 
	  if (ret)
	    rcc_error_print(stdout, ret);
	  if(testmode)
	    printf("EVWORD = %x\n", rdata);

	  /***  Reset DMEM2 flag    ***/
	  ccode = C_RS_DMEM2;
	  cdata = 0;
	  ret = U2F_Exec_Command(handle, ccode, cdata);
	  if (ret)
	    rcc_error_print(stdout, ret);

	  /* Read EVWORD */ 
	  rcode = O_EVWORD;
	  ret = U2F_Reg_Read(handle, rcode, &rdata); 
	  if (ret)
	    rcc_error_print(stdout, ret);
	  if(testmode)
	    printf("EVWORD = %x\n", rdata);
	}
	chnum++;
      }

      /*** Reset EVWORD ***/
      ccode = C_TRG_CLR;
      cdata = 0;
      ret = U2F_Exec_Command(handle, ccode, cdata);
      if (ret)
	rcc_error_print(stdout, ret);

      /*** Read EVWORD ***/ 
      rcode = O_EVWORD;
      ret = U2F_Reg_Read(handle, rcode, &rdata); 
      if (ret)
	rcc_error_print(stdout, ret);
      if(testmode)
	printf("EVWORD = %x\n", rdata);
    }
  }
  ret = U2F_Close(handle);
  ret = ts_close(TS_DUMMY);
  exit(0);
}

